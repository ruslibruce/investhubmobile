package id.investhub;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import android.hardware.usb.UsbManager;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

public class UsbModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private boolean isUsbConnected = false;
    private BroadcastReceiver usbReceiver;

    public UsbModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        registerUsbReceiver();
    }

    @Override
    public String getName() {
        return "UsbModule";
    }

    private void registerUsbReceiver() {
        usbReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                    // USB device connected
                    isUsbConnected = true;
                } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    // USB device disconnected
                    isUsbConnected = false;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        reactContext.registerReceiver(usbReceiver, filter);
    }

    @ReactMethod
    public void isUsbDebuggingEnabled(Callback callback) {
        int adb = Settings.Global.getInt(reactContext.getContentResolver(), Settings.Global.ADB_ENABLED, 0);
        boolean isEnabled = adb == 1;
        callback.invoke(isEnabled);
    }

    @ReactMethod
    public void openDeveloperOptions() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        reactContext.startActivity(intent);
    }

    @ReactMethod
    public void isUsbDeviceConnected(Callback callback) {
        Intent intent = reactContext.registerReceiver(null, new IntentFilter("android.hardware.usb.action.USB_STATE"));
        boolean isConnected = intent != null && intent.getExtras().getBoolean("connected");
        callback.invoke(isConnected);
    }
}