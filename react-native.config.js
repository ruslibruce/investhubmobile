module.exports = {
  project: {
    ios: {},
    android: {},
  },
  assets: ['./src/assets/fonts', 'node_modules/@ant-design/icons-react-native/fonts'],
  dependencies: {
    'react-native-vector-icons': {
      platforms: {
        ios: null,
      },
    },
  },
};
