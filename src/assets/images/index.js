import iconGoogle from './Google_Logo.png';
import iconLoginIDX from './IDX_Logo.png';
import iconLoginIDXMobile from './IDX_Mobile_Logo.png';
import iconLoginRDIS from './RDIS_Logo.png';
import iconLoginTICMI from './TICMI_Logo.png';
import iconApple from './apple_logo_button.png';
import dummyCarousel from './dummyCarousel.webp';
import hero from './hero.webp';
import imageDummyCard from './imageDummy.webp';
import iconLogo from './logo_investhub.webp';
import imageProfile from './profileImage.webp';

export {
  dummyCarousel,
  hero,
  iconApple,
  iconGoogle,
  iconLoginIDX,
  iconLoginIDXMobile,
  iconLoginRDIS,
  iconLoginTICMI,
  iconLogo,
  imageDummyCard,
  imageProfile,
};
