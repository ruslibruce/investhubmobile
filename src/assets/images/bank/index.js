import imageBCA from './icon_BCA_Securitas.webp';
import imageBNI from './icon_BNI_Securitas.webp';
import imageBRI from './icon_BRI_Danareksa_Securitas.webp';
import imageCGSCIMB from './icon_CGSCIMB.webp';
import imageCiptadana from './icon_Ciptadana_Securitas_Asia.webp';
import imageIndoCapital from './icon_Indo_Capital_Securitas.webp';
import imageIndoPremier from './icon_Indopremier.webp';
import imageMaybank from './icon_Maybank_Securitas_Indonesia.webp';

export {
  imageBCA,
  imageBNI,
  imageBRI,
  imageCGSCIMB,
  imageCiptadana,
  imageIndoCapital,
  imageIndoPremier,
  imageMaybank,
};
