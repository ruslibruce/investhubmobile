import IconBGLiveChat from './bg_live_chat.svg';
import IconBrokerTab from './brokerTab.svg';
import IconBrokerTabActive from './brokerTabActive.svg';
import IconCourseTab from './courseTab.svg';
import IconCourseTabActive from './courseTabActive.svg';
import IconEventTab from './eventTab.svg';
import IconEventTabActive from './eventTabActive.svg';
import IconHomeTab from './homeTab.svg';
import IconHomeTabActive from './homeTabActive.svg';
import IconAbout from './icon_about_red.svg';
import IconAddress from './icon_address.svg';
import IconAvatar from './icon_avatar.svg';
import IconAvatarHome from './icon_avatar_home.svg';
import IconBack from './icon_back.svg';
import IconBookmark from './icon_bookmark.svg';
import IconBulbRed from './icon_bulb_red.svg';
import IconCalendar from './icon_calendar.svg';
import IconCalendarRed from './icon_calendar_red.svg';
import IconChangePasswordProfile from './icon_change_password_profile.svg';
import IconCollection from './icon_collection.svg';
import IconComment from './icon_comment.svg';
import IconCommentRed from './icon_comment_red.svg';
import IconCommunityRed from './icon_community_red.svg';
import IconCountry from './icon_country.svg';
import IconCup from './icon_cup.svg';
import IconDeleteAccount from './icon_delete_account.svg';
import IconDisclaimerRed from './icon_disclaimer_red.svg';
import IconDotHorizontal from './icon_dot_horizontal.svg';
import IconChevDown from './icon_down_chevron.svg';
import IconEbookRed from './icon_ebook_red.svg';
import IconEmail from './icon_email.svg';
import IconEmailConfirm from './icon_email_confirm.svg';
import IconEyeLogin from './icon_eye_login.svg';
import IconEyeRed from './icon_eye_red.svg';
import IconFacebook from './icon_facebook.svg';
import IconFAQ from './icon_faq_red.svg';
import IconFilter from './icon_filter.svg';
import IconFullName from './icon_full_name_login.svg';
import IconGlossariumRed from './icon_glossarium_red.svg';
import IconHelpProfile from './icon_help_profile.svg';
import IconInstagram from './icon_instagram.svg';
import IconInstitution from './icon_institution.svg';
import IconJourney100 from './icon_journey_100.svg';
import IconJourneyLock from './icon_journey_lock.svg';
import IconJourneyRun from './icon_journey_run.svg';
import IconLanguage from './icon_language.svg';
import IconChevLeft from './icon_left_chevron.svg';
import IconLevelUpRed from './icon_level_up_red.svg';
import IconLogout from './icon_logout.svg';
import IconLove from './icon_love.svg';
import IconMenuCourse from './icon_menu_course.svg';
import IconSertifikat from './icon_my_certificate.svg';
import IconMyCourse from './icon_my_course.svg';
import IconNote from './icon_note.svg';
import IconNotification from './icon_notification.svg';
import IconNotificationActive from './icon_notification_active.svg';
import IconNotificationProfile from './icon_notification_profile.svg';
import IconPasswordLogin from './icon_password_login.svg';
import IconPickFile from './icon_pick_file.svg';
import IconPreferenced from './icon_preferences_profile.svg';
import IconPrivacyRed from './icon_privacy_red.svg';
import IconReportRed from './icon_report_red.svg';
import IconRightCarousel from './icon_right_carousel.svg';
import IconChevRight from './icon_right_chevron.svg';
import IconRoundCheckGreen from './icon_round_check_circle.svg';
import IconSearch from './icon_search.svg';
import IconSearchHeader from './icon_search_header.svg';
import IconSeeAllRed from './icon_see_all_red.svg';
import IconShareBlack from './icon_share_black.svg';
import IconShareRed from './icon_share_red.svg';
import IconSpeakerRed from './icon_speaker_shout_red.svg';
import IconStockScreenRed from './icon_stock_screen_red.svg';
import IconStudentRed from './icon_student_red.svg';
import IconID from './icon_symbols_language_id.svg';
import IconEN from './icon_symbols_language_us.svg';
import IconTelephone from './icon_telephone.svg';
import IconThumbBlack from './icon_thumb_black.svg';
import IconThumbRed from './icon_thumb_red.svg';
import IconTimer from './icon_timer.svg';
import IconToken from './icon_token.svg';
import IconTwitter from './icon_twitter.svg';
import IconChevUp from './icon_up_chevron.svg';
import IconUpdateProfile from './icon_update_profile.svg';
import IconUserLogin from './icon_user_login.svg';
import IconLockLogin from './lock_24px.svg';
import IconProfileTab from './profileTab.svg';
import IconProfileTabActive from './profileTabActive.svg';

export {
  IconAbout,
  IconAddress,
  IconAvatar,
  IconAvatarHome,
  IconBGLiveChat,
  IconBack,
  IconBookmark,
  IconBrokerTab,
  IconBrokerTabActive,
  IconBulbRed,
  IconCalendar,
  IconCalendarRed,
  IconChangePasswordProfile,
  IconChevDown,
  IconChevLeft,
  IconChevRight,
  IconChevUp,
  IconCollection,
  IconComment,
  IconCommentRed,
  IconCommunityRed,
  IconCountry,
  IconCourseTab,
  IconCourseTabActive,
  IconCup,
  IconDeleteAccount,
  IconDisclaimerRed,
  IconDotHorizontal,
  IconEN,
  IconEbookRed,
  IconEmail,
  IconEmailConfirm,
  IconEventTab,
  IconEventTabActive,
  IconEyeLogin,
  IconEyeRed,
  IconFAQ,
  IconFacebook,
  IconFilter,
  IconFullName,
  IconGlossariumRed,
  IconHelpProfile,
  IconHomeTab,
  IconHomeTabActive,
  IconID,
  IconInstagram,
  IconInstitution,
  IconJourney100,
  IconJourneyLock,
  IconJourneyRun,
  IconLanguage,
  IconLevelUpRed,
  IconLockLogin,
  IconLogout,
  IconLove,
  IconMenuCourse,
  IconMyCourse,
  IconNote,
  IconNotification,
  IconNotificationActive,
  IconNotificationProfile,
  IconPasswordLogin,
  IconPickFile,
  IconPreferenced,
  IconPrivacyRed,
  IconProfileTab,
  IconProfileTabActive,
  IconReportRed,
  IconRightCarousel,
  IconRoundCheckGreen,
  IconSearch,
  IconSearchHeader,
  IconSeeAllRed,
  IconSertifikat,
  IconShareBlack,
  IconShareRed,
  IconSpeakerRed,
  IconStockScreenRed,
  IconStudentRed,
  IconTelephone,
  IconThumbBlack,
  IconThumbRed,
  IconTimer,
  IconToken,
  IconTwitter,
  IconUpdateProfile,
  IconUserLogin,
};
