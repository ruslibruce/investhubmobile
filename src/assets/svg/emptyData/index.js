import IconEmptyTable from './icon_empty_table.svg';
import IconEmptyTableWhite from './icon_empty_table_white.svg';

export {IconEmptyTable, IconEmptyTableWhite};
