import {Provider as ProviderAntd} from '@ant-design/react-native';
import * as eva from '@eva-design/eva';
import {NavigationContainer} from '@react-navigation/native';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import React from 'react';
import Config from 'react-native-config';
import 'react-native-gesture-handler';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {MagicModalPortal} from 'react-native-magic-modal';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Toast from 'react-native-toast-message';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {default as theme} from '../evaInvesthub-theme.json';
import {persistor, store} from './redux/store';
import Router from './router/Router';
import {navigationRef, toastConfig} from './utility';
import IconWhatsapp from './widgets/IconWhatsapp';
import 'react-native-get-random-values'
import { AxiosContextProvider } from './contexts';

const config = {
  screens: {
    Login: {
      path: 'api-iam/email/verify/:code',
    },
    BottomNav: {
      screens: {
        Home: 'home',
        Course: {
          screens: {
            CourseDetail: 'courses/detail/:id',
          },
        },
        Tools: {
          screens: {
            ToolsMainPage: 'tools/',
            UpdateProfile: 'updateProfile/',
          },
        },
      },
    },
    MyCertificate: {
      path: 'certificate',
    },
    MyLearningJourneyDetail: {
      path: 'detail-mylearning',
      parse: {
        id: id => `${id}`,
        name: name => `${name}`,
      },
    },
  },
};

const linking = {
  enabled: true,
  prefixes: [
    `${Config.SCHEME_CALLBACK}://`,
    `${Config.BASE_URL}/`,
    `http://investhub.code-dev.id`,
  ],
  config,
};

const App = () => {
  const [routeNameRef, setRouteNameRef] = React.useState(false);
  console.log('Cek BASE_URL ========', Config.BASE_URL , '========= BASE_URL')
  return (
    <SafeAreaProvider>
      <GestureHandlerRootView style={{flex: 1}}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <AxiosContextProvider>
              <ProviderAntd>
                <NavigationContainer
                  ref={navigationRef}
                  onStateChange={() => {
                    setRouteNameRef(true);
                  }}
                  linking={linking}>
                  <IconRegistry icons={EvaIconsPack} />
                  <ApplicationProvider {...eva} theme={{...eva.dark, ...theme}}>
                    <Router />
                    {routeNameRef && <IconWhatsapp bottom={70} />}
                    <MagicModalPortal />
                  </ApplicationProvider>
                </NavigationContainer>
              </ProviderAntd>
            </AxiosContextProvider>
          </PersistGate>
        </Provider>
      </GestureHandlerRootView>
      <Toast config={toastConfig} />
    </SafeAreaProvider>
  );
};

export default App;
