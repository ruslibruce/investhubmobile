import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import CourseDetail from '../pages/CourseDetail';
import CourseMainPage from '../pages/CourseMainPage';
import CourseUploadCertificate from '../pages/CourseUploadCertificate';
import MyCourseMainPage from '../pages/MyCourse';
import {
  TAB_COURSE_DETAIL,
  TAB_COURSE_MAIN,
  TAB_COURSE_UPLOAD_CERTIFICATE,
} from '../utility';
import { TAB_MY_COURSE } from '../utility/path';

const CourseStack = createStackNavigator();

export function CourseStackNavigator() {
  return (
    <CourseStack.Navigator initialRouteName={TAB_COURSE_MAIN}>
      <CourseStack.Screen
        name={TAB_COURSE_MAIN}
        component={CourseMainPage}
        options={{headerShown: false}}
      />
      <CourseStack.Screen
        name={TAB_MY_COURSE}
        component={MyCourseMainPage}
        options={{headerShown: false}}
      />
      <CourseStack.Screen
        name={TAB_COURSE_DETAIL}
        component={CourseDetail}
        options={{headerShown: false}}
      />
      <CourseStack.Screen
        name={TAB_COURSE_UPLOAD_CERTIFICATE}
        component={CourseUploadCertificate}
        options={{headerShown: false}}
      />
    </CourseStack.Navigator>
  );
}
