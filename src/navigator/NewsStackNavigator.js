import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import NewsDetailPage from '../pages/NewsDetailPage';
import NewsMainPage from '../pages/NewsMainPage';
import {TAB_NEWS_DETAIL, TAB_NEWS_MAIN} from '../utility';

const NewsStack = createStackNavigator();

export function NewsStackNavigator() {
  return (
    <NewsStack.Navigator>
      <NewsStack.Screen
        name={TAB_NEWS_MAIN}
        component={NewsMainPage}
        options={{headerShown: false}}
      />
      <NewsStack.Screen
        name={TAB_NEWS_DETAIL}
        component={NewsDetailPage}
        options={{headerShown: false}}
      />
    </NewsStack.Navigator>
  );
}
