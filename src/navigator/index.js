import { CourseStackNavigator } from './CourseStackNavigator';
import { NewsStackNavigator } from './NewsStackNavigator';
import { ToolsStackNavigator } from './ToolsStackNavigator';

export { CourseStackNavigator, NewsStackNavigator, ToolsStackNavigator };

