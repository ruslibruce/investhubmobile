import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import AboutUs from '../pages/AboutUs';
import ChangePassword from '../pages/ChangePassword';
import Disclaimer from '../pages/Disclaimer';
import EventDetailPage from '../pages/EventDetailPage';
import FAQ from '../pages/FAQ';
import ForumMainPage from '../pages/ForumMainPage';
import PrivacyPolicy from '../pages/PrivacyPolicy';
import TermsCondition from '../pages/TermsCondition';
import ToolsMainPage from '../pages/ToolsMainPage';
import {
  TAB_TOOLS_ABOUT_US,
  TAB_TOOLS_CHANGE_PASSWORD,
  TAB_TOOLS_DISCLAIMER,
  TAB_TOOLS_EVENT_DETAIL,
  TAB_TOOLS_FAQ,
  TAB_TOOLS_FORUM,
  TAB_TOOLS_MAIN,
  TAB_TOOLS_PRIVACY_POLICY,
  TAB_TOOLS_TERMS_CONDITION
} from '../utility';

const ToolsStack = createStackNavigator();

export function ToolsStackNavigator() {
  return (
    <ToolsStack.Navigator initialRouteName={TAB_TOOLS_MAIN}>
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_MAIN}
        component={ToolsMainPage}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_CHANGE_PASSWORD}
        component={ChangePassword}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_EVENT_DETAIL}
        component={EventDetailPage}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_FORUM}
        component={ForumMainPage}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_FAQ}
        component={FAQ}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_ABOUT_US}
        component={AboutUs}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_TERMS_CONDITION}
        component={TermsCondition}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_PRIVACY_POLICY}
        component={PrivacyPolicy}
      />
      <ToolsStack.Screen
        options={{headerShown: false}}
        name={TAB_TOOLS_DISCLAIMER}
        component={Disclaimer}
      />
    </ToolsStack.Navigator>
  );
}
