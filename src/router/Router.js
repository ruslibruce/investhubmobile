import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {useSelector} from 'react-redux';
import {BottomTabConfig} from '../components/BottomTabConfig';
import {
  CourseStackNavigator,
  NewsStackNavigator,
  ToolsStackNavigator,
} from '../navigator';
import CertificatePage from '../pages/Certificate';
import ChangeEmail from '../pages/ChangeEmail';
import CollectionPage from '../pages/Collection';
import CollectionChoosePage from '../pages/CollectionChoose';
import CollectionDetailPage from '../pages/CollectionDetail';
import CourseContent from '../pages/CourseContent';
import CourseDetailProgress from '../pages/CourseDetailProgress';
import CourseQuizPage from '../pages/CourseQuiz';
import EmailConfirm from '../pages/EmailConfirm';
import EventDetailPage from '../pages/EventDetailPage';
import EventMainPage from '../pages/EventMainPage';
import ForgotPassword from '../pages/ForgotPassword';
import HomeMainPage from '../pages/HomeMainPage';
import InvestmentMainPage from '../pages/InvestmentMainPage';
import Language from '../pages/Language';
import LevelUp from '../pages/LevelUp';
import Login from '../pages/Login';
import MenuViewAllPage from '../pages/MenuViewAll';
import MyLearningJourneyDetail from '../pages/MyLearningJourneyDetail';
import NotificationPage from '../pages/Notification';
import Preferences from '../pages/Preferences';
import Preliminary from '../pages/Preliminary';
import Register from '../pages/Register';
import ResetPasswordForm from '../pages/ResetPasswordForm';
import SearchAllPortal from '../pages/SearchAll';
import StockScreener from '../pages/StockScreener';
import StockScreenerColoumn from '../pages/StockScreenerColoumn';
import StockScreenerProfileCompany from '../pages/StockScreenerProfileCompany';
import UpdateProfile from '../pages/UpdateProfile';
import ValidateTokenForm from '../pages/ValidateTokenForm';
import {
  CHANGE_EMAIL,
  COLLECTION,
  COLLECTION_CHOOSE,
  COLLECTION_DETAIL,
  COURSE_CONTENT,
  COURSE_DETAIL_PROGRESS,
  COURSE_QUIZ,
  DASHBOARD,
  DASHBOARD_COURSE,
  DASHBOARD_HOME,
  DASHBOARD_INVESTMENT,
  DASHBOARD_NEWS,
  DASHBOARD_TOOLS,
  EMAIL_CONFIRM,
  EVENTS,
  EVENT_DETAIL_PAGE,
  FORGOT_PASSWORD,
  LANGUAGE,
  LEVEL_UP,
  LOGIN,
  MY_CERTIFICATE,
  MY_LEARNING_JOURNEY_DETAIL,
  MY_MENU,
  NOTIFICATION,
  PREFERENCES,
  PRELIMINARY,
  REGISTER,
  RESET_PASSWORD_FORM,
  SEARCH_ALL,
  STOCK_SCREENER,
  STOCK_SCREENER_COLOUMN,
  STOCK_SCREENER_PROFILE_COMPANY,
  TAB_COURSE_MAIN,
  TAB_NEWS_MAIN,
  TAB_TOOLS_MAIN,
  UPDATE_PROFILE,
  VALIDATE_TOKEN_FORM,
} from '../utility';
import Splash from '../widgets/Splash';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const BottomNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName={DASHBOARD_HOME}
      screenOptions={{headerShown: false, unmountOnBlur: true}}
      tabBar={props => <BottomTabConfig {...props} />}>
      <Tab.Screen name={DASHBOARD_HOME} component={HomeMainPage} />
      <Tab.Screen
        listeners={({navigation}) => ({
          tabPress: e => {
            e.preventDefault();
            navigation.navigate(DASHBOARD_COURSE, {screen: TAB_COURSE_MAIN});
          },
        })}
        name={DASHBOARD_COURSE}
        component={CourseStackNavigator}
      />
      <Tab.Screen
        listeners={({navigation}) => ({
          tabPress: e => {
            e.preventDefault();
            navigation.navigate(DASHBOARD_NEWS, {screen: TAB_NEWS_MAIN});
          },
        })}
        name={DASHBOARD_NEWS}
        component={NewsStackNavigator}
      />
      <Tab.Screen name={DASHBOARD_INVESTMENT} component={InvestmentMainPage} />
      <Tab.Screen
        listeners={({navigation}) => ({
          tabPress: e => {
            e.preventDefault();
            navigation.navigate(DASHBOARD_TOOLS, {screen: TAB_TOOLS_MAIN});
          },
        })}
        name={DASHBOARD_TOOLS}
        component={ToolsStackNavigator}
      />
    </Tab.Navigator>
  );
};

const Router = () => {
  const appMethod = useSelector(state => state.app);
  if (appMethod.isLoading) {
    return <Splash />;
  }
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={DASHBOARD}
        component={BottomNavigator}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={LOGIN}
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={REGISTER}
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={CHANGE_EMAIL}
        component={ChangeEmail}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={FORGOT_PASSWORD}
        component={ForgotPassword}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={RESET_PASSWORD_FORM}
        component={ResetPasswordForm}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={VALIDATE_TOKEN_FORM}
        component={ValidateTokenForm}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={EMAIL_CONFIRM}
        component={EmailConfirm}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={COURSE_DETAIL_PROGRESS}
        component={CourseDetailProgress}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={COURSE_CONTENT}
        component={CourseContent}
        options={{headerShown: false}}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={EVENTS}
        component={EventMainPage}
      />
      <Stack.Screen
        name={EVENT_DETAIL_PAGE}
        component={EventDetailPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={LEVEL_UP}
        component={LevelUp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={STOCK_SCREENER}
        component={StockScreener}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={STOCK_SCREENER_COLOUMN}
        component={StockScreenerColoumn}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={STOCK_SCREENER_PROFILE_COMPANY}
        component={StockScreenerProfileCompany}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={NOTIFICATION}
        component={NotificationPage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={SEARCH_ALL}
        component={SearchAllPortal}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={COURSE_QUIZ}
        component={CourseQuizPage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={LANGUAGE}
        component={Language}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={COLLECTION}
        component={CollectionPage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={COLLECTION_DETAIL}
        component={CollectionDetailPage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={COLLECTION_CHOOSE}
        component={CollectionChoosePage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={MY_LEARNING_JOURNEY_DETAIL}
        component={MyLearningJourneyDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={MY_MENU}
        component={MenuViewAllPage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={MY_CERTIFICATE}
        component={CertificatePage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={UPDATE_PROFILE}
        component={UpdateProfile}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={PREFERENCES}
        component={Preferences}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={PRELIMINARY}
        component={Preliminary}
      />
    </Stack.Navigator>
  );
};

export default Router;
