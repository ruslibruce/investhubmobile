import React from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import LoadingSpinner from '../../components/LoadingSpinner';
import { Colors, SEARCH_ALL, STOCK_SCREENER } from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import { StatusCustomBarDark } from '../../widgets/StatusCustomBar';
import { styling } from './styles';
import useMenuAll from './useMenuAll';

const MenuViewAllPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    translate,
    handleNavigation,
    authMethod,
    filteredMenuBottom,
    filteredMenuHistory,
    menuIcon,
    isLoadingGetUrl,
  } = useMenuAll();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity
            style={styles.buttonListHeader}
            onPress={() => navigation.goBack()}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>{translate('AllTools')}</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate(SEARCH_ALL)}
          style={styles.buttonListHeaderRight}>
          <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
      <SizedBox height={8} />
      {filteredMenuHistory.length > 0 && (
        <View style={styles.containerCard}>
          <Text style={styles.textHeaderCard}>{translate('LastOpened')}</Text>
          <SizedBox height={20} />
          <View style={styles.containerLastOpened}>
            {filteredMenuHistory.map((item, index) => {
              const ObjectIcon = menuIcon.find(i => i.label === item.label);
              return (
                <TouchableOpacity
                  key={item.id}
                  onPress={() => handleNavigation(item)}
                  style={styles.containerButtonShortcut}>
                  {isLoadingGetUrl && item.url === STOCK_SCREENER ? (
                    <View style={styles.containerButtonIcon}>
                      <LoadingSpinner />
                    </View>
                  ) : (
                    <View style={styles.containerButtonIcon}>
                      {ObjectIcon?.icon}
                    </View>
                  )}
                  <Text style={styles.textButtonShortcut}>{item.name}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      )}
      <SizedBox height={30} />
      <View style={styles.containerCard}>
        <Text style={styles.textHeaderCard}>{translate('AllTools')}</Text>
        <SizedBox height={20} />
        <FlatList
          data={filteredMenuBottom}
          ref={listRef}
          scrollEnabled={false}
          onScroll={onScroll}
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          showsHorizontalScrollIndicator={false}
          numColumns={4}
          renderItem={({item, index}) => {
            const ObjectIcon = menuIcon.find(i => i.label === item.label);
            return (
              <TouchableOpacity
                key={item.id}
                onPress={() => handleNavigation(item)}
                style={styles.containerButtonShortcut}>
                {isLoadingGetUrl && item.url === STOCK_SCREENER ? (
                  <View style={styles.containerButtonIcon}>
                    <LoadingSpinner />
                  </View>
                ) : (
                  <View style={styles.containerButtonIcon}>
                    {ObjectIcon?.icon}
                  </View>
                )}
                <Text style={styles.textButtonShortcut}>{item.name}</Text>
              </TouchableOpacity>
            );
          }}
        />
      </View>
      <IconFloat
        bottom={70}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
      <BottomTabOnlyView />
    </View>
  );
};

export default MenuViewAllPage;
