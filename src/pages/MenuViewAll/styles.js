import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.WHITE,
      position: 'relative',
      paddingTop: insets.top,
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: 5,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
    },
    containerButtonHeaderRight: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 17.5,
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    contentFlatlist: {
      backgroundColor: Colors.WHITE,
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    marginScrollView: {
      // width: '100%',
    },
    containerScroll: {
      paddingVertical: 8,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopColor: '#E8E8E8',
      borderBottomColor: '#E8E8E8',
    },
    contentScroll: {
      paddingLeft: 20,
    },
    containerViewDateMarkAll: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginVertical: 21.5,
    },
    textDate: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
      lineHeight: 19.07,
    },
    textMarkAll: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
      lineHeight: 16.34,
    },
    containerCard: {
      backgroundColor: Colors.WHITE,
      alignItems: 'flex-start',
      marginHorizontal: 20,
      padding: 15,
      borderRadius: 15,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 3,
      },
      shadowOpacity: 0.27,
      shadowRadius: 4.65,
      elevation: 6,
    },
    viewIcon: {
      width: 40,
      height: 40,
      borderRadius: 40,
      backgroundColor: Colors.PINK,
      justifyContent: 'center',
      alignItems: 'center',
    },
    containerTextNotif: {
      width: '70%',
    },
    textHeaderCard: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      lineHeight: 16,
    },
    textDateNotif: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    containerSubNotif: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 6,
    },
    textTagNotif: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(10),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    containerButtonShortcut: {
      alignItems: 'center',
      maxWidth: 80,
      width: 80,
      height: 50,
      gap: 8,
      marginBottom: 30,
    },
    containerButtonIcon: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.PINK,
      padding: 5,
      borderRadius: 24,
    },
    textButtonShortcut: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(9),
      color: '#464646',
      textAlign: 'center',
    },
    containerLastOpened: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
  });
