import React from 'react';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {
  IconCalendarRed,
  IconEbookRed,
  IconGlossariumRed,
  IconPreferenced,
  IconSertifikat,
  IconSpeakerRed,
  IconStockScreenRed,
  IconStudentRed,
  IconMyCourse,
  IconCollection,
} from '../../assets/svg';
import i18n from '../../i18n';
import {
  EVENTS,
  handleOpenLink,
  STOCK_SCREENER,
  TAB_TOOLS_FORUM,
  PREFERENCES,
} from '../../utility';
import {
  COLLECTION,
  DASHBOARD_COURSE,
  DASHBOARD_TOOLS,
  MY_CERTIFICATE,
  TAB_MY_COURSE,
} from '../../utility/path';
import {addMenuHistory} from '../../redux/reducers/auth.reducers';
import {useNavigation} from '@react-navigation/native';
import Config from 'react-native-config';
import useGetUrlWebIDXStockScreener from '../../api/useGetUrlWebIDXStockScreener';
import Toast from 'react-native-toast-message';

const useMenuAll = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t: translate, i18n} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const menuHistory = useSelector(state => state.auth.menuHistory);
  const {accessGetUrlWebIDXStockScreener, isLoading: isLoadingGetUrl} = useGetUrlWebIDXStockScreener();

  const menuIcon = [
    {
      id: 1,
      icon: <IconStudentRed width={20} height={20} />,
      label: 'IconStudentRed',
    },
    {
      id: 2,
      icon: <IconEbookRed width={20} height={20} />,
      label: 'IconEbookRed',
    },
    {
      id: 3,
      icon: <IconGlossariumRed width={20} height={20} />,
      label: 'IconGlossariumRed',
    },
    {
      id: 4,
      icon: <IconSpeakerRed width={20} height={20} />,
      label: 'IconSpeakerRed',
    },
    {
      id: 5,
      icon: <IconCalendarRed width={20} height={20} />,
      label: 'IconCalendarRed',
    },
    {
      id: 6,
      icon: <IconStockScreenRed width={20} height={20} />,
      label: 'IconStockScreenRed',
    },
    {
      id: 7,
      icon: <IconPreferenced width={20} height={20} />,
      label: 'IconPreferenced',
    },
    {
      id: 8,
      icon: <IconSertifikat width={20} height={20} />,
      label: 'IconSertifikat',
    },
    {
      id: 9,
      icon: <IconMyCourse width={20} height={20} />,
      label: 'IconMyCourse',
    },
    {
      id: 10,
      icon: <IconCollection width={20} height={20} />,
      label: 'IconCollection',
    },
  ];

  const menuBottom = [
    {
      id: 1,
      name: translate('Forum_Tab_Capital'),
      label: 'IconStudentRed',
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 2,
      name: translate('Ebook_Capital'),
      label: 'IconEbookRed',
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 3,
      name: translate('Glossarium_Capital'),
      label: 'IconGlossariumRed',
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 4,
      name: translate('Event_Tab_Capital'),
      label: 'IconSpeakerRed',
      url: EVENTS,
      nesting: '',
      isLogin: false,
    },
    {
      id: 5,
      name: translate('Class_Capital'),
      label: 'IconCalendarRed',
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 6,
      name: translate('Stock_Capital'),
      label: 'IconStockScreenRed',
      url: STOCK_SCREENER,
      nesting: '',
      isLogin: false,
    },
    {
      id: 7,
      name: translate('Preferences'),
      label: 'IconPreferenced',
      url: PREFERENCES,
      nesting: '',
      isLogin: true,
    },
    {
      id: 8,
      name: translate('MyCertificate'),
      label: 'IconSertifikat',
      url: MY_CERTIFICATE,
      nesting: '',
      isLogin: true,
    },
    {
      id: 9,
      label: 'IconMyCourse',
      name: translate('My_Course'),
      url: DASHBOARD_COURSE,
      nesting: 'TAB_MY_COURSE',
      isLogin: true,
    },
    {
      id: 10,
      label: 'IconCollection',
      name: translate('MyCollection'),
      url: COLLECTION,
      nesting: '',
      isLogin: true,
    },
  ];

  const filteredMenuBottom = menuBottom.filter(item => {
    // Add your condition to hide items when not logged in
    if (!authMethod.isLogin && item.isLogin === true) {
      return false; // Hide 'Profile' when not logged in
    }
    return true; // Show other items
  });

  const filteredMenuHistory = menuHistory.filter(item => {
    // Add your condition to hide items when not logged in
    if (!authMethod.isLogin && item.isLogin === true) {
      return false; // Hide 'Profile' when not logged in
    }
    return true; // Show other items
  });

  const handleNavigation = item => {
    dispatch(addMenuHistory(item));
    if (item.url === '') {
      Toast.show({
        type: 'info',
        position: 'bottom',
        text1: translate('UpdateTitle'),
        text2: translate('UpdateDesc'),
      });
      return;
    }
    if (item.url === STOCK_SCREENER) {
      accessGetUrlWebIDXStockScreener({
        tokenInvesthub: authMethod?.token,
      });
      return;
    }
    if (item.nesting) {
      navigation.navigate(item.url, {screen: item.nesting});
    } else {
      navigation.navigate(item.url);
    }
  };

  return {
    translate,
    handleNavigation,
    authMethod,
    filteredMenuBottom,
    filteredMenuHistory,
    menuIcon,
    isLoadingGetUrl,
  };
};

export default useMenuAll;
