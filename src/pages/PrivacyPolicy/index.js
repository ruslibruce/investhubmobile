import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import useGetPrivacyPolicy from '../../api/useGetPrivacyPolicy';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import {Colors} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import NoData from '../../components/NoData';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import IconFloat from '../../widgets/IconFloat';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import WebDisplay from '../../components/WebDisplay';

const PrivacyPolicy = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const tabHeight = useBottomTabBarHeight();
  const {isLoading, dataPrivacyPolicy} = useGetPrivacyPolicy();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} hiddenSearch={true} />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>{dataPrivacyPolicy?.title}</Text>
        <View />
      </View>
      <SizedBox height={60} />
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <ScrollView
          ref={listRef}
          onScroll={onScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.contentScroll}>
          {dataPrivacyPolicy ? (
            <WebDisplay html={dataPrivacyPolicy?.body} />
          ) : (
            <NoData />
          )}
        </ScrollView>
      )}
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default PrivacyPolicy;
