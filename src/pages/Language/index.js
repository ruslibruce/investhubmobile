import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import { IconEN, IconID } from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import { Colors, DASHBOARD, DASHBOARD_HOME } from '../../utility';
import { StatusCustomBarDark } from '../../widgets/StatusCustomBar';
import { styling } from './styles';
import useLanguage from './useLanguage';

const Language = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {handleChangeLanguage, handleSaveLanguage, valueLang, translate} =
    useLanguage();
  const styles = styling(insets);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() =>
            navigation.navigate(DASHBOARD, {screen: DASHBOARD_HOME})
          }>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textTitle}>{translate('ChangeLanguage')}</Text>
        <TouchableOpacity
          onPress={handleSaveLanguage}
          style={styles.buttonListHeaderRight}>
          <Text style={styles.textSave}>{translate('Save')}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.containerBody}>
        <TouchableOpacity
          onPress={() => handleChangeLanguage('en')}
          style={styles.containerButtonLang}>
          <View style={styles.containerSymbol}>
            <IconEN />
          </View>
          <Text style={styles.textLang}>English</Text>
          {valueLang == 'en' && (
            <Entypo size={30} color={Colors.GREEN} name="dot-single" />
          )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleChangeLanguage('id')}
          style={styles.containerButtonLang}>
          <View style={styles.containerSymbol}>
            <IconID />
          </View>
          <Text style={styles.textLang}>Indonesia</Text>
          {valueLang == 'id' && (
            <Entypo size={30} color={Colors.GREEN} name="dot-single" />
          )}
        </TouchableOpacity>
      </View>
      <BottomTabOnlyView />
    </View>
  );
};

export default Language;
