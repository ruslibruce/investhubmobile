import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { BackHandler } from 'react-native';
import Toast from 'react-native-toast-message';
import { useDispatch, useSelector } from 'react-redux';
import { changeLanguage } from '../../redux/reducers/auth.reducers';
import { DASHBOARD, DASHBOARD_HOME } from '../../utility';

const useLanguage = () => {
  const {i18n, t: translate} = useTranslation();
  const lang = useSelector(state => state.auth.language);
  const [valueLang, setValueLang] = React.useState(lang);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  console.log('cekkk', valueLang);

  const handleChangeLanguage = values => {
    setValueLang(prev => (prev = values));
  };

  const handleSaveLanguage = () => {
    i18n.changeLanguage(valueLang);
    dispatch(changeLanguage(valueLang));
    Toast.show({
      type: 'success',
      position: 'bottom',
      text1: translate('Language'),
      text2: translate('DescLanguage'),
    });
  };

  React.useEffect(() => {
    const backAction = () => {
      navigation.navigate(DASHBOARD, {screen: DASHBOARD_HOME});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return {handleChangeLanguage, handleSaveLanguage, valueLang, translate};
};

export default useLanguage;
