import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.WHITE,
      paddingTop: insets.top,
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: 15,
      borderBottomWidth: 1,
      borderBottomColor: '#E8E8E8',
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      paddingHorizontal: 15,
      paddingVertical: 5,
      borderRadius: 12,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textTitle: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
    },
    textSave: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
      letterSpacing: 0.2,
    },
    containerBody: {
      paddingHorizontal: 20,
      gap: 20,
      paddingTop: 15,
    },
    containerButtonLang: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    containerSymbol: {
      width: 40,
      height: 40,
      borderRadius: 80,
      backgroundColor: Colors.PINK,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textLang: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      textAlign: 'left',
      flex: 1,
      marginLeft: 16,
    },
  });
