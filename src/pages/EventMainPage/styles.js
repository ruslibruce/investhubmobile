import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (insets, value) =>
  StyleSheet.create({
    container: {
      flex: 1,
      position: 'relative',
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: insets.top,
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: 5,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    contentFlatlist: {
      width: matrics.screenWidth,
      // backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: value,
    },
    textHeadline: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(24),
      color: Colors.BLACK,
      lineHeight: 33,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    containerImageHeadline: {
      height: 152,
      width: '100%',
      borderRadius: 12,
      overflow: 'hidden',
    },
    containerTextDescriptionHeadline: {
      width: '100%',
    },
    textDescriptionHeadline: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(16),
      color: Colors.BLACK,
      lineHeight: 22,
    },
    containerHeadlineInfo: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    containerTextHeadlineInfo: {
      flexDirection: 'row',
      gap: 12,
      alignItems: 'center',
    },
    textHeadlineInfoRed: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
    },
    dotStyling: {
      backgroundColor: Colors.GRAY,
      width: 4,
      height: 4,
      borderRadius: 4,
    },
    textHeadlineInfo: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#696F79',
    },
    containerScroll: {
      paddingVertical: 8,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopColor: '#E8E8E8',
      borderBottomColor: '#E8E8E8',
      marginHorizontal: -20,
    },
    contentScroll: {
      paddingLeft: 20,
    },
    containerButtonTabs: {
      height: 34,
      paddingHorizontal: 12,
      paddingVertical: 10,
      borderRadius: 8,
      marginRight: 12,
    },
    textButtonTabs: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
    },
    containerListFooterItem: {
      paddingHorizontal: 10,
      paddingVertical: 6,
      borderWidth: 1,
      borderColor: Colors.GRAY,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 15,
      alignSelf: 'center',
      maxWidth: 100,
    },
    textBottomLeftItem: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: Colors.PRIMARY,
    },
  });
