import moment from 'moment';
import React from 'react';
import {
  FlatList,
  Image,
  Pressable,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import EventCard from '../../components/EventCard';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import WebDisplay from '../../components/WebDisplay';
import {Colors, SEARCH_ALL, isHTML} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useEventMainPage from './useEventMainPage';

const EventMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    dataTabs,
    handleClickTabs,
    isLoading,
    dataHeadlines,
    handleNavigation,
    dataEvent,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useEventMainPage();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, 150);
  const tagsStyles = {
    p: {
      fontSize: 16,
      fontWeight: 700,
      fontFamily: 'OpenSans-Regular',
      color: '#000000',
    },
  };

  const renderHeader = () => (
    <>
      {Object.keys(dataHeadlines).length > 0 && (
        <>
          <SizedBox height={7} />
          <Text style={styles.textHeadline}>Event Headlines</Text>
          <SizedBox height={16} />
          <Pressable onPress={() => handleNavigation(dataHeadlines.id)}>
            <View style={styles.containerImageHeadline}>
              <Image
                style={styles.widthImage}
                source={{uri: dataHeadlines.cover_image}}
              />
            </View>
            <SizedBox height={16} />
            <View style={styles.containerTextDescriptionHeadline}>
              {isHTML(dataHeadlines.title) ? (
                <View
                  style={{
                    height: 65,
                    overflow: 'hidden',
                  }}>
                  <WebDisplay
                    html={dataHeadlines.title}
                    tagsStyles={tagsStyles}
                  />
                </View>
              ) : (
                <>
                  <Text
                    numberOfLines={2}
                    style={styles.textDescriptionHeadline}>
                    {dataHeadlines.title}
                  </Text>
                </>
              )}
            </View>
            <SizedBox height={8} />
            <View style={styles.containerHeadlineInfo}>
              <View style={styles.containerListFooterItem}>
                <Text style={styles.textBottomLeftItem}>
                  {dataHeadlines.category}
                </Text>
              </View>
              <View style={styles.containerTextHeadlineInfo}>
                <Text style={styles.textHeadlineInfoRed}>
                  {dataHeadlines.location}
                </Text>
                <View style={styles.dotStyling} />
                <Text style={styles.textHeadlineInfo}>
                  {moment(dataHeadlines.publish_time).format('MMM DD, YYYY')}
                </Text>
              </View>
            </View>
          </Pressable>
        </>
      )}
      <SizedBox height={20} />
      <ScrollView
        style={styles.containerScroll}
        contentContainerStyle={styles.contentScroll}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {dataTabs.map((tab, i) => (
          <TouchableOpacity
            key={tab.id}
            onPress={() => handleClickTabs(tab)}
            style={[
              styles.containerButtonTabs,
              {backgroundColor: tab.isSelected ? Colors.PINK : Colors.WHITE},
            ]}>
            <Text
              style={[
                styles.textButtonTabs,
                {color: tab.isSelected ? Colors.PRIMARY : '#333333'},
              ]}>
              {tab.name}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
      <SizedBox height={20} />
    </>
  );

  const renderFooter = () => (
    <>
      {isLoadingPage ? <LoadingSpinner /> : null}
      {/* <SizedBox height={20} />
      <UpcomingEvent />
      <SizedBox height={20} />
      <TopEvent title={'Top Event'} /> */}
    </>
  );

  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => navigation.goBack()}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate(SEARCH_ALL)}
          style={styles.buttonListHeaderRight}>
          <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <FlatList
          data={dataEvent}
          ref={listRef}
          onScroll={onScroll}
          contentContainerStyle={styles.contentFlatlist}
          ListHeaderComponent={renderHeader()}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => <EventCard item={item} />}
          ListEmptyComponent={() => <NoData />}
          onEndReached={() => {
            if (totalPage > params.page) {
              setIsLoadingPage(true);
              setParams(prev => ({
                ...prev,
                page: prev.page + 1,
              }));
            }
          }}
          ListFooterComponent={renderFooter()}
        />
      )}
      <IconFloat
        bottom={70}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
      <BottomTabOnlyView />
    </View>
  );
};

export default EventMainPage;
