import {useNavigation} from '@react-navigation/native';
import React from 'react';
import useGetEvent from '../../api/useGetEvent';
import useGetEventCategory from '../../api/useGetEventCategory';
import {EVENT_DETAIL_PAGE} from '../../utility';

const useEventMainPage = () => {
  const navigation = useNavigation();
  const {dataTabs, setDataTabs} = useGetEventCategory();
  const {
    dataEvent,
    isLoading,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useGetEvent();
  const [dataHeadlines, setDataHeadlines] = React.useState({});

  React.useEffect(() => {
    if (dataEvent.length > 0) {
      setDataHeadlines(dataEvent[0]);
    }
  }, [dataEvent]);

  const handleClickTabs = value => {
    const result = dataTabs.map(tab => {
      if (tab === value) {
        tab.isSelected = !tab.isSelected;
        setParams(prev => ({
          ...prev,
          page: 1,
          category: tab.name === 'All' ? '' : tab.id,
        }));
      } else {
        tab.isSelected = false;
      }
      return tab;
    });
    setDataTabs(prev => (prev = result));
  };

  const handleNavigation = id => {
    if (!id) return;
    navigation.navigate(EVENT_DETAIL_PAGE, {
      id: id,
    });
  };

  return {
    dataTabs,
    handleClickTabs,
    dataEvent,
    isLoading,
    dataHeadlines,
    handleNavigation,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  };
};

export default useEventMainPage;
