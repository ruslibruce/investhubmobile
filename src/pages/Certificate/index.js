import {Card} from '@ui-kitten/components';
import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import MyModalBottom from '../../components/MyModalBottom';
import {
  BUTTON_COLOR,
  COLLECTION_DETAIL,
  Colors,
  DASHBOARD_COURSE,
  getStatusColor,
  SEARCH_ALL,
  TAB_COURSE_MAIN,
} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputCustom from '../../widgets/TextInputCostum';
import {styling} from './styles';
import useCertificate from './useCertificate';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {IconSertifikat} from '../../assets/svg';
import NoData from '../../components/NoData';

const CertificatePage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {dataCertificate} = useCertificate();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, 150);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity
            style={styles.buttonListHeader}
            onPress={() => navigation.goBack()}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>My Certificate</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate(SEARCH_ALL)}
          style={styles.buttonListHeaderRight}>
          <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
      <SizedBox height={8} />
      <FlatList
        data={dataCertificate}
        ref={listRef}
        onScroll={onScroll}
        contentContainerStyle={styles.contentFlatlist}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item, index}) => (
          <Card style={styles.containerCard}>
            <View style={styles.viewCard}>
              <View style={styles.containerLeft}>
                <View style={styles.containerIcon}>
                  <IconSertifikat width={25} height={25} />
                </View>
                <View>
                  <Text style={styles.textCollection}>{item.provider}</Text>
                  <Text style={styles.textCollection}>{item.title}</Text>
                </View>
              </View>
              <View
                style={{
                  backgroundColor: !item.is_verified
                    ? getStatusColor('pending')
                    : item.is_approved
                    ? getStatusColor('published')
                    : getStatusColor('rejected'),
                  padding: 10,
                  borderRadius: 5,
                }}>
                <Text style={styles.textButton}>
                  {!item.is_verified
                    ? 'Pending'
                    : item.is_approved
                    ? 'Approved'
                    : 'Rejected'}
                </Text>
              </View>
            </View>
          </Card>
        )}
        ListEmptyComponent={() => <NoData />}
      />
      <IconFloat
        bottom={70}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
      <BottomTabOnlyView />
    </View>
  );
};

export default CertificatePage;
