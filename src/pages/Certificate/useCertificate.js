import { useSelector } from 'react-redux';
import useGetCertificate from '../../api/useGetCertificate';
import React from 'react';
import { useNavigation } from '@react-navigation/native';

const useCertificate = () => {
  const authMethod = useSelector(state => state.auth);
  const navigation = useNavigation();
  const {dataCertificate} = useGetCertificate();

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod]);

  return {
    dataCertificate,
  };
};

export default useCertificate;
