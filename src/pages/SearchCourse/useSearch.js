import React from 'react';
import useGetSearchCourse from '../../api/useGetSearchCourse';
import {useNavigation} from '@react-navigation/native';
import {DASHBOARD_COURSE, SEARCH_COURSE, TAB_COURSE_DETAIL} from '../../utility';

const useSearch = () => {
  const [search, setSearch] = React.useState('');
  const {accessSearchCourse, dataSearchCourse, isLoading} =
    useGetSearchCourse();
  const navigation = useNavigation();

  React.useEffect(() => {
    if (search.length > 3) {
      accessSearchCourse(search);
    }

    if (search.length === 0) {
      accessSearchCourse();
    }
  }, [search]);

  const handleNavigateCourse = value => {
    navigation.navigate(DASHBOARD_COURSE, {
      screen: TAB_COURSE_DETAIL,
      params: {
        detail: value,
        route: SEARCH_COURSE,
      },
    });
  };

  return {
    search,
    setSearch,
    dataSearchCourse,
    isLoading,
    handleNavigateCourse,
  };
};

export default useSearch;
