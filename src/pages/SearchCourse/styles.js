import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      backgroundColor: Colors.WHITE,
      flex: 1,
      position: 'relative',
    },
    containerScroll: {
      flex: 1,
    },
    contentScrollView: {
      backgroundColor: Colors.WHITE,
      paddingBottom: 20,
      paddingHorizontal: 20,
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingTop: insets.top,
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      gap: 12,
    },
    inputSearch: {
      width: '70%',
      shadowColor: '#000',
      height: 40,
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,
      elevation: 10,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 40,
      width: 82,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,
      elevation: 10,
    },
    textTitleHeader: {
      fontSize: getFontSize(18),
      fontFamily: FontType.openSansBold700,
      color: Colors.BLACK,
    },
    containerListCard: {
      marginBottom: 20,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 6,
      },
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
      backgroundColor: Colors.WHITE,
      padding: 10,
      borderRadius: 15,
    },
    textTitleCard: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
    },
    textDescription: {
      fontSize: getFontSize(10),
      fontFamily: FontType.openSansRegular400,
      color: Colors.LABELINPUT,
      lineHeight: 16,
    },
    containerUpload: {
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: '#E8E8E8',
      borderRadius: 6,
      height: 72,
      borderStyle: 'dashed',
      marginTop: 4,
      gap: 8,
    },
    viewTextUpload: {
      paddingHorizontal: 8,
      paddingVertical: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.PINK,
      borderRadius: 4,
    },
    textUpload: {
      fontSize: getFontSize(10),
      fontFamily: FontType.openSansRegular400,
      color: Colors.PRIMARY,
      lineHeight: 16,
    },
  });
