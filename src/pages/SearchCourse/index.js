import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import {Colors, DASHBOARD_COURSE, TAB_COURSE_MAIN} from '../../utility';
import Divider from '../../widgets/Divider';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputCustom from '../../widgets/TextInputCostum';
import {styling} from './styles';
import useSearch from './useSearch';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const SearchCoursePage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {search, setSearch, dataSearchCourse, isLoading, handleNavigateCourse} =
    useSearch();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TextInputCustom
          iconRight={
            search ? (
              <AntDesign
                onPress={() => setSearch(null)}
                name="close"
                color={Colors.BLACK}
              />
            ) : (
              <Feather name="search" color={Colors.BLACK} />
            )
          }
          placeholder={'Search Course'}
          styleInput={{width: '100%'}}
          placeholderTextColor={Colors.GRAYTEXT}
          autoCapitalize="none"
          value={search}
          containerInputText={styles.inputSearch}
          onChangeText={text => setSearch(text)}
        />
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() =>
            navigation.navigate(DASHBOARD_COURSE, {
              screen: TAB_COURSE_MAIN,
            })
          }>
          <Text>Cancel</Text>
        </TouchableOpacity>
      </View>
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={10} />
      <View style={styles.containerScroll}>
        <ScrollView
          ref={listRef}
          onScroll={onScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.contentScrollView}>
          {dataSearchCourse.map((item, index) => (
            <TouchableOpacity
              onPress={() => handleNavigateCourse(item)}
              style={styles.containerListCard}
              key={index}>
              <Text numberOfLines={1} style={styles.textTitleCard}>
                {item.title}
              </Text>
              <SizedBox height={10} />
              <Text numberOfLines={1} style={styles.textDescription}>
                {item.description}
              </Text>
            </TouchableOpacity>
          ))}
          {dataSearchCourse.length === 0 && !isLoading && <NoData />}
          {isLoading && <LoadingSpinner isFullScreen={true} />}
        </ScrollView>
      </View>
      <IconFloat
        bottom={70}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default SearchCoursePage;
