import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import * as yup from 'yup';
import usePostForgotPassword from '../../api/usePostForgotPassword';
import {VALIDATE_TOKEN_FORM, regexEmail} from '../../utility';

const useForgotPassword = () => {
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const {accessPostForgotPassword, isSuccess, isError, isLoading} =
    usePostForgotPassword();
  const recaptchaRef = React.useRef(null);
  const [tokenRecaptcha, setTokenRecaptcha] = React.useState('');
  const [timeLeft, setTimeLeft] = React.useState(120);
  const [isTimerRun, setIsTimerRun] = React.useState(false);
  const [dataForm, setDataForm] = React.useState({
    email: '',
  });

  // Convert timeLeft into minutes and seconds
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;

  React.useEffect(() => {
    if (!isTimerRun) {
      return;
    }
    // Exit early when we reach 0
    if (timeLeft === 0) {
      setIsTimerRun(false);
      setTimeLeft(120);
      return;
    }

    // Save intervalId to clear the interval when the component re-renders
    const intervalId = setInterval(() => {
      // Decrease time left by one second
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // Clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // Add timeLeft as a dependency to re-run the effect when we update it
  }, [timeLeft, isTimerRun]);

  React.useEffect(() => {
    if (isSuccess) {
      navigation.navigate(VALIDATE_TOKEN_FORM);
    }
  }, [isSuccess]);

  const forgotValidationSchema = React.useCallback(
    yup.object().shape({
      email: yup
        .string()
        .matches(regexEmail(), translate('ValidEmail'))
        .max(
          30,
          ({max}) =>
            `${translate('EmailLength')} ${max} ${translate('Characters')}`,
        )
        .required(translate('RequireEmail')),
    }),
    [],
  );

  const onVerify = token => {
    setTokenRecaptcha(token);
  };

  const onExpire = () => {
    setTokenRecaptcha('');
    Toast.show({
      type: 'info',
      position: 'top',
      text1: translate('Recaptcha_Expired'),
      onHide: () => setTokenRecaptcha(''),
    });
  };

  const onOpenRecaptcha = React.useCallback(
    value => {
      if (!tokenRecaptcha) {
        recaptchaRef.current.open();
        setDataForm(prev => ({
          ...prev,
          ...value,
        }));
      }
    },
    [tokenRecaptcha],
  );

  React.useEffect(() => {
    if (isError) {
      setTokenRecaptcha('');
    }
  }, [isError]);

  React.useEffect(() => {
    if (tokenRecaptcha) {
      onClickSendEmail();
    }
  }, [tokenRecaptcha]);

  const onClickSendEmail = React.useCallback(() => {
    // setIsTimerRun(true);
    accessPostForgotPassword({
      ...dataForm,
      'g-recaptcha-response': tokenRecaptcha,
    });
  }, [dataForm, tokenRecaptcha]);

  return {
    translate,
    forgotValidationSchema,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    isTimerRun,
    minutes,
    seconds,
    isLoading,
  };
};

export default useForgotPassword;
