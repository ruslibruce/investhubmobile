import {Field, Formik} from 'formik';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity} from 'react-native';
import {iconLogo} from '../../assets/images';
import {IconBack, IconUserLogin} from '../../assets/svg';
import GroupButtonAuth from '../../components/GroupButtonAuth';
import NoHaveAccToSignIn from '../../components/NoHaveAccToSignIn';
import TextYourInfo from '../../components/TextYourInfo';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import Divider from '../../widgets/Divider';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styling} from './styles';
import useForgotPassword from './useForgotPassword';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Recaptcha from 'react-native-recaptcha-that-works';
import Config from 'react-native-config';

const ForgotPassword = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    translate,
    forgotValidationSchema,
    onOpenRecaptcha,
    onExpire,
    onVerify,
    recaptchaRef,
    isTimerRun,
    minutes,
    seconds,
    isLoading,
  } = useForgotPassword();
  const styles = styling(insets);
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.buttonTop}>
        <IconBack />
      </TouchableOpacity>
      <SizedBox height={20} />
      <Image source={iconLogo} style={styles.widthImage} />
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={16} />
      <Text style={styles.textTitle}>{translate('ForgotPassword_Title')}</Text>
      <SizedBox height={4} />
      <Text style={styles.textSubTitle}>
        {translate('Desc_ForgotPassword')}
      </Text>
      <SizedBox height={16} />
      <Formik
        validationSchema={forgotValidationSchema}
        initialValues={{email: ''}}
        onSubmit={values => onOpenRecaptcha(values)}>
        {({handleSubmit, isValid}) => (
          <>
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelEmail')}
              placeholder={translate('EnterEmail')}
              icon={<IconUserLogin width={20} />}
              autoCapitalize="none"
              name={'email'}
            />
            <SizedBox height={16} />
            <Recaptcha
              ref={recaptchaRef}
              siteKey={Config.RECAPTCHA_SITE_KEY}
              baseUrl={Config.BASE_URL}
              onVerify={onVerify}
              onExpire={onExpire}
              size="normal"
            />
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading || !isValid}
              onPress={handleSubmit}
              label={isTimerRun ? `${minutes}:${seconds}` : translate('Send')}
            />
          </>
        )}
      </Formik>
      <SizedBox height={16} />
      <TextYourInfo />
      <GroupButtonAuth isLogin={true}  />
      <NoHaveAccToSignIn />
    </ScrollView>
  );
};

export default ForgotPassword;
