import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import usePostLogout from '../../api/usePostLogout';
import {logout} from '../../redux/reducers/auth.reducers';
import {useTranslation} from 'react-i18next';

const useToolsMainPage = () => {
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const {profile} = authMethod;
  const dispatch = useDispatch();
  const scrollViewRef = React.useRef();
  const navigation = useNavigation();
  const {accessLogout, isLoading} = usePostLogout();
  const {token} = useSelector(state => state.auth);

  const isCloseToTop = React.useCallback(
    ({layoutMeasurement, contentOffset, contentSize}) => {
      return contentOffset.y == 0;
    },
    [],
  );

  const onMomentumScrollEnd = React.useCallback(({nativeEvent}) => {
    if (isCloseToTop(nativeEvent)) {
      scrollViewRef.current.scrollTo({animated: true, y: 200});
    }
  }, []);

  const onContentSizeChange = React.useCallback(
    () => scrollViewRef.current.scrollTo({animated: true, y: 200}),
    [],
  );

  const handleLoginLogout = values => {
    if (values === 'Logout') {
      try {
        accessLogout(token);
        return;
      } catch (error) {
        return;
      }
    } else {
      navigation.navigate(values);
    }
  };
  return {
    profile,
    scrollViewRef,
    isCloseToTop,
    onMomentumScrollEnd,
    onContentSizeChange,
    handleLoginLogout,
    translate,
    authMethod,
    isLoading,
  };
};

export default useToolsMainPage;
