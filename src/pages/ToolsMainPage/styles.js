import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (insets, isLogin, height) =>
  StyleSheet.create({
    imageBackgroundStyle: {
      flex: 1,
      position: 'relative',
      width: '100%',
      height: '100%',
    },
    containerNavigator: {
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: 20,
      paddingTop: insets.top,
      position: 'absolute',
    },
    leftTopButton: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 3,
    },
    rightTopButton: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 3,
    },
    container: {
      height: isLogin ? matrics.screenHeight + height : matrics.screenHeight,
      backgroundColor: Colors.WHITE,
      borderTopEndRadius: 24,
      borderTopStartRadius: 24,
      alignItems: 'center',
      paddingHorizontal: 30,
    },
    containerPositionAvatar: {
      marginTop: -45,
      backgroundColor: Colors.WHITE,
      borderRadius: 50,
      padding: 5,
    },
    containerAvatar: {
      backgroundColor: Colors.GRAY3,
      borderRadius: 100,
      height: 100,
      width: 100,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textUsername: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(16),
      color: Colors.BLACK,
    },
    textEmail: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
    },
    stylingDivider: {
      borderColor: Colors.GRAY3,
    },
    textTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(16),
      color: Colors.BLACK,
      alignSelf: 'flex-start',
    },
    buttonLoginLogout: {
      backgroundColor: Colors.WHITE,
      borderColor: Colors.PRIMARY,
      borderWidth: 1,
    },
    textButtonLoginLogout: {
      color: Colors.PRIMARY,
    },
    widthImage: {
      height: '100%',
      width: '100%',
      borderRadius: 100,
      backgroundColor: 'pink',
    },
  });
