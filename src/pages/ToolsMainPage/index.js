import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {imageProfile} from '../../assets/images';
import {IconAvatar} from '../../assets/svg';
import ProfileButtonGeneral from '../../components/ProfileButtonGeneral';
import ProfileButtonMore from '../../components/ProfileButtonMore';
import ProfileGroupTopButton from '../../components/ProfileGroupTopButton';
import {Colors, DASHBOARD_HOME, matrics} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarTranslucent} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useToolsMainPage from './useToolsMainPage';

const ToolsMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    profile,
    scrollViewRef,
    onMomentumScrollEnd,
    onContentSizeChange,
    handleLoginLogout,
    translate,
    authMethod,
    isLoading,
  } = useToolsMainPage();
  const styles = styling(
    insets,
    authMethod.isLogin,
    insets.top + matrics.screenHeight / 2.2,
  );
  return (
    <ImageBackground style={styles.imageBackgroundStyle} source={imageProfile}>
      <StatusCustomBarTranslucent />
      <View style={styles.containerNavigator}>
        {/* <TouchableOpacity style={styles.rightTopButton}>
          <IconBookmark />
        </TouchableOpacity> */}
      </View>
      <SizedBox height={30} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        pagingEnabled
        ref={scrollViewRef}
        onMomentumScrollEnd={onMomentumScrollEnd}
        onContentSizeChange={onContentSizeChange}>
        <SizedBox height={matrics.screenHeight / 2.2} />
        <View
          style={styles.container}>
          <View style={styles.containerPositionAvatar}>
            <View style={styles.containerAvatar}>
              {authMethod.isLogin ? (
                <Image
                  style={styles.widthImage}
                  source={{uri: authMethod.profile?.photo}}
                />
              ) : (
                <IconAvatar />
              )}
            </View>
          </View>
          <SizedBox height={18} />
          {authMethod.isLogin ? (
            <>
              <Text style={styles.textUsername}>{profile?.name}</Text>
              <SizedBox height={6} />
              <Text style={styles.textEmail}>{profile?.email}</Text>
              <SizedBox height={16} />
              <ProfileGroupTopButton />
              <SizedBox height={16} />
              <Text style={styles.textTitle}>{'General Settings'}</Text>
              <ProfileButtonGeneral />
              <SizedBox height={16} />
            </>
          ) : (
            <ButtonPrimary
              styleLabel={styles.textButtonLoginLogout}
              style={styles.buttonLoginLogout}
              label={translate('Login')}
              onPress={() => handleLoginLogout('Login')}
            />
          )}
          <Text style={styles.textTitle}>{'More'}</Text>
          <ProfileButtonMore user={profile} />
          <SizedBox height={16} />
          {authMethod.isLogin && (
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading}
              styleLabel={styles.textButtonLoginLogout}
              style={styles.buttonLoginLogout}
              label={translate('Logout')}
              onPress={() => handleLoginLogout('Logout')}
            />
          )}
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

export default ToolsMainPage;
