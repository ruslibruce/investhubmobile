import {Card} from '@ui-kitten/components';
import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import MyModalBottom from '../../components/MyModalBottom';
import {COLLECTION, Colors} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputCustom from '../../widgets/TextInputCostum';
import {styling} from './styles';
import useCollectionChoose from './useCollectionChoose';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import NoData from '../../components/NoData';
import LoadingSpinner from '../../components/LoadingSpinner';

const CollectionChoosePage = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const {id} = route.params;
  const styles = styling(insets, 150);
  const {
    itemCollection,
    handleShowModal,
    isModalCollection,
    onChangeAddCollection,
    textAdd,
    handleSaveCollection,
    indexChoose,
    handleChoose,
    handleAddContent,
    translate,
    isLoading,
    isLoadingPost,
    isLoadingPostCollectionContent,
  } = useCollectionChoose(styles);
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity
            style={styles.buttonListHeader}
            onPress={() => navigation.goBack()}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>Select a Collection</Text>
        </View>
      </View>
      <SizedBox height={8} />
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <FlatList
          data={itemCollection}
          ref={listRef}
          onScroll={onScroll}
          contentContainerStyle={styles.contentFlatlist}
          ListFooterComponent={() => (
            <ButtonPrimary
              disabled={isLoadingPostCollectionContent}
              loading={isLoadingPostCollectionContent}
              label={
                itemCollection.length === 0
                  ? translate('NavCollection')
                  : translate('Save')
              }
              onPress={() => {
                if (itemCollection.length === 0) {
                  navigation.navigate(COLLECTION);
                  return;
                }
                handleAddContent(id);
              }}
            />
          )}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <Card
              style={[
                styles.containerCard,
                {
                  backgroundColor:
                    indexChoose === index ? Colors.PINK : Colors.WHITE,
                },
              ]}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => handleChoose(index)}
                style={styles.viewCard}>
                <View style={styles.containerLeft}>
                  <View style={styles.containerIcon}>
                    <Feather
                      name={'bookmark'}
                      size={20}
                      color={Colors.PRIMARY}
                    />
                  </View>
                  <Text style={styles.textCollection}>{item.name}</Text>
                </View>
              </TouchableOpacity>
            </Card>
          )}
          ListEmptyComponent={() => (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <NoData />
              <Text style={styles.textHeaderModal}>
                {translate('CreateFolderCollection')}
              </Text>
            </View>
          )}
        />
      )}
      <IconFloat
        bottom={70}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
      <MyModalBottom
        visible={isModalCollection}
        onBackdropPress={handleShowModal}>
        <Text style={styles.textHeaderModal}>Collection Name</Text>
        <SizedBox height={16} />
        <TextInputCustom
          value={textAdd}
          placeholder={'Enter Collection Name'}
          placeholderTextColor={Colors.GRAY}
          styleInput={styles.textHeaderModal}
          onChangeText={onChangeAddCollection}
        />
        <SizedBox height={24} />
        <View style={styles.containerButtonModal}>
          <ButtonPrimary
            onPress={handleShowModal}
            style={styles.styleButtonCancel}
            styleLabel={styles.textButtonCancel}
            label={'Cancel'}
          />
          <ButtonPrimary
            loading={isLoadingPost}
            disabled={isLoadingPost}
            onPress={() => handleSaveCollection(textAdd)}
            style={styles.styleButtonSave}
            label={'Save'}
          />
        </View>
      </MyModalBottom>
      <BottomTabOnlyView />
    </View>
  );
};

export default CollectionChoosePage;
