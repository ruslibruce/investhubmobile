import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {View} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import Toast from 'react-native-toast-message';
import Octicons from 'react-native-vector-icons/Octicons';
import useGetCollection from '../../api/useGetCollection';
import usePostCollection from '../../api/usePostCollection';
import usePostCollectionContent from '../../api/usePostCollectionContent';
import {Colors} from '../../utility';
import ModalCustom from '../../widgets/ModalCustom';

const useCollectionChoose = styles => {
  const {t: translate} = useTranslation();
  const {dataCollection, accessCollection, isLoading} = useGetCollection();
  const {accessPostCollection, isLoading: isLoadingPost} = usePostCollection();
  const {
    accessPostCollectionContent,
    isLoading: isLoadingPostCollectionContent,
  } = usePostCollectionContent();
  const listRef = React.useRef(null);
  const [contentVerticalOffset, setContentVerticalOffset] = React.useState(0);
  const CONTENT_OFFSET_THRESHOLD = 250;
  const [isModalCollection, setIsModalCollection] = React.useState(false);
  const [textAdd, setTextAdd] = React.useState('');
  const [itemCollection, setItemCollection] = React.useState([]);
  const [indexChoose, setIndexChoose] = React.useState();
  const [isModalSukses, setIsModalSukses] = React.useState(false);
  const navigation = useNavigation();

  React.useEffect(() => {
    if (dataCollection) {
      setItemCollection(dataCollection);
    }
  }, [dataCollection]);

  const handleShowModal = () => {
    setIsModalCollection(prev => !prev);
  };

  const onChangeAddCollection = value => {
    setTextAdd(prev => (prev = value));
  };

  const handleSaveCollection = async value => {
    const result = await accessPostCollection(value);
    if (result) {
      accessCollection();
      handleShowModal();
      setTextAdd(prev => (prev = ''));
    }
  };

  const handleChoose = index => {
    setIndexChoose(prev => (prev = index));
  };

  const handleAddContent = id => {
    if (indexChoose === undefined) {
      Toast.show({
        type: 'info',
        position: 'top',
        text1: 'Choose Collection',
        text2: 'Please choose your collection',
      });
      return;
    }
    let data = {
      collection_id: dataCollection[indexChoose].id,
      course_id: id,
    };
    const result = accessPostCollectionContent(data);
    if (result) {
      setIsModalSukses(true);
    }
  };

  const hideModalSukses = () => {
    magicModal.hide();
    setIndexChoose();
    navigation.goBack();
  };

  React.useEffect(() => {
    if (isModalSukses) {
      magicModal.show(() => (
        <ModalCustom
          hideModalSukses={hideModalSukses}
          iconModal={
            <View style={styles.containerModalIcon}>
              <Octicons
                name={'check-circle-fill'}
                size={40}
                color={Colors.GREEN}
              />
            </View>
          }
          singleButton={true}
          textButton={translate('LabelOK')}
          textModalJudul={translate('Done')}
          textModalSub={translate('CollectionSuccess')}
          containerButtonSingle={styles.containerButtonSingle}
        />
      ));
    }
  }, [isModalSukses]);

  return {
    listRef,
    contentVerticalOffset,
    setContentVerticalOffset,
    CONTENT_OFFSET_THRESHOLD,
    itemCollection,
    handleShowModal,
    isModalCollection,
    onChangeAddCollection,
    textAdd,
    handleSaveCollection,
    indexChoose,
    handleChoose,
    handleAddContent,
    translate,
    isLoading,
    isLoadingPost,
    isLoadingPostCollectionContent,
  };
};

export default useCollectionChoose;
