import moment from 'moment';
import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {IconFacebook, IconInstagram, IconTwitter} from '../../assets/svg';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import WebDisplay from '../../components/WebDisplay';
import {Colors, isHTML} from '../../utility';
import DividerVertical from '../../widgets/DividerVertical';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {
  StatusCustomBarDark,
  StatusCustomBarRed,
} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useNewsDetailPage from './useNewsDetailPage';

const NewsDetailPage = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const {tabHeight, dataNewsDetail, isLoading} = useNewsDetailPage(
    route.params,
  );
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  const tagsStyles = {
    p: {
      fontSize: 14,
      fontWeight: 700,
      fontFamily: 'OpenSans-Bold',
      color: '#A62829',
      textAlign: 'justify',
    },
  };

  const tagsStylesBody = {
    p: {
      fontSize: 12,
      fontWeight: 400,
      fontFamily: 'OpenSans-Regular',
      color: '#696F79',
      textAlign: 'justify',
    },
  };

  const renderHeader = () => {
    if (isLoading) {
      return <LoadingSpinner />;
    }
    if (Object.keys(dataNewsDetail).length === 0) {
      return <NoData />;
    }
    return (
      <>
        <View style={styles.containerImageHeadline}>
          <Image
            style={styles.widthImage}
            source={{uri: dataNewsDetail?.cover_image}}
          />
        </View>
        <SizedBox height={16} />
        <View style={styles.containerTextDescriptionHeadline}>
          {isHTML(dataNewsDetail.subject) ? (
            <WebDisplay html={dataNewsDetail.subject} tagsStyles={tagsStyles} />
          ) : (
            <Text style={styles.textDescriptionHeadline} numberOfLines={2}>
              {dataNewsDetail.subject}
            </Text>
          )}
        </View>
        <SizedBox height={8} />
        <View style={styles.containerTextHeadlineInfo}>
          <Text style={styles.textHeadlineInfoRed}>
            {dataNewsDetail.category ? dataNewsDetail.category : 'RSS NEWS'}
          </Text>
          <DividerVertical />
          <Text style={styles.textHeadlineInfo}>
            {moment(dataNewsDetail.publish_time).format('MMM DD, YYYY')}
          </Text>
        </View>
        <SizedBox height={32} />
        {isHTML(dataNewsDetail?.body) ? (
          <WebDisplay html={dataNewsDetail.body} tagsStyles={tagsStylesBody} />
        ) : (
          <Text style={styles.textHeadlineInfo}>{dataNewsDetail?.body}</Text>
        )}
        {/* <SizedBox height={20} />
        <PopularNews divider={true} />
        <SizedBox height={20} /> */}
      </>
    );
  };

  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} hiddenSearch={true} />
      <View style={styles.containerButtonHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity style={styles.buttonListHeaderRight}>
            <Feather name={'share-2'} size={20} color={Colors.BLACK} />
          </TouchableOpacity>
          {/* <TouchableOpacity style={styles.buttonListHeaderRight}>
            <IconBookmark />
          </TouchableOpacity> */}
        </View>
      </View>
      <FlatList
        data={[]}
        ref={listRef}
        onScroll={onScroll}
        contentContainerStyle={styles.contentFlatlist}
        ListHeaderComponent={renderHeader()}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item, index}) => {
          return <View />;
        }}
      />
      <IconFloat
        bottom={1}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default NewsDetailPage;
