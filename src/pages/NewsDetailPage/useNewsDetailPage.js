import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import React from 'react';
import useGetNewsDetail from '../../api/useGetNewsDetail';
import useGetNewsDetailRSS from '../../api/useGetNewsDetailRSS';

const useNewsDetailPage = params => {
  const tabHeight = useBottomTabBarHeight();
  const {dataNewsDetail, isLoading} = useGetNewsDetail(params.id);

  return {
    tabHeight,
    dataNewsDetail,
    isLoading,
  };
};

export default useNewsDetailPage;
