import {ActivityIndicator} from '@ant-design/react-native';
import {Layout, Radio, RadioGroup} from '@ui-kitten/components';
import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {IconBack, IconChevLeft, IconChevRight} from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import WebDisplay from '../../components/WebDisplay';
import {arrayContainsEmptyString, Colors} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import usePreliminary from './usePreliminary';
import LoadingSpinner from '../../components/LoadingSpinner';

const Preliminary = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const styles = styling(insets);
  const {
    dataExercise,
    selectedIndex,
    indexSoal,
    handleChooseRadioAnswer,
    handleButtonLeft,
    handleButtonRight,
    handleSubmitLevelUp,
    translate,
    authMethod,
    isLoadingPostSubmit,
  } = usePreliminary(styles);
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <SizedBox height={25} />
      {authMethod.user.is_placement_test_taken && (
        <>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.positionButton}>
            <IconBack />
          </TouchableOpacity>
          <SizedBox height={21} />
        </>
      )}
      <View style={styles.containerTop}>
        <View>
          <Text style={styles.textTitle}>{translate('PlacementTest')}</Text>
          <View style={{height: 4}} />
          <Text style={styles.textSubTitle}>
            {translate('Desc_PlacementTest')}
          </Text>
        </View>
        {/* <View style={styles.containerTimer}>
          <IconTimer />
          <SizedBox width={8} />
          {timer > 0 ? (
            <MyTimer
              expiryTimestamp={timer}
              handleTimeCountDown={handleTimeCountDown}
            />
          ) : (
            <LoadingSpinner />
          )}
        </View> */}
      </View>
      <SizedBox height={10} />
      {dataExercise.length > 0 ? (
        <View style={styles.containerExercise}>
          <WebDisplay
            html={dataExercise[indexSoal]?.question}
            styleText={styles.textRadio}
          />
          <SizedBox height={16} />
          <Layout style={styles.layoutRadio} level="1">
            <RadioGroup
              style={styles.containerRadio}
              selectedIndex={selectedIndex}
              onChange={index => {
                handleChooseRadioAnswer(index, indexSoal, dataExercise);
              }}>
              {dataExercise[indexSoal]?.options.map((val, idx) => (
                <Radio key={idx}>
                  {evaProps => (
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        alignItems: 'center',
                        paddingRight: 5,
                      }}>
                      <WebDisplay
                        html={val.value}
                        // isOneLine={true}
                        styleText={styles.textRadio}
                      />
                    </View>
                  )}
                </Radio>
              ))}
            </RadioGroup>
          </Layout>
        </View>
      ) : (
        <LoadingSpinner />
      )}
      <View style={styles.containerButton}>
        {indexSoal > 0 && (
          <TouchableOpacity
            onPress={handleButtonLeft}
            style={styles.buttonLeft}>
            <IconChevLeft />
          </TouchableOpacity>
        )}
        {indexSoal === dataExercise.length - 1 ? (
          <>
            {arrayContainsEmptyString(dataExercise) ? (
              <TouchableOpacity disabled style={styles.buttonMidGray}>
                <Text style={styles.textSubmitGray}>{translate('Submit')}</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                disabled={isLoadingPostSubmit}
                onPress={() => handleSubmitLevelUp()}
                style={styles.buttonMid}>
                {isLoadingPostSubmit && (
                  <ActivityIndicator
                    color={Colors.PRIMARY}
                    styles={{marginRight: 5}}
                  />
                )}
                <Text style={styles.textSubmit}>{translate('Submit')}</Text>
              </TouchableOpacity>
            )}
          </>
        ) : (
          <View style={styles.containerButtonMidDropdown}>
            <Text style={styles.textSubmit}>{indexSoal + 1}</Text>
          </View>
        )}
        {indexSoal < dataExercise.length - 1 && (
          <TouchableOpacity
            onPress={handleButtonRight}
            style={styles.buttonRight}>
            <IconChevRight />
          </TouchableOpacity>
        )}
      </View>
      <BottomTabOnlyView hiddenNavigate={true} />
    </ScrollView>
  );
};

export default Preliminary;
