import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {Text, View} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import Octicons from 'react-native-vector-icons/Octicons';
import {useSelector} from 'react-redux';
import usePostSubmitQuizLevelUp from '../../api/usePostSubmitQuizLevelUp';
import {Colors, DASHBOARD_HOME} from '../../utility';
import ModalCustom from '../../widgets/ModalCustom';
import useGetQuizPlacement from '../../api/useGetQuizPlacement';

const usePreliminary = (styles) => {
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const {
    dataQuizLevelUp,
    isLoading: isLoadingApi,
    accessQuiz,
  } = useGetQuizPlacement();
  const navigation = useNavigation();
  const [dataExercise, setDataExercise] = React.useState([]);
  const [selectedIndex, setSelectedIndex] = React.useState('');
  const [indexSoal, setIndexSoal] = React.useState(0);

  const {
    dataSubmitQuiz,
    setDataSubmitQuiz,
    isLoading: isLoadingPostSubmit,
    accessSubmit,
  } = usePostSubmitQuizLevelUp();

  React.useEffect(() => {
    accessQuiz();
  }, []);

  const hideModalSukses = () => {
    magicModal.hide();
    setDataSubmitQuiz('');
    navigation.navigate(DASHBOARD_HOME);
  };

  const handleSubmitLevelUp = () => {
    if(isLoadingPostSubmit){
      return
    }
    let answer = {};
    dataExercise.map(item => {
      answer[item.id] = item.answer;
    });
    accessSubmit(dataQuizLevelUp.id, answer);
  };

  React.useEffect(() => {
    if (dataSubmitQuiz) {
      magicModal.show(() => (
        <ModalCustom
          hideModalSukses={hideModalSukses}
          iconModal={
            <View style={styles.containerModalIcon}>
              <Octicons
                name={'check-circle-fill'}
                size={40}
                color={Colors.GREEN}
              />
            </View>
          }
          singleButton={true}
          textButton={'OK'}
          textModalJudul={translate('LabelResultTest')}
          textCustom={
            <View style={{alignItems: 'center', marginTop: 24, gap: 5}}>
              <Text style={styles.textModalRed}>
                {translate('DescResultPlacement')}
              </Text>
            </View>
          }
          containerButtonSingle={styles.containerButtonSingle}
        />
      ));
    }
  }, [dataSubmitQuiz]);

  const handleChooseRadioAnswer = React.useCallback(
    (idx, indexSoal, dataExercise) => {
      dataExercise[indexSoal]?.options?.map((key, ind) => {
        if (idx == ind) {
          dataExercise[indexSoal].answer = key.option;
          setSelectedIndex(idx);
        }
      });
    },
    [],
  );

  const handleButtonLeft = () => {
    setSelectedIndex('');
    let resulIndex = indexSoal === 0 ? indexSoal : indexSoal - 1;
    setIndexSoal(resulIndex);
    dataExercise[resulIndex]?.options?.map((key, ind) => {
      if (key.option == dataExercise[resulIndex].answer) {
        setSelectedIndex(ind);
      }
    });
  };

  const handleButtonRight = () => {
    setSelectedIndex('');
    let resulIndex =
      indexSoal === dataExercise.length - 1 ? indexSoal : indexSoal + 1;
    setIndexSoal(resulIndex);
    dataExercise[resulIndex]?.options?.map((key, ind) => {
      if (key.option == dataExercise[resulIndex].answer) {
        setSelectedIndex(ind);
      }
    });
  };

  React.useEffect(() => {
    if (dataQuizLevelUp) {
      let dataTemp = dataQuizLevelUp.questions.map(item => {
        item.answer = '';
        return item;
      });
      setDataExercise(dataTemp);
    }
  }, [dataQuizLevelUp]);

  return {
    dataExercise,
    selectedIndex,
    indexSoal,
    handleChooseRadioAnswer,
    handleButtonLeft,
    handleButtonRight,
    handleSubmitLevelUp,
    translate,
    authMethod,
    isLoadingPostSubmit,
  };
};

export default usePreliminary;
