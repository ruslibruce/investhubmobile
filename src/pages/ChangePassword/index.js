import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {iconLogo} from '../../assets/images';
import {IconBack, IconPasswordLogin} from '../../assets/svg';
import TextYourInfo from '../../components/TextYourInfo';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useChangePassword from './useChangePassword';
import {Field, Formik} from 'formik';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const ChangePassword = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    handleSetNewPassword,
    translate,
    changePassValidationSchema,
    isLoading,
  } = useChangePassword();
  const styles = styling(insets);
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.positionTopButton}>
        <IconBack />
      </TouchableOpacity>
      <SizedBox height={20} />
      <View style={styles.containerImage}>
        <Image source={iconLogo} style={styles.widthImage} />
      </View>
      <SizedBox height={48} />
      <Text style={styles.textTitle}>
        {translate('ForgotPasswordForm_Title')}
      </Text>
      <SizedBox height={4} />
      <Text style={styles.textSubTitle}>
        {translate('ForgotPasswordForm_Desc')}
      </Text>
      <SizedBox height={16} />
      <Formik
        validationSchema={changePassValidationSchema}
        initialValues={{
          old_password: '',
          new_password: '',
          new_password_confirmation: '',
        }}
        onSubmit={values => handleSetNewPassword(values)}>
        {({handleSubmit, isValid}) => (
          <>
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelOldPassword')}
              placeholder={translate('EnterOldPassword')}
              icon={<IconPasswordLogin width={20} />}
              autoCapitalize="none"
              name={'old_password'}
              secureTextEntry={true}
            />
            <SizedBox height={16} />
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelNewPassword')}
              placeholder={translate('EnterNewPassword')}
              icon={<IconPasswordLogin width={20} />}
              autoCapitalize="none"
              name={'new_password'}
              secureTextEntry={true}
            />
            <SizedBox height={16} />
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelConfirmPassword')}
              placeholder={translate('EnterConfirmPassword')}
              icon={<IconPasswordLogin width={20} />}
              autoCapitalize="none"
              name={'new_password_confirmation'}
              secureTextEntry={true}
            />
            <SizedBox height={16} />
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading || !isValid}
              onPress={handleSubmit}
              label={translate('Submit')}
            />
          </>
        )}
      </Formik>
      <SizedBox height={16} />
      <TextYourInfo />
    </ScrollView>
  );
};

export default ChangePassword;
