import React, {useCallback} from 'react';
import {useTranslation} from 'react-i18next';
import * as yup from 'yup';
import YupPassword from 'yup-password';
import usePostSetNewPassword from '../../api/usePostSetNewPassword';
YupPassword(yup); // extend yup with password method

const useChangePassword = () => {
  const {t: translate} = useTranslation();
  const {accessNewPassword, isLoading} = usePostSetNewPassword();

  const handleSetNewPassword = useCallback(async value => {
    accessNewPassword(value);
  }, []);

  const changePassValidationSchema = React.useCallback(
    yup.object().shape({
      old_password: yup
        .string()
        .min(
          8,
          ({min}) =>
            `${translate('PasswordLength')} ${min} ${translate('Characters')}`,
        )
        .required(translate('RequireOldPassword')),
      new_password: yup
        .string()
        .min(
          8,
          ({min}) =>
            `${translate('PasswordLength')} ${min} ${translate('Characters')}`,
        )
        .required(translate('RequirePassword'))
        .minLowercase(1, translate('Lower_Password'))
        .minUppercase(1, translate('Upper_Password'))
        .minNumbers(1, translate('Number_Password'))
        .minWords(1, translate('Word_Password'))
        .minSymbols(1, translate("Symbol_Password")),
      new_password_confirmation: yup
        .string()
        .oneOf([yup.ref('new_password')], translate('Match_Password'))
        .required(translate('Confirm_Password')),
    }),
    [],
  );

  return {
    handleSetNewPassword,
    translate,
    changePassValidationSchema,
    isLoading,
  };
};

export default useChangePassword;
