import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import useGetDisclaimer from '../../api/useGetDisclaimer';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import {Colors} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import NoData from '../../components/NoData';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import IconFloat from '../../widgets/IconFloat';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import WebDisplay from '../../components/WebDisplay';

const Disclaimer = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const tabHeight = useBottomTabBarHeight();
  const {isLoading, dataDisclaimer} = useGetDisclaimer();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} hiddenSearch={true} />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>{dataDisclaimer?.title}</Text>
        <View />
      </View>
      <SizedBox height={60} />
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <ScrollView
          ref={listRef}
          onScroll={onScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.contentScroll}>
          {dataDisclaimer ? (
            <WebDisplay html={dataDisclaimer?.body} />
          ) : (
            <NoData />
          )}
        </ScrollView>
      )}
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default Disclaimer;
