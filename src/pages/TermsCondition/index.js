import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import useGetTermsCondition from '../../api/useGetTermsCondition';
import {iconLogo} from '../../assets/images';
import LoadingSpinner from '../../components/LoadingSpinner';
import {Colors, REGISTER} from '../../utility';
import Divider from '../../widgets/Divider';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import WebDisplay from '../../components/WebDisplay';
import NoData from '../../components/NoData';

const TermsCondition = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const tabHeight = useBottomTabBarHeight();
  const {isLoading, dataTermsCondition} = useGetTermsCondition();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.navigate(REGISTER)}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <View style={styles.containerLogo}>
          <Image source={iconLogo} style={styles.widthImage} />
        </View>
        <SizedBox height={10} />
        <Divider />
        <SizedBox height={16} />
        <Text style={styles.textHeader}>{dataTermsCondition?.title}</Text>
      </View>
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <ScrollView
          onScroll={onScroll}
          scrollEventThrottle={16}
          ref={listRef}
          contentContainerStyle={styles.contentScroll}>
          {dataTermsCondition ? (
            <WebDisplay html={dataTermsCondition?.body} />
          ) : (
            <NoData />
          )}
          {/* <SizedBox height={18} />
          <Text style={styles.textContentSub}>
            {`Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.`}
          </Text>
          <SizedBox height={10} /> */}
        </ScrollView>
      )}
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default TermsCondition;
