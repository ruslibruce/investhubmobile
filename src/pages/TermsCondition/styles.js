import {Platform, StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    containerHeader: {
      paddingTop: insets.top,
      paddingBottom: 10,
      width: '100%',
      paddingHorizontal: 20,
    },
    buttonListHeader: {
      justifyContent: 'center',
    },
    containerLogo: {
      width: 103,
      height: 36,
      alignSelf: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.BLACK,
      lineHeight: 20,
    },
    contentScroll: {
      paddingHorizontal: 20,
      paddingBottom: tabHeight + 20,
    },
    textContentTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
      lineHeight: 27,
    },
    textContentSub: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#696F79',
      lineHeight: 16,
      textAlign: 'justify',
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    containerImage: {
      width: '100%',
      height: 153,
      maxHeight: 153,
    },
  });
