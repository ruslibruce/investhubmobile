import React from 'react';
import {
  FlatList,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CarouselTop from '../../components/CarouselTop';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import {Colors, DASHBOARD_HOME, handleOpenLink} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useInvestmentMainPage from './useInvestmentMainPage';

const InvestmentMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {authMethod, tabHeight, dataInvestment, isLoading, translate} =
    useInvestmentMainPage();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);

  const renderFooter = () => (
    <>
      <SizedBox height={18} />
      <CarouselTop />
    </>
  );
  return (
    <View style={styles.containerMain}>
      <StatusCustomBarRed />
      <HeaderSearch
        height={100 + insets.top}
        placeholder={'Search Investment Partner'}
      />
      <View style={styles.containerListHeader}>
        <TouchableOpacity
          onPress={() => navigation.navigate(DASHBOARD_HOME)}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textListHeader}>
          {translate('InvestmentPartner_Capital')}
        </Text>
        <SizedBox width={32} />
        {/* <TouchableOpacity style={styles.buttonListHeaderRight}>
          <MaterialCommunityIcons
            name={'dots-vertical'}
            size={24}
            color={Colors.BLACK}
          />
        </TouchableOpacity> */}
      </View>
      {isLoading ? (
        <View style={styles.listEmpty}>
          <CarouselTop />
          <LoadingSpinner isFullScreen={true} />
        </View>
      ) : (
        <FlatList
          data={dataInvestment}
          showsVerticalScrollIndicator={false}
          numColumns={2}
          columnWrapperStyle={styles.columnWrapperFlatlist}
          ref={listRef}
          onScroll={onScroll}
          contentContainerStyle={styles.contentFlatlist}
          ListHeaderComponent={() => <SizedBox height={18} />}
          // ListFooterComponent={renderFooter()}
          keyExtractor={(item, index) => `${index}`}
          ListEmptyComponent={() => <NoData />}
          renderItem={({item, index}) => (
            <View
              key={item.id}
              onPress={() => {}}
              style={styles.containerFlatlistItem}>
              <ImageBackground
                resizeMode="contain"
                source={item.cover_image ? {uri: item.cover_image} : item.image}
                style={styles.imageBackgroundStyle}
              />
              <View style={styles.containerBottomCard}>
                <Text numberOfLines={1} style={styles.textTitle}>
                  {item.name}
                </Text>
                <SizedBox height={6} />
                {/* <View style={styles.containerSubtitle}>
                    <Text style={styles.textLeftCard}>{item.code}</Text>
                    <View style={styles.dotStyling} />
                    <Text style={styles.textRightCard}>{item.type}</Text>
                  </View> */}
                <SizedBox height={6} />
                <ButtonPrimary
                  onPress={() => handleOpenLink(item.url)}
                  label={'Open Account'}
                  style={styles.buttonCard}
                  styleLabel={styles.textButtonCard}
                />
              </View>
              <SizedBox height={12} />
            </View>
          )}
        />
      )}
      <IconFloat
        listRef={listRef}
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default InvestmentMainPage;
