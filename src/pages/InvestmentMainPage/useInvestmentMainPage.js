import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useSelector } from 'react-redux';
import { useAxiosContext } from '../../contexts';
import { UrlInvestment } from '../../utility';

const useInvestmentMainPage = () => {
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const tabHeight = useBottomTabBarHeight();
  const [dataInvestment, setDataInvestment] = React.useState([]);
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(true);

  const getInvestment = async () => {
    try {
      const response = await authAxios.get(UrlInvestment);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataInvestment(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Investment Failed',
          text2: response.message ?? 'Your Access Investment Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      if (error.response) {
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: error?.response?.data ?? 'Access Investment Failed',
          text2: error ?? 'Your Access Investment Failed',
        });
      } else {
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Access Investment Failed',
          text2: error.message ?? 'Your Access Investment Failed',
        });
      }
    }
  };

  React.useEffect(() => {
    getInvestment();
  }, []);

  return {
    authMethod,
    tabHeight,
    dataInvestment,
    isLoading,
    translate,
  };
};

export default useInvestmentMainPage;
