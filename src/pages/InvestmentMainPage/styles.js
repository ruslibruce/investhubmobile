import {Platform, StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    containerMain: {
      backgroundColor: Colors.WHITE,
      flex: 1,
      position: 'relative',
    },
    contentFlatlist: {
      gap: 20,
      backgroundColor: Colors.WHITE,
      paddingBottom: Platform.OS === 'ios' ? tabHeight + 250 : tabHeight + 100,
      paddingRight: 30,
      paddingLeft: 20,
    },
    columnWrapperFlatlist: {
      justifyContent: 'space-between',
      gap: 10,
    },
    containerListHeader: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      position: 'absolute',
      top: insets.top,
      width: '100%',
      paddingHorizontal: 20,
    },
    textListHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(18),
      color: Colors.WHITE,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerFlatlistItem: {
      borderRadius: 12,
      backgroundColor: Colors.WHITE,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 6,
      },
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
      flexBasis: '48%',
      paddingTop: 10,
    },
    imageBackgroundStyle: {
      height: 90,
      flex: 1,
      marginHorizontal: 10,
      borderRadius: 10,
      overflow: 'hidden',
    },
    containerBottomCard: {
      paddingHorizontal: 12,
    },
    textTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(10),
      color: Colors.BLACK,
    },
    containerSubtitle: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 6,
    },
    dotStyling: {
      backgroundColor: Colors.GRAY,
      width: 4,
      height: 4,
      borderRadius: 4,
    },
    textLeftCard: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(8),
      color: Colors.PRIMARY,
    },
    textRightCard: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: '#696F79',
    },
    buttonCard: {
      height: 23,
      maxWidth: 85,
      borderRadius: 24,
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
    },
    textButtonCard: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(8),
      color: Colors.WHITE,
      lineHeight: 18,
    },
    listEmpty: {
      flex: 2,
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 40,
    },
  });
