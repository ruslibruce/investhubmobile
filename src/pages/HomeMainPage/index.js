import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {
  IconAvatarHome,
  IconNotification,
  IconSearchHeader,
} from '../../assets/svg';
import HeaderSearch from '../../components/HeaderSearch';
import LMS from '../../components/LMS';
import LatestEvent from '../../components/LatestEvent';
import LatestNews from '../../components/LatestNews';
import {LOGIN, NOTIFICATION, SEARCH_ALL} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useHomeMainPage from './useHomeMainPage';
import LearningJourney from '../../components/LearningJourney';
import {Badge} from '@ant-design/react-native';

const HomeMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {authMethod, tabHeight, translate, badge} = useHomeMainPage();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  console.log('authMethod', authMethod);
  const styles = styling(insets);

  const renderHeader = () => (
    <>
      <SizedBox height={10} />
      {authMethod.isLogin && (
        <>
          <LearningJourney />
          <SizedBox height={20} />
          <LMS />
          <SizedBox height={20} />
          {/* <ListClassSchedule />
          <SizedBox height={20} /> */}
        </>
      )}
      <LatestNews title={translate('LatestNews')} />
      <SizedBox height={20} />
      <LatestEvent />
      {/* <SizedBox height={20} />
      <LearningEbook /> */}
    </>
  );

  return (
    <View style={styles.containerMain}>
      <StatusCustomBarRed />
      <HeaderSearch
        height={100 + insets.top}
        showMenu={true}
        hiddenSearch={true}
      />
      <View style={styles.containerTop}>
        <View style={styles.containerImage}>
          {authMethod.isLogin ? (
            <Image
              style={styles.widthImage}
              source={{uri: authMethod.profile?.photo}}
            />
          ) : (
            <IconAvatarHome />
          )}
        </View>
        <SizedBox width={8} />
        <View style={styles.containerTextTop}>
          <Text style={styles.textTopWelcome}>
            {authMethod.isLogin
              ? translate('Welcome_Home')
              : translate('Hello')}
          </Text>
          <Text style={styles.textTopUsername}>
            {authMethod.isLogin
              ? authMethod.profile?.name
              : translate('Welcome_Home_Guest')}
          </Text>
        </View>
        <View style={styles.groupTopButton}>
          {authMethod.isLogin ? (
            <>
              <TouchableOpacity
                onPress={() => navigation.navigate(SEARCH_ALL)}
                style={styles.containerIconTop}>
                <IconSearchHeader />
              </TouchableOpacity>
              <SizedBox width={10} />
              <TouchableOpacity
                onPress={() => navigation.navigate(NOTIFICATION)}
                style={styles.containerIconTop}>
                <Badge text={badge}>
                  <IconNotification />
                </Badge>
              </TouchableOpacity>
            </>
          ) : (
            <>
              <TouchableOpacity
                onPress={() => navigation.navigate(SEARCH_ALL)}
                style={styles.containerIconTop}>
                <IconSearchHeader />
              </TouchableOpacity>
              <ButtonPrimary
                styleLabel={styles.textButtonLoginLogout}
                style={styles.buttonLoginLogout}
                label={translate('Login')}
                onPress={() => navigation.navigate(LOGIN)}
              />
            </>
          )}
        </View>
      </View>
      <FlatList
        data={[]}
        ref={listRef}
        onScroll={onScroll}
        contentContainerStyle={[
          styles.contentFlatlist,
          {paddingBottom: insets.top},
        ]}
        ListHeaderComponent={renderHeader()}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item, index}) => {
          return <View />;
        }}
      />
      <IconFloat
        bottom={1}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default HomeMainPage;
