import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import useGetNotification from '../../api/useGetNotification';

const useHomeMainPage = () => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const tabHeight = useBottomTabBarHeight();
  const {badge} = useGetNotification();

  return {
    authMethod,
    tabHeight,
    translate,
    badge,
  };
};

export default useHomeMainPage;
