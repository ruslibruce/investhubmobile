import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    containerMain: {
      backgroundColor: Colors.WHITE,
      flex: 1,
      position: 'relative',
    },
    contentFlatlist: {
      width: matrics.screenWidth,
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
    },
    containerTop: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      position: 'absolute',
      top: insets.top,
      alignSelf: 'center',
      paddingHorizontal: 20,
    },
    containerImage: {
      height: 42,
      width: 42,
      borderRadius: 50,
      overflow: 'hidden',
    },
    widthImage: {
      height: '100%',
      width: '100%',
      backgroundColor: 'pink',
    },
    containerTextTop: {
      flex: 1,
      alignItems: 'flex-start',
    },
    textTopWelcome: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.WHITE,
    },
    textTopUsername: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(16),
      color: Colors.WHITE,
    },
    groupTopButton: {
      flexDirection: 'row',
    },
    containerIconTop: {
      backgroundColor: Colors.WHITE,
      padding: 6,
      borderRadius: 24,
    },
    textButtonLoginLogout: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.PRIMARY,
    },
    buttonLoginLogout: {
      backgroundColor: Colors.WHITE,
      height: 35,
      width: 100,
      borderRadius: 16,
      paddingHorizontal: 16,
      alignItems: 'center',
      justifyContent: 'center',
      marginLeft: 8,
    },
  });
