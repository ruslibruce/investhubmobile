import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  contentScrollView: {
    alignItems: 'center',
    paddingVertical: 20,
    paddingBottom: 20,
    paddingHorizontal: 20,
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  widthImage: {
    width: 165,
    height: 50,
  },
  textTitle: {
    fontSize: getFontSize(20),
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.BLACK,
  },
  textDesc: {
    fontSize: getFontSize(12),
    fontFamily: FontType.openSansRegular400,
    color: Colors.TEXTINFO,
  },
  textEmail: {
    fontSize: getFontSize(12),
    fontFamily: FontType.openSansRegular400,
    color: Colors.REDBAR,
  },
  containerButton: {
    flexDirection: 'row',
    gap: 16,
  },
  buttonStyle: {
    maxWidth: '48%',
  },
  buttonLeft: {
    width: '100%',
    paddingHorizontal: 30,
  },
  successStyle: {
    marginTop: 10,
    borderRadius: 10,
    padding: 10,
    backgroundColor: Colors.GREEN,
  },
  textSuccess: {
    fontSize: getFontSize(12),
    fontFamily: FontType.openSansRegular400,
    color: Colors.WHITE,
  },
});
