import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import {CHANGE_EMAIL} from '../../utility';
import usePostResendEmail from '../../api/usePostResendEmail';

const useEmailConfirm = () => {
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const {accessPostResendEmail, isSuccess, isError, isLoading} =
    usePostResendEmail();
  const [timeLeft, setTimeLeft] = React.useState(300);
  const [isTimerRun, setIsTimerRun] = React.useState(false);
  const [isSuccessPopUp, setIsSuccessPopUp] = React.useState(false);
  const recaptchaRef = React.useRef(null);
  const [tokenRecaptcha, setTokenRecaptcha] = React.useState('');

  // Convert timeLeft into minutes and seconds
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;

  React.useEffect(() => {
    if (isSuccess) {
      setIsSuccessPopUp(true);
    }
  }, [isSuccess]);

  React.useEffect(() => {
    setTimeout(() => {
      setIsSuccessPopUp(false);
    }, 3000);
  }, [isSuccessPopUp]);

  React.useEffect(() => {
    if (!isTimerRun) {
      return;
    }
    // Exit early when we reach 0
    if (timeLeft === 0) {
      setIsTimerRun(false);
      setTimeLeft(300);
      return;
    }

    // Save intervalId to clear the interval when the component re-renders
    const intervalId = setInterval(() => {
      // Decrease time left by one second
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // Clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // Add timeLeft as a dependency to re-run the effect when we update it
  }, [timeLeft, isTimerRun]);

  const handleChangeEmail = React.useCallback(() => {
    navigation.navigate(CHANGE_EMAIL);
  }, []);

  const onVerify = token => {
    setTokenRecaptcha(token);
  };

  const onExpire = () => {
    setTokenRecaptcha('');
    Toast.show({
      type: 'info',
      position: 'top',
      text1: translate('Recaptcha_Expired'),
      onHide: () => setTokenRecaptcha(''),
    });
  };

  const onOpenRecaptcha = React.useCallback(() => {
    if (!tokenRecaptcha) {
      recaptchaRef.current.open();
    }
  }, [tokenRecaptcha]);

  React.useEffect(() => {
    if (isError) {
      setTokenRecaptcha('');
    }
  }, [isError]);

  React.useEffect(() => {
    if (tokenRecaptcha) {
      onClickResend();
    }
  }, [tokenRecaptcha]);

  const onClickResend = React.useCallback(() => {
    setIsTimerRun(true);
    accessPostResendEmail({
      'g-recaptcha-response': tokenRecaptcha,
    });
  }, [tokenRecaptcha]);

  return {
    handleChangeEmail,
    translate,
    authMethod,
    isTimerRun,
    minutes,
    seconds,
    isSuccessPopUp,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    isLoading,
  };
};

export default useEmailConfirm;
