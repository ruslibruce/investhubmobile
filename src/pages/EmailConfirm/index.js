import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {iconLogo} from '../../assets/images';
import {IconEmailConfirm} from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import Divider from '../../widgets/Divider';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styles} from './styles';
import useEmailConfirm from './useEmailConfirm';
import NoHaveAccToSignIn from '../../components/NoHaveAccToSignIn';
import Recaptcha from 'react-native-recaptcha-that-works';
import Config from 'react-native-config';

const EmailConfirm = ({navigation}) => {
  const {
    handleChangeEmail,
    translate,
    authMethod,
    isTimerRun,
    minutes,
    seconds,
    isSuccessPopUp,
    onOpenRecaptcha,
    onExpire,
    onVerify,
    recaptchaRef,
    isLoading,
  } = useEmailConfirm();
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <SizedBox height={20} />
      <Image source={iconLogo} style={styles.widthImage} />
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={124} />
      <IconEmailConfirm />
      <SizedBox height={32} />
      <Text style={styles.textTitle}>{translate('Title_EmailConfirm')}</Text>
      <SizedBox height={16} />
      <Text style={styles.textDesc}>
        {translate('Desc_EmailConfirm')}{' '}
        <Text style={styles.textEmail}>{authMethod?.user?.email}</Text>
      </Text>
      <SizedBox height={32} />
      <View style={styles.containerButton}>
        <View style={styles.buttonStyle}>
          <Recaptcha
            ref={recaptchaRef}
            siteKey={Config.RECAPTCHA_SITE_KEY}
            baseUrl={Config.BASE_URL}
            onVerify={onVerify}
            onExpire={onExpire}
            size="normal"
          />
          <ButtonPrimary
            loading={isLoading}
            style={styles.buttonLeft}
            label={
              isTimerRun
                ? `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`
                : 'Resend Email'
            }
            disabled={isLoading || isTimerRun}
            onPress={onOpenRecaptcha}
          />
          {isSuccessPopUp && (
            <View style={styles.successStyle}>
              <Text style={styles.textSuccess}>
                {'Email sent successfully'}
              </Text>
            </View>
          )}
        </View>
        <ButtonPrimary
          style={styles.buttonStyle}
          onPress={handleChangeEmail}
          label={'Change Email'}
        />
      </View>
      <SizedBox height={50} />
      <NoHaveAccToSignIn backToLogin={true} />
      <BottomTabOnlyView />
    </ScrollView>
  );
};

export default EmailConfirm;
