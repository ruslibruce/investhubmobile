import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {IconBack} from '../../assets/svg';
import TextYourInfo from '../../components/TextYourInfo';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import usePreferences from './usePreferences';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Colors} from '../../utility';

const Preferences = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    listPreferences,
    handlePref,
    handleChoosePref,
    translate,
    authMethod,
    checkMax3Value,
    isLoading,
  } = usePreferences();
  const styles = styling(insets);
  return (
    <SafeAreaView style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerTop}>
        <SizedBox height={25} />
        {authMethod.user.is_preference_updated && (
          <>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={styles.buttonPosition}>
              <IconBack />
            </TouchableOpacity>
            <SizedBox height={21} />
          </>
        )}
        <Text style={styles.textTitle}>{translate('Preferences')}</Text>
        <SizedBox height={4} />
        <Text style={styles.textSubTitle}>{translate('Desc_Preferences')}</Text>
        <SizedBox height={16} />
        <ScrollView contentContainerStyle={styles.contentScrollView}>
          {listPreferences.map((item, index) => {
            return (
              <TouchableOpacity
                key={item.id}
                onPress={() => handleChoosePref(item)}
                style={[
                  styles.containerItem,
                  {
                    backgroundColor: item.choosen
                      ? Colors.PRIMARY
                      : Colors.WHITE,
                  },
                ]}>
                <Text
                  style={[
                    styles.textContainer,
                    {color: item.choosen ? Colors.WHITE : Colors.PRIMARY},
                  ]}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <View style={styles.containerButton}>
          {checkMax3Value ? (
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading}
              onPress={() => handlePref(listPreferences)}
              label={
                !authMethod.user?.is_placement_test_taken
                  ? translate('Next_Capital')
                  : translate('Update')
              }
            />
          ) : (
            <ButtonPrimary
              disabled
              style={{backgroundColor: 'gray'}}
              label={
                !authMethod.user?.is_placement_test_taken
                  ? translate('Next_Capital')
                  : translate('Update')
              }
            />
          )}
          <SizedBox height={10} />
          <TextYourInfo />
        </View>
      </View>
      <BottomTabOnlyView
        hiddenNavigate={
          !authMethod.user?.is_placement_test_taken ? true : false
        }
      />
    </SafeAreaView>
  );
};

export default Preferences;
