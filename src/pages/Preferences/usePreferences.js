import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import useGetPreferences from '../../api/useGetPreferences';
import usePutPreferences from '../../api/usePutPreferences';
import { useNavigation } from '@react-navigation/native';

const usePreferences = () => {
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const [listPreferences, setListPreferences] = React.useState([]);
  const authMethod = useSelector(state => state.auth);
  const {dataPreferences} = useGetPreferences();
  const {accessPutPreferences, isLoading} = usePutPreferences();

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod.isLogin]);

  React.useEffect(() => {
    if (dataPreferences) {
      setListPreferences(dataPreferences);
    }
  }, [dataPreferences]);

  const handlePref = values => {
    let temp = values.filter(item => item.choosen);
    let data = {
      preferences: temp.map(item => item.id),
    };

    accessPutPreferences(data);
  };

  const handleChoosePref = item => {
    let temp = listPreferences.map(val => {
      if (val === item) {
        val.choosen = !val.choosen;
      }
      return val;
    });
    setListPreferences(temp);
  };

  const checkMax3Value = React.useMemo(() => {
    let temp = listPreferences.filter(item => item.choosen);
    if (temp.length >= 3) {
      return true;
    }
    return false;
  }, [listPreferences]);

  return {
    listPreferences,
    handlePref,
    handleChoosePref,
    translate,
    authMethod,
    checkMax3Value,
    isLoading,
  };
};

export default usePreferences;
