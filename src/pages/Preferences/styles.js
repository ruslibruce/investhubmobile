import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.WHITE,
    },
    containerTop: {
      paddingVertical: 20,
      flex: 1,
      alignItems: 'center',
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
    },
    buttonPosition: {
      alignSelf: 'flex-start',
    },
    textTitle: {
      fontSize: getFontSize(20),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
      alignSelf: 'flex-start',
    },
    textSubTitle: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.TEXTINFO,
      alignSelf: 'flex-start',
    },
    contentScrollView: {
      gap: 12,
      justifyContent: 'flex-start',
      backgroundColor: Colors.WHITE,
      flexDirection: 'row',
      flexWrap: 'wrap',
      paddingBottom: insets.top + 70 + 70,
    },
    containerItem: {
      paddingHorizontal: 12,
      paddingVertical: 8,
      backgroundColor: Colors.WHITE,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      borderRadius: 24,
      marginRight: 12,
    },
    textContainer: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
    },
    containerButton: {
      flex: 1,
      alignItems: 'center',
      paddingHorizontal: 20,
      justifyContent: 'center',
      position: 'absolute',
      bottom: insets.bottom + 70,
    },
  });
