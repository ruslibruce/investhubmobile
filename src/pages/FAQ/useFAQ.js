import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import React from 'react';
import {useTranslation} from 'react-i18next';
import useGetFaq from '../../api/useGetFaq';
import { paramsToString } from '../../utility';

const useFAQ = () => {
  const {t: translate} = useTranslation();
  const dataCheck = [
    {
      id: 1,
      isChecked: false,
      name: 'Course',
    },
    {
      id: 2,
      isChecked: false,
      name: 'Forum',
    },
    {
      id: 3,
      isChecked: false,
      name: 'News',
    },
    {
      id: 4,
      isChecked: false,
      name: 'Event',
    },
    {
      id: 5,
      isChecked: false,
      name: 'Level Up Test',
    },
    {
      id: 6,
      isChecked: false,
      name: 'Investment Partner',
    },
    {
      id: 7,
      isChecked: false,
      name: 'Class Schedule',
    },
  ];
  const [isFiltered, setFiltered] = React.useState(false);
  const [dataFilter, setDataFilter] = React.useState(dataCheck);
  const tabHeight = useBottomTabBarHeight();
  const {isLoading, dataFaq, accessFaq} = useGetFaq();
  const [search, setSearch] = React.useState('');

  React.useEffect(() => {
    if (search.length > 3) {
      accessFaq(paramsToString({question: search}));
    }

    if (search.length === 0) {
      accessFaq();
    }
  }, [search]);

  const handleChecked = React.useCallback(item => {
    const result = dataFilter.map(x => {
      if (x === item) {
        x.isChecked = !x.isChecked;
      }
      return x;
    });
    setDataFilter(result);
  }, []);
  return {
    isFiltered,
    setFiltered,
    dataFilter,
    handleChecked,
    tabHeight,
    translate,
    search,
    setSearch,
    isLoading,
    dataFaq,
  };
};

export default useFAQ;
