import {Platform, StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, tabHeight, value) =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    containerHeader: {
      position: 'absolute',
      top: insets.top + 10,
      zIndex: 2,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      paddingHorizontal: 20,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.WHITE,
      lineHeight: 20,
    },
    contentFlatlist: {
      paddingHorizontal: 20,
      paddingTop: 10,
      paddingBottom: Platform.OS === 'ios' ? tabHeight + 90 : tabHeight + 20,
    },
    containerPagination: {
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'center',
    },
    buttonPagination: {
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      borderRadius: 32,
      height: 32,
      width: 32,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: value ? Colors.PRIMARY : Colors.WHITE,
    },
    textButtonPagination: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      color: value ? Colors.WHITE : Colors.PRIMARY,
      lineHeight: 24,
    },
    containerLabelTextInput: {
      flexDirection: 'row',
    },
    textInput: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
    },
    textInputRed: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
    },
    containerInputText: {
      borderRadius: 12,
      height: 105,
    },
    containerButton: {
      borderRadius: 32,
    },
  });
