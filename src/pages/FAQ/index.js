import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FilterMenu from '../../components/FilterMenu';
import HeaderSearch from '../../components/HeaderSearch';
import ListItemCard from '../../components/ListItemCard';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import {Colors} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useFAQ from './useFAQ';

const FAQ = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    isFiltered,
    setFiltered,
    dataFilter,
    handleChecked,
    tabHeight,
    translate,
    search,
    setSearch,
    isLoading,
    dataFaq,
  } = useFAQ();
  const {listRef, onScroll, contentVerticalOffset} = useStateFloat();
  const styles = styling(insets, tabHeight, isFiltered);

  // const renderFooter = () => (
  //   <>
  //     <SizedBox height={20} />
  //     <View style={styles.containerPagination}>
  //       <TouchableOpacity style={styles.buttonPagination}>
  //         <IconChevLeft />
  //       </TouchableOpacity>
  //       <SizedBox width={5} />
  //       {[1, 2, 3].map((item, index) => (
  //         <>
  //           <View style={styles.buttonPagination(index === 0 && true)}>
  //             <Text style={styles.textButtonPagination(index === 0 && true)}>
  //               {item}
  //             </Text>
  //           </View>
  //           <SizedBox width={5} />
  //         </>
  //       ))}
  //       <TouchableOpacity style={styles.buttonPagination()}>
  //         <IconChevRight />
  //       </TouchableOpacity>
  //     </View>
  //     <SizedBox height={20} />
  //     <Divider />
  //     <SizedBox height={16} />
  //     <View style={styles.containerLabelTextInput}>
  //       <Text style={styles.textInput}>{translate('DescFindFAQ')}</Text>
  //       <Text style={styles.textInputRed}>{translate('DescFindFAQ2')}</Text>
  //     </View>
  //     <SizedBox height={11} />
  //     <TextInputCustom
  //       containerInputText={styles.containerInputText}
  //       multiline
  //       placeholder={translate('EnterQuestion')}
  //       placeholderTextColor={Colors.GRAYTEXT}
  //     />
  //     <SizedBox height={16} />
  //     <ButtonPrimary
  //       style={styles.containerButton}
  //       onPress={() => {
  //         console.log('click');
  //       }}
  //       label={translate('Send')}
  //     />
  //   </>
  // );
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch
        lokalSearch={true}
        search={search}
        setSearch={setSearch}
        height={100 + insets.top}
        placeholder={`${translate('FindFAQ')}`}
        hiddenSearch={true}
      />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>{translate('Faq')}</Text>
        <SizedBox width={32} />
        {/* <TouchableOpacity
          onPress={() => setFiltered(true)}
          style={styles.buttonListHeaderRight}>
          <IconFilter />
        </TouchableOpacity> */}
      </View>
      {isFiltered && (
        <FilterMenu
          dataFilter={dataFilter}
          handleChecked={handleChecked}
          setFiltered={setFiltered}
        />
      )}
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <FlatList
          data={dataFaq}
          onScroll={onScroll}
          ref={listRef}
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={item => item.id}
          renderItem={({item, index}) => <ListItemCard item={item} />}
          ListEmptyComponent={() => <NoData />}
          // ListFooterComponent={renderFooter()}
        />
      )}
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
      />
    </View>
  );
};

export default FAQ;
