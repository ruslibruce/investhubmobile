import {ProgressBar} from '@ui-kitten/components';
import React from 'react';
import {
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TouchableOpacity as TouchableOpacityGesture} from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {imageProfile} from '../../assets/images';
import {IconBookmark, IconCup} from '../../assets/svg';
import CourseContentComponent from '../../components/CourseContentComponent';
import {Colors} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarTranslucent} from '../../widgets/StatusCustomBar';
import {styles} from './styles';
import useCourseDetailProgress from './useCourseDetailProgress';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';

const CourseDetailProgress = ({navigation, route}) => {
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const {dataCourseDetail, authMethod, isLoading} = useCourseDetailProgress(
    route.params.id,
  );
  return (
    <ImageBackground style={styles.imageBackgroundStyle} source={imageProfile}>
      <StatusCustomBarTranslucent />
      <View style={styles.containerHeaderButton}>
        <TouchableOpacityGesture
          onPress={() => navigation.goBack()}
          style={styles.leftTopButton}>
          <AntDesign
            name={'arrowleft'}
            size={24}
            color={Colors.BLACK}
            onPress={() => {}}
          />
        </TouchableOpacityGesture>
        <SizedBox width={32} />
        {/* <TouchableOpacity style={styles.rightTopButton}>
          <IconBookmark />
        </TouchableOpacity> */}
      </View>
      <SizedBox height={25} />
      <View style={styles.containerTopTitle}>
        <Text style={styles.textTopTitle}>{dataCourseDetail?.title}</Text>
        <Text style={styles.textTopSubTitle}>
          {dataCourseDetail?.description}
        </Text>
      </View>
      <SizedBox height={25} />
      <View style={styles.container}>
        {isLoading ? (
          <LoadingSpinner isFullScreen={true} />
        ) : (
          <ScrollView
            ref={listRef}
            onScroll={onScroll}
            scrollEventThrottle={16}
            contentContainerStyle={styles.contentScrollView}
            showsVerticalScrollIndicator={false}>
            <View style={styles.containerTopScroll}>
              <View style={styles.containerPositionCup}>
                <IconCup />
                <SizedBox width={4} />
                <Text style={styles.textLevelCup}>
                  {dataCourseDetail?.course_level}
                </Text>
              </View>
              {/* <View style={styles.containerTime}>
              <MaterialCommunityIcons
                name={'clock-outline'}
                size={20}
                color={Colors.PRIMARY}
              />
              <SizedBox width={6} />
              <Text style={styles.textTime}>{'3 hours, 20 min'}</Text>
            </View> */}
            </View>
            {authMethod.isLogin && (
              <>
                <SizedBox height={15} />
                <Text style={styles.textProgress}>{`${
                  dataCourseDetail?.participant_progress?.progress ?? 0
                }% completed`}</Text>
                <SizedBox height={5} />
                <View style={styles.containerProgress}>
                  <ProgressBar
                    status={'success'}
                    style={styles.progressColor}
                    progress={
                      dataCourseDetail?.participant_progress
                        ? Number(
                            dataCourseDetail?.participant_progress?.progress,
                          ) / 100
                        : 0
                    }
                  />
                </View>
              </>
            )}
            <SizedBox height={15} />
            {dataCourseDetail?.sections?.length === 0 ? (
              <View style={{marginTop: 50}}>
                <NoData />
              </View>
            ) : (
              <CourseContentComponent
                id={route.params.id}
                content={dataCourseDetail}
                navigation={navigation}
                authMethod={authMethod}
              />
            )}
          </ScrollView>
        )}
      </View>
      <IconFloat
        bottom={70}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </ImageBackground>
  );
};

export default CourseDetailProgress;
