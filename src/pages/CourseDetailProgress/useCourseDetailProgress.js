import {useSelector} from 'react-redux';
import useGetCourseDetail from '../../api/useGetCourseDetail';
import useGetCourseDetailProgress from '../../api/useGetCourseDetailProgress';
import {useFocusEffect} from '@react-navigation/native';
import React from 'react';

const useCourseDetailProgress = id => {
  const authMethod = useSelector(state => state.auth);
  const {dataCourseDetail, accessCourseDetail, isLoading} = authMethod.isLogin
    ? useGetCourseDetailProgress()
    : useGetCourseDetail();

  useFocusEffect(
    React.useCallback(() => {
      accessCourseDetail(id, true);
    }, []),
  );

  return {
    dataCourseDetail,
    authMethod,
    isLoading,
  };
};

export default useCourseDetailProgress;
