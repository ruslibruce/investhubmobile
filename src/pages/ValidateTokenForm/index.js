import {Field, Formik} from 'formik';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {iconLogo} from '../../assets/images';
import {IconBack, IconToken, IconUserLogin} from '../../assets/svg';
import GroupButtonAuth from '../../components/GroupButtonAuth';
import NoHaveAccToSignIn from '../../components/NoHaveAccToSignIn';
import TextYourInfo from '../../components/TextYourInfo';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import Divider from '../../widgets/Divider';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styling} from './styles';
import useValidateTokenForm from './useValidateTokenForm';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Recaptcha from 'react-native-recaptcha-that-works';
import Config from 'react-native-config';

const ValidateTokenForm = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    tokenValidationSchema,
    translate,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    isLoading,
    isTimerRun,
    minutes,
    seconds,
  } = useValidateTokenForm();
  const styles = styling(insets);
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.buttonTopPosition}>
        <IconBack />
      </TouchableOpacity>
      <SizedBox height={20} />
      <View style={styles.containerTop}>
        <Image source={iconLogo} style={styles.widthImage} />
      </View>
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={16} />
      <Text style={styles.textTitle}>{translate('TokenForm_Title')}</Text>
      <SizedBox height={4} />
      <Text style={styles.textSubTitle}>{translate('TokenForm_Desc')}</Text>
      <SizedBox height={16} />
      <Formik
        validationSchema={tokenValidationSchema}
        initialValues={{email: '', token: ''}}
        onSubmit={values => onOpenRecaptcha(values)}>
        {({handleSubmit, isValid}) => (
          <>
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelEmail')}
              placeholder={translate('EnterEmail')}
              icon={<IconUserLogin width={20} />}
              autoCapitalize="none"
              name={'email'}
            />
            <SizedBox height={16} />
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelToken')}
              placeholder={translate('EnterToken')}
              icon={<IconToken width={20} />}
              autoCapitalize="none"
              name={'token'}
            />
            <SizedBox height={16} />
            <Recaptcha
              ref={recaptchaRef}
              siteKey={Config.RECAPTCHA_SITE_KEY}
              baseUrl={Config.BASE_URL}
              onVerify={onVerify}
              onExpire={onExpire}
              size="normal"
            />
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading || !isValid}
              onPress={handleSubmit}
              label={
                isTimerRun ? `${minutes}:${seconds}` : translate('Validate')
              }
            />
          </>
        )}
      </Formik>
      <SizedBox height={16} />
      <TextYourInfo />
      <GroupButtonAuth isLogin={true}  />
      <NoHaveAccToSignIn />
    </ScrollView>
  );
};

export default ValidateTokenForm;
