import React from 'react';
import {useTranslation} from 'react-i18next';
import * as yup from 'yup';
import usePostValidateToken from '../../api/usePostValidateToken';
import {regexEmail} from '../../utility';
import Toast from 'react-native-toast-message';

const useValidateTokenForm = () => {
  const {t: translate} = useTranslation();
  const {accessValidateToken, isError, isLoading} = usePostValidateToken();
  const recaptchaRef = React.useRef(null);
  const [tokenRecaptcha, setTokenRecaptcha] = React.useState('');
  const [timeLeft, setTimeLeft] = React.useState(120);
  const [isTimerRun, setIsTimerRun] = React.useState(false);
  const [dataForm, setDataForm] = React.useState({
    email: '',
    token: '',
  });

  // Convert timeLeft into minutes and seconds
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;

  React.useEffect(() => {
    if (!isTimerRun) {
      return;
    }
    // Exit early when we reach 0
    if (timeLeft === 0) {
      setIsTimerRun(false);
      setTimeLeft(120);
      return;
    }

    // Save intervalId to clear the interval when the component re-renders
    const intervalId = setInterval(() => {
      // Decrease time left by one second
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // Clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // Add timeLeft as a dependency to re-run the effect when we update it
  }, [timeLeft, isTimerRun]);

  const tokenValidationSchema = React.useCallback(
    yup.object().shape({
      token: yup
        .string()
        .min(
          6,
          ({min}) =>
            `${translate('TokenLength')} ${min} ${translate('Characters')}`,
        )
        .required(translate('RequireToken')),
      email: yup
        .string()
        .matches(regexEmail(), translate('ValidEmail'))
        .max(
          30,
          ({max}) =>
            `${translate('TokenLength')} ${max} ${translate('Characters')}`,
        )
        .required(translate('RequireToken')),
    }),
    [],
  );

  const onVerify = token => {
    setTokenRecaptcha(token);
  };

  const onExpire = () => {
    setTokenRecaptcha('');
    Toast.show({
      type: 'info',
      position: 'top',
      text1: translate('Recaptcha_Expired'),
      onHide: () => setTokenRecaptcha(''),
    });
  };

  const onOpenRecaptcha = React.useCallback(
    value => {
      if (!tokenRecaptcha) {
        recaptchaRef.current.open();
        setDataForm(prev => ({
          ...prev,
          ...value,
        }));
      }
    },
    [tokenRecaptcha],
  );

  React.useEffect(() => {
    if (isError) {
      setTokenRecaptcha('');
    }
  }, [isError]);

  React.useEffect(() => {
    if (tokenRecaptcha) {
      onValidate();
    }
  }, [tokenRecaptcha]);

  const onValidate = React.useCallback(() => {
    // setIsTimerRun(true);
    accessValidateToken({...dataForm, 'g-recaptcha-response': tokenRecaptcha});
  }, [dataForm, tokenRecaptcha]);

  return {
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    translate,
    tokenValidationSchema,
    isLoading,
    isTimerRun,
    minutes,
    seconds,
  };
};

export default useValidateTokenForm;
