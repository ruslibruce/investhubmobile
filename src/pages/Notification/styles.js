import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.WHITE,
      position: 'relative',
      paddingTop: insets.top,
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: 5,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
    },
    containerButtonHeaderRight: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 17.5,
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    contentFlatlist: {
      width: matrics.screenWidth,
      paddingHorizontal: 20,
      paddingBottom: tabHeight,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    marginScrollView: {
      // width: '100%',
    },
    containerScroll: {
      paddingVertical: 8,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopColor: '#E8E8E8',
      borderBottomColor: '#E8E8E8',
    },
    contentScroll: {
      paddingLeft: 20,
    },
    containerViewDateMarkAll: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginVertical: 21.5,
    },
    textDate: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
      lineHeight: 19.07,
    },
    textMarkAll: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
      lineHeight: 16.34,
    },
    containerCard: {
      borderColor: Colors.WHITE,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,
      elevation: 10,
      marginBottom: 10,
    },
    viewCard: {
      marginHorizontal: -20,
      marginVertical: -11,
      paddingVertical: 11,
      paddingHorizontal: 25,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    viewIcon: {
      width: 40,
      height: 40,
      borderRadius: 40,
      backgroundColor: Colors.PINK,
      justifyContent: 'center',
      alignItems: 'center',
    },
    containerTextNotif: {
      width: '70%',
    },
    textHeaderNotif: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      lineHeight: 16,
    },
    textHeaderNotifRead: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      lineHeight: 16,
    },
    textDateNotif: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    containerSubNotif: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 6,
    },
    textTagNotif: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(10),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    containerEmpty: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    textEmpty: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
      lineHeight: 19.07,
    },
  });
