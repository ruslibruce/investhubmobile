import {Card} from '@ui-kitten/components';
import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import {Colors, SEARCH_ALL} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useNotification from './useNotification';

const NotificationPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    handleReadNotif,
    handleReadNotifAll,
    translate,
    dataNotification,
    isLoadingPage,
    params,
    setParams,
    setIsLoadingPage,
    totalPage,
    isLoading,
  } = useNotification();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets, 150);

  const renderFooter = () => <>{isLoadingPage ? <LoadingSpinner /> : null}</>;

  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity
            style={styles.buttonListHeader}
            onPress={() => navigation.goBack()}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>{translate('Notification')}</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate(SEARCH_ALL)}
          style={styles.buttonListHeaderRight}>
          <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
      <SizedBox height={8} />
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <FlatList
          data={dataNotification}
          ref={listRef}
          onScroll={onScroll}
          ListHeaderComponent={() => (
            <View style={styles.containerViewDateMarkAll}>
              <View />
              {dataNotification.length > 0 && (
                <TouchableOpacity onPress={handleReadNotifAll}>
                  <Text style={styles.textMarkAll}>
                    {translate('Mark_All')}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          )}
          ListEmptyComponent={() => <NoData />}
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <Card
              style={[
                styles.containerCard,
                {backgroundColor: item.is_read ? Colors.WHITE : Colors.PINK},
              ]}>
              <TouchableOpacity
                onPress={() => handleReadNotif(item)}
                style={styles.viewCard}>
                {/* <View style={styles.viewIcon}>{item.icon}</View> */}
                <View style={styles.containerTextNotif}>
                  <Text
                    style={
                      item.is_read
                        ? styles.textHeaderNotifRead
                        : styles.textHeaderNotif
                    }>
                    {item.message}
                  </Text>
                  {/* <View style={styles.containerSubNotif}>
                  <Text style={styles.textDateNotif}>
                    {moment(item.created_at).fromNow()}
                  </Text>
                  <Octicons name="dot-fill" size={10} color={Colors.GRAY} />
                  <Text style={styles.textTagNotif}>{item.category}</Text>
                </View> */}
                </View>
                {/* {!item.is_read && (
                <Octicons name="dot-fill" color={Colors.GREEN} />
              )} */}
              </TouchableOpacity>
            </Card>
          )}
          onEndReached={() => {
            if (totalPage > params.page) {
              setIsLoadingPage(true);
              setParams(prev => ({
                ...prev,
                page: prev.page + 1,
              }));
            }
          }}
          ListFooterComponent={renderFooter()}
        />
      )}
      <IconFloat
        bottom={70}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default NotificationPage;
