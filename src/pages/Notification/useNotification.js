import React from 'react';
import {useTranslation} from 'react-i18next';
import {Linking} from 'react-native';
import {useSelector} from 'react-redux';
import useGetNotification from '../../api/useGetNotification';
import usePutNotification from '../../api/usePutNotification';
import i18n from '../../i18n';
import {useNavigation} from '@react-navigation/native';
import {getDeepLink} from '../../utility';

const tabItems = [
  {
    id: 1,
    name: i18n.t('All'),
    key: 'all', // Add key 'all
    isSelected: true,
  },
  {
    id: 2,
    name: i18n.t('Course'),
    key: 'course', // Add key 'course'
    isSelected: false,
  },
  {
    id: 3,
    name: i18n.t('Event'),
    key: 'event', // Add key 'event'
    isSelected: false,
  },
  {
    id: 4,
    name: i18n.t('News'),
    key: 'news', // Add key 'event'
    isSelected: false,
  },
  {
    id: 5,
    name: i18n.t('Forum'),
    key: 'forum', // Add key 'forum'
    isSelected: false,
  },
];

const useNotification = () => {
  const {t: translate} = useTranslation();
  const navigation = useNavigation();
  const authMethod = useSelector(state => state.auth);
  const {
    dataNotification,
    isLoading,
    isLoadingPage,
    params,
    setIsLoadingPage,
    setParams,
    totalPage,
  } = useGetNotification();
  const {accessReadNotif, dataResult} = usePutNotification();

  React.useEffect(() => {
    if (dataResult) {
      setParams(prev => ({
        ...prev,
        page: 1,
      }));
      return;
    }

    if (!authMethod.isLogin) {
      navigation.goBack();
      return;
    }
  }, [dataResult, authMethod]);

  const handleReadNotif = item => {
    accessReadNotif(item.id, {
      is_read: true,
    });
    console.log(`url notif`,item.url);
    if (item.url) {
      // Use a regular expression to extract the path and query string
      const regex = /https?:\/\/[^\/]+\/([^?]+)(\?.+)?/;
      const match = item.url.match(regex);
      console.log(`match`,match);
      const path = match ? match[1] + (match[2] || '') : '';
      const resultUrl = getDeepLink(path);
      console.log(`resultUrl`,resultUrl);
      Linking.openURL(resultUrl);

    }
  };

  const handleReadNotifAll = () => {
    dataNotification.map(notif => {
      accessReadNotif(notif.id, {
        is_read: true,
      });
    });
  };

  return {
    dataNotification,
    handleReadNotif,
    handleReadNotifAll,
    translate,
    isLoadingPage,
    params,
    setParams,
    setIsLoadingPage,
    totalPage,
    isLoading,
  };
};

export default useNotification;
