import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import useGetCourse from '../../api/useGetCourse';

const useCourseMainPage = () => {
  const tabHeight = useBottomTabBarHeight();
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const [isFiltered, setIsFiltered] = React.useState(false);
  const {
    isLoading,
    dataCourse,
    isLoadingPage,
    params,
    setIsLoadingPage,
    setParams,
    totalPage,
  } = useGetCourse();

  const handleGetParamsFilter = resultFilter => {
    setParams({
      ...params,
      category: resultFilter.category,
      content_type: resultFilter.content_type,
      level: resultFilter.level,
    });
  };

  return {
    tabHeight,
    translate,
    isFiltered,
    setIsFiltered,
    handleGetParamsFilter,
    authMethod,
    isLoading,
    isLoadingPage,
    params,
    setIsLoadingPage,
    setParams,
    totalPage,
    dataCourse,
  };
};

export default useCourseMainPage;
