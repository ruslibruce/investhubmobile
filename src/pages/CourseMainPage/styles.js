import {Platform, StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    container: {
      backgroundColor: Colors.WHITE,
      flex: 1,
    },
    contentFlatlist: {
      gap: 20,
      backgroundColor: Colors.WHITE,
      paddingBottom: Platform.OS === 'ios' ? tabHeight + 120 : tabHeight + 100,
      paddingTop: 20,
      paddingHorizontal: 20,
    },
    columnWrapperFlatlist: {
      justifyContent: 'space-between',
      gap: 10,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerListHeader: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      position: 'absolute',
      top: insets.top,
      width: '100%',
      paddingHorizontal: 20,
    },
    textListHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(16),
      lineHeight: 18,
      color: Colors.WHITE,
      width: '65%',
    },
    containerButtonTopHeader: {
      flexDirection: 'row',
      gap: 8,
    },
  });
