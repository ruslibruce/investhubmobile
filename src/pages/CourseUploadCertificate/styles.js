import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      backgroundColor: Colors.WHITE,
      flex: 1,
    },
    containerScroll: {
      flex: 1,
    },
    contentScrollView: {
      backgroundColor: Colors.WHITE,
      paddingBottom: 20,
      paddingHorizontal: 20,
    },
    containerHeader: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingTop: 30,
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      gap: 12,
      paddingTop: insets.top,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textTitleHeader: {
      fontSize: getFontSize(18),
      fontFamily: FontType.openSansBold700,
      color: Colors.BLACK,
    },
    textTitleCourse: {
      fontSize: getFontSize(16),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
    },
    textLabel: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.LABELINPUT,
      lineHeight: 20,
    },
    containerUpload: {
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: '#E8E8E8',
      borderRadius: 6,
      height: 72,
      borderStyle: 'dashed',
      marginTop: 4,
      gap: 8,
    },
    viewTextUpload: {
      paddingHorizontal: 8,
      paddingVertical: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.PINK,
      borderRadius: 4,
    },
    textUpload: {
      fontSize: getFontSize(10),
      fontFamily: FontType.openSansRegular400,
      color: Colors.PRIMARY,
      lineHeight: 16,
    },
    containerFile: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      position: 'relative',
    },
    errorText: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.PRIMARY,
      alignSelf: 'flex-start',
      marginTop: 10,
    },
    imageFile: {
      width: 400,
      height: 200,
    },
    buttonDelete: {
      color: 'red',
      position: 'absolute',
      top: 10,
      right: 10,
      backgroundColor: 'white',
      padding: 5,
      borderRadius: 100,
    },
  });
