import {Field, Formik} from 'formik';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {IconPickFile} from '../../assets/svg';
import PDFViewer from '../../components/PDFViewer';
import PercentProgressUpload from '../../components/PercentProgressUpload';
import {Colors, imagePORTAL} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styling} from './styles';
import useCourseUploadCertificate from './useCourseUploadCertificate';

const CourseUploadCertificate = ({navigation}) => {
  const {
    uploadSertificateValidationSchema,
    handleOpenOption,
    handleUploadSertifikat,
    translate,
    isLoadingUploadFoto,
    percentage,
    isLoadingUploadCertificate,
  } = useCourseUploadCertificate();
  const insets = useSafeAreaInsets();
  const styles = styling(insets);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => navigation.goBack()}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.PRIMARY} />
        </TouchableOpacity>
        <Text style={styles.textTitleHeader}>
          {translate('Course_Tab_Capital')}
        </Text>
      </View>
      <View style={styles.containerScroll}>
        <ScrollView contentContainerStyle={styles.contentScrollView}>
          <SizedBox height={24} />
          <Text style={styles.textTitleCourse}>{translate('Upload_Cert')}</Text>
          <SizedBox height={16} />
          <Formik
            validationSchema={uploadSertificateValidationSchema}
            initialValues={{
              provider: '',
              title: '',
              duration: 0,
              description: '',
              file: '',
            }}
            onSubmit={handleUploadSertifikat}>
            {({handleSubmit, isValid, setFieldValue, values, errors}) => (
              <>
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('CourseProvider')}
                  placeholder={translate('EnterCourseProvider')}
                  autoCapitalize="none"
                  name={'provider'}
                />
                <SizedBox height={20} />
                <Field
                  component={TextInputWithLabelFormik}
                  label={`${translate('Duration')} (${translate('Hours')})`}
                  placeholder={'20'}
                  autoCapitalize="none"
                  name={'duration'}
                  keyboardType="numeric"
                />
                <SizedBox height={20} />
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('TitleCourse')}
                  placeholder={translate('EnterTitleCourse')}
                  autoCapitalize="none"
                  name={'title'}
                />
                <SizedBox height={20} />
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('Description')}
                  placeholder={translate('EnterDescription')}
                  autoCapitalize="none"
                  name={'description'}
                />
                <SizedBox height={20} />
                <Text style={styles.textLabel}>{`${translate(
                  'Upload_Cert',
                )} (Max. 5MB)`}</Text>
                {values.file ? (
                  <View style={styles.containerFile}>
                    {values.file.includes('.pdf') ? (
                      <PDFViewer url={`${imagePORTAL}/${values.file}`} />
                    ) : (
                      <Image
                        source={{
                          uri: `${imagePORTAL}/${values.file}`,
                        }}
                        style={styles.imageFile}
                      />
                    )}
                    <TouchableOpacity
                      onPress={() => setFieldValue('file', '')}
                      style={styles.buttonDelete}>
                      <Feather
                        name="trash-2"
                        size={20}
                        color={Colors.PRIMARY}
                      />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <>
                    {isLoadingUploadFoto ? (
                      <PercentProgressUpload percentage={percentage} />
                    ) : (
                      <TouchableOpacity
                        onPress={() => handleOpenOption(setFieldValue)}
                        style={styles.containerUpload}>
                        <IconPickFile />
                        <View style={styles.viewTextUpload}>
                          <Text style={styles.textUpload}>
                            {translate('UploadFile')}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    )}
                  </>
                )}
                {errors.file && (
                  <Text style={styles.errorText}>{errors.file}</Text>
                )}
                <SizedBox height={20} />
                <ButtonPrimary
                  loading={isLoadingUploadCertificate}
                  disabled={isLoadingUploadCertificate || !isValid}
                  onPress={handleSubmit}
                  label={translate('Submit')}
                />
              </>
            )}
          </Formik>
        </ScrollView>
      </View>
    </View>
  );
};

export default CourseUploadCertificate;
