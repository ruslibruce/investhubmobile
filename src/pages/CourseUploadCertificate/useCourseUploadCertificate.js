import {useNavigation} from '@react-navigation/native';
import mime from 'mime';
import React, {useRef} from 'react';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import Toast from 'react-native-toast-message';
import * as yup from 'yup';
import useGetFileNameUploadPortal from '../../api/useGetFileNameUploadPortal';
import usePostSertifikat from '../../api/usePostSertifikat';
import {magicModal} from 'react-native-magic-modal';
import ModalCustom from '../../widgets/ModalCustom';
import {useSelector} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';

const useCourseUploadCertificate = () => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const {
    isLoading: isLoadingUploadFoto,
    accessUploadFile,
    percentage,
  } = useGetFileNameUploadPortal();
  const {accessUploadSertifikat, dataSertifikat, isLoading: isLoadingUploadCertificate} = usePostSertifikat();
  const navigation = useNavigation();
  const [typeDoc, setTypeDoc] = React.useState('');
  const methodFormik = useRef(null);

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod]);

  const uploadSertificateValidationSchema = React.useCallback(
    yup.object().shape({
      provider: yup
        .string()
        .required(translate('CourseProviderRequired')),
      title: yup
        .string()
        .required(translate('TitleRequired'))
        .max(
          50,
          ({max}) =>
            `${translate('TitleLength')} ${max} ${translate('Characters')}`,
        ),
      duration: yup
        .number()
        .required(translate('DurationRequired'))
        .max(
          100,
          ({max}) =>
            `${translate('DurationLength')} ${max} ${translate('Characters')}`,
        ),
      description: yup
        .string()
        .required(translate('DescriptionRequired'))
        .max(
          100,
          ({max}) =>
            `${translate('DescriptionLength')} ${max} ${translate(
              'Characters',
            )}`,
        ),
      file: yup.string().required(translate('FileRequired')),
    }),
    [],
  );

  const handleError = err => {
    if (isCancel(err)) {
      console.warn('cancelled');
      // User cancelled the picker, exit any dialogs or menus and move on
    } else if (isInProgress(err)) {
      console.warn(
        'multiple pickers were opened, only the last will be considered',
      );
    } else {
      throw err;
    }
  };

  const hideModalSukses = async event => {
    magicModal.hide();
    await new Promise(resolve => setTimeout(resolve, 2000));
    if (event === 'Upload Image From Gallery') {
      handleOpenPicGallery();
    } else if (event === 'Upload PDF From Gallery') {
      handleUploadFilePDF();
    } else {
      return;
    }
  };

  const handleOpenOption = setFieldValue => {
    methodFormik.current = setFieldValue;
    magicModal.show(() => (
      <ModalCustom
        hideModalSukses={hideModalSukses}
        textButton={'Upload PDF From Gallery'}
        textButton2={'Upload Image From Gallery'}
        textModalJudul={'File Upload'}
        textModalSub={'Please choose your file upload method'}
        containerButtonDouble={{
          flexDirection: 'row',
        }}
        doubleButton={true}
      />
    ));
  };

  const handleOpenPicGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
    }).then(image => {
      console.log('image', image);
      if (image?.size > 5000000) {
        Toast.show({
          type: 'info',
          position: 'bottom',
          text1: 'File is too large',
          text2: 'Please upload a file with a maximum size of 5MB',
        });
        return;
      }
      const formData = new FormData();
      if (Platform.OS == 'android') {
        formData.append('file', {
          name: image.path.split('/').pop(),
          uri: image.path,
          type: image.mime,
        });
      } else {
        formData.append('file', {
          name: image.path.split('/').pop(),
          uri: image.path.replace('file://', ''),
          type: image.mime,
        });
      }
      console.log('formData result', JSON.stringify(formData));
      handleUploadFile(formData);
    });
  };

  const handleUploadFile = async formData => {
    try {
      accessUploadFile(formData, methodFormik.current);
      magicModal.hide();
    } catch (err) {
      // see error handling
      handleError(err);
    }
  };

  const handleUploadFilePDF = async () => {
    try {
      const formData = new FormData();
      const pickerResultPDF = await DocumentPicker.pick({
        copyTo: 'cachesDirectory',
        type: [
          // DocumentPicker.types.doc,
          // DocumentPicker.types.docx,
          DocumentPicker.types.pdf,
        ],
      });
      if (pickerResultPDF[0]?.size > 5000000) {
        Toast.show({
          type: 'info',
          position: 'bottom',
          text1: 'File is too large',
          text2: 'Please upload a file with a maximum size of 5MB',
        });
        return;
      }
      if (Platform.OS == 'android') {
        formData.append('file', {
          name: pickerResultPDF[0]?.name,
          uri: pickerResultPDF[0].fileCopyUri,
          type: mime.getType(pickerResultPDF[0].fileCopyUri),
        });
      } else {
        formData.append('file', {
          name: pickerResultPDF[0]?.name,
          uri: pickerResultPDF[0].uri.replace('file://', ''),
          type: mime.getType(pickerResultPDF[0].fileCopyUri),
        });
      }
      setTypeDoc(mime.getType(pickerResultPDF[0].fileCopyUri));
      console.log('formData result', JSON.stringify(formData));
      handleUploadFile(formData);
      magicModal.hide();
    } catch (err) {
      // see error handling
      handleError(err);
    }
  };

  React.useEffect(() => {
    if (dataSertifikat.status === 'success') {
      Toast.show({
        type: 'success',
        position: 'bottom',
        text1: 'Upload Sertificate Success',
        text2: 'Your sertificate has been uploaded successfully',
        onHide: () => navigation.goBack(),
      });
    }
  }, [dataSertifikat]);

  const handleUploadSertifikat = async values => {
    accessUploadSertifikat(values);
  };

  return {
    uploadSertificateValidationSchema,
    handleOpenOption,
    handleUploadSertifikat,
    translate,
    typeDoc,
    percentage,
    isLoadingUploadFoto,
    isLoadingUploadCertificate,
  };
};

export default useCourseUploadCertificate;
