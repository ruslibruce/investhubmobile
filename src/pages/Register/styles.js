import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, red) =>
  StyleSheet.create({
    contentScrollView: {
      alignItems: 'center',
      paddingVertical: 20,
      paddingBottom: 20,
      paddingHorizontal: 20,
      backgroundColor: Colors.WHITE,
    },
    widthImage: {
      width: 165,
      height: 50,
    },
    positionButtonTop: {
      alignSelf: 'flex-start',
      marginTop: insets.top,
    },
    containerTitleRegister: {
      width: '100%',
      borderWidth: 0.5,
      borderColor: Colors.PLACEHOLDER,
      height: 1,
    },
    textTitleRegister: {
      fontSize: getFontSize(20),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
      alignSelf: 'flex-start',
    },
    textSubRegister: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.TEXTINFO,
      alignSelf: 'flex-start',
    },
    containerChecbox: {
      flexDirection: 'row',
      gap: 10,
      alignSelf: 'flex-start',
    },
    textChecbox: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansRegular400,
    },
  });
