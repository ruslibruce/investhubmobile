import {CheckBox} from '@ui-kitten/components';
import {Field, Formik} from 'formik';
import React from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Config from 'react-native-config';
import Recaptcha from 'react-native-recaptcha-that-works';
import {iconLogo} from '../../assets/images';
import {
  IconBack,
  IconFullName,
  IconPasswordLogin,
  IconUserLogin,
} from '../../assets/svg';
import GroupButtonAuth from '../../components/GroupButtonAuth';
import NoHaveAccToSignIn from '../../components/NoHaveAccToSignIn';
import TextYourInfo from '../../components/TextYourInfo';
import {
  Colors,
  DASHBOARD,
  DASHBOARD_TOOLS,
  LOGIN,
  TAB_TOOLS_TERMS_CONDITION,
} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styling} from './styles';
import useRegister from './useRegister';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const Register = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    activeChecked,
    setActiveChecked,
    registerValidationSchema,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    translate,
    isLoading,
  } = useRegister();
  const styles = styling(insets);
  return (
    <SafeAreaView>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView contentContainerStyle={styles.contentScrollView}>
          <StatusCustomBarDark />
          <TouchableOpacity
            onPress={() => navigation.navigate(LOGIN)}
            style={styles.positionButtonTop}>
            <IconBack />
          </TouchableOpacity>
          <SizedBox height={20} />
          <Image source={iconLogo} style={styles.widthImage} />
          <SizedBox height={10} />
          <View style={styles.containerTitleRegister} />
          <SizedBox height={16} />
          <Text style={styles.textTitleRegister}>
            {translate('Account_Register')}
          </Text>
          <SizedBox height={4} />
          <Text style={styles.textSubRegister}>
            {translate('Desc_Register')}
          </Text>
          <SizedBox height={16} />
          <Formik
            validationSchema={registerValidationSchema}
            initialValues={{
              name: '',
              email: '',
              password: '',
              password_confirmation: '',
            }}
            onSubmit={values => onOpenRecaptcha(values)}>
            {({handleSubmit, isValid}) => (
              <>
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('LabelFullName')}
                  placeholder={translate('EnterFullName')}
                  icon={<IconFullName width={20} />}
                  autoCapitalize="none"
                  name={'name'}
                />
                <SizedBox height={16} />
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('LabelEmail')}
                  placeholder={translate('EnterEmail')}
                  icon={<IconUserLogin width={20} />}
                  autoCapitalize="none"
                  name={'email'}
                />
                <SizedBox height={16} />
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('LabelPasswordCreate')}
                  placeholder={translate('EnterPassword')}
                  icon={<IconPasswordLogin width={20} />}
                  autoCapitalize="none"
                  name={'password'}
                  secureTextEntry={true}
                />
                <SizedBox height={16} />
                <Field
                  component={TextInputWithLabelFormik}
                  label={translate('LabelConfirmPassword')}
                  placeholder={translate('EnterConfirmPassword')}
                  icon={<IconPasswordLogin width={20} />}
                  autoCapitalize="none"
                  name={'password_confirmation'}
                  secureTextEntry={true}
                />
                <SizedBox height={26} />
                <View style={styles.containerChecbox}>
                  <CheckBox
                    checked={activeChecked}
                    onChange={nextChecked => setActiveChecked(nextChecked)}
                  />
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={[styles.textChecbox, {color: Colors.PLACEHOLDER}]}>
                      {translate('AgreeTerms')}
                    </Text>
                    <SizedBox width={5} />
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate(DASHBOARD, {
                          screen: DASHBOARD_TOOLS,
                          params: {
                            screen: TAB_TOOLS_TERMS_CONDITION,
                          },
                        })
                      }>
                      <Text
                        style={[styles.textChecbox, {color: Colors.PRIMARY}]}>
                        {translate('Terms')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <SizedBox height={16.5} />
                <Recaptcha
                  ref={recaptchaRef}
                  siteKey={Config.RECAPTCHA_SITE_KEY}
                  baseUrl={Config.BASE_URL}
                  onVerify={onVerify}
                  onExpire={onExpire}
                  size="normal"
                />
                <ButtonPrimary
                  loading={isLoading}
                  disabled={isLoading || !isValid}
                  onPress={handleSubmit}
                  label={translate('Submit_Register')}
                />
              </>
            )}
          </Formik>
          <SizedBox height={10} />
          <TextYourInfo />
          <GroupButtonAuth isLogin={false}  />
          <NoHaveAccToSignIn />
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Register;
