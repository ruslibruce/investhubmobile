import React from 'react';
import {useTranslation} from 'react-i18next';
import Toast from 'react-native-toast-message';
import * as yup from 'yup';
import YupPassword from 'yup-password';
YupPassword(yup); // extend yup with password method
import usePostRegister from '../../api/usePostRegister';
import {regexEmail} from '../../utility';

function useRegister() {
  const {t: translate} = useTranslation();
  const [dataForm, setDataForm] = React.useState({
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
  });
  const [activeChecked, setActiveChecked] = React.useState(false);
  const recaptchaRef = React.useRef(null);
  const [tokenRecaptcha, setTokenRecaptcha] = React.useState('');
  const {
    accessRegister: handleRegister,
    isError,
    isLoading,
  } = usePostRegister();

  const registerValidationSchema = React.useCallback(
    yup.object().shape({
      name: yup
        .string()
        .required(translate('RequireName'))
        .max(
          50,
          ({max}) =>
            `${translate('NameLength')} ${max} ${translate('Characters')}`,
        ),
      email: yup
        .string()
        .matches(regexEmail(), translate('ValidEmail'))
        .max(
          30,
          ({max}) =>
            `${translate('EmailLength')} ${max} ${translate('Characters')}`,
        )
        .required(translate('RequireEmail')),
      password: yup
        .string()
        .min(
          8,
          ({min}) =>
            `${translate('PasswordLength')} ${min} ${translate('Characters')}`,
        )
        .required(translate('RequirePassword'))
        .minLowercase(1, translate('Lower_Password'))
        .minUppercase(1, translate('Upper_Password'))
        .minNumbers(1, translate('Number_Password'))
        .minWords(1, translate('Word_Password'))
        .minSymbols(1, translate('Symbol_Password')),
      password_confirmation: yup
        .string()
        .oneOf([yup.ref('password')], translate('Match_Password'))
        .required(translate('Confirm_Password')),
    }),
    [],
  );

  const onVerify = token => {
    setTokenRecaptcha(token);
  };

  const onExpire = () => {
    setTokenRecaptcha('');
    Toast.show({
      type: 'info',
      position: 'top',
      text1: translate('Recaptcha_Expired'),
      onHide: () => setTokenRecaptcha(''),
    });
  };

  const onOpenRecaptcha = React.useCallback(
    value => {
      if (!tokenRecaptcha) {
        recaptchaRef.current.open();
        setDataForm(prev => ({
          ...prev,
          ...value,
        }));
      }
    },
    [tokenRecaptcha],
  );

  React.useEffect(() => {
    if (isError) {
      setTokenRecaptcha('');
    }
  }, [isError]);

  React.useEffect(() => {
    if (tokenRecaptcha) {
      onRegister();
    }
  }, [tokenRecaptcha]);

  const onRegister = React.useCallback(() => {
    handleRegister({...dataForm, 'g-recaptcha-response': tokenRecaptcha});
  }, [dataForm, tokenRecaptcha]);

  return {
    activeChecked,
    setActiveChecked,
    registerValidationSchema,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    translate,
    isLoading,
  };
}

export default useRegister;
