import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { styles } from './styles';

const useStockScreenerColoumn = () => {
  const [heightTableData, setHeightTableData] = React.useState([]);

  const elementButtonSort = (text, width) => (
    <TouchableOpacity style={styles.buttonHeaderTable}>
      <View style={{width: width}}>
        <Text style={styles.textHeaderTable}>{text}</Text>
      </View>
      <FontAwesome name="sort" size={15} />
    </TouchableOpacity>
  );

  const elementButtonSingkatan = text => (
    <Text style={styles.textBodyTableRed}>{text}</Text>
  );

  const tableHead = ['No', elementButtonSort('singkatan', 50)];

  const tableHeadSub = [
    elementButtonSort('Satuan', 40),
    elementButtonSort('Kepanjangan', 70),
    elementButtonSort('Definisi', 120),
  ];

  const tableData = [
    ['1', elementButtonSingkatan('PER')],
    ['2', elementButtonSingkatan('PBV')],
    ['3', elementButtonSingkatan('ROE')],
    ['4', elementButtonSingkatan('ROA')],
    ['5', elementButtonSingkatan('DER')],
    ['6', elementButtonSingkatan('NPM')],
    ['7', elementButtonSingkatan('Total Rev')],
    ['8', elementButtonSingkatan('Mkt Cap')],
    ['9', elementButtonSingkatan('4wk. %Pr. Change')],
    ['10', elementButtonSingkatan('13wk. %Pr. Change')],
    ['11', elementButtonSingkatan('26wk. %Pr. Change')],
    ['12', elementButtonSingkatan('52wk. %Pr. Change')],
    ['13', elementButtonSingkatan('MTD')],
    ['14', elementButtonSingkatan('YTD')],
  ];

  const tableDataSub = [
    [
      'X (kali)',
      'Price to Earnings Ratio',
      'Harga saham dibagi dengan Laba per Saham (EPS). EPS diperoleh dengan membagi trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dengan jumlah saham tercatat.',
    ],
    [
      'X (kali)',
      'Price to Book Value Ratio',
      'Harga saham dibagi dengan nilai buku (BV). BV diperoleh dengan membagi total ekuitas dengan jumlah saham tercatat.',
    ],
    [
      '%',
      'Return on Equity Ratio',
      'Trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dibagi dengan total ekuitas.',
    ],
    [
      '%',
      'Return on Asset Ratio',
      'Trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dibagi dengan total aset.',
    ],
    [
      'X',
      'Debt to Equity Ratio',
      '	Total liabilitas dibagi dengan total ekuitas.',
    ],
    [
      '%',
      'Net Profit Margin Ratio',
      'Trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dibagi dengan total pendapatan.',
    ],
    ['Rp', 'Total Revenue', 'Total pendapatan dari perusahaan.'],
    [
      'Rp',
      'Market Capitalization',
      'Harga saham dikali dengan jumlah saham tercatat.',
    ],
    [
      '%',
      '4 week Percentage Price Change',
      'Persentase perubahan harga dalam waktu 4 minggu.',
    ],
    [
      '%',
      '13 week Percentage Price Change',
      'Persentase perubahan harga dalam waktu 13 minggu.',
    ],
    [
      '%',
      '26 week Percentage Price Change',
      'Persentase perubahan harga dalam waktu 26 minggu.',
    ],
    [
      '%',
      '52 week Percentage Price Change',
      'Persentase perubahan harga dalam waktu 52 minggu.',
    ],
    [
      '%',
      'Month to Date',
      'Persentase perubahan harga sejak hari pertama bulan berjalan dengan tanggal hari bursa terakhir.',
    ],
    [
      '%',
      'Year to Date',
      'Persentase perubahan harga sejak hari pertama tahun kalender sampai dengan tanggal hari bursa terakhir.',
    ],
  ];

  React.useEffect(() => {
    let height = [];
    tableData.map(item => height.push(80));
    setHeightTableData(height);
  }, []);

  return {
    tableHead,
    tableHeadSub,
    tableData,
    tableDataSub,
    heightTableData,
  };
};

export default useStockScreenerColoumn;
