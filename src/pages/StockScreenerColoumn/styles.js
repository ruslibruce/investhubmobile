import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    containerHeader: {
      position: 'absolute',
      top: insets.top,
      zIndex: 2,
      justifyContent: 'flex-start',
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      paddingHorizontal: 20,
      gap: 14,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.WHITE,
      lineHeight: 20,
    },
    containerScroll: {
      paddingHorizontal: 20,
      paddingTop: 20,
      flex: 1,
      paddingBottom: tabHeight,
    },
    containerTable: {
      flexDirection: 'row',
    },
    headerRowTable: {
      height: 40,
      backgroundColor: Colors.PRIMARY,
    },
    styleRowBodyLeft: {
      borderBottomWidth: 0.5,
      borderLeftWidth: 0.5,
      borderColor: '#E8E8E8',
      paddingHorizontal: 5,
    },
    styleRowBodyRight: {
      borderBottomWidth: 0.5,
      borderRightWidth: 0.5,
      borderColor: '#E8E8E8',
      paddingHorizontal: 5,
    },
    textHeaderTable: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(8),
      color: Colors.WHITE,
      textAlign: 'center',
    },
    buttonHeaderTable: {
      flexDirection: 'row',
      gap: 5,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textBodyTableLeft: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: Colors.BLACK,
      textAlign: 'center',
    },
    textBodyTableRight: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: Colors.BLACK,
      textAlign: 'left',
    },
    textBodyTableRed: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: Colors.PRIMARY,
      textAlign: 'center',
    },
  });
