import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Row, Rows, Table, TableWrapper} from 'react-native-reanimated-table';
import AntDesign from 'react-native-vector-icons/AntDesign';
import HeaderSearch from '../../components/HeaderSearch';
import {Colors} from '../../utility';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useStockScreenerColoumn from './useStockScreenerColoumn';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const StockScreenerColoumn = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {tableHead, tableHeadSub, tableData, tableDataSub, heightTableData} =
    useStockScreenerColoumn();
  const styles = styling(insets, 80);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch
        height={100 + insets.top}
        placeholder={'Search Coloumn Explanation'}
      />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>Coloumn Expalanation</Text>
      </View>
      <View style={styles.containerScroll}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.containerTable}>
            <Table>
              <Row
                data={tableHead}
                widthArr={[25, 80]}
                style={styles.headerRowTable}
                textStyle={styles.textHeaderTable}
              />
              <TableWrapper>
                <Rows
                  data={tableData}
                  heightArr={heightTableData}
                  widthArr={[25, 80]}
                  style={styles.styleRowBodyLeft}
                  textStyle={styles.textBodyTableLeft}
                />
              </TableWrapper>
            </Table>
            <ScrollView style={{marginLeft: -1}} horizontal>
              <Table>
                <Row
                  data={tableHeadSub}
                  widthArr={[70, 100, 170]}
                  style={styles.headerRowTable}
                  textStyle={styles.textHeaderTable}
                />
                <TableWrapper>
                  <Rows
                    data={tableDataSub}
                    widthArr={[70, 100, 170]}
                    heightArr={heightTableData}
                    style={styles.styleRowBodyRight}
                    textStyle={styles.textBodyTableRight}
                  />
                </TableWrapper>
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default StockScreenerColoumn;
