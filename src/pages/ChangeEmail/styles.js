import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    alignItems: 'center',
    paddingVertical: 20,
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
  positionTopButton: {
    alignSelf: 'flex-start',
  },
  widthImage: {
    width: 165,
    height: 50,
  },
  containerTopTitle: {
    width: '100%',
    borderWidth: 0.5,
    borderColor: Colors.PLACEHOLDER,
    height: 1,
  },
  textTitle: {
    fontSize: getFontSize(20),
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.BLACK,
    alignSelf: 'flex-start',
  },
  textSubTitle: {
    fontSize: getFontSize(12),
    fontFamily: FontType.openSansRegular400,
    color: Colors.TEXTINFO,
    alignSelf: 'flex-start',
  },
});
