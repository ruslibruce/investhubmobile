import {Field, Formik} from 'formik';
import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {iconLogo} from '../../assets/images';
import {IconBack, IconUserLogin} from '../../assets/svg';
import TextYourInfo from '../../components/TextYourInfo';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styles} from './styles';
import useChangeEmail from './useChangeEmail';

const ChangeEmail = ({navigation}) => {
  const {handleChangeEmail, emailValidationSchema, translate, isLoading} =
    useChangeEmail();
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.positionTopButton}>
        <IconBack />
      </TouchableOpacity>
      <SizedBox height={20} />
      <Image source={iconLogo} style={styles.widthImage} />
      <SizedBox height={10} />
      <View style={styles.containerTopTitle} />
      <SizedBox height={16} />
      <Text style={styles.textTitle}>{'Change Email'}</Text>
      <SizedBox height={4} />
      <Formik
        validationSchema={emailValidationSchema}
        initialValues={{
          email: '',
        }}
        onSubmit={values => handleChangeEmail(values)}>
        {({handleSubmit, isValid}) => (
          <>
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelEmail')}
              placeholder={translate('EnterEmail')}
              icon={<IconUserLogin width={20} />}
              autoCapitalize="none"
              name={'email'}
            />
            <SizedBox height={16} />
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading || !isValid}
              onPress={handleSubmit}
              label={translate('Send')}
            />
          </>
        )}
      </Formik>
      <SizedBox height={16} />
      <TextYourInfo />
    </View>
  );
};

export default ChangeEmail;
