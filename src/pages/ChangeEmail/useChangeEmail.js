import {useNavigation} from '@react-navigation/native';
import React from 'react';
import * as yup from 'yup';
import usePostChangeEmail from '../../api/usePostChangeEmail';
import {useTranslation} from 'react-i18next';
import {regexEmail} from '../../utility';

const useChangeEmail = () => {
  const navigation = useNavigation();
  const {accessChangeEmail, isLoading} = usePostChangeEmail();
  const {t: translate} = useTranslation();

  const handleChangeEmail = values => {
    accessChangeEmail(values);
  };

  const emailValidationSchema = React.useCallback(
    yup.object().shape({
      email: yup
        .string()
        .matches(regexEmail(), translate('ValidEmail'))
        .max(
          30,
          ({max}) =>
            `${translate('EmailLength')} ${max} ${translate('Characters')}`,
        )
        .required(translate('RequireEmail')),
    }),
    [],
  );

  return {handleChangeEmail, emailValidationSchema, translate, isLoading};
};

export default useChangeEmail;
