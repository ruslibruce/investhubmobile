import moment from 'moment';
import React from 'react';
import {
  FlatList,
  Image,
  Pressable,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LoadingSpinner from '../../components/LoadingSpinner';
import NewsCard from '../../components/NewsCard';
import NoData from '../../components/NoData';
import WebDisplay from '../../components/WebDisplay';
import {Colors, SEARCH_ALL, isHTML} from '../../utility';
import DividerVertical from '../../widgets/DividerVertical';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useNewsMainPage from './useNewsMainPage';

const NewsMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    tabHeight,
    dataTabs,
    handleClickTabs,
    isLoading,
    dataHeadlines,
    handleNavigation,
    dataNews,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useNewsMainPage();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  const tagsStyles = {
    p: {
      fontSize: 16,
      fontWeight: 700,
      fontFamily: 'OpenSans-Regular',
      color: '#000000',
    },
  };

  const renderHeader = () => (
    <>
      {Object.keys(dataHeadlines).length > 0 && (
        <>
          <SizedBox height={7} />
          <Text style={styles.textHeadline}>Today Headlines</Text>
          <SizedBox height={16} />
          <Pressable
            onPress={() => handleNavigation(dataHeadlines.id, dataHeadlines)}>
            <View style={styles.containerImageHeadline}>
              <Image
                style={styles.widthImage}
                source={{uri: dataHeadlines.cover_image}}
              />
            </View>
            <SizedBox height={16} />
            <View style={styles.containerTextDescriptionHeadline}>
              {isHTML(dataHeadlines.subject) ? (
                <View
                  style={{
                    height: 65,
                    overflow: 'hidden',
                  }}>
                  <WebDisplay
                    html={dataHeadlines.subject}
                    tagsStyles={tagsStyles}
                  />
                </View>
              ) : (
                <>
                  <Text
                    numberOfLines={2}
                    style={styles.textDescriptionHeadline}>
                    {dataHeadlines.subject}
                  </Text>
                </>
              )}
            </View>
            <SizedBox height={8} />
            <View style={styles.containerTextHeadlineInfo}>
              <Text style={styles.textHeadlineInfoRed}>
                {dataHeadlines.category ? dataHeadlines.category : 'RSS NEWS'}
              </Text>
              <DividerVertical />
              <Text style={styles.textHeadlineInfo}>
                {moment(dataHeadlines.publish_time).format('MMM DD, YYYY')}
              </Text>
            </View>
          </Pressable>
        </>
      )}
      <SizedBox height={24} />
      {/* <PopularNews />
          <SizedBox height={31} /> */}
      <ScrollView
        style={styles.containerScroll}
        contentContainerStyle={styles.contentScroll}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {dataTabs.map((tab, i) => (
          <TouchableOpacity
            key={tab.id}
            onPress={() => handleClickTabs(tab)}
            style={[
              styles.containerButtonTabs,
              {backgroundColor: tab.isSelected ? Colors.PINK : Colors.WHITE},
            ]}>
            <Text
              style={[
                styles.textButtonTabs,
                {color: tab.isSelected ? Colors.PRIMARY : '#333333'},
              ]}>
              {tab.name}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
      <SizedBox height={20} />
    </>
  );

  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => navigation.goBack()}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate(SEARCH_ALL)}
          style={styles.buttonListHeaderRight}>
          <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <FlatList
          data={dataNews}
          ref={listRef}
          onScroll={onScroll}
          contentContainerStyle={styles.contentFlatlist}
          ListHeaderComponent={renderHeader()}
          ListEmptyComponent={() => <NoData />}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => {
            return <NewsCard item={item} />;
          }}
          onEndReached={() => {
            if (totalPage > params.page) {
              setIsLoadingPage(true);
              setParams(prev => ({
                ...prev,
                page: prev.page + 1,
              }));
            }
          }}
          ListFooterComponent={() =>
            isLoadingPage ? <LoadingSpinner /> : null
          }
        />
      )}
      <IconFloat
        bottom={1}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default NewsMainPage;
