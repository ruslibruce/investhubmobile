import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import useGetNews from '../../api/useGetNews';
import useGetNewsCategory from '../../api/useGetNewsCategory';
import {handleOpenLink, TAB_NEWS_DETAIL} from '../../utility';

const useNewsMainPage = () => {
  const navigation = useNavigation();
  const tabHeight = useBottomTabBarHeight();
  const {dataTabs, setDataTabs} = useGetNewsCategory();
  const {
    dataNews,
    isLoading,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useGetNews();
  const [dataHeadlines, setDataHeadlines] = React.useState({});

  React.useEffect(() => {
    if (dataNews.length > 0) {
      setDataHeadlines(dataNews[0]);
    }
  }, [dataNews]);

  const handleClickTabs = value => {
    const result = dataTabs.map(tab => {
      if (tab === value) {
        tab.isSelected = !tab.isSelected;
        setParams(prev => ({
          ...prev,
          page: 1,
          category: tab.name === 'All' ? '' : tab.id,
        }));
      } else {
        tab.isSelected = false;
      }
      return tab;
    });
    setDataTabs(prev => (prev = result));
  };

  const handleNavigation = (id, value) => {
    if (value.source_id) {
      handleOpenLink(value.link);
      return;
    }
    navigation.navigate(TAB_NEWS_DETAIL, {
      id: id,
    });
  };

  return {
    tabHeight,
    dataTabs,
    handleClickTabs,
    isLoading,
    dataHeadlines,
    handleNavigation,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
    dataNews,
  };
};

export default useNewsMainPage;
