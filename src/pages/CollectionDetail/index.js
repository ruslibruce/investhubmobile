import React from 'react';
import {
  FlatList,
  ImageBackground,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { TouchableOpacity as TouchableOpacityGesture } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import { imageProfile } from '../../assets/images';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import CourseCard from '../../components/CourseCard';
import NoData from '../../components/NoData';
import { Colors, DASHBOARD_COURSE, TAB_COURSE_MAIN } from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import { StatusCustomBarTranslucent } from '../../widgets/StatusCustomBar';
import { styles } from './styles';
import useCollectionDetail from './useCollectionDetail';

const CollectionDetailPage = ({navigation, route}) => {
  const {
    params: {item},
  } = route;
  const {dataCollectionContent} = useCollectionDetail(item.id);
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();

  const renderHeader = () => (
    <>
      <View style={styles.containerLengthCourse}>
        <Text
          style={
            styles.textLengthCourse
          }>{`${dataCollectionContent.length} Course`}</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate(DASHBOARD_COURSE, {
              screen: TAB_COURSE_MAIN,
            })
          }>
          <AntDesign color={Colors.BLACK} size={20} name="plus" />
        </TouchableOpacity>
      </View>
      <SizedBox height={15} />
    </>
  );
  return (
    <ImageBackground style={styles.imageBackgroundStyle} source={imageProfile}>
      <StatusCustomBarTranslucent />
      <View style={styles.containerHeaderButton}>
        <TouchableOpacityGesture
          onPress={() => navigation.goBack()}
          style={styles.leftTopButton}>
          <AntDesign
            name={'arrowleft'}
            size={24}
            color={Colors.BLACK}
            onPress={() => {}}
          />
        </TouchableOpacityGesture>
        <View />
      </View>
      <SizedBox height={25} />
      <View style={styles.containerTopTitle}>
        <View style={styles.containerIcon}>
          <Feather name={'bookmark'} size={20} color={Colors.PRIMARY} />
        </View>
        <Text style={styles.textTopTitle}>{item.name}</Text>
      </View>
      <SizedBox height={25} />
      <View style={styles.container}>
        <FlatList
          contentContainerStyle={styles.contentScrollView}
          showsVerticalScrollIndicator={false}
          numColumns={2}
          columnWrapperStyle={styles.columnWrapperFlatlist}
          onScroll={onScroll}
          ref={listRef}
          data={dataCollectionContent}
          keyExtractor={(item, index) => `${index}`}
          ListHeaderComponent={renderHeader()}
          ListEmptyComponent={() => <NoData />}
          ListFooterComponent={() => (
            <SizedBox height={dataCollectionContent.length > 5 ? 0 : 50} />
          )}
          renderItem={({item, index}) => (
            <CourseCard item={item} isCollection={true} />
          )}
        />
      </View>
      <IconFloat
        bottom={70}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
      />
      <BottomTabOnlyView />
    </ImageBackground>
  );
};

export default CollectionDetailPage;
