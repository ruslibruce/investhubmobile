import useGetCollectionContent from '../../api/useGetCollectionContent';

export default function useCollectionDetail(params) {
  const {dataCollectionContent} = useGetCollectionContent(params);
  return {dataCollectionContent};
}
