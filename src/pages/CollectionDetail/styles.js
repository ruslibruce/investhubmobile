import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  imageBackgroundStyle: {
    flex: 1,
    position: 'relative',
  },
  containerHeaderButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 40,
    paddingHorizontal: 20,
  },
  leftTopButton: {
    backgroundColor: Colors.WHITE,
    height: 32,
    width: 32,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightTopButton: {
    backgroundColor: Colors.WHITE,
    height: 32,
    width: 32,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerTopTitle: {
    flexDirection: 'row',
    paddingLeft: 20,
    alignItems: 'center',
    gap: 10,
  },
  textTopTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(20),
    color: Colors.WHITE,
  },
  textTopSubTitle: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.WHITE,
  },
  contentScrollView: {
    paddingHorizontal: 30,
    paddingTop: 30,
    alignItems: 'flex-start',
    paddingBottom: 30,
  },
  container: {
    backgroundColor: Colors.WHITE,
    borderTopEndRadius: 24,
    borderTopStartRadius: 24,
    flex: 1,
  },
  containerTopScroll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  containerPositionCup: {
    backgroundColor: Colors.PINK,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    maxWidth: 91,
    height: 26,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  textLengthCourse: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.PRIMARY,
    lineHeight: 16.34
  },
  containerLengthCourse: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%'
  },
  textTime: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.PRIMARY,
  },
  textProgress: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.GREEN,
  },
  containerProgress: {
    width: '100%',
  },
  progressColor: {
    backgroundColor: '#D9D9D9',
  },
  textContent: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(18),
    color: Colors.BLACK,
  },
  containerIcon: {
    borderRadius: 80,
    backgroundColor: Colors.PINK,
    padding: 10,
  },
  columnWrapperFlatlist: {
    justifyContent: 'space-between',
    gap: 10,
    marginBottom: 20,
  },
});
