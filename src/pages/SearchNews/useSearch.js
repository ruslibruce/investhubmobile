import { useNavigation } from '@react-navigation/native';
import React from 'react';
import useGetSearchNews from '../../api/useGetSearchNews';
import { DASHBOARD_NEWS, TAB_NEWS_DETAIL, handleOpenLink, paramsToString } from '../../utility';

const useSearch = () => {
  const [search, setSearch] = React.useState('');
  const {accessSearchNews, dataSearchNews, isLoading} = useGetSearchNews();
  const navigation = useNavigation();

  React.useEffect(() => {
    if (search.length > 3) {
      accessSearchNews(paramsToString({q: search}));
    }

    if (search.length === 0) {
      accessSearchNews();
    }
  }, [search]);

  const handleNavigateNewsEvent = (id, value) => {
    if (value.source_id) {
      handleOpenLink(value.link);
      return;
    }
    navigation.navigate(DASHBOARD_NEWS, {
      screen: TAB_NEWS_DETAIL,
      params: {id: id},
    });
  };

  return {
    search,
    setSearch,
    dataSearchNews,
    isLoading,
    handleNavigateNewsEvent,
  };
};

export default useSearch;
