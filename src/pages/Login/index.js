import {Field, Formik} from 'formik';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Config from 'react-native-config';
import Recaptcha from 'react-native-recaptcha-that-works';
import {iconLogo} from '../../assets/images';
import {IconPasswordLogin, IconUserLogin} from '../../assets/svg';
import GroupButtonAuth from '../../components/GroupButtonAuth';
import TextYourInfo from '../../components/TextYourInfo';
import {FORGOT_PASSWORD, REGISTER} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styles} from './styles';
import useLogin from './useLogin';

const Login = ({navigation, route}) => {
  const {
    loginValidationSchema,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    translate,
    isLoading,
  } = useLogin(route);

  return (
    <ScrollView
      contentContainerStyle={styles.contentContainerScroll}
      style={styles.containerScroll}>
      <StatusCustomBarDark />
      <Image source={iconLogo} style={styles.widthImage} />
      <SizedBox height={60} />
      <Formik
        validationSchema={loginValidationSchema}
        initialValues={{email: '', password: ''}}
        onSubmit={values => onOpenRecaptcha(values)}>
        {({handleSubmit, isValid}) => (
          <>
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelEmail')}
              placeholder={translate('EnterEmail')}
              icon={<IconUserLogin width={20} />}
              autoCapitalize="none"
              name={'email'}
            />
            <SizedBox height={20} />
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelPassword')}
              placeholder={translate('EnterPassword')}
              icon={<IconPasswordLogin width={20} />}
              autoCapitalize="none"
              name={'password'}
              secureTextEntry={true}
            />
            <SizedBox height={20} />
            <TouchableOpacity
              onPress={() => navigation.navigate(FORGOT_PASSWORD)}
              style={styles.buttonForgot}>
              <Text style={styles.textForgot}>
                {translate('ForgotPassword')}
              </Text>
            </TouchableOpacity>
            <Recaptcha
              ref={recaptchaRef}
              siteKey={Config.RECAPTCHA_SITE_KEY}
              baseUrl={Config.BASE_URL}
              onVerify={onVerify}
              onExpire={onExpire}
              size="normal"
            />
            <SizedBox height={20} />
            <ButtonPrimary
              disabled={isLoading || !isValid}
              onPress={handleSubmit}
              label={translate('Login')}
              loading={isLoading}
            />
          </>
        )}
      </Formik>
      <SizedBox height={10} />
      <TextYourInfo />
      <GroupButtonAuth isLogin={true} />
      <SizedBox height={30} />
      <View style={styles.containerSignUp}>
        <Text style={styles.textDontHaveAccount}>
          {translate('Doesnt_have_acc')}
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate(REGISTER)}>
          <Text style={styles.textSignUp}>{translate('Register')}</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Login;
