import React from 'react';
import {useTranslation} from 'react-i18next';
import Toast from 'react-native-toast-message';
import * as yup from 'yup';
import usePostLogin from '../../api/usePostLogin';
import {regexEmail} from '../../utility';
import usePostVerification from '../../api/usePostVerification';

function useLogin(route) {
  const {t: translate} = useTranslation();
  const [dataForm, setDataForm] = React.useState({
    email: '',
    password: '',
  });
  const recaptchaRef = React.useRef(null);
  const [tokenRecaptcha, setTokenRecaptcha] = React.useState('');
  const {accessLogin: handleLogin, isError, isLoading} = usePostLogin();
  const {accessVerification} = usePostVerification();

  React.useEffect(() => {
    const data = route?.params ?? {code: null};
    console.log('params', JSON.stringify(data));
    if (data.code) {
      const code = data.code;
      const queryString = Object.keys(data)
        .filter(key => key !== 'code')
        .map(key => `${key}=${data[key]}`)
        .join('&');

      const result = `${code}?${queryString}`;
      accessVerification(result);
    }

    return () => {};
  }, [route.params]);

  const loginValidationSchema = React.useCallback(
    yup.object().shape({
      email: yup
        .string()
        .matches(regexEmail(), translate('ValidEmail'))
        .max(
          30,
          ({max}) =>
            `${translate('EmailLength')} ${max} ${translate('Characters')}`,
        )
        .required(translate('RequireEmail')),
      password: yup
        .string()
        .min(
          8,
          ({min}) =>
            `${translate('PasswordLength')} ${min} ${translate('Characters')}`,
        )
        .required(translate('RequirePassword')),
    }),
    [],
  );

  const onVerify = token => {
    setTokenRecaptcha(token);
  };

  const onExpire = () => {
    setTokenRecaptcha('');
    Toast.show({
      type: 'info',
      position: 'top',
      text1: translate('Recaptcha_Expired'),
      onHide: () => setTokenRecaptcha(''),
    });
  };

  const onOpenRecaptcha = React.useCallback(
    value => {
      if (!tokenRecaptcha) {
        recaptchaRef.current.open();
        setDataForm(prev => ({
          ...prev,
          ...value,
        }));
      }
    },
    [tokenRecaptcha],
  );

  React.useEffect(() => {
    if (isError) {
      setTokenRecaptcha('');
    }
  }, [isError]);

  React.useEffect(() => {
    if (tokenRecaptcha) {
      onLogin();
    }
  }, [tokenRecaptcha]);

  const onLogin = React.useCallback(() => {
    handleLogin({...dataForm, 'g-recaptcha-response': tokenRecaptcha});
  }, [dataForm, tokenRecaptcha]);

  return {
    loginValidationSchema,
    recaptchaRef,
    onVerify,
    onExpire,
    onOpenRecaptcha,
    translate,
    isLoading,
  };
}

export default useLogin;
