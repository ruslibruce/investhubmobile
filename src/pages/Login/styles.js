import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styles = StyleSheet.create({
  containerScroll: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  widthImage: {
    width: 165,
    height: 50,
  },
  contentContainerScroll: {
    alignItems: 'center',
    paddingTop: 80,
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
  buttonForgot: {
    alignSelf: 'flex-end',
  },
  textForgot: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
    textAlign: 'right',
  },
  containerSignIn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stylingGaris: {
    width: '35%',
    borderWidth: 0.5,
    borderColor: Colors.PLACEHOLDER,
    height: 1,
  },
  textSignInWith: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PLACEHOLDER,
    paddingBottom: 6,
    marginHorizontal: 11,
  },
  containerSignUp: {
    flexDirection: 'row',
    gap: 5,
  },
  textDontHaveAccount: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.LABELINPUT,
  },
  textSignUp: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
});
