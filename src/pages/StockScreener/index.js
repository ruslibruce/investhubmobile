import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import {Row, Rows, Table, TableWrapper} from 'react-native-reanimated-table';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {
  IconChevDown,
  IconChevLeft,
  IconChevRight,
  IconChevUp,
  IconFilter,
} from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import FilterStockScreen from '../../components/FilterStockScreen';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import {Colors, STOCK_SCREENER_COLOUMN} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useStockScreener from './useStockScreener';

const StockScreener = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const styles = styling(insets, 80);
  const {
    isFiltered,
    setIsFiltered,
    tableHead,
    tableHeadSub,
    tableData,
    tableDataFilter,
    tableDataSubFilter,
    heightTableData,
    page,
    isFocus,
    setIsFocus,
    dataShowPage,
    handleShowPage,
    nextPage,
    handleNextPage,
    handlePrevPage,
    startPage,
    lastPage,
    handleGetParamsFilter,
    isLoading,
  } = useStockScreener(styles);

  const renderItemDropdown = item => {
    return (
      <View style={styles.containerDropdown}>
        <Text style={styles.textDropdown}>{item.value}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch
        height={100 + insets.top}
        placeholder={'Search Stock Screener'}
      />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>Stock Screener</Text>
        <View style={styles.containerButtonTopHeader}>
          <TouchableOpacity style={styles.buttonListHeaderRight}>
            <Feather name="upload" size={18} color={Colors.BLACK} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setIsFiltered(true)}
            style={styles.buttonListHeaderRight}>
            <IconFilter />
          </TouchableOpacity>
        </View>
      </View>
      {isFiltered && (
        <FilterStockScreen
          handleGetParamsFilter={handleGetParamsFilter}
          setIsFiltered={setIsFiltered}
        />
      )}
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <View style={styles.contentScroll}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <Table>
                <Row
                  data={tableHead}
                  widthArr={[25, 80]}
                  style={styles.headerRowTable}
                  textStyle={styles.textHeaderTable}
                />
                <TableWrapper>
                  <Rows
                    data={tableDataFilter}
                    heightArr={heightTableData}
                    widthArr={[25, 80]}
                    style={styles.styleRowBodyLeft}
                    textStyle={styles.textBodyTableLeft}
                  />
                </TableWrapper>
              </Table>
              <ScrollView
                style={{marginLeft: -1}}
                horizontal
                showsHorizontalScrollIndicator={false}>
                <Table>
                  <Row
                    data={tableHeadSub}
                    widthArr={[60, 70, 80, 80, 80, 120, 70, 70, 70, 70, 70, 80]}
                    style={styles.headerRowTable}
                    textStyle={styles.textHeaderTable}
                  />
                  <TableWrapper>
                    <Rows
                      data={tableDataSubFilter}
                      widthArr={[
                        60, 70, 80, 80, 80, 120, 70, 70, 70, 70, 70, 80,
                      ]}
                      heightArr={heightTableData}
                      style={styles.styleRowBodyRight}
                      textStyle={styles.textBodyTableRight}
                    />
                  </TableWrapper>
                </Table>
              </ScrollView>
            </View>
            <SizedBox height={12} />
            <View style={styles.containerPagination}>
              <View style={styles.containerLeftPagination}>
                <Text style={styles.textPagination}>Show</Text>
                <Dropdown
                  style={styles.containerButtonLeftPagination}
                  placeholderStyle={styles.textButtonLeftPagination}
                  selectedTextStyle={styles.textButtonLeftPagination}
                  data={dataShowPage}
                  maxHeight={300}
                  labelField="label"
                  valueField="value"
                  placeholder={page}
                  onFocus={() => setIsFocus(true)}
                  onBlur={() => setIsFocus(false)}
                  value={page}
                  dropdownPosition="top"
                  renderItem={renderItemDropdown}
                  onChange={item => {
                    handleShowPage(item);
                  }}
                  renderRightIcon={() => {
                    if (isFocus) {
                      return <IconChevUp />;
                    } else {
                      return <IconChevDown />;
                    }
                  }}
                />
              </View>
              <View style={styles.containerRightPagination}>
                <Text style={styles.textPagination}>{`${
                  Number(startPage) + 1
                } - ${
                  tableData.length < 10 ? tableData.length : lastPage
                } dari ${tableData.length}`}</Text>
                <TouchableOpacity
                  onPress={() => handlePrevPage(nextPage)}
                  disabled={page === 'All' ? true : false}
                  style={styles.containerButtonRightArrowPagination}>
                  <IconChevLeft />
                </TouchableOpacity>
                <View style={styles.containerButtonRightPagination}>
                  <Text style={styles.textButtonRightPagination}>
                    {nextPage}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => handleNextPage(nextPage)}
                  disabled={
                    page === 'All'
                      ? true
                      : Number(lastPage) === tableData.length
                      ? true
                      : false
                  }
                  style={styles.containerButtonRightArrowPagination}>
                  <IconChevRight />
                </TouchableOpacity>
              </View>
            </View>
            <SizedBox height={28} />
            <Text style={styles.textPagination}>
              An explanation of the stock screener column can be seen
            </Text>
            <SizedBox height={8} />
            <TouchableOpacity
              onPress={() => navigation.navigate(STOCK_SCREENER_COLOUMN)}
              style={styles.buttonColoumnExplanation}>
              <Text style={styles.textButtonLeftPagination}>
                Coloumn Explanation
              </Text>
            </TouchableOpacity>
          </ScrollView>
          <BottomTabOnlyView />
        </View>
      )}
    </View>
  );
};

export default StockScreener;
