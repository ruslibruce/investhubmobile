import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import useGetStockScreener from '../../api/useGetStockScreener';
import {STOCK_SCREENER_PROFILE_COMPANY, paramsToString} from '../../utility';

const useStockScreener = (styles) => {
  const navigation = useNavigation();
  const {accessStockScreener, dataStockScreener, isLoading} =
    useGetStockScreener();
  const [isFiltered, setIsFiltered] = React.useState(false);
  const [tableData, setTableData] = React.useState([]);
  const [tableDataFilter, setTableDataFilter] = React.useState([]);
  const [heightTableData, setHeightTableData] = React.useState([]);
  const [tableDataSub, setTableDataSub] = React.useState([]);
  const [tableDataSubFilter, setTableDataSubFilter] = React.useState([]);
  const [page, setPage] = React.useState('10');
  const [nextPage, setNextPage] = React.useState('1');
  const [startPage, setStartPage] = React.useState('0');
  const [lastPage, setLastPage] = React.useState('10');
  const [isFocus, setIsFocus] = React.useState(false);
  const dataShowPage = [
    {label: '10', value: '10'},
    {label: '20', value: '20'},
    {label: '30', value: '30'},
    {label: '40', value: '40'},
    {label: '50', value: '50'},
    {label: 'All', value: 'All'},
  ];

  const handleShowPage = item => {
    if (item.value === 'All') {
      if (tableDataFilter.length === tableData.length) {
        return;
      }
      setIsFocus(false);
      setPage(item.value);
      setLastPage(tableData.length);
      setStartPage('0');
      setTableDataFilter(tableData);
      setTableDataSubFilter(tableDataSub);
      return;
    }
    setIsFocus(false);
    setPage(item.value);
    setLastPage(item.value);
    setStartPage('0');
    setTableDataFilter(tableData.slice(0, Number(item.value)));
    setTableDataSubFilter(tableDataSub.slice(0, Number(item.value)));
  };

  const elementButtonSort = (text, width) => (
    <TouchableOpacity style={styles.buttonHeaderTable}>
      <View style={{width: width}}>
        <Text style={styles.textHeaderTable}>{text}</Text>
      </View>
      <FontAwesome name="sort" size={15} />
    </TouchableOpacity>
  );

  const elementButtonCompany = (text, sahamCode) => (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate(STOCK_SCREENER_PROFILE_COMPANY, {
          sahamCode: sahamCode,
        })
      }
      style={styles.buttonHeaderTable}>
      <Text style={styles.textBodyTableRed}>{text}</Text>
    </TouchableOpacity>
  );

  const tableHead = ['No', elementButtonSort('Company name', 50)];

  const tableHeadSub = [
    elementButtonSort('Kode Saham', 30),
    elementButtonSort('Sektor', 30),
    elementButtonSort('Subsektor', 50),
    elementButtonSort('Industri', 40),
    elementButtonSort('SubIndustri', 50),
    elementButtonSort('Index', 60),
    elementButtonSort('PER', 30),
    elementButtonSort('PBV', 30),
    elementButtonSort('ROE %', 30),
    elementButtonSort('ROA %', 30),
    elementButtonSort('DER', 30),
    elementButtonSort('Mkt Cap', 50),
  ];

  React.useEffect(() => {
    let dataTemp = [];
    let dataTempSub = [];
    let height = [];
    dataStockScreener.map((item, index) => {
      data = [
        (index + 1).toString(),
        elementButtonCompany(item.companyName, item.stockCode),
      ];
      dataSub = [
        item.stockCode,
        item.sector,
        item.subSector,
        item.industry,
        item.subIndustry,
        item.indexCode,
        item.per,
        item.pbv,
        item.roe,
        item.roa,
        item.der,
        item.marketCapital,
      ];
      dataTemp.push(data);
      dataTempSub.push(dataSub);
      height.push(80);
    });
    setTableData(dataTemp);
    setTableDataSub(dataTempSub);
    setHeightTableData(height);
    setTableDataFilter(dataTemp.slice(0, Number(page)));
    setTableDataSubFilter(dataTempSub.slice(0, Number(page)));
  }, [dataStockScreener]);

  const handleNextPage = value => {
    setNextPage(`${Number(value) + 1}`);
    setStartPage(`${Number(startPage) + Number(page)}`);
    setLastPage(
      Number(lastPage) + Number(page) > tableDataSub.length
        ? `${tableDataSub.length}`
        : `${Number(lastPage) + Number(page)}`,
    );
    setTableDataFilter(
      tableData.slice(
        Number(startPage) + Number(page),
        Number(lastPage) + Number(page),
      ),
    );
    setTableDataSubFilter(
      tableDataSub.slice(
        Number(startPage) + Number(page),
        Number(lastPage) + Number(page),
      ),
    );
  };

  const handlePrevPage = value => {
    if (Number(value) === 1) {
      return;
    }
    setNextPage(`${Number(value) - 1}`);
    setStartPage(`${Number(startPage) - Number(page)}`);
    setLastPage(
      Number(lastPage) === tableData.length
        ? `${Number(startPage)}`
        : `${Number(lastPage) - Number(page)}`,
    );
    setTableDataFilter(
      tableData.slice(
        Number(startPage) - Number(page),
        Number(lastPage) === tableData.length
          ? Number(startPage)
          : Number(lastPage) - Number(page),
      ),
    );
    setTableDataSubFilter(
      tableDataSub.slice(
        Number(startPage) - Number(page),
        Number(lastPage) === tableData.length
          ? Number(startPage)
          : Number(lastPage) - Number(page),
      ),
    );
  };

  const handleGetParamsFilter = category => {
    let data = paramsToString(category);
    accessStockScreener(data);
    setIsFiltered(false);
  };

  return {
    isFiltered,
    setIsFiltered,
    tableHead,
    tableHeadSub,
    tableData,
    tableDataFilter,
    tableDataSubFilter,
    heightTableData,
    page,
    isFocus,
    setIsFocus,
    dataShowPage,
    handleShowPage,
    nextPage,
    setNextPage,
    handleNextPage,
    handlePrevPage,
    startPage,
    lastPage,
    handleGetParamsFilter,
    isLoading,
  };
};

export default useStockScreener;
