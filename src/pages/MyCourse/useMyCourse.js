import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import useGetMyCourse from '../../api/useGetMyCourse';

const useMyCourse = () => {
  const tabHeight = useBottomTabBarHeight();
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const [isFiltered, setIsFiltered] = React.useState(false);
  const {
    dataCourse,
    isLoading,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useGetMyCourse();

  const handleGetParamsFilter = resultFilter => {
    setParams({
      ...params,
      category: resultFilter.category,
      content_type: resultFilter.content_type,
      level: resultFilter.level,
    });
    setIsFiltered(false);
  };

  return {
    tabHeight,
    translate,
    isFiltered,
    setIsFiltered,
    handleGetParamsFilter,
    authMethod,
    isLoading,
    isLoadingPage,
    dataCourse,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  };
};

export default useMyCourse;
