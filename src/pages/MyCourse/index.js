import React from 'react';
import {
  FlatList,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import { IconFilter } from '../../assets/svg';
import CourseCard from '../../components/CourseCard';
import FilterCourse from '../../components/FilterCourse';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import {
  Colors,
  DASHBOARD_HOME,
  TAB_COURSE_UPLOAD_CERTIFICATE,
} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import { StatusCustomBarRed } from '../../widgets/StatusCustomBar';
import { styling } from './styles';
import useMyCourse from './useMyCourse';

const MyCourseMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    tabHeight,
    translate,
    isFiltered,
    setIsFiltered,
    handleGetParamsFilter,
    authMethod,
    isLoading,
    isLoadingPage,
    dataCourse,
    params,
    setIsLoadingPage,
    setParams,
    totalPage,
  } = useMyCourse();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <React.Fragment>
        <HeaderSearch
          height={100 + insets.top}
          placeholder={translate('Search_Course')}
        />
        <View style={styles.containerListHeader}>
          <TouchableOpacity
            onPress={() => navigation.navigate(DASHBOARD_HOME)}
            style={styles.buttonListHeader}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textListHeader}>{translate('My_Course')}</Text>
          <View style={styles.containerButtonTopHeader}>
            <TouchableOpacity
              onPress={() => navigation.navigate(TAB_COURSE_UPLOAD_CERTIFICATE)}
              style={styles.buttonListHeaderRight}>
              <Feather name="upload" size={20} color={Colors.BLACK} />
            </TouchableOpacity>
            {authMethod.isLogin && (
              <TouchableOpacity
                onPress={() => setIsFiltered(true)}
                style={styles.buttonListHeaderRight}>
                <IconFilter />
              </TouchableOpacity>
            )}
          </View>
        </View>
        <SizedBox height={10} />
      </React.Fragment>
      {isFiltered && (
        <FilterCourse
          handleGetParamsFilter={handleGetParamsFilter}
          setIsFiltered={setIsFiltered}
        />
      )}
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <FlatList
          showsVerticalScrollIndicator={false}
          ref={listRef}
          onScroll={onScroll}
          numColumns={2}
          columnWrapperStyle={styles.columnWrapperFlatlist}
          data={dataCourse}
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          ListEmptyComponent={() => <NoData />}
          ListFooterComponent={() => (
            <>
              {isLoadingPage ? <LoadingSpinner /> : null}
              {/* <SizedBox height={filteredCourses.length > 5 ? 0 : 0} /> */}
            </>
          )}
          onEndReached={() => {
            if (totalPage > params.page) {
              setIsLoadingPage(true);
              setParams(prev => ({
                ...prev,
                page: prev.page + 1,
              }));
            }
          }}
          renderItem={({item, index}) => (
            <CourseCard item={item} isCourse={true} isMyCourse={true} />
          )}
        />
      )}
      <IconFloat
        bottom={1}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default MyCourseMainPage;
