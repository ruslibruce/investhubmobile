import React, {useRef} from 'react';
import {useTranslation} from 'react-i18next';
import {Platform, Text, View} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import {magicModal} from 'react-native-magic-modal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useCameraPermission} from 'react-native-vision-camera';
import {useSelector} from 'react-redux';
import * as yup from 'yup';
import useGetCountry from '../../api/useGetCountry';
import useGetFileNameUploadIAM from '../../api/useGetFileNameUploadIAM';
import usePostUpdateProfile from '../../api/usePostUpdateProfile';
import PermissionsPage from '../../components/PermissionPage';
import VisionCamera from '../../components/VisionCamera';
import {Colors, regexPhoneNumber} from '../../utility';
import ModalCustom from '../../widgets/ModalCustom';
import moment from 'moment';
import {useNavigation} from '@react-navigation/native';
import Toast from 'react-native-toast-message';

const today = new Date();

const dataInstitusi = [
  {label: 'Public', value: 1},
  {label: 'OJK', value: 2},
];

const useUpdateProfile = styles => {
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const [openCalendar, setOpenCalendar] = React.useState(false);
  const [isFocus, setIsFocus] = React.useState(false);
  const [isFocus2, setIsFocus2] = React.useState(false);
  const {accessUpdateProfile, isLoading} = usePostUpdateProfile();
  const {dataCountry} = useGetCountry();
  const {profile} = authMethod;
  const {
    isLoading: isLoadingUploadFoto,
    accessUploadFile,
    percentage,
  } = useGetFileNameUploadIAM();
  const {hasPermission} = useCameraPermission();
  const methodFormik = useRef(null);

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod.isLogin]);

  const handleUpdate = async values => {
    let dataResult = {
      name: values.name,
      date_of_birth: values.date_of_birth,
      phone: values.phone,
      gender: values.gender,
      email: values.email,
      address: values.address,
      country: values.country,
      institution_id: values.institution_id,
      photo: values?.photo,
    };
    accessUpdateProfile(dataResult);
  };

  const renderItemDropdown = item => {
    return (
      <View style={styles.renderStyle}>
        <Text style={styles.selectedStyle}>{item.label}</Text>
      </View>
    );
  };

  const profileValidationSchema = yup.object().shape({
    name: yup
      .string()
      .required(translate('RequireName'))
      .max(
        50,
        ({max}) =>
          `${translate('NameLength')} ${max} ${translate('Characters')}`,
      ),
    address: yup
      .string()
      .max(
        100,
        ({max}) =>
          `${translate('AddressLength')} ${max} ${translate('Characters')}`,
      )
      .nullable(),
    date_of_birth: yup.string().nullable(),
    phone: yup
      .string()
      .matches(regexPhoneNumber(), translate('ValidPhone'))
      .max(
        15,
        ({max}) =>
          `${translate('PhoneNumberLength')} ${max} ${translate('Characters')}`,
      )
      .nullable(),
    country: yup.string().required(translate('RequireCountry')),
  });

  const handleOpenPicGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      cropperCircleOverlay: true,
      showCropGuidelines: true,
    }).then(image => {
      console.log('image', image);
      if (image?.size > 2000000) {
        Toast.show({
          type: 'info',
          position: 'bottom',
          text1: 'File is too large',
          text2: 'Please upload a file with a maximum size of 2MB',
        });
        return;
      }
      const formData = new FormData();
      if (Platform.OS == 'android') {
        formData.append('file', {
          name: image.path.split('/').pop(),
          uri: image.path,
          type: image.mime,
        });
      } else {
        formData.append('file', {
          name: image.path.split('/').pop(),
          uri: image.path.replace('file://', ''),
          type: image.mime,
        });
      }
      console.log('formData result', JSON.stringify(formData));
      handleUploadFile(formData);
    });
  };

  const cekPermission = () => {
    if (!hasPermission)
      return magicModal.show(() => (
        <PermissionsPage
          title={translate('PermissionCamera')}
          isCameraPermission={true}
          handleAction={handleOpenCamera}
          icon={
            <MaterialCommunityIcons
              size={80}
              name="camera"
              color={Colors.PRIMARY}
            />
          }
        />
      ));

    if (hasPermission) handleOpenCamera();
  };

  const handleOpenCamera = () => {
    magicModal.show(() => (
      <VisionCamera handleUpload={handleUploadFile} isFront={true} />
    ));
  };

  const handleUploadFile = async formData => {
    try {
      accessUploadFile(formData, methodFormik.current);
      magicModal.hide();
    } catch (err) {
      // see error handling
      handleError(err);
    }
  };

  const hideModalSukses = async event => {
    magicModal.hide();
    await new Promise(resolve => setTimeout(resolve, 2000));
    if (event === 'Open Camera') {
      cekPermission();
    } else if (event === 'Upload From Gallery') {
      handleOpenPicGallery();
    } else {
      return;
    }
  };

  const handleOpenOption = setFieldValue => {
    methodFormik.current = setFieldValue;
    magicModal.show(() => (
      <ModalCustom
        hideModalSukses={hideModalSukses}
        textButton={'Upload From Gallery'}
        textButton2={'Open Camera'}
        textModalJudul={'Picture Upload'}
        textModalSub={'Please choose your picture upload method'}
        containerButtonDouble={styles.containerButtonDouble}
        doubleButton={true}
      />
    ));
  };

  const onChangeCalendar = (event, selectedDate, setFieldValue) => {
    console.log('event.type', event.type);
    Platform.OS === 'android' && setOpenCalendar(false);
    setFieldValue('date_of_birth', moment(selectedDate).format('YYYY-MM-DD'));
  };

  return {
    today,
    dataCountry,
    dataInstitusi,
    openCalendar,
    setOpenCalendar,
    isFocus,
    setIsFocus,
    isFocus2,
    setIsFocus2,
    handleUpdate,
    renderItemDropdown,
    profileValidationSchema,
    translate,
    profile,
    authMethod,
    handleOpenOption,
    onChangeCalendar,
    isLoadingUploadFoto,
    percentage,
    isLoading,
  };
};

export default useUpdateProfile;
