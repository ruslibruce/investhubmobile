import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (insets) =>
  StyleSheet.create({
    contentScroll: {
      alignItems: 'center',
      paddingVertical: 20,
      paddingHorizontal: 20,
      paddingBottom: insets.top + 70,
    },
    scroll: {
      backgroundColor: Colors.WHITE,
      flex: 1,
    },
    textTitle: {
      fontSize: getFontSize(20),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
      alignSelf: 'flex-start',
    },
    textDesc: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.TEXTINFO,
      alignSelf: 'flex-start',
    },
    textGender: {
      fontFamily: FontType.openSansBold700,
      textAlign: 'left',
      fontSize: getFontSize(14),
      color: Colors.LABELINPUT,
    },
    backgroundRadio: {
      backgroundColor: Colors.WHITE,
    },
    radioStyle: {
      backgroundColor: Colors.WHITE,
      borderRadius: 24,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 40,
    },
    textRadio: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      backgroundColor: Colors.WHITE,
      color: Colors.BLACK,
      marginLeft: 10,
    },
    textLabel: {
      fontFamily: FontType.openSansBold700,
      textAlign: 'left',
      fontSize: getFontSize(14),
      color: Colors.LABELINPUT,
    },
    dropdownStyle: {
      width: '100%',
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 15,
      paddingVertical: 10,
      borderWidth: 1,
      borderRadius: 32,
    },
    placeholderStyle: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.GRAYTEXT,
    },
    selectedStyle: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
    },
    inputStyle: {
      height: 40,
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
    },
    iconStyle: {
      width: 100,
      height: 100,
    },
    renderStyle: {
      padding: 12,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    errorText: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.PRIMARY,
      alignSelf: 'flex-start',
      marginTop: 10,
    },
    containerButtonDouble: {
      flexDirection: 'row',
    },
    containerFile: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    textUpload: {
      fontSize: getFontSize(10),
      fontFamily: FontType.openSansRegular400,
      color: Colors.PRIMARY,
      lineHeight: 16,
    },
    containerUpload: {
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: '#E8E8E8',
      borderRadius: 6,
      height: 200,
      width: matrics.screenWidth - 40,
      borderStyle: 'dashed',
      marginTop: 4,
      gap: 8,
      overflow: 'hidden',
    },
    viewTextUpload: {
      paddingHorizontal: 8,
      paddingVertical: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.PINK,
      borderRadius: 4,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    buttonTrash: {
      position: 'absolute',
      right: 5,
      top: 5,
      backgroundColor: Colors.PRIMARY,
      borderRadius: 50,
      padding: 5,
    },
    containerCalendar:{
      alignItems: 'center',
      justifyContent: 'space-between',
      borderWidth: 1,
      borderColor: '#E8E8E8',
      borderRadius: 6,
      width: matrics.screenWidth - 40,
      borderStyle: 'dashed',
      marginTop: 4,
      gap: 8,
      overflow: 'hidden',
    }
  });
