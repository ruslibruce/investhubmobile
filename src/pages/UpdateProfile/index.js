import DateTimePicker from '@react-native-community/datetimepicker';
import {Layout, Radio, RadioGroup} from '@ui-kitten/components';
import {Field, Formik} from 'formik';
import React from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Feather from 'react-native-vector-icons/Feather';
import {
  IconAddress,
  IconBack,
  IconCalendar,
  IconChevDown,
  IconChevUp,
  IconCountry,
  IconEmail,
  IconFullName,
  IconInstitution,
  IconPickFile,
  IconTelephone,
} from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import LoadingSpinner from '../../components/LoadingSpinner';
import PercentProgressUpload from '../../components/PercentProgressUpload';
import {Colors, imageIAM} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabel from '../../widgets/TextInputWithLabel';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styling} from './styles';
import useUpdateProfile from './useUpdateProfile';

const UpdateProfile = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const styles = styling(insets);
  const {
    today,
    dataCountry,
    dataInstitusi,
    openCalendar,
    setOpenCalendar,
    isFocus,
    setIsFocus,
    isFocus2,
    setIsFocus2,
    handleUpdate,
    renderItemDropdown,
    profileValidationSchema,
    translate,
    profile,
    authMethod,
    handleOpenOption,
    onChangeCalendar,
    isLoadingUploadFoto,
    percentage,
    isLoading,
  } = useUpdateProfile(styles);
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  return (
    <React.Fragment>
      <KeyboardAvoidingView
        style={styles.scroll}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView
          onScroll={onScroll}
          scrollEventThrottle={16}
          ref={listRef}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentScroll}>
          <StatusCustomBarDark />
          <SizedBox height={25} />
          {authMethod.user?.is_profile_updated && (
            <>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{alignSelf: 'flex-start', paddingTop: insets}}>
                <IconBack />
              </TouchableOpacity>
              <SizedBox height={21} />
            </>
          )}
          {!authMethod.isLogin && (
            <>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{alignSelf: 'flex-start', paddingTop: insets}}>
                <IconBack />
              </TouchableOpacity>
              <SizedBox height={21} />
            </>
          )}
          <Text style={styles.textTitle}>{translate('UpdateProfile')}</Text>
          <SizedBox height={4} />
          <Text style={styles.textDesc}>{translate('Desc_UpdateProfile')}</Text>
          <SizedBox height={16} />
          {profile ? (
            <Formik
              validationSchema={profileValidationSchema}
              initialValues={{
                name: profile?.name ? profile?.name : authMethod.user?.name,
                date_of_birth: profile?.date_of_birth,
                phone: profile?.phone,
                gender: profile?.gender,
                email: profile?.email,
                address: profile?.address,
                country: profile?.country,
                institution_id: profile?.institution_id,
                photo: profile?.photo.split('/').pop(),
              }}
              onSubmit={values => {
                handleUpdate(values);
              }}>
              {({handleSubmit, isValid, setFieldValue, values, errors}) => (
                <>
                  <Field
                    component={TextInputWithLabelFormik}
                    label={translate('LabelFullName')}
                    placeholder={translate('EnterFullName')}
                    icon={<IconFullName width={20} />}
                    autoCapitalize="none"
                    name={'name'}
                    styleProps={{width: '100%'}}
                    placeholderTextColor={Colors.GRAYTEXT}
                  />
                  <SizedBox height={16} />
                  <TextInputWithLabel
                    label={`${translate('LabelDateOfBirth')} ${translate(
                      'Optional',
                    )}`}
                    placeholder={translate('EnterDateOfBirth')}
                    icon={<IconCalendar width={20} />}
                    autoCapitalize="none"
                    styleProps={{width: '100%'}}
                    placeholderTextColor={Colors.GRAYTEXT}
                    date={true}
                    value={values.date_of_birth}
                    onChangeText={() => setOpenCalendar(true)}
                    required={false}
                  />
                  {errors.date_of_birth && (
                    <Text style={styles.errorText}>{errors.date_of_birth}</Text>
                  )}
                  {openCalendar && (
                    <View style={styles.containerCalendar}>
                      <DateTimePicker
                        testID="Date of Birth"
                        value={today}
                        mode="date"
                        maximumDate={new Date()}
                        display={Platform.OS === 'ios' ? 'spinner' : 'default'}
                        onChange={(event, selectedDate) =>
                          onChangeCalendar(event, selectedDate, setFieldValue)
                        }
                        style={{backgroundColor: Colors.WHITE}}
                        textColor={Colors.PRIMARY}
                      />
                      {Platform.OS === 'ios' && (
                        <ButtonPrimary
                          label={'OK'}
                          onPress={() => setOpenCalendar(false)}
                        />
                      )}
                    </View>
                  )}
                  <SizedBox height={16} />
                  <Field
                    component={TextInputWithLabelFormik}
                    label={`${translate('LabelPhone')} ${translate(
                      'Optional',
                    )}`}
                    placeholder={translate('EnterPhone')}
                    icon={<IconTelephone width={20} />}
                    autoCapitalize="none"
                    name={'phone'}
                    styleProps={{width: '100%'}}
                    placeholderTextColor={Colors.GRAYTEXT}
                    required={false}
                  />
                  <SizedBox height={16} />
                  <View style={{width: '100%'}}>
                    <Text style={styles.textGender}>
                      {`${translate('LabelGender')} ${translate('Optional')}`}
                    </Text>
                    <Layout style={styles.backgroundRadio} level="1">
                      <RadioGroup
                        style={styles.radioStyle}
                        selectedIndex={
                          values.gender === null || values.gender === undefined
                            ? false
                            : values.gender === 'L'
                            ? 0
                            : 1
                        }
                        onChange={index => {
                          setFieldValue('gender', index === 0 ? 'L' : 'F');
                        }}>
                        <Radio>
                          {evaProps => (
                            <Text style={styles.textRadio}>
                              {translate('Man')}
                            </Text>
                          )}
                        </Radio>
                        <Radio>
                          {evaProps => (
                            <Text style={styles.textRadio}>
                              {translate('Woman')}
                            </Text>
                          )}
                        </Radio>
                      </RadioGroup>
                    </Layout>
                  </View>
                  <SizedBox height={16} />
                  <Field
                    component={TextInputWithLabelFormik}
                    label={translate('LabelEmail')}
                    placeholder={translate('EnterEmail')}
                    icon={<IconEmail width={20} />}
                    autoCapitalize="none"
                    name={'email'}
                    styleProps={{width: '100%'}}
                    placeholderTextColor={Colors.GRAYTEXT}
                    readOnly={true}
                  />
                  <SizedBox height={16} />
                  <Field
                    component={TextInputWithLabelFormik}
                    label={`${translate('LabelAddress')} ${translate(
                      'Optional',
                    )}`}
                    placeholder={translate('EnterAddress')}
                    icon={<IconAddress width={20} />}
                    autoCapitalize="none"
                    name={'address'}
                    styleProps={{width: '100%'}}
                    placeholderTextColor={Colors.GRAYTEXT}
                    multiline={true}
                    required={false}
                  />
                  <SizedBox height={16} />
                  <View style={{width: '100%'}}>
                    <Text style={styles.textLabel}>
                      {translate('LabelCountry')}
                      <Text style={{color: Colors.REDBAR}}>{' *'}</Text>
                    </Text>
                    <SizedBox height={10} />
                    <Dropdown
                      style={[
                        styles.dropdownStyle,
                        {
                          borderColor: isFocus
                            ? Colors.PRIMARY
                            : Colors.BORDER_RADIUS_AUTH,
                        },
                      ]}
                      placeholderStyle={styles.placeholderStyle}
                      selectedTextStyle={styles.selectedStyle}
                      inputSearchStyle={styles.inputStyle}
                      iconStyle={styles.iconStyle}
                      data={dataCountry}
                      search
                      maxHeight={300}
                      labelField="label"
                      valueField="value"
                      placeholder={translate('EnterCountry')}
                      searchPlaceholder={translate('Search')}
                      onFocus={() => setIsFocus(true)}
                      onBlur={() => setIsFocus(false)}
                      value={values.country}
                      renderItem={renderItemDropdown}
                      onChange={country => {
                        setFieldValue('country', country.value);
                      }}
                      renderLeftIcon={() => (
                        <IconCountry width={20} marginRight={15} />
                      )}
                      renderRightIcon={() => {
                        if (isFocus) {
                          return <IconChevUp />;
                        } else {
                          return <IconChevDown />;
                        }
                      }}
                    />
                  </View>
                  {errors.country && (
                    <Text style={styles.errorText}>{errors.country}</Text>
                  )}
                  <SizedBox height={16} />
                  <View style={{width: '100%'}}>
                    <Text style={styles.textLabel}>
                      {translate('LabelIns')}
                      <Text style={{color: Colors.REDBAR}}>{' *'}</Text>
                    </Text>
                    <SizedBox height={10} />
                    <Dropdown
                      style={[
                        styles.dropdownStyle,
                        {
                          borderColor: isFocus2
                            ? Colors.PRIMARY
                            : Colors.BORDER_RADIUS_AUTH,
                        },
                      ]}
                      placeholderStyle={styles.placeholderStyle}
                      selectedTextStyle={styles.selectedStyle}
                      inputSearchStyle={styles.inputStyle}
                      iconStyle={styles.iconStyle}
                      data={dataInstitusi}
                      search
                      maxHeight={300}
                      labelField="label"
                      valueField="value"
                      placeholder={translate('EnterIns')}
                      searchPlaceholder={translate('Search')}
                      onFocus={() => setIsFocus2(true)}
                      onBlur={() => setIsFocus2(false)}
                      value={values.institution_id}
                      renderItem={renderItemDropdown}
                      onChange={institution_id => {
                        setFieldValue('institution_id', institution_id.value);
                      }}
                      renderLeftIcon={() => (
                        <IconInstitution width={20} marginRight={15} />
                      )}
                      renderRightIcon={() => {
                        if (isFocus2) {
                          return <IconChevUp />;
                        } else {
                          return <IconChevDown />;
                        }
                      }}
                    />
                  </View>
                  <SizedBox height={16} />
                  <View style={{width: '100%'}}>
                    <Text style={styles.textLabel}>
                      {`${translate('PhotoProfile')} ${translate(
                        'Optional',
                      )} (Max. 2MB)`}
                    </Text>
                    <>
                      {values.photo ? (
                        <View style={styles.containerUpload}>
                          <Image
                            style={styles.widthImage}
                            source={{
                              uri: `${imageIAM}/${values.photo}`,
                            }}
                          />
                          <TouchableOpacity
                            onPress={() => {
                              setFieldValue('photo', '');
                            }}
                            style={styles.buttonTrash}>
                            <Feather
                              name="trash-2"
                              size={25}
                              color={Colors.WHITE}
                            />
                          </TouchableOpacity>
                        </View>
                      ) : (
                        <>
                          {isLoadingUploadFoto ? (
                            <PercentProgressUpload percentage={percentage} />
                          ) : (
                            <TouchableOpacity
                              onPress={() => handleOpenOption(setFieldValue)}
                              style={styles.containerUpload}>
                              <IconPickFile />
                              <View style={styles.viewTextUpload}>
                                <Text style={styles.textUpload}>
                                  {translate('TakePhoto')}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        </>
                      )}
                    </>
                  </View>
                  <SizedBox height={16.5} />
                  <ButtonPrimary
                    loading={isLoading}
                    disabled={isLoading || !isValid}
                    onPress={handleSubmit}
                    label={
                      !authMethod.user?.is_preference_updated
                        ? translate('Next_Capital')
                        : translate('Update')
                    }
                  />
                </>
              )}
            </Formik>
          ) : (
            <LoadingSpinner />
          )}
        </ScrollView>
      </KeyboardAvoidingView>
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
      <BottomTabOnlyView
        hiddenNavigate={!authMethod.user?.is_preference_updated ? true : false}
      />
    </React.Fragment>
  );
};

export default UpdateProfile;
