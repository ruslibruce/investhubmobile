import moment from 'moment';
import React from 'react';
import { Linking, Text, TouchableOpacity } from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import Toast from 'react-native-toast-message';
import { styles } from './styles';

const tabItems = [
  {
    id: 1,
    name: 'Profile',
    isSelected: true,
  },
  {
    id: 2,
    name: 'Dividen',
    isSelected: false,
  },
  {
    id: 3,
    name: 'Pencatatan Saham',
    isSelected: false,
  },
  {
    id: 4,
    name: 'Pengumuman',
    isSelected: false,
  },
  {
    id: 5,
    name: 'Obligasi & Sukuk',
    isSelected: false,
  },
  {
    id: 6,
    name: 'Info Perdagangan',
    isSelected: false,
  },
  {
    id: 7,
    name: 'Kalender',
    isSelected: false,
  },
  {
    id: 8,
    name: 'Laporan keuangan',
    isSelected: false,
  },
  {
    id: 9,
    name: 'Historikal',
    isSelected: false,
  },
  {
    id: 10,
    name: 'Sejarah Dividen',
    isSelected: false,
  },
];

const useStockScreenerProfileCompany = dataStockScreenerProfileCompany => {
  const [heightTableData, setHeightTableData] = React.useState([]);
  const [dataTabs, setDataTabs] = React.useState(tabItems);
  const [selectedTab, setSelectedTab] = React.useState('Profile');
  const [tableProfile, setTableProfile] = React.useState([]);

  const handleClickTabs = value => {
    const result = dataTabs.map(tab => {
      if (tab === value) {
        tab.isSelected = !tab.isSelected;
      } else {
        tab.isSelected = false;
      }
      return tab;
    });
    setSelectedTab(value.name);
    setDataTabs(result);
  };

  const tableTitleProfile = [
    'Nama',
    'Kode',
    'Alamat Kantor',
    'Alamat Email',
    'Telepon',
    'FAX',
    'NPWP',
    'Situs',
    'Tanggal Pencatatan',
    'Papan Pencatatan',
    'Bidang Usaha Utama',
    'Sektor',
    'Subsektor',
    'Industri',
    'Subindustri',
    'Biro Administrasi Efek',
  ];

  const elementSite = text => (
    <TouchableOpacity onPress={()=> doOpenWeb(text)}>
      <Text style={styles.textBodyTableRed}>{text}</Text>
    </TouchableOpacity>
  );

  const doOpenWeb = async (url) => {
    try {
      var url = `${url}`;
      if (await InAppBrowser.isAvailable()) {
        if (await InAppBrowser.isAvailable()) {
          await InAppBrowser.open(url, {
            // iOS Properties
            ephemeralWebSession: false,
            // Android Properties
            showTitle: false,
            enableUrlBarHiding: true,
            enableDefaultShare: false,
          });
        }
      } else {
        Linking.openURL(url);
      }
    } catch (error) {
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: 'Open Browser Failed',
        text2: error.message ?? 'Your Login Failed',
      });
    }
  };

  React.useEffect(() => {
    if (dataStockScreenerProfileCompany) {
      dataTemp = [];
      dataStockScreenerProfileCompany.Profiles?.map(item => {
        dataTemp.push(
          [item.NamaEmiten],
          [item.KodeEmiten],
          [item.Alamat],
          [item.Email],
          [item.Telepon],
          [item.Fax],
          [item.NPWP],
          [elementSite(item.Website)],
          [moment(item.TanggalPencatatan).format('YYYY-MM-DD')],
          [item.PapanPencatatan],
          [item.KegiatanUsahaUtama],
          [item.Sektor],
          [item.SubSektor],
          [item.Industri],
          [item.SubIndustri],
          [item.BAE],
        );
      });
      setTableProfile(dataTemp);
    }
    let heightTemp = [];
    tableTitleProfile.map(item => {
      if (item === 'Alamat Kantor') {
        heightTemp.push(100);
      }

      heightTemp.push(50);
    });
    setHeightTableData(heightTemp);
  }, [dataStockScreenerProfileCompany]);

  return {
    dataTabs,
    handleClickTabs,
    selectedTab,
    tableTitleProfile,
    tableProfile,
    heightTableData,
  };
};

export default useStockScreenerProfileCompany;
