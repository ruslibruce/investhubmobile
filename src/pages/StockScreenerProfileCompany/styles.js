import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    containerHeader: {
      position: 'absolute',
      top: insets.top,
      zIndex: 2,
      justifyContent: 'flex-start',
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      paddingHorizontal: 20,
      gap: 14,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.WHITE,
      lineHeight: 20,
    },
    textTitle: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    textBodyTable: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#686868',
      textAlign: 'left',
    },
    textBodyTableRed: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
      textAlign: 'left',
    },
    textBodyTableColoumn: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      textAlign: 'left',
    },
    containerButtonTabs: {
      height: 34,
      paddingHorizontal: 20,
      paddingVertical: 10,
      borderBottomWidth: 1,
    },
    textButtonTabs: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      letterSpacing: 0.2,
    },
  });
