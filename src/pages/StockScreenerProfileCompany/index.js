import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Col, Rows, TableWrapper} from 'react-native-reanimated-table';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import useGetStockScreenerProfileCompany from '../../api/useGetStockScreenerProfileCompany';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import {Colors} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useStockScreenerProfileCompany from './useStockScreenerProfileCompany';

const StockScreenerProfileCompany = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const {dataStockScreenerProfileCompany, isLoading} =
    useGetStockScreenerProfileCompany(route.params.sahamCode);
  const {
    dataTabs,
    handleClickTabs,
    selectedTab,
    tableTitleProfile,
    tableProfile,
    heightTableData,
  } = useStockScreenerProfileCompany(dataStockScreenerProfileCompany);
  const styles = styling(insets);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch
        height={100 + insets.top}
        placeholder={'Search Company Code'}
      />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>Profile Company</Text>
      </View>
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <View
          style={{
            paddingHorizontal: 20,
            paddingTop: 20,
            flex: 1,
            paddingBottom: 80,
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={styles.textTitle}>
              {dataStockScreenerProfileCompany.Profiles[0]?.NamaEmiten}
            </Text>
            <SizedBox height={12} />
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {dataTabs.map(tab => (
                <TouchableOpacity
                  key={tab.id}
                  onPress={() => handleClickTabs(tab)}
                  style={[
                    styles.containerButtonTabs,
                    {
                      borderBottomColor: tab.isSelected
                        ? Colors.PRIMARY
                        : Colors.GRAY,
                    },
                  ]}>
                  <Text
                    style={[
                      styles.textButtonTabs,
                      {color: tab.isSelected ? Colors.PRIMARY : '#333333'},
                    ]}>
                    {tab.name}
                  </Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
            {selectedTab === 'Profile' && (
              <TableWrapper style={{flexDirection: 'row', paddingTop: 32}}>
                <Col
                  data={tableTitleProfile}
                  style={{
                    width: 140,
                    justifyContent: 'flex-start',
                    paddingRight: 10,
                  }}
                  heightArr={heightTableData}
                  textStyle={styles.textBodyTableColoumn}
                />
                <Rows
                  data={tableProfile}
                  heightArr={heightTableData}
                  widthArr={[150]}
                  style={{flex: 1, justifyContent: 'flex-start'}}
                  textStyle={styles.textBodyTable}
                />
              </TableWrapper>
            )}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

export default StockScreenerProfileCompany;
