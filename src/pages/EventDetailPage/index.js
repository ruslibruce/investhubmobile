import moment from 'moment';
import React from 'react';
import {
  FlatList,
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import useGetEventDetail from '../../api/useGetEventDetail';
import {IconFacebook, IconInstagram, IconTwitter} from '../../assets/svg';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import WebDisplay from '../../components/WebDisplay';
import {Colors, isHTML, matrics} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';

const EventDetailPage = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();

  const {isLoading, dataEventDetail} = useGetEventDetail(route.params.id);

  const styles = styling(insets);

  const tagsStyles = {
    p: {
      fontSize: 14,
      fontWeight: 700,
      fontFamily: 'OpenSans-Bold',
      color: '#A62829',
      textAlign: 'justify',
    },
  };

  const tagsStylesDesc = {
    p: {
      fontSize: 12,
      fontWeight: 400,
      fontFamily: 'OpenSans-Regular',
      color: '#696F79',
      textAlign: 'justify',
    },
  };

  const renderHeader = () => (
    <>
      <View style={styles.containerImageHeadline}>
        <Image
          style={styles.widthImage}
          source={{uri: dataEventDetail.cover_image}}
        />
      </View>
      <SizedBox height={16} />
      <View style={styles.containerTextDescriptionHeadline}>
        {isHTML(dataEventDetail.title) ? (
          <WebDisplay html={dataEventDetail.title} tagsStyles={tagsStyles} />
        ) : (
          <Text style={styles.textDescriptionHeadline} numberOfLines={2}>
            {dataEventDetail.title}
          </Text>
        )}
      </View>
      <SizedBox height={8} />
      <View style={styles.containerHeadlineInfo}>
        <View style={styles.containerListFooterItem}>
          <Text style={styles.textBottomLeftItem}>
            {dataEventDetail.category}
          </Text>
        </View>
        <View style={styles.containerTextHeadlineInfo}>
          <Text style={styles.textHeadlineInfoRed}>
            {dataEventDetail.location}
          </Text>
          <View style={styles.dotStyling} />
          <Text style={styles.textHeadlineInfo}>
            {moment(dataEventDetail.start_time).format('MMM DD, YYYY')}
          </Text>
        </View>
      </View>
      <SizedBox height={32} />
      {isHTML(dataEventDetail.description) ? (
        <WebDisplay
          html={dataEventDetail.description}
          tagsStyles={tagsStylesDesc}
        />
      ) : (
        <Text style={styles.textHeadlineInfo}>
          {dataEventDetail.description}
        </Text>
      )}
      {/* <SizedBox height={20} />
      <UpcomingEvent />
      <SizedBox height={20} /> */}
    </>
  );

  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} hiddenSearch={true} />
      <View style={styles.containerButtonHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <View style={styles.containerButtonHeaderRight}>
          <SizedBox width={32} />
          {/* <TouchableOpacity style={styles.buttonListHeaderRight}>
            <IconBookmark />
          </TouchableOpacity> */}
          <TouchableOpacity style={styles.buttonListHeaderRight}>
            <Feather name={'share-2'} size={20} color={Colors.BLACK} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.marginFlatlist}>
        {isLoading ? (
          <LoadingSpinner isFullScreen={true} />
        ) : (
          <FlatList
            data={dataEventDetail ? [dataEventDetail] : []}
            ref={listRef}
            onScroll={onScroll}
            contentContainerStyle={styles.contentFlatlist}
            ListEmptyComponent={() => <NoData />}
            ListHeaderComponent={renderHeader()}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item, index}) => <View />}
          />
        )}
      </View>
      <IconFloat
        listRef={listRef}
        bottom={
          Platform.OS === 'ios'
            ? matrics.screenHeight / 3.15
            : matrics.screenHeight / 4.25
        }
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default EventDetailPage;
