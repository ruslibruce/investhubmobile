import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      position: 'relative',
    },
    containerButtonHeader: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 20,
      position: 'absolute',
      width: '100%',
      top: insets.top,
      zIndex: 2,
    },
    containerButtonHeaderRight: {
      flexDirection: 'row',
      gap: 10,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    marginFlatlist: {
      paddingTop: 40,
      position: 'relative',
    },
    contentFlatlist: {
      paddingBottom: 100,
      paddingHorizontal: 20,
    },
    textHeadline: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(24),
      color: Colors.BLACK,
      lineHeight: 33,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    containerImageHeadline: {
      height: 152,
      width: '100%',
      borderRadius: 12,
      overflow: 'hidden',
    },
    containerTextDescriptionHeadline: {
      width: '95%',
    },
    containerHeadlineInfo: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    textDescriptionHeadline: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.BLACK,
      lineHeight: 22,
    },
    containerTextHeadlineInfo: {
      flexDirection: 'row',
      gap: 12,
      alignItems: 'center',
    },
    textHeadlineInfoRed: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
    },
    dotStyling: {
      backgroundColor: Colors.GRAY,
      width: 4,
      height: 4,
      borderRadius: 4,
    },
    textHeadlineInfo: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#696F79',
      textAlign: 'justify',
    },
    textFollow: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(16),
      color: Colors.BLACK,
    },
    containerButtonSosmed: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
      gap: 20,
    },
    containerIconSosmed: {
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 10,
      padding: 10,
      backgroundColor: '#696F79',
    },
    containerButtonBottom: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    containerButtonRed: {
      backgroundColor: Colors.PINK,
      paddingHorizontal: 20,
      paddingVertical: 2,
      borderRadius: 15,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerListFooterItem: {
      paddingHorizontal: 10,
      paddingVertical: 6,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 15,
      alignSelf: 'center',
      maxWidth: 100,
    },
    textBottomLeftItem: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: Colors.PRIMARY,
    },
  });
