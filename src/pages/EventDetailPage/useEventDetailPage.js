import React from 'react';
import { useSelector } from 'react-redux';

const tabItems = [
  {
    id: 1,
    name: 'All',
    isSelected: true,
  },
  {
    id: 2,
    name: 'Business',
    isSelected: false,
  },
  {
    id: 3,
    name: 'Investasi',
    isSelected: false,
  },
  {
    id: 4,
    name: 'Technology',
    isSelected: false,
  },
  {
    id: 5,
    name: 'Finance',
    isSelected: false,
  },
  {
    id: 6,
    name: 'Health Care',
    isSelected: false,
  },
];

const useEventDetailPage = () => {
  const authMethod = useSelector(state => state.auth);
  const listRef = React.useRef(null);
  const [contentVerticalOffset, setContentVerticalOffset] = React.useState(0);
  const CONTENT_OFFSET_THRESHOLD = 250;
  const [dataTabs, setDataTabs] = React.useState(tabItems);

  const handleClickTabs = value => {
    const result = dataTabs.map(tab => {
      if (tab === value) {
        tab.isSelected = !tab.isSelected;
      } else {
        tab.isSelected = false;
      }
      return tab;
    });
    setDataTabs(result);
  };

  return {
    authMethod,
    listRef,
    contentVerticalOffset,
    setContentVerticalOffset,
    CONTENT_OFFSET_THRESHOLD,
    dataTabs,
    setDataTabs,
    handleClickTabs,
  };
};

export default useEventDetailPage;
