import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import useGetAboutUs from '../../api/useGetAboutUs';
import {iconLogo} from '../../assets/images';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import {Colors} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import IconFloat from '../../widgets/IconFloat';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import WebDisplay from '../../components/WebDisplay';
import NoData from '../../components/NoData';

const AboutUs = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const tabHeight = useBottomTabBarHeight();
  const {isLoading, dataAbout} = useGetAboutUs();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} hiddenSearch={true} />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>{dataAbout?.title}</Text>
        <View />
      </View>
      <SizedBox height={60} />
      {isLoading ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <ScrollView
          ref={listRef}
          onScroll={onScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.contentScroll}>
          {dataAbout ? <WebDisplay html={dataAbout?.body} /> : <NoData />}
          {/* <SizedBox height={18} />
          <Text
            style={
              styles.textContentSub
            }>{`Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.`}</Text>
          <SizedBox height={10} />
          <View style={styles.containerImage}>
            <Image style={styles.widthImage} source={iconLogo} />
          </View>
          <SizedBox height={28} />
          <Text style={styles.textContentTitle}>{'How Portal Edukasi ?'}</Text>
          <SizedBox height={18} />
          <Text style={styles.textContentSub}>
            {
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            }
          </Text>
          <SizedBox height={28} />
          <Text style={styles.textContentTitle}>
            {'Who is the originator Portal Edukasi ?'}
          </Text>
          <SizedBox height={18} />
          <Text style={styles.textContentSub}>
            {
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            }
          </Text> */}
        </ScrollView>
      )}
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default AboutUs;
