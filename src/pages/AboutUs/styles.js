import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    containerHeader: {
      position: 'absolute',
      top: insets.top,
      zIndex: 2,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      paddingHorizontal: 20,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.WHITE,
      lineHeight: 20,
    },
    contentScroll: {
      paddingHorizontal: 20,
      paddingTop: 10,
      paddingBottom: tabHeight + 20,
    },
    textContentTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
      lineHeight: 27,
    },
    textContentSub: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#696F79',
      lineHeight: 16,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    containerImage: {
      width: '100%',
      height: 153,
      maxHeight: 153,
    },
  });
