import React from 'react';
import useGetLearningJourneyId from '../../api/useGetLearningJourneyId';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {
  DASHBOARD_COURSE,
  LEVEL_UP,
  MY_LEARNING_JOURNEY_DETAIL,
  TAB_COURSE_DETAIL,
} from '../../utility';
import {useTranslation} from 'react-i18next';
import { useSelector } from 'react-redux';

export default function useMyLearningJourneyDetail(id) {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const navigation = useNavigation();
  const [visible, setVisible] = React.useState(false);
  const {dataLearningJourney, isLoading, accessJourney} = useGetLearningJourneyId(id);

  useFocusEffect(React.useCallback(() => {
    accessJourney(id);
  }, []));

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod]);

  const handleNavigateCourse = value => {
    navigation.navigate(DASHBOARD_COURSE, {
      screen: TAB_COURSE_DETAIL,
      params: {
        id: value.id,
        route: MY_LEARNING_JOURNEY_DETAIL,
        idPrevious: id,
        namePrevious: dataLearningJourney.name,
      },
    });
  };

  const handleNavigateQuiz = (idQuiz, name) => {
    navigation.navigate(LEVEL_UP, {
      id: idQuiz,
      route: MY_LEARNING_JOURNEY_DETAIL,
      idPrevious: id,
      namePrevious: dataLearningJourney.name,
      name: name,
    });
  };

  return {
    dataLearningJourney,
    visible,
    setVisible,
    handleNavigateCourse,
    translate,
    isLoading,
    handleNavigateQuiz,
  };
}
