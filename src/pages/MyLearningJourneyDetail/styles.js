import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  imageBackgroundStyle: {
    flex: 1,
    position: 'relative',
  },
  containerHeaderButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 40,
    paddingHorizontal: 20,
  },
  leftTopButton: {
    backgroundColor: Colors.WHITE,
    height: 32,
    width: 32,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightTopButton: {
    backgroundColor: Colors.WHITE,
    height: 32,
    width: 32,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerTopTitle: {
    flexDirection: 'row',
    paddingLeft: 20,
    alignItems: 'center',
    gap: 10,
  },
  textTopTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(20),
    color: Colors.WHITE,
  },
  textTopSubTitle: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.WHITE,
  },
  contentScrollView: {
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  container: {
    backgroundColor: Colors.WHITE,
    borderTopEndRadius: 24,
    borderTopStartRadius: 24,
    flex: 1,
  },
  containerTopScroll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  containerPositionCup: {
    backgroundColor: Colors.PINK,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    maxWidth: 91,
    height: 26,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  textLengthCourse: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(16),
    color: Colors.PRIMARY,
  },
  containerLengthCourse: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    marginLeft: 20,
  },
  progressColor: {
    backgroundColor: '#D9D9D9',
  },
  textProgress: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(8),
    color: Colors.WHITE,
  },
  textContent: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(18),
    color: Colors.BLACK,
  },
  containerIcon: {
    borderRadius: 80,
    backgroundColor: Colors.PINK,
    position: 'relative',
  },
  columnWrapperFlatlist: {
    justifyContent: 'space-between',
    gap: 10,
    marginBottom: 20,
  },
  containerCard: {
    backgroundColor: Colors.WHITE,
    borderColor: Colors.WHITE,
  },
  rowContainer:{
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
    backgroundColor: Colors.WHITE,
    height: 20,
    flexDirection: 'row',
    marginBottom: 15,
  },
  leftContent:{
    flexBasis: '65%',
    alignItems: 'flex-start',
  },
  rightContent:{
    flexBasis: '30%',
  },
  textTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
  },
  textTitleContent: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(12),
    color: Colors.GRAY2,
  },
  containerButton: {
    width: '100%',
    height: 48,
    backgroundColor: Colors.GRAY,
    borderRadius: 24,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  textButton: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(12),
    color: Colors.WHITE,
  },
});
