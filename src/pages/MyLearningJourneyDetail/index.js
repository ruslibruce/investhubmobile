import {Card, ProgressBar, Tooltip} from '@ui-kitten/components';
import React from 'react';
import {
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TouchableOpacity as TouchableOpacityGesture} from 'react-native-gesture-handler';
import PieChart from 'react-native-pie-chart';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {imageProfile} from '../../assets/images';
import {
  IconJourney100,
  IconJourneyLock,
  IconJourneyRun,
} from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import {Colors, DASHBOARD_HOME} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarTranslucent} from '../../widgets/StatusCustomBar';
import {styles} from './styles';
import useMyLearningJourneyDetail from './useMyLearningJourneyDetail';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import IconFloat from '../../widgets/IconFloat';
import LoadingSpinner from '../../components/LoadingSpinner';

const MyLearningJourneyDetail = ({navigation, route}) => {
  const {
    params: {id, name},
  } = route;
  const {
    dataLearningJourney,
    handleNavigateCourse,
    visible,
    setVisible,
    translate,
    isLoading,
    handleNavigateQuiz,
  } = useMyLearningJourneyDetail(id);
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  console.log(
    'dataLearningJourney',
    JSON.stringify(dataLearningJourney, null, 2),
  );

  const testCompleted =
    Object.keys(dataLearningJourney).length > 0 &&
    dataLearningJourney?.is_test_passed
      ? 1
      : 0;
  const completedCount =
    Object.keys(dataLearningJourney).length > 0 &&
    dataLearningJourney?.requirements?.length > 0
      ? dataLearningJourney?.requirements?.filter(
          val => val.progress && val.progress.is_completed,
        ).length + testCompleted
      : 0;
  const coursePlusTest =
    Object.keys(dataLearningJourney).length > 0 &&
    dataLearningJourney?.requirements?.length + 1;
  const valueCircular =
    completedCount === 0 ? 0 : (completedCount / coursePlusTest) * 100;
  const renderHeader = () => (
    <>
      <SizedBox height={15} />
      <View style={styles.containerLengthCourse}>
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          <Text style={styles.textLengthCourse}>
            {`${dataLearningJourney.requirements?.length} ${translate(
              'Courses',
            )} + 1 ${translate('LabelTest')}`}
          </Text>
        )}
      </View>
      <SizedBox height={15} />
    </>
  );

  return (
    <ImageBackground style={styles.imageBackgroundStyle} source={imageProfile}>
      <StatusCustomBarTranslucent />
      <View style={styles.containerHeaderButton}>
        <TouchableOpacityGesture
          onPress={() => navigation.navigate(DASHBOARD_HOME)}
          style={styles.leftTopButton}>
          <AntDesign
            name={'arrowleft'}
            size={24}
            color={Colors.BLACK}
            onPress={() => {}}
          />
        </TouchableOpacityGesture>
        <View />
      </View>
      <SizedBox height={25} />
      <View style={styles.containerTopTitle}>
        <View style={styles.containerIcon}>
          <PieChart
            widthAndHeight={50}
            series={[valueCircular, 100 - valueCircular]}
            sliceColor={[Colors.GREEN, Colors.GRAY]}
            coverRadius={0.75}
            coverFill={'#FFF'}
          />
          <View style={{position: 'absolute', top: 12, left: 12}}>
            {dataLearningJourney.achieved_at ? (
              <IconJourney100 />
            ) : dataLearningJourney.requirements?.length === 0 ? (
              <IconJourneyLock />
            ) : (
              <IconJourneyRun />
            )}
          </View>
        </View>
        <Text style={styles.textTopTitle}>{name}</Text>
      </View>
      <SizedBox height={25} />
      <View style={styles.container}>
        {renderHeader()}
        <ScrollView
          ref={listRef}
          onScroll={onScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.contentScrollView}
          style={styles.containerCard}>
          <View style={styles.rowContainer}>
            <View style={styles.leftContent}>
              <Text style={styles.textTitle}>{translate('CourseTitle')}</Text>
            </View>
            <View style={styles.rightContent}>
              <Text style={styles.textTitle}>{translate('Progress')}</Text>
            </View>
          </View>
          {dataLearningJourney.requirements?.map(item => {
            return (
              <TouchableOpacity
                style={styles.rowContainer}
                onPress={() => handleNavigateCourse(item)}
                key={item.id}>
                <View style={styles.leftContent}>
                  <Text numberOfLines={1} style={styles.textTitleContent}>
                    {item.title}
                  </Text>
                </View>
                <View style={styles.rightContent}>
                  {item.progress ? (
                    <ProgressBar
                      status={'success'}
                      style={styles.progressColor}
                      progress={Number(item.progress?.progress) / 100}
                      size="giant"
                    />
                  ) : (
                    <ProgressBar
                      status={'success'}
                      style={styles.progressColor}
                      progress={0}
                      size="giant"
                    />
                  )}
                  <View
                    style={{
                      position: 'absolute',
                      width: '100%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text style={styles.textProgress}>
                      {item.progress?.progress} %
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
          {dataLearningJourney.is_test_enabled ? (
            <TouchableOpacity
              style={styles.rowContainer}
              onPress={() => handleNavigateQuiz(dataLearningJourney.test_attempt_id, name)}>
              <View style={styles.leftContent}>
                <Text numberOfLines={1} style={styles.textTitleContent}>
                  {translate('Level_Up_Capital')}
                </Text>
              </View>
              <View style={styles.rightContent}>
                <ProgressBar
                  status={'success'}
                  style={styles.progressColor}
                  progress={dataLearningJourney.is_test_passed ? 1 : 0}
                  size="giant"
                />
                <View
                  style={{
                    position: 'absolute',
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.textProgress}>
                    {dataLearningJourney.is_test_passed ? '100' : '0'} %
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          ) : (
            <Tooltip
              anchor={() => (
                <TouchableOpacity
                  style={styles.rowContainer}
                  onPress={() => setVisible(true)}>
                  <View style={styles.leftContent}>
                    <Text numberOfLines={1} style={styles.textTitleContent}>
                      {translate('Level_Up_Capital')}
                    </Text>
                  </View>
                  <View style={styles.rightContent}>
                    <ProgressBar
                      status={'success'}
                      style={styles.progressColor}
                      progress={0}
                      size="giant"
                    />
                    <View
                      style={{
                        position: 'absolute',
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.textProgress}>{'0'} %</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
              visible={visible}
              animationType="fade"
              placement={'bottom'}
              // accessoryLeft={InfoIcon}
              onBackdropPress={() => setVisible(false)}>
              {evaProps => (
                <Text style={styles.textTitle}>
                  {translate('CompleteProgress')}
                </Text>
              )}
            </Tooltip>
          )}
        </ScrollView>
      </View>
      <IconFloat
        bottom={1}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
      <BottomTabOnlyView />
    </ImageBackground>
  );
};

export default MyLearningJourneyDetail;
