import React from 'react';
import {FlatList, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import {Colors, DASHBOARD_COURSE, isHTML, TAB_COURSE_MAIN} from '../../utility';
import Divider from '../../widgets/Divider';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputCustom from '../../widgets/TextInputCostum';
import {styling} from './styles';
import useSearch from './useSearch';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import WebDisplay from '../../components/WebDisplay';
import {ActivityIndicator} from '@ant-design/react-native';

const SearchAllPortal = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    translate,
    search,
    setSearch,
    dataSearchAll,
    isLoading,
    handleNavigate,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useSearch();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets);
  console.log('dataSearchAll', JSON.stringify(dataSearchAll, null, 2));

  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TextInputCustom
          iconRight={
            search ? (
              <AntDesign
                onPress={() => setSearch('')}
                name="close"
                color={Colors.BLACK}
              />
            ) : (
              <Feather name="search" color={Colors.BLACK} />
            )
          }
          placeholder={translate('Search')}
          styleInput={{width: '100%'}}
          placeholderTextColor={Colors.GRAYTEXT}
          autoCapitalize="none"
          value={search}
          containerInputText={styles.inputSearch}
          onChangeText={text => setSearch(text)}
        />
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textCancel}>Cancel</Text>
        </TouchableOpacity>
      </View>
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={10} />
      <View style={styles.containerScroll}>
        <FlatList
          data={dataSearchAll}
          ref={listRef}
          onScroll={onScroll}
          contentContainerStyle={styles.contentScrollView}
          // ListHeaderComponent={renderHeader()}
          ListEmptyComponent={() => {
            if (isLoading) {
              return <LoadingSpinner isFullScreen={true} />;
            }
            return <NoData />;
          }}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => {
            if (item?.source === 'investments') {
              return (
                <TouchableOpacity
                  onPress={() => handleNavigate(item)}
                  style={styles.containerListCard}
                  key={index}>
                  <Text numberOfLines={1} style={styles.textTitleCard}>
                    {item.name}
                  </Text>
                  <SizedBox height={8} />
                  <Text numberOfLines={2} style={styles.textDescription}>
                    {item.url}
                  </Text>
                </TouchableOpacity>
              );
            }
            return (
              <TouchableOpacity
                onPress={() => handleNavigate(item)}
                style={styles.containerListCard}
                key={index}>
                <View
                  style={{
                    height: 21,
                    overflow: 'hidden',
                    paddingHorizontal: 5,
                  }}>
                  {item.title && (
                    <WebDisplay
                      html={item.title}
                      numberOfLines={1}
                      styleText={styles.textTitleCard}
                    />
                  )}
                  {item.subject && (
                    <WebDisplay
                      html={item.subject}
                      numberOfLines={1}
                      styleText={styles.textTitleCard}
                    />
                  )}
                </View>
                <SizedBox height={8} />
                <View
                  style={{
                    height: 39,
                    overflow: 'hidden',
                    paddingHorizontal: 5,
                  }}>
                  {item.description && (
                    <WebDisplay
                      html={item.description}
                      numberOfLines={2}
                      styleText={styles.textDescription}
                    />
                  )}
                  {item.body && (
                    <WebDisplay
                      html={item.body}
                      numberOfLines={2}
                      styleText={styles.textDescription}
                    />
                  )}
                  <SizedBox height={10} />
                </View>
              </TouchableOpacity>
            );
          }}
          onEndReached={() => {
            if (totalPage > params.page) {
              if (!search) {
                return;
              }
              setIsLoadingPage(true);
              setParams(prev => ({
                ...prev,
                page: prev.page + 1,
              }));
            }
          }}
          ListFooterComponent={() =>
            isLoadingPage ? <LoadingSpinner /> : null
          }
        />
      </View>
      <IconFloat
        bottom={70}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
      />
    </View>
  );
};

export default SearchAllPortal;
