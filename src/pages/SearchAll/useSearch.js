import {useNavigation} from '@react-navigation/native';
import React from 'react';
import useGetSearchAll from '../../api/useGetSearchAll';
import {
  DASHBOARD_COURSE,
  DASHBOARD_NEWS,
  EVENT_DETAIL_PAGE,
  handleOpenLink,
  SEARCH_ALL,
  TAB_COURSE_DETAIL,
  TAB_NEWS_DETAIL,
} from '../../utility';
import { useTranslation } from 'react-i18next';

const useSearch = () => {
  const {t: translate} = useTranslation();
  const [search, setSearch] = React.useState('');
  const {
    dataSearchAll,
    isLoading,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  } = useGetSearchAll();
  const navigation = useNavigation();

  React.useEffect(() => {
    if (search.length > 2) {
      setParams({
        page: 1,
        limit: 10,
        search,
      })
    }

    if (search.length === 0) {
      setParams({
        page: 1,
        limit: 10,
        search,
      })
    }
  }, [search]);

  const handleNavigate = item => {
    switch (item.source) {
      case 'investments':
        handleOpenLink(item.url);
        break;
      case 'news':
        if (item.source_id) {
          handleOpenLink(item.link);
          return;
        }
        navigation.navigate(DASHBOARD_NEWS, {
          screen: TAB_NEWS_DETAIL,
          params: {id: item.id},
        });
        break;

      case 'events':
        navigation.navigate(EVENT_DETAIL_PAGE, {id: item.id});
        break;

      case 'course':
        navigation.navigate(DASHBOARD_COURSE, {
          screen: TAB_COURSE_DETAIL,
          params: {
            id: item.id,
            route: SEARCH_ALL,
          },
        });
        break;

      default:
        break;
    }
  };

  return {
    translate,
    search,
    setSearch,
    dataSearchAll,
    isLoading,
    handleNavigate,
    isLoadingPage,
    setParams,
    totalPage,
    params,
    setIsLoadingPage,
  };
};

export default useSearch;
