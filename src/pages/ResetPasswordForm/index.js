import {Field, Formik} from 'formik';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {iconLogo} from '../../assets/images';
import {IconBack, IconPasswordLogin} from '../../assets/svg';
import GroupButtonAuth from '../../components/GroupButtonAuth';
import NoHaveAccToSignIn from '../../components/NoHaveAccToSignIn';
import TextYourInfo from '../../components/TextYourInfo';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import Divider from '../../widgets/Divider';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputWithLabelFormik from '../../widgets/TextInputWithLabelFormik';
import {styling} from './styles';
import useResetPasswordForm from './useResetPasswordForm';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const ResetPasswordForm = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {handleSetNewPassword, forgotValidationSchema, translate, isLoading} =
    useResetPasswordForm();
  const styles = styling(insets);
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.buttonTopPosition}>
        <IconBack />
      </TouchableOpacity>
      <SizedBox height={20} />
      <View style={styles.containerTop}>
        <Image source={iconLogo} style={styles.widthImage} />
      </View>
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={16} />
      <Text style={styles.textTitle}>
        {translate('ForgotPasswordForm_Title')}
      </Text>
      <SizedBox height={4} />
      <Text style={styles.textSubTitle}>
        {translate('ForgotPasswordForm_Desc')}
      </Text>
      <SizedBox height={16} />
      <Formik
        validationSchema={forgotValidationSchema}
        initialValues={{password: '', password_confirmation: ''}}
        onSubmit={values => handleSetNewPassword(values)}>
        {({handleSubmit, isValid}) => (
          <>
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelPasswordCreate')}
              placeholder={translate('EnterPassword')}
              icon={<IconPasswordLogin width={20} />}
              autoCapitalize="none"
              name={'password'}
              secureTextEntry={true}
            />
            <SizedBox height={16} />
            <Field
              component={TextInputWithLabelFormik}
              label={translate('LabelConfirmPassword')}
              placeholder={translate('EnterConfirmPassword')}
              icon={<IconPasswordLogin width={20} />}
              autoCapitalize="none"
              name={'password_confirmation'}
              secureTextEntry={true}
            />
            <SizedBox height={16} />
            <ButtonPrimary
              loading={isLoading}
              disabled={isLoading || !isValid}
              onPress={handleSubmit}
              label={translate('Submit')}
            />
          </>
        )}
      </Formik>
      <SizedBox height={16} />
      <TextYourInfo />
      <GroupButtonAuth isLogin={true}  />
      <NoHaveAccToSignIn />
    </ScrollView>
  );
};

export default ResetPasswordForm;
