import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    contentScrollView: {
      alignItems: 'center',
      paddingVertical: 20,
      paddingBottom: 20,
      paddingHorizontal: 20,
      flex: 1,
      backgroundColor: Colors.WHITE,
      paddingTop: insets.top,
    },
    buttonTopPosition: {
      alignSelf: 'flex-start',
    },
    containerTop: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
    },
    widthImage: {
      width: 165,
      height: 50,
    },
    containerAlignTop: {
      alignItems: 'flex-end',
    },
    textTopTitle: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(10),
      color: Colors.TEXTINFO,
    },
    textTitle: {
      fontSize: getFontSize(20),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
      alignSelf: 'flex-start',
    },
    textSubTitle: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.TEXTINFO,
      alignSelf: 'flex-start',
    },
  });
