import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import * as yup from 'yup';
import usePostResetPassword from '../../api/usePostResetPassword';

const useResetPasswordForm = () => {
  const {t: translate} = useTranslation();
  const {accessResetPassword, isLoading} = usePostResetPassword();
  const validateValue = useSelector(state => state.app.validateValue);

  const handleSetNewPassword = React.useCallback(values => {
    let data = { ...values, ...validateValue };
    accessResetPassword(data);
  }, []);

  const forgotValidationSchema = React.useCallback(
    yup.object().shape({
      password: yup
        .string()
        .min(
          8,
          ({min}) =>
            `${translate('PasswordLength')} ${min} ${translate('Characters')}`,
        )
        .required(translate('RequirePassword'))
        .minLowercase(1, translate('Lower_Password'))
        .minUppercase(1, translate('Upper_Password'))
        .minNumbers(1, translate('Number_Password'))
        .minWords(1, translate('Word_Password'))
        .minSymbols(1, translate("Symbol_Password")),
      password_confirmation: yup
        .string()
        .oneOf([yup.ref('password')], translate('Match_Password'))
        .required(translate('Confirm_Password')),
    }),
    [],
  );

  return {
    handleSetNewPassword,
    translate,
    forgotValidationSchema,
    isLoading,
  };
};

export default useResetPasswordForm;
