import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import WebDisplay from '../../components/WebDisplay';
import {Colors, isHTML} from '../../utility';
import Divider from '../../widgets/Divider';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputCustom from '../../widgets/TextInputCostum';
import {styling} from './styles';
import useSearch from './useSearch';

const SearchEventPage = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const {
    search,
    setSearch,
    dataSearchEvent,
    isLoading,
    handleNavigateNewsEvent,
  } = useSearch();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const dataArray = Object.values(dataSearchEvent);
  const styles = styling(insets);
  const tagsStyles = {
    p: {
      fontSize: 12,
      fontWeight: 600,
      fontFamily: 'OpenSans-SemiBold',
      color: '#000000',
      textAlign: 'justify',
    },
  };

  const tagsStylesDesc = {
    p: {
      fontSize: 10,
      fontWeight: 400,
      fontFamily: 'OpenSans-Regular',
      color: '#656565',
      textAlign: 'justify',
    },
  };
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TextInputCustom
          iconRight={
            search ? (
              <AntDesign
                onPress={() => setSearch(null)}
                name="close"
                color={Colors.BLACK}
              />
            ) : (
              <Feather name="search" color={Colors.BLACK} />
            )
          }
          placeholder={`Search Event`}
          styleInput={{width: '100%'}}
          placeholderTextColor={Colors.GRAYTEXT}
          autoCapitalize="none"
          value={search}
          containerInputText={styles.inputSearch}
          onChangeText={text => setSearch(text)}
        />
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => navigation.goBack()}>
          <Text>Cancel</Text>
        </TouchableOpacity>
      </View>
      <SizedBox height={10} />
      <Divider />
      <SizedBox height={10} />
      <View style={styles.containerScroll}>
        <ScrollView
          ref={listRef}
          onScroll={onScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.contentScrollView}>
          {dataSearchEvent.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => handleNavigateNewsEvent(item.id)}
                style={styles.containerListCard}
                key={index}>
                {isHTML(item.title) ? (
                  <WebDisplay
                    html={item.title}
                    tagsStyles={tagsStyles}
                    isOneLine={true}
                    styleText={styles.textTitleCard}
                  />
                ) : (
                  <Text numberOfLines={1} style={styles.textTitleCard}>
                    {item.title}
                  </Text>
                )}
                {isHTML(item.description) ? (
                  <WebDisplay
                    html={item.description}
                    tagsStyles={tagsStylesDesc}
                    isOneLine={true}
                    styleText={styles.textDescription}
                  />
                ) : (
                  <Text numberOfLines={1} style={styles.textDescription}>
                    {item.description}
                  </Text>
                )}
              </TouchableOpacity>
            );
          })}
          {dataSearchEvent.length === 0 && <NoData />}
          {isLoading && <LoadingSpinner isFullScreen={true} />}
        </ScrollView>
      </View>
      <IconFloat
        bottom={70}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default SearchEventPage;
