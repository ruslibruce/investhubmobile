import { useNavigation } from '@react-navigation/native';
import React from 'react';
import useGetSearchEvent from '../../api/useGetSearchEvent';
import { EVENT_DETAIL_PAGE, paramsToString } from '../../utility';

const useSearch = () => {
  const [search, setSearch] = React.useState('');
  const {accessSearchNews, dataSearchEvent, isLoading} = useGetSearchEvent();
  const navigation = useNavigation();

  React.useEffect(() => {
    if (search.length > 3) {
      accessSearchNews(paramsToString({q: search}));
    }

    if (search.length === 0) {
      accessSearchNews();
    }
  }, [search]);

  const handleNavigateNewsEvent = (id) => {
    navigation.navigate(EVENT_DETAIL_PAGE, {id: id});
  };

  return {
    search,
    setSearch,
    dataSearchEvent,
    isLoading,
    handleNavigateNewsEvent,
  };
};

export default useSearch;
