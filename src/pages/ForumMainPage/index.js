import React from 'react';
import {FlatList, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ForumCard from '../../components/ForumCard';
import {Colors, SEARCH_ALL} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useForumMainPage from './useForumMainPage';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const ForumMainPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {tabHeight, dataTabs, handleClickTabs} = useForumMainPage();
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const styles = styling(insets, tabHeight);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => navigation.goBack()}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity style={styles.buttonListHeaderRight}>
            <Ionicons name="settings-sharp" size={20} color={Colors.BLACK} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate(SEARCH_ALL)}
            style={styles.buttonListHeaderRight}>
            <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
          </TouchableOpacity>
        </View>
      </View>
      <SizedBox height={8} />
      <View style={styles.marginScrollView}>
        <ScrollView
          style={styles.containerScroll}
          contentContainerStyle={styles.contentScroll}
          horizontal
          showsHorizontalScrollIndicator={false}>
          {dataTabs.map((tab, i) => (
            <TouchableOpacity
              key={tab.id}
              onPress={() => handleClickTabs(tab)}
              style={[
                styles.containerButtonTabs,
                {backgroundColor: tab.isSelected ? Colors.PINK : Colors.WHITE},
              ]}>
              <Text
                style={[
                  styles.textButtonTabs,
                  {color: tab.isSelected ? Colors.PRIMARY : '#333333'},
                ]}>
                {tab.name}
              </Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
      <FlatList
        data={[1, 2, 3, 4, 5, 6]}
        ref={listRef}
        onScroll={onScroll}
        ListHeaderComponent={() => <SizedBox height={10} />}
        contentContainerStyle={styles.contentFlatlist}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item, index}) => <ForumCard />}
      />
      <IconFloat
        bottom={1}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
    </View>
  );
};

export default ForumMainPage;
