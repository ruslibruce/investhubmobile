import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (insets, tabHeight) =>
  StyleSheet.create({
    container: {
      flex: 1,
      position: 'relative',
      paddingTop: insets.top,
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: 5,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerButtonHeaderRight: {
      flexDirection: 'row',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    contentFlatlist: {
      width: matrics.screenWidth,
      // backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: tabHeight + 200,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    marginScrollView: {
      // width: '100%',
    },
    containerScroll: {
      paddingVertical: 8,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopColor: '#E8E8E8',
      borderBottomColor: '#E8E8E8',
    },
    contentScroll: {
      paddingLeft: 20,
    },
    containerButtonTabs: {
      height: 34,
      paddingHorizontal: 8,
      paddingVertical: 10,
      borderRadius: 8,
      marginRight: 12,
    },
    textButtonTabs: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      letterSpacing: 0.2,
    },
    containerListItems: {
      width: '100%',
      height: 88,
      backgroundColor: Colors.WHITE,
      borderTopLeftRadius: 16,
      borderBottomLeftRadius: 16,
      borderTopRightRadius: 8,
      borderBottomRightRadius: 8,
      flexDirection: 'row',
      overflow: 'hidden',
      alignItems: 'center',
      gap: 12,
      marginBottom: 15,
    },
    containerListFooterItem: {
      paddingHorizontal: 10,
      paddingVertical: 6,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 15,
      alignSelf: 'center',
      maxWidth: 100,
    },
  });
