import {View, Text} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';

const tabItems = [
  {
    id: 1,
    name: 'Home',
    isSelected: true,
  },
  {
    id: 2,
    name: 'My Forum',
    isSelected: false,
  },
  {
    id: 3,
    name: 'Others Forum',
    isSelected: false,
  },
  {
    id: 4,
    name: 'Events',
    isSelected: false,
  },
];

const useForumMainPage = () => {
  const authMethod = useSelector(state => state.auth);
  const tabHeight = useBottomTabBarHeight();
  const [dataTabs, setDataTabs] = React.useState(tabItems);

  const handleClickTabs = value => {
    const result = dataTabs.map(tab => {
      if (tab === value) {
        tab.isSelected = !tab.isSelected;
      } else {
        tab.isSelected = false;
      }
      return tab;
    });
    setDataTabs(result);
  };

  return {
    tabHeight,
    dataTabs,
    setDataTabs,
    handleClickTabs,
  };
};

export default useForumMainPage;
