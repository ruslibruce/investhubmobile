import React from 'react';
import useGetCourseDetail from '../../api/useGetCourseDetail';
import {useSelector} from 'react-redux';
import useGetCourseDetailProgress from '../../api/useGetCourseDetailProgress';
import {useIsFocused} from '@react-navigation/native';
import {magicModal} from 'react-native-magic-modal';
import CommentComponent from '../../components/CommentComponent';
import {useTranslation} from 'react-i18next';

const useCourseContent = (id, content) => {
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [contentData, setContentData] = React.useState(null);
  const [contentDataPDF, setContentDataPDF] = React.useState(null);
  const {dataCourseDetail, accessCourseDetail, isLoading} = authMethod.isLogin
    ? useGetCourseDetailProgress()
    : useGetCourseDetail();
  const focused = useIsFocused();
  const [isModalVisibleComment, setIsModalVisibleComment] =
    React.useState(false);

  React.useEffect(() => {
    if (focused && content) {
      if (content.content_type.toLowerCase() === 'article') {
        accessCourseDetail(id, true);
        setContentDataPDF(content);
        setContentData(null);
      } else {
        accessCourseDetail(id, true);
        setContentDataPDF(null);
        setContentData(content);
      }
    }
  }, [content, focused]);

  React.useEffect(() => {
    if (isModalVisibleComment) {
      magicModal.show(
        () => (
          <CommentComponent
            dataCourseDetail={contentData ? contentData : contentDataPDF}
            isModalVisibleComment={isModalVisibleComment}
            setIsModalVisibleComment={setIsModalVisibleComment}
            type="content"
          />
        ),
        {
          style: {
            justifyContent: 'flex-end',
          },
          // This is important to make the modal scroll properly
          swipeDirection: undefined,
          onBackButtonPress: () => {
            setIsModalVisibleComment(false);
          },
          onBackdropPress: () => {
            setIsModalVisibleComment(false);
          },
        },
      );
    } else {
      magicModal.hide();
    }
  }, [isModalVisibleComment]);

  return {
    selectedIndex,
    setSelectedIndex,
    dataCourseDetail,
    contentData,
    contentDataPDF,
    isLoading,
    authMethod,
    accessCourseDetail,
    setIsModalVisibleComment,
    translate,
  };
};

export default useCourseContent;
