import {Layout, Tab, TabView} from '@ui-kitten/components';
import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {IconRoundCheckGreen} from '../../assets/svg';
import CourseContentComponent from '../../components/CourseContentComponent';
import VideoPlayer from '../../components/VideoPlayer';
import {Colors, isYoutubeUrl} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useCourseContent from './useCourseContent';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import PDFViewer from '../../components/PDFViewer';
import LoadingSpinner from '../../components/LoadingSpinner';
import NoData from '../../components/NoData';
import VideoPlayerYoutube from '../../components/VideoPlayerYoutube';

const CourseContent = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const {
    selectedIndex,
    setSelectedIndex,
    dataCourseDetail,
    contentData,
    contentDataPDF,
    isLoading,
    authMethod,
    accessCourseDetail,
    setIsModalVisibleComment,
    translate,
  } = useCourseContent(route.params?.id, route.params?.content);
  const {listRef, contentVerticalOffset, onScroll} = useStateFloat();
  const minutes = Math.floor(route.params?.content?.content_length / 60);
  const seconds = route.params?.content?.content_length % 60;
  const [isNavigateBack, setIsNavigateBack] = React.useState(false);
  const styles = styling(insets);
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={styles.buttonListHeader}
          onPress={() => {
            setIsNavigateBack(true);
            navigation.goBack();
          }}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.PRIMARY} />
        </TouchableOpacity>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity style={styles.buttonListHeaderRight}>
            <Feather name={'share-2'} size={20} color={Colors.PRIMARY} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonListHeaderRight}>
            <Feather name={'bookmark'} size={20} color={Colors.PRIMARY} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.container}>
        {isLoading ? (
          <LoadingSpinner isFullScreen={true} />
        ) : (
          <ScrollView
            ref={listRef}
            onScroll={onScroll}
            scrollEventThrottle={16}
            contentContainerStyle={styles.contentScrollView}>
            <SizedBox height={23} />
            {contentData ? (
              <>
                {isYoutubeUrl(contentData.content_file) ? (
                  <VideoPlayerYoutube
                    content={contentData}
                    idProgress={dataCourseDetail?.participant_progress?.id}
                    idCourse={route.params?.id}
                    accessCourseDetail={accessCourseDetail}
                    isNavigateBack={isNavigateBack}
                  />
                ) : (
                  <VideoPlayer
                    content={contentData}
                    idProgress={dataCourseDetail?.participant_progress?.id}
                    idCourse={route.params?.id}
                    accessCourseDetail={accessCourseDetail}
                    isNavigateBack={isNavigateBack}
                  />
                )}
              </>
            ) : contentDataPDF ? (
              <PDFViewer
                url={contentDataPDF?.content_file}
                content={contentDataPDF}
                idProgress={dataCourseDetail?.participant_progress?.id}
                idCourse={route.params?.id}
                accessCourseDetail={accessCourseDetail}
                isNavigateBack={isNavigateBack}
              />
            ) : null}
            <SizedBox height={20} />
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={styles.textTitleCourse}>
                {contentData?.title || contentDataPDF?.title}
              </Text>
              {authMethod.isLogin && (
                <>
                  {contentData && (
                    <TouchableOpacity
                      style={styles.buttonComment}
                      onPress={() => setIsModalVisibleComment(true)}>
                      <Text style={styles.textComment}>
                        {translate('CommentButton')}
                      </Text>
                    </TouchableOpacity>
                  )}
                  {contentDataPDF && (
                    <TouchableOpacity
                      style={styles.buttonComment}
                      onPress={() => setIsModalVisibleComment(true)}>
                      <Text style={styles.textComment}>
                        {translate('CommentButton')}
                      </Text>
                    </TouchableOpacity>
                  )}
                </>
              )}
            </View>
            <SizedBox height={10} />
            <View style={styles.containerGroupTimeProgress}>
              {/* <View style={styles.containerArchievements}>
              <IconRoundCheckGreen />
              <Text style={styles.textPassed}>{'Passed'}</Text>
            </View> */}
              <Text style={styles.textSecondary}>{`${minutes
                .toString()
                .padStart(2, '0')}:${seconds
                .toString()
                .padStart(2, '0')}`}</Text>
            </View>
            {/* <SizedBox height={10} />
          <Text style={styles.textSecondary}>
            {
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ipsum dolor sit amet, consectetur.'
            }
          </Text> */}
            <SizedBox height={10} />
            <TabView
              selectedIndex={selectedIndex}
              tabBarStyle={styles.tabContainer}
              indicatorStyle={styles.indicatorStyle}
              onSelect={index => setSelectedIndex(index)}>
              <Tab
                title={evaProps => (
                  <Text
                    {...evaProps}
                    style={[
                      styles.textTab,
                      {
                        color:
                          selectedIndex === 0
                            ? Colors.PRIMARY
                            : Colors.TEXTSECONDARY,
                      },
                    ]}>
                    {'Course Content'}
                  </Text>
                )}>
                <Layout style={styles.tabContainer}>
                  <SizedBox height={20} />
                  {dataCourseDetail?.sections?.length === 0 ? (
                    <View style={{marginTop: 50}}>
                      <NoData />
                    </View>
                  ) : (
                    <CourseContentComponent
                      id={route.params?.id}
                      content={dataCourseDetail}
                      navigation={navigation}
                      authMethod={authMethod}
                    />
                  )}
                </Layout>
              </Tab>
              {/* <Tab
              title={evaProps => (
                <Text {...evaProps} style={[styles.textTab, {color: selectedIndex === 1 ? Colors.PRIMARY : Colors.TEXTSECONDARY}]}>
                  {'Discussion'}
                </Text>
              )}>
              <Layout style={styles.tabContainer}>
                <Text category="h5">Discussion</Text>
              </Layout>
            </Tab> */}
            </TabView>
          </ScrollView>
        )}
        <IconFloat
          bottom={70}
          contentVerticalOffset={contentVerticalOffset}
          listRef={listRef}
          isScrollView={true}
        />
      </View>
    </View>
  );
};

export default CourseContent;
