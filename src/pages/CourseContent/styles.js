import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets => StyleSheet.create({
  container: {
    backgroundColor: Colors.WHITE,
    flex: 1,
    position: 'relative',
    paddingTop: insets.top,
  },
  contentScrollView: {
    backgroundColor: Colors.WHITE,
    paddingBottom: insets.top,
    paddingHorizontal: 20,
  },
  containerHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 30,
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 20,
  },
  buttonListHeader: {
    backgroundColor: Colors.WHITE,
    borderWidth: 1,
    borderColor: Colors.PRIMARY,
    height: 32,
    width: 32,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerButtonHeaderRight: {
    flexDirection: 'row',
    gap: 10,
  },
  buttonListHeaderRight: {
    backgroundColor: Colors.WHITE,
    borderWidth: 1,
    borderColor: Colors.PRIMARY,
    height: 32,
    width: 32,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerListHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    position: 'absolute',
    marginTop: 100,
    marginLeft: 20,
  },
  textTitleCourse: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(20),
    color: Colors.BLACK,
  },
  containerGroupTimeProgress: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
  },
  containerArchievements: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
    backgroundColor: '#E9F9F1',
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  textPassed: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.GREEN,
  },
  textSecondary: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.TEXTSECONDARY,
  },
  textTab: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
  },
  tabContainer: {
    backgroundColor: Colors.WHITE,
  },
  indicatorStyle: {
    height: 1,
  },
  textComment: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.WHITE,
  },
  buttonComment: {
    backgroundColor: Colors.PRIMARY,
    padding: 10,
    borderRadius: 5,
    alignItems: 'center',
  },
});
