import React from 'react';
import {useSelector} from 'react-redux';
import useGetCollection from '../../api/useGetCollection';
import usePostCollection from '../../api/usePostCollection';

const useCollection = () => {
  const authMethod = useSelector(state => state.auth);
  const {dataCollection, accessCollection, isLoading: isLoadingGetCollection} = useGetCollection();
  const {accessPostCollection, isLoading: isLoadingPostCollection} = usePostCollection();
  const [isModalCollection, setIsModalCollection] = React.useState(false);
  const [textAdd, setTextAdd] = React.useState('');
  const [itemCollection, setItemCollection] = React.useState([]);

  React.useEffect(() => {
    if (dataCollection) {
      setItemCollection(dataCollection);
    }
  }, [dataCollection]);

  const handleShowModal = () => {
    setIsModalCollection(prev => !prev);
  };

  const onChangeAddCollection = value => {
    setTextAdd(prev => (prev = value));
  };

  const handleSaveCollection = async value => {
    const result = await accessPostCollection(value);
    if (result) {
      accessCollection();
      handleShowModal();
      setTextAdd(prev => (prev = ''));
    }
  };

  return {
    itemCollection,
    handleShowModal,
    isModalCollection,
    onChangeAddCollection,
    textAdd,
    handleSaveCollection,
    isLoadingGetCollection,
    isLoadingPostCollection,
  };
};

export default useCollection;
