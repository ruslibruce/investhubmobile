import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.WHITE,
      position: 'relative',
    },
    containerHeader: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: insets.top,
      backgroundColor: Colors.WHITE,
      paddingHorizontal: 20,
      paddingBottom: 5,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
    },
    containerButtonHeaderRight: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 17.5,
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    contentFlatlist: {
      width: matrics.screenWidth,
      paddingHorizontal: 20,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    marginScrollView: {
      // width: '100%',
    },
    containerScroll: {
      paddingVertical: 8,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopColor: '#E8E8E8',
      borderBottomColor: '#E8E8E8',
    },
    contentScroll: {
      paddingLeft: 20,
    },
    containerViewDateMarkAll: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginVertical: 21.5,
    },
    textDate: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
      lineHeight: 19.07,
    },
    textMarkAll: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
      lineHeight: 16.34,
    },
    containerCard: {
      backgroundColor: Colors.WHITE,
      borderColor: Colors.WHITE,
    },
    viewCard: {
      marginHorizontal: -20,
      marginVertical: -11,
      paddingVertical: 11,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    viewIcon: {
      width: 40,
      height: 40,
      borderRadius: 40,
      backgroundColor: Colors.PINK,
      justifyContent: 'center',
      alignItems: 'center',
    },
    containerTextNotif: {
      width: '70%',
    },
    textDateNotif: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    containerSubNotif: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 6,
    },
    textCollection: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      lineHeight: 16,
    },
    buttonAddCollection: {
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      borderRadius: 12,
      paddingHorizontal: 10,
      paddingVertical: 5,
    },
    containerLeft: {
      flexDirection: 'row',
      gap: 16,
      alignItems: 'center',
    },
    containerIcon: {
      borderRadius: 80,
      backgroundColor: Colors.PINK,
      padding: 10,
    },
    containerButtonModal: {
      flexDirection: 'row',
      gap: 10,
    },
    textHeaderModal: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      lineHeight: 16,
    },
    styleButtonCancel: {
      width: '48%',
      backgroundColor: Colors.WHITE,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
    },
    styleButtonSave: {
      width: '48%',
    },
    textButtonCancel: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.PRIMARY,
      lineHeight: 18,
    },
  });
