import {Card} from '@ui-kitten/components';
import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import MyModalBottom from '../../components/MyModalBottom';
import {
  COLLECTION_DETAIL,
  Colors,
  DASHBOARD_COURSE,
  SEARCH_ALL,
  TAB_COURSE_MAIN,
} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import TextInputCustom from '../../widgets/TextInputCostum';
import {styling} from './styles';
import useCollection from './useCollection';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import NoData from '../../components/NoData';
import LoadingSpinner from '../../components/LoadingSpinner';

const CollectionPage = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const {
    itemCollection,
    handleShowModal,
    isModalCollection,
    onChangeAddCollection,
    textAdd,
    handleSaveCollection,
    isLoadingGetCollection,
    isLoadingPostCollection,
  } = useCollection();
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  const styles = styling(insets);
  const renderHeader = () => (
    <View style={styles.containerViewDateMarkAll}>
      <Text style={styles.textDate}>Collection Course</Text>
      <TouchableOpacity
        style={styles.buttonAddCollection}
        onPress={handleShowModal}>
        <Text style={styles.textMarkAll}>Add Collection</Text>
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={styles.container}>
      <StatusCustomBarDark />
      <View style={styles.containerHeader}>
        <View style={styles.containerButtonHeaderRight}>
          <TouchableOpacity
            style={styles.buttonListHeader}
            onPress={() => navigation.goBack()}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>My Collections</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate(SEARCH_ALL)}
          style={styles.buttonListHeaderRight}>
          <FontAwesome5 name={'search'} size={15} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
      <SizedBox height={8} />
      {isLoadingGetCollection ? (
        <LoadingSpinner />
      ) : (
        <FlatList
          data={itemCollection}
          ref={listRef}
          onScroll={onScroll}
          ListHeaderComponent={renderHeader()}
          contentContainerStyle={[
            styles.contentFlatlist,
            {
              paddingBottom: 150,
            },
          ]}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <Card style={styles.containerCard}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate(COLLECTION_DETAIL, {item: item});
                }}
                style={styles.viewCard}>
                <View style={styles.containerLeft}>
                  <View style={styles.containerIcon}>
                    <Feather
                      name={'bookmark'}
                      size={20}
                      color={Colors.PRIMARY}
                    />
                  </View>
                  <Text style={styles.textCollection}>{item.name}</Text>
                </View>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate(DASHBOARD_COURSE, {
                      screen: TAB_COURSE_MAIN,
                    })
                  }>
                  <AntDesign
                    color={Colors.PRIMARY}
                    size={18}
                    name="pluscircleo"
                  />
                </TouchableOpacity>
              </TouchableOpacity>
            </Card>
          )}
          ListEmptyComponent={() => <NoData />}
        />
      )}
      <IconFloat
        bottom={70}
        listRef={listRef}
        contentVerticalOffset={contentVerticalOffset}
      />
      <BottomTabOnlyView />
      <MyModalBottom
        visible={isModalCollection}
        onBackdropPress={handleShowModal}>
        <Text style={styles.textHeaderModal}>Collection Name</Text>
        <SizedBox height={16} />
        <TextInputCustom
          value={textAdd}
          placeholder={'Enter Collection Name'}
          placeholderTextColor={Colors.GRAY}
          styleInput={styles.textHeaderModal}
          onChangeText={onChangeAddCollection}
        />
        <SizedBox height={24} />
        <View style={styles.containerButtonModal}>
          <ButtonPrimary
            onPress={handleShowModal}
            style={styles.styleButtonCancel}
            styleLabel={styles.textButtonCancel}
            label={'Cancel'}
          />
          <ButtonPrimary
            loading={isLoadingPostCollection}
            disabled={isLoadingPostCollection}
            onPress={() => handleSaveCollection(textAdd)}
            style={styles.styleButtonSave}
            label={'Save'}
          />
        </View>
      </MyModalBottom>
    </View>
  );
};

export default CollectionPage;
