import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {magicModal} from 'react-native-magic-modal';
import Share from 'react-native-share';
import {useSelector} from 'react-redux';
import useGetCourseDetail from '../../api/useGetCourseDetail';
import useGetCourseDetailProgress from '../../api/useGetCourseDetailProgress';
import usePostEnrollCourse from '../../api/usePostEnrollCourse';
import usePostLike from '../../api/usePostLike';
import CommentComponent from '../../components/CommentComponent';
import {COURSE_DETAIL_PROGRESS} from '../../utility';
import useGetLike from '../../api/useGetLike';

const useCourseDetail = id => {
  const {t: translate} = useTranslation();
  const navigation = useNavigation();
  const authMethod = useSelector(state => state.auth);
  const {accessPostLike, isLoading: isLoadingPostLike, isSuccess: isSuccessPostLike} = usePostLike();
  const {
    accessGetLike,
    dataGetLike,
    dataResultLike,
    isLoading: isLoadingGetLike,
  } = useGetLike();
  const {dataCourseDetail, accessCourseDetail, isLoading} = authMethod.isLogin
    ? useGetCourseDetailProgress()
    : useGetCourseDetail();
  const {accessPostEnrollCourse, isLoading: isLoadingPostEnroll} = usePostEnrollCourse();
  const [isModalVisibleComment, setIsModalVisibleComment] =
    React.useState(false);

    React.useEffect(() => {
      if(isSuccessPostLike){
        accessGetLike(id);
      }
    }, [isSuccessPostLike])
    

  useFocusEffect(
    React.useCallback(() => {
      accessCourseDetail(id, true);
      authMethod.isLogin && accessGetLike(id);
    }, [id, authMethod]),
  );

  const handleContinueCourse = () => {
    navigation.navigate(COURSE_DETAIL_PROGRESS, {
      id: id,
    });
  };

  const handleCourseEnroll = () => {
    accessPostEnrollCourse({course_id: id, id: id});
  };
  const tabHeight = useBottomTabBarHeight();

  const handleOpenShare = url => {
    Share.open({
      url: url,
      title: 'Share via',
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };

  const handleLike = () => {
    accessPostLike({course_id: id});
  };

  React.useEffect(() => {
    if (isModalVisibleComment) {
      magicModal.show(
        () => (
          <CommentComponent
            dataCourseDetail={dataCourseDetail}
            isModalVisibleComment={isModalVisibleComment}
            setIsModalVisibleComment={setIsModalVisibleComment}
            type="course"
          />
        ),
        {
          style: {
            justifyContent: 'flex-end',
          },
          // This is important to make the modal scroll properly
          swipeDirection: undefined,
          onBackButtonPress: () => {
            setIsModalVisibleComment(false);
          },
          onBackdropPress: () => {
            setIsModalVisibleComment(false);
          },
        },
      );
    } else {
      magicModal.hide();
    }
  }, [isModalVisibleComment]);

  return {
    handleContinueCourse,
    tabHeight,
    dataCourseDetail,
    authMethod,
    handleCourseEnroll,
    isLoading,
    handleOpenShare,
    handleLike,
    setIsModalVisibleComment,
    dataGetLike,
    dataResultLike,
    isLoadingPostEnroll,
  };
};

export default useCourseDetail;
