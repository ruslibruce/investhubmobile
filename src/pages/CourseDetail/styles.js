import {Platform, StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (insets, tabHeight, value) =>
  StyleSheet.create({
    containerMain: {
      flex: 1,
      backgroundColor: Colors.WHITE,
      position: 'relative',
    },
    container: {
      backgroundColor: Colors.WHITE,
    },
    contentScrollView: {
      backgroundColor: Colors.WHITE,
      paddingBottom: Platform.OS === 'ios' ? tabHeight + 220 : tabHeight + 100,
      paddingHorizontal: 20,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonListHeaderRight: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerListHeader: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      position: 'absolute',
      top: insets.top,
      width: '100%',
      paddingHorizontal: 20,
    },
    textListHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(18),
      color: Colors.WHITE,
    },
    containerTopTitle: {
      flexDirection: 'row',
      gap: 16,
      alignItems: 'center',
    },
    containerIcon: {
      backgroundColor: Colors.PINK,
      width: 40,
      height: 40,
      borderRadius: 8,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerProgress: {
      width: '80%',
    },
    textTopTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(16),
      color: Colors.BLACK,
    },
    textTopSubTitle: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      color: Colors.GREEN,
    },
    progressColor: {
      backgroundColor: '#D9D9D9',
    },
    buttonColor: {
      backgroundColor: Colors.PINK,
      flexDirection: 'row',
      gap: 4,
      width: '50%',
    },
    buttonLabel: {
      color: Colors.PRIMARY,
    },
    containerBottomTitle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    containerBottomTitleTime: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    textBottomTitle: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
    },
    containerTitle: {
      borderColor: '#D9D9D9',
      borderTopWidth: 1,
      borderBottomWidth: 1,
      marginHorizontal: -20,
      paddingHorizontal: 20,
      paddingVertical: 20,
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    containerImage: {
      width: 'auto',
      height: 152,
      borderRadius: 12,
      overflow: 'hidden',
    },
    dotStyling: {
      backgroundColor: Colors.GRAY,
      width: 4,
      height: 4,
      borderRadius: 4,
    },
    textCourseName: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      color: value ? Colors.PRIMARY : '#696F79',
    },
    containerTextCourse: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    textCourseInfo: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
    },
    textCourseInfoRed: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
    },
    textCourseIcon: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: '#696F79',
    },
    textCourseAbout: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(16),
      color: Colors.BLACK,
    },
    textCourseDesc: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#696F79',
    },
    containerTag: {
      flexWrap: 'wrap',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      gap: 10,
    },
    containerTagChild: {
      backgroundColor: Colors.PINK,
      paddingVertical: 8,
      paddingHorizontal: 15,
      borderRadius: 15,
    },
    textTag: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
    },
    containerButton: {
      flexWrap: 'wrap',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    containerButtonChild: {
      backgroundColor: Colors.PINK,
      paddingHorizontal: 10,
      width: 60,
      borderRadius: 15,
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 2,
    },
    textTitleModal: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
    },
    textTitleComment: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
    },
    textComment: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
    },
    containerAvatar: {
      backgroundColor: Colors.PRIMARY,
      width: 20,
      height: 20,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    textAvatar: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.WHITE,
    },
  });
