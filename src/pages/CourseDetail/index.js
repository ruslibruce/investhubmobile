import {ProgressBar} from '@ui-kitten/components';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  IconCommentRed,
  IconEyeRed,
  IconMenuCourse,
  IconShareRed,
  IconStudentRed,
  IconThumbRed,
} from '../../assets/svg';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import {COLLECTION_CHOOSE, Colors, matrics} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useCourseDetail from './useCourseDetail';
import Config from 'react-native-config';
import {useMagicModal} from 'react-native-magic-modal';
import Feather from 'react-native-vector-icons/Feather';

const CourseDetail = ({navigation, route}) => {
  const {hide} = useMagicModal();
  const insets = useSafeAreaInsets();
  const {
    handleContinueCourse,
    tabHeight,
    dataCourseDetail,
    authMethod,
    handleCourseEnroll,
    isLoading,
    handleOpenShare,
    handleLike,
    setIsModalVisibleComment,
    dataGetLike,
    dataResultLike,
    isLoadingPostEnroll,
  } = useCourseDetail(route.params.id);
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  // console.log('dataCourseDetail', JSON.stringify(dataCourseDetail));
  const styles = styling(insets, tabHeight, true);
  return (
    <View style={styles.containerMain}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} placeholder={'Search Course'} />
      <View style={styles.containerListHeader}>
        <TouchableOpacity
          onPress={() => {
            if (route.params?.route && authMethod.isLogin) {
              navigation.navigate(route.params?.route, {
                id: route.params?.idPrevious,
                name: route.params?.namePrevious,
              });
              return;
            }
            navigation.goBack();
          }}
          style={styles.buttonListHeader}>
          <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
        </TouchableOpacity>
        <Text style={styles.textListHeader}>
          {'Find a best course for you!'}
        </Text>
        <SizedBox width={32} />
        {/* <TouchableOpacity style={styles.buttonListHeaderRight}>
          <IconBookmark />
        </TouchableOpacity> */}
      </View>
      <View style={styles.container}>
        {isLoading ? (
          <LoadingSpinner isFullScreen={true} />
        ) : (
          <ScrollView
            onScroll={onScroll}
            scrollEventThrottle={16}
            ref={listRef}
            contentContainerStyle={styles.contentScrollView}>
            <SizedBox height={16} />
            <View style={styles.containerTitle}>
              <View style={styles.containerTopTitle}>
                <View style={styles.containerIcon}>
                  <IconMenuCourse />
                </View>
                <View style={styles.containerProgress}>
                  <Text style={styles.textTopTitle}>
                    {dataCourseDetail?.title}
                  </Text>
                  {authMethod.isLogin && (
                    <>
                      <SizedBox height={5} />
                      <Text style={styles.textTopSubTitle}>{`${
                        dataCourseDetail?.participant_progress?.progress ?? 0
                      }% completed`}</Text>
                      <SizedBox height={5} />
                      <View style={styles.containerProgress}>
                        <ProgressBar
                          status={'success'}
                          style={styles.progressColor}
                          progress={
                            dataCourseDetail?.participant_progress
                              ? Number(
                                  dataCourseDetail?.participant_progress
                                    ?.progress,
                                ) / 100
                              : 0
                          }
                        />
                      </View>
                    </>
                  )}
                </View>
              </View>
              <SizedBox height={15} />
              <View style={styles.containerBottomTitle}>
                <View style={styles.containerBottomTitle}>
                  <MaterialCommunityIcons
                    name={'clock-outline'}
                    size={24}
                    color={Colors.PRIMARY}
                  />
                  <SizedBox width={6} />
                  <Text style={styles.textBottomTitle}>
                    {'3 hours, 20 min'}
                  </Text>
                </View>
                {authMethod.isLogin ? (
                  <ButtonPrimary
                    loading={isLoadingPostEnroll}
                    disabled={isLoadingPostEnroll}
                    styleLabel={styles.buttonLabel}
                    style={styles.buttonColor}
                    onPress={
                      dataCourseDetail?.participant_progress
                        ? handleContinueCourse
                        : handleCourseEnroll
                    }
                    label={
                      dataCourseDetail?.participant_progress
                        ? 'Continue Course'
                        : 'Start Course'
                    }
                    icon={
                      <MaterialCommunityIcons
                        name={'arrow-right'}
                        size={20}
                        color={Colors.PRIMARY}
                      />
                    }
                  />
                ) : (
                  <ButtonPrimary
                    styleLabel={styles.buttonLabel}
                    style={styles.buttonColor}
                    onPress={handleContinueCourse}
                    label={'View Course'}
                    icon={
                      <MaterialCommunityIcons
                        name={'arrow-right'}
                        size={20}
                        color={Colors.PRIMARY}
                      />
                    }
                  />
                )}
              </View>
            </View>
            <SizedBox height={20} />
            <View style={styles.containerImage}>
              <Image
                source={{uri: dataCourseDetail?.cover_image_file}}
                style={styles.widthImage}
              />
            </View>
            <SizedBox height={20} />
            <View style={styles.containerTextCourse}>
              <Text style={styles.textCourseName}>
                {dataCourseDetail?.conten_provider}
              </Text>
              <Text style={styles.textCourseName}>{' ( 3 Section '}</Text>
              <View style={styles.dotStyling} />
              <Text style={styles.textCourseName}>{' 21 Course )'}</Text>
            </View>
            <SizedBox height={6} />
            <View style={styles.containerTextCourse}>
              <Text style={styles.textCourseInfo}>{'Instructor by '}</Text>
              <Text style={styles.textCourseInfoRed}>{'Henry S. Miller'}</Text>
            </View>
            <SizedBox height={17} />
            <View style={styles.containerTextCourse}>
              <IconStudentRed />
              <SizedBox width={5} />
              <Text
                style={
                  styles.textCourseIcon
                }>{`${dataCourseDetail?.number_of_participants} students`}</Text>
              <SizedBox width={10} />
              <IconEyeRed />
              <SizedBox width={5} />
              <Text
                style={
                  styles.textCourseIcon
                }>{`${dataCourseDetail?.number_of_recommendations} view`}</Text>
            </View>
            <SizedBox height={26} />
            <Text style={styles.textCourseAbout}>{'About Course'}</Text>
            <SizedBox height={6} />
            <Text style={styles.textCourseDesc}>
              {dataCourseDetail?.description}
            </Text>
            <SizedBox height={26} />
            <Text style={styles.textCourseAbout}>{'Category'}</Text>
            <SizedBox height={16} />
            <View style={styles.containerTag}>
              <View style={styles.containerTagChild}>
                <Text style={styles.textTag}>{dataCourseDetail?.category}</Text>
              </View>
            </View>
            <SizedBox height={26} />
            <Text style={styles.textCourseAbout}>
              {'What will you learn in this online course?'}
            </Text>
            <SizedBox height={6} />
            <Text style={styles.textCourseDesc}>
              {dataCourseDetail?.description}
            </Text>
            <SizedBox height={15} />
            {authMethod.isLogin && dataGetLike && (
              <View style={{flexDirection: 'row'}}>
                {dataGetLike?.map((val, ind) => {
                  if (ind > 2) return null;
                  return (
                    <View
                      key={val.id}
                      style={[
                        styles.containerAvatar,
                        {marginLeft: ind === 0 ? 0 : -5},
                      ]}
                      size={20}>
                      <Text style={styles.textAvatar}>
                        {val.name.slice(0, 1).toLowerCase()}
                      </Text>
                    </View>
                  );
                })}
                {dataResultLike.count > 3 && (
                  <View style={[styles.containerAvatar, {marginLeft: -5}]}>
                    <Text style={styles.textAvatar}>
                      {`+${Number(dataResultLike.count) - 3}`}
                    </Text>
                  </View>
                )}
              </View>
            )}
            <SizedBox height={15} />
            {authMethod.isLogin && (
              <View style={styles.containerButton}>
                <TouchableOpacity
                  onPress={handleLike}
                  style={styles.containerButtonChild}>
                  {dataResultLike.liked ? (
                    <AntDesign
                      name="like1"
                      size={20}
                      style={{color: Colors.PRIMARY}}
                    />
                  ) : (
                    <AntDesign
                      name="like2"
                      size={20}
                      style={{color: Colors.PRIMARY}}
                    />
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setIsModalVisibleComment(true);
                  }}
                  style={styles.containerButtonChild}>
                  <IconCommentRed width={24} height={24} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate(COLLECTION_CHOOSE, {
                      id: dataCourseDetail?.id,
                    })
                  }
                  style={styles.containerButtonChild}>
                  <Feather name={'bookmark'} size={20} color={Colors.PRIMARY} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() =>
                    handleOpenShare(
                      `${Config.BASE_URL}/courses/detail/${dataCourseDetail?.id}`,
                    )
                  }
                  style={styles.containerButtonChild}>
                  <IconShareRed width={24} height={24} />
                </TouchableOpacity>
              </View>
            )}
          </ScrollView>
        )}
      </View>
      <IconFloat
        bottom={
          Platform.OS === 'ios'
            ? matrics.screenHeight / 4.3
            : matrics.screenHeight / 4.45
        }
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
    </View>
  );
};

export default CourseDetail;
