import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Text, View} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import Octicons from 'react-native-vector-icons/Octicons';
import usePostSubmitQuizLevelUp from '../../api/usePostSubmitQuizLevelUp';
import {Colors} from '../../utility';
import ModalCustom from '../../widgets/ModalCustom';
import useGetQuizLevelUp from '../../api/useGetQuizLevelUp';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import usePostRetakeQuizLevelUp from '../../api/usePostRetakeQuizLevelUp';

const useCourseQuiz = (id, styles) => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const {
    dataQuizLevelUp,
    isLoading: isLoadingQuiz,
    accessQuiz,
  } = useGetQuizLevelUp();
  const navigation = useNavigation();
  const [dataExercise, setDataExercise] = React.useState([]);
  const [selectedIndex, setSelectedIndex] = React.useState('');
  const [indexSoal, setIndexSoal] = React.useState(0);
  const {
    dataSubmitQuiz,
    setDataSubmitQuiz,
    isLoading: isLoadingPostSubmit,
    accessSubmit,
  } = usePostSubmitQuizLevelUp();
  const time = new Date();
  const [timer, setTimer] = React.useState(0);
  const {isSuccess, retakeDataQuiz} = usePostRetakeQuizLevelUp();

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod]);

  React.useEffect(() => {
    accessQuiz(id);
  }, []);

  const hideModalSukses = () => {
    magicModal.hide();
    setDataSubmitQuiz('');
    navigation.goBack();
  };

  const handleSubmitLevelUp = (dataExercise, dataQuizLevelUp) => {
    let answer = {};
    dataExercise.map(item => {
      answer[item.id] = item.answer;
    });
    console.log('dataExercise', dataExercise);
    console.log('dataQuizLevelUp', dataQuizLevelUp);
    accessSubmit(dataQuizLevelUp.id, answer);
  };

  React.useEffect(() => {
    if (dataSubmitQuiz) {
      console.log(`dataSubmitQuiz`, dataSubmitQuiz);
      magicModal.show(() => (
        <ModalCustom
          hideModalSukses={hideModalSukses}
          iconModal={
            <View style={styles.containerModalIcon}>
              <Octicons
                name={'check-circle-fill'}
                size={40}
                color={Colors.GREEN}
              />
            </View>
          }
          singleButton={true}
          textButton={'OK'}
          textModalJudul={
            dataSubmitQuiz.is_passed ? 'Congratulations !' : 'Sorry !'
          }
          textCustom={
            <View style={{alignItems: 'center', marginTop: 24, gap: 5}}>
              <Text
                style={
                  styles.textModalRed
                }>{`Score : ${dataSubmitQuiz.score}`}</Text>
            </View>
          }
          containerButtonSingle={styles.containerButtonSingle}
        />
      ));
      return;
    }

    if (isSuccess) {
      navigation.goBack();
      return;
    }
  }, [dataSubmitQuiz, isSuccess]);

  const handleChooseRadioAnswer = React.useCallback(
    (idx, indexSoal, dataExercise) => {
      dataExercise[indexSoal]?.options?.map((key, ind) => {
        if (idx == ind) {
          dataExercise[indexSoal].answer = key.option;
          setSelectedIndex(idx);
        }
      });
    },
    [],
  );

  const handleButtonLeft = () => {
    setSelectedIndex('');
    let resulIndex = indexSoal === 0 ? indexSoal : indexSoal - 1;
    setIndexSoal(resulIndex);
    dataExercise[resulIndex]?.options?.map((key, ind) => {
      if (key.option == dataExercise[resulIndex].answer) {
        setSelectedIndex(ind);
      }
    });
  };

  const handleButtonRight = () => {
    setSelectedIndex('');
    let resulIndex =
      indexSoal === dataExercise.length - 1 ? indexSoal : indexSoal + 1;
    setIndexSoal(resulIndex);
    dataExercise[resulIndex]?.options?.map((key, ind) => {
      if (key.option == dataExercise[resulIndex].answer) {
        setSelectedIndex(ind);
      }
    });
  };

  React.useEffect(() => {
    if (dataQuizLevelUp) {
      let dataTemp = dataQuizLevelUp;
      if (dataTemp.is_completed) {
        let dataAnswer = JSON.parse(dataTemp.answers);
        let dataReview = JSON.parse(dataTemp.review);
        dataTemp.questions.map(item => {
          let dataReviewResult = dataReview.find(
            key => key.question_id === item.id,
          );
          item.answer = dataAnswer[item.id];
          if (dataReviewResult) {
            item.review = dataReviewResult;
          }
          return item;
        });
        setDataExercise(dataTemp.questions);
        let index = dataTemp.questions[indexSoal].options.findIndex(
          item => item.option === Object.values(dataAnswer)[0],
        );
        setSelectedIndex(index);
        setTimer(0);
      } else {
        let dataAnswer = JSON.parse(dataTemp.answers);
        dataTemp.questions.map(item => {
          item.answer = dataAnswer ? dataAnswer[item.id] : '';
          return item;
        });
        setDataExercise(dataTemp.questions);
        setTimer(
          time.setSeconds(
            time.getSeconds() + dataTemp.remaining_time &&
              dataTemp.remaining_time > 0
              ? dataTemp.remaining_time
              : dataTemp.duration * 60,
          ),
        );
        if (dataAnswer) {
          let index = dataTemp.questions[indexSoal].options.findIndex(
            item => item.option === Object.values(dataAnswer)[0],
          );
          setSelectedIndex(index);
          return;
        }
        setSelectedIndex('');
      }
    }
  }, [dataQuizLevelUp]);

  const handleTimeCountDown = React.useCallback(() => {
    // alert('Time is over');
    handleSubmitLevelUp(dataExercise, dataQuizLevelUp);
  }, [dataExercise, dataQuizLevelUp]);

  return {
    dataExercise,
    selectedIndex,
    indexSoal,
    handleChooseRadioAnswer,
    handleButtonLeft,
    handleButtonRight,
    handleSubmitLevelUp,
    translate,
    timer,
    handleTimeCountDown,
    dataQuizLevelUp,
    retakeDataQuiz,
    isLoadingQuiz,
  };
};

export default useCourseQuiz;
