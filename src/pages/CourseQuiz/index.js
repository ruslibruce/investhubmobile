import {Layout, Radio, RadioGroup} from '@ui-kitten/components';
import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import {IconChevLeft, IconChevRight, IconTimer} from '../../assets/svg';
import LoadingSpinner from '../../components/LoadingSpinner';
import WebDisplay from '../../components/WebDisplay';
import {arrayContainsEmptyString, Colors, getStatusColor} from '../../utility';
import MyTimer from '../../widgets/MyTimer';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarDark} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useCourseQuiz from './useCourseQuiz';
import NoData from '../../components/NoData';

const CourseQuizPage = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const styles = styling(insets);
  const {
    dataExercise,
    selectedIndex,
    indexSoal,
    handleChooseRadioAnswer,
    handleButtonLeft,
    handleButtonRight,
    handleSubmitLevelUp,
    translate,
    timer,
    handleTimeCountDown,
    dataQuizLevelUp,
    retakeDataQuiz,
    isLoadingQuiz,
  } = useCourseQuiz(route.params?.id, styles);
  return (
    <ScrollView contentContainerStyle={styles.contentScrollView}>
      <StatusCustomBarDark />
      <View style={styles.containerListHeader}>
        <View style={styles.containerHeader}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.buttonListHeader}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.PRIMARY} />
          </TouchableOpacity>
          <Text style={styles.textListHeader}>{route.params?.name}</Text>
        </View>
        {dataQuizLevelUp?.duration !== 0 && dataExercise.length > 0 && (
          <View style={styles.containerTimer}>
            {timer > 0 ? (
              <>
                <IconTimer />
                <SizedBox width={8} />
                <MyTimer
                  expiryTimestamp={timer}
                  handleTimeCountDown={handleTimeCountDown}
                  dataExercise={dataExercise}
                  id={dataQuizLevelUp?.id}
                />
              </>
            ) : null}
            {dataQuizLevelUp?.is_completed && (
              <>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row',
                    marginRight: 20,
                  }}>
                  <View>
                    <Text style={styles.textHeader}>{'Score'}</Text>
                  </View>
                  <View>
                    <Text style={styles.textHeader}>{'  :  '}</Text>
                  </View>
                  <View>
                    <Text style={styles.textHeader}>
                      {dataQuizLevelUp?.score}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', gap: 10}}>
                  <TouchableOpacity
                    onPress={() => retakeDataQuiz(dataQuizLevelUp?.id)}
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: getStatusColor('pending'),
                      padding: 8,
                      borderRadius: 8,
                    }}>
                    <Text
                      style={[
                        styles.textButtonExercise,
                        {color: Colors.WHITE},
                      ]}>
                      {translate('RetakeQuiz')}
                    </Text>
                  </TouchableOpacity>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: dataQuizLevelUp?.is_passed
                        ? '#00C82C'
                        : '#F22350',
                      padding: 8,
                      borderRadius: 8,
                    }}>
                    <Text
                      style={[
                        styles.textButtonExercise,
                        {color: Colors.WHITE},
                      ]}>
                      {dataQuizLevelUp?.is_passed
                        ? translate('Passed')
                        : translate('Failed')}
                    </Text>
                  </View>
                </View>
              </>
            )}
          </View>
        )}
      </View>
      <SizedBox height={21} />
      {!isLoadingQuiz && dataQuizLevelUp ? (
        <>
          {dataExercise.length === 0 ? (
            <NoData />
          ) : (
            <>
              <View style={styles.containerExercise}>
                <WebDisplay
                  html={dataExercise[indexSoal]?.question}
                  styleText={styles.textExercise}
                />
                <SizedBox height={16} />
                <Layout style={styles.layoutRadio} level="1">
                  <RadioGroup
                    style={styles.containerRadio}
                    selectedIndex={selectedIndex}
                    onChange={idx => {
                      handleChooseRadioAnswer(idx, indexSoal, dataExercise);
                    }}>
                    {dataExercise[indexSoal]?.options.map((val, idx) => (
                      <Radio
                        disabled={dataQuizLevelUp?.is_completed ? true : false}
                        key={idx}>
                        {evaProps => (
                          <View
                            style={{
                              flexDirection: 'row',
                              flex: 1,
                              alignItems: 'center',
                              paddingRight: 5,
                            }}>
                            <WebDisplay
                              html={val.value}
                              // isOneLine={true}
                              styleText={styles.textRadio}
                            />
                            {dataExercise[indexSoal].review?.answer ==
                              val.option && (
                              <>
                                {dataExercise[indexSoal].review?.correct ? (
                                  <Octicons
                                    name="x-circle-fill"
                                    size={25}
                                    color={'#00C82C'}
                                  />
                                ) : (
                                  <Octicons
                                    name="x-circle-fill"
                                    size={25}
                                    color={'#F22350'}
                                  />
                                )}
                              </>
                            )}
                          </View>
                        )}
                      </Radio>
                    ))}
                  </RadioGroup>
                </Layout>
              </View>
              <View style={styles.containerViewButton}>
                <View style={styles.containerButton}>
                  {indexSoal > 0 ? (
                    <TouchableOpacity
                      onPress={handleButtonLeft}
                      style={styles.buttonExercise}>
                      <IconChevLeft />
                      <Text style={styles.textButtonExercise}>Back</Text>
                    </TouchableOpacity>
                  ) : (
                    <View />
                  )}
                  {indexSoal === dataExercise.length - 1 ? (
                    <>
                      {arrayContainsEmptyString(dataExercise) ? (
                        <TouchableOpacity disabled style={styles.buttonMidGray}>
                          <Text style={styles.textSubmitGray}>
                            {translate('Submit')}
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <>
                          {dataQuizLevelUp.is_completed ? null : (
                            // <TouchableOpacity
                            //   onPress={handleButtonLeft}
                            //   style={styles.buttonExercise}>
                            //   <Text style={styles.textButtonExercise}>Previous</Text>
                            //   <IconChevRight />
                            // </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() =>
                                handleSubmitLevelUp(
                                  dataExercise,
                                  dataQuizLevelUp,
                                )
                              }
                              style={styles.buttonExercise}>
                              <Text style={styles.textButtonExercise}>
                                {translate('Submit')}
                              </Text>
                            </TouchableOpacity>
                          )}
                        </>
                      )}
                    </>
                  ) : null}
                  {indexSoal < dataExercise.length - 1 && (
                    <TouchableOpacity
                      onPress={handleButtonRight}
                      style={styles.buttonExercise}>
                      <Text style={styles.textButtonExercise}>Next</Text>
                      <IconChevRight />
                    </TouchableOpacity>
                  )}
                </View>
                <SizedBox height={10} />
                <Text style={styles.textPagination}>{`Question ${
                  indexSoal + 1
                } of ${dataExercise.length}`}</Text>
              </View>
            </>
          )}
        </>
      ) : (
        <LoadingSpinner isFullScreen={true} />
      )}
    </ScrollView>
  );
};

export default CourseQuizPage;
