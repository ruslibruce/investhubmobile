import {Layout, Radio, RadioGroup} from '@ui-kitten/components';
import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import {IconChevLeft, IconChevRight, IconTimer} from '../../assets/svg';
import BottomTabOnlyView from '../../components/BottomTabOnlyView';
import HeaderSearch from '../../components/HeaderSearch';
import LoadingSpinner from '../../components/LoadingSpinner';
import WebDisplay from '../../components/WebDisplay';
import {arrayContainsEmptyString, Colors, getStatusColor} from '../../utility';
import IconFloat from '../../widgets/IconFloat';
import useStateFloat from '../../widgets/IconFloat/useStateFloat';
import MyTimer from '../../widgets/MyTimer';
import SizedBox from '../../widgets/SizedBox';
import {StatusCustomBarRed} from '../../widgets/StatusCustomBar';
import {styling} from './styles';
import useLevelUp from './useLevelUp';

const LevelUp = ({navigation, route}) => {
  const insets = useSafeAreaInsets();
  const styles = styling(insets);
  const {
    dataExercise,
    selectedIndex,
    indexSoal,
    timer,
    handleSubmitLevelUp,
    handleChooseRadioAnswer,
    handleTimeCountDown,
    handleButtonLeft,
    handleButtonRight,
    dataQuizLevelUp,
    translate,
    retakeDataQuiz,
    isLoadingApi,
  } = useLevelUp(route.params?.id, styles);
  const {contentVerticalOffset, listRef, onScroll} = useStateFloat();
  console.log('dataQuizLevelUp', dataQuizLevelUp)

  return (
    <View style={styles.container}>
      <StatusCustomBarRed />
      <HeaderSearch height={100 + insets.top} hiddenSearch={true} />
      <View style={styles.containerHeader}>
        <View style={{flexDirection: 'row', gap: 10, alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.buttonListHeader}>
            <AntDesign name={'arrowleft'} size={24} color={Colors.BLACK} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>Level Up Test</Text>
        </View>
        {dataQuizLevelUp?.is_completed && (
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
              flexDirection: 'row',
            }}>
            <View>
              <Text style={styles.textHeader}>{'Score'}</Text>
            </View>
            <View>
              <Text style={styles.textHeader}>{'  :  '}</Text>
            </View>
            <View>
              <Text style={styles.textHeader}>{dataQuizLevelUp?.score}</Text>
            </View>
          </View>
        )}
      </View>
      {isLoadingApi ? (
        <LoadingSpinner isFullScreen={true} />
      ) : (
        <View
          style={{
            paddingTop: 20,
            flex: 1,
            paddingBottom: 60,
          }}>
          <ScrollView
            ref={listRef}
            onScroll={onScroll}
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={16}
            contentContainerStyle={styles.containerScroll}>
              <React.Fragment>
                <View style={styles.containerTop}>
                  <Text style={styles.textTitle}>{`Question ${
                    indexSoal + 1
                  }`}</Text>
                  {dataQuizLevelUp?.duration !== 0 && (
                    <View style={styles.containerTimer}>
                      {timer > 0 ? (
                        <>
                          <IconTimer />
                          <SizedBox width={8} />
                          <MyTimer
                            expiryTimestamp={timer}
                            handleTimeCountDown={handleTimeCountDown}
                            dataExercise={dataExercise}
                            id={dataQuizLevelUp?.id}
                          />
                        </>
                      ) : null}
                      {dataQuizLevelUp?.is_completed && (
                        <View style={{flexDirection: 'row', gap: 10}}>
                          {!dataQuizLevelUp?.is_passed && (
                            <TouchableOpacity
                              onPress={() =>
                                retakeDataQuiz(dataQuizLevelUp?.id)
                              }
                              style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: getStatusColor('pending'),
                                padding: 8,
                                borderRadius: 8,
                              }}>
                              <Text
                                style={[
                                  styles.textButtonExercise,
                                  {color: Colors.WHITE},
                                ]}>
                                {translate('RetakeQuiz')}
                              </Text>
                            </TouchableOpacity>
                          )}
                          <View
                            style={{
                              alignItems: 'center',
                              justifyContent: 'center',
                              backgroundColor: dataQuizLevelUp?.is_passed
                                ? '#00C82C'
                                : '#F22350',
                              padding: 8,
                              borderRadius: 8,
                            }}>
                            <Text
                              style={[
                                styles.textButtonExercise,
                                {color: Colors.WHITE},
                              ]}>
                              {dataQuizLevelUp?.is_passed
                                ? translate('Passed')
                                : translate('Failed')}
                            </Text>
                          </View>
                        </View>
                      )}
                    </View>
                  )}
                </View>
                <SizedBox height={10} />
                {dataExercise.length > 0 ? (
                  <View style={styles.containerExercise}>
                    <WebDisplay
                      html={dataExercise[indexSoal]?.question}
                      styleText={styles.textExercise}
                    />
                    <SizedBox height={16} />
                    <Layout style={styles.layoutRadio} level="1">
                      <RadioGroup
                        style={styles.containerRadio}
                        selectedIndex={selectedIndex}
                        onChange={index => {
                          handleChooseRadioAnswer(
                            index,
                            indexSoal,
                            dataExercise,
                          );
                        }}>
                        {dataExercise[indexSoal]?.options.map((val, idx) => (
                          <Radio
                            disabled={
                              dataQuizLevelUp?.is_completed ? true : false
                            }
                            key={idx}>
                            {evaProps => (
                              <View
                                style={{
                                  flexDirection: 'row',
                                  flex: 1,
                                  alignItems: 'center',
                                  paddingRight: 5,
                                }}>
                                <WebDisplay
                                  html={val.value}
                                  // isOneLine={true}
                                  styleText={styles.textRadio}
                                />
                                {dataExercise[indexSoal].review?.answer ==
                                  val.option && (
                                  <>
                                    {dataExercise[indexSoal].review?.correct ? (
                                      <Octicons
                                        name="x-circle-fill"
                                        size={25}
                                        color={'#00C82C'}
                                      />
                                    ) : (
                                      <Octicons
                                        name="x-circle-fill"
                                        size={25}
                                        color={'#F22350'}
                                      />
                                    )}
                                  </>
                                )}
                              </View>
                            )}
                          </Radio>
                        ))}
                      </RadioGroup>
                    </Layout>
                  </View>
                ) : (
                  <LoadingSpinner />
                )}
                <SizedBox height={50} />
                <View style={styles.containerButton}>
                  {indexSoal > 0 ? (
                    <TouchableOpacity
                      onPress={handleButtonLeft}
                      style={styles.buttonExercise}>
                      <IconChevLeft />
                      <Text style={styles.textButtonExercise}>Back</Text>
                    </TouchableOpacity>
                  ) : (
                    <View />
                  )}
                  {indexSoal === dataExercise.length - 1 ? (
                    <>
                      {arrayContainsEmptyString(dataExercise) ? (
                        <TouchableOpacity disabled style={styles.buttonMidGray}>
                          <Text style={styles.textSubmitGray}>
                            {translate('Submit')}
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <>
                          {dataQuizLevelUp.is_completed ? null : (
                            // <TouchableOpacity
                            //   onPress={handleButtonLeft}
                            //   style={styles.buttonExercise}>
                            //   <Text style={styles.textButtonExercise}>
                            //     Previous
                            //   </Text>
                            //   <IconChevRight />
                            // </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() => handleSubmitLevelUp()}
                              style={styles.buttonExercise}>
                              <Text style={styles.textButtonExercise}>
                                {translate('Submit')}
                              </Text>
                            </TouchableOpacity>
                          )}
                        </>
                      )}
                    </>
                  ) : null}
                  {indexSoal < dataExercise.length - 1 && (
                    <TouchableOpacity
                      onPress={handleButtonRight}
                      style={styles.buttonExercise}>
                      <Text style={styles.textButtonExercise}>Next</Text>
                      <IconChevRight />
                    </TouchableOpacity>
                  )}
                </View>
                <SizedBox height={10} />
                <Text style={styles.textPagination}>{`Question ${
                  indexSoal + 1
                } of ${dataExercise.length}`}</Text>
              </React.Fragment>
          </ScrollView>
        </View>
      )}
      <IconFloat
        bottom={70}
        contentVerticalOffset={contentVerticalOffset}
        listRef={listRef}
        isScrollView={true}
      />
      <BottomTabOnlyView />
    </View>
  );
};

export default LevelUp;
