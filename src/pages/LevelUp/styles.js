import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      position: 'relative',
    },
    containerHeader: {
      position: 'absolute',
      top: insets.top + 10,
      zIndex: 2,
      justifyContent: 'flex-start',
      width: '100%',
      paddingHorizontal: 20,
      gap: 14,
    },
    containerScroll: {
      paddingHorizontal: 20,
      paddingBottom: 30,
    },
    buttonListHeader: {
      backgroundColor: Colors.WHITE,
      height: 32,
      width: 32,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textHeader: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.WHITE,
      lineHeight: 20,
    },
    textTitle: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(18),
      color: Colors.BLACK,
      lineHeight: 24,
    },
    textBodyTable: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#686868',
      textAlign: 'left',
    },
    textBodyTableRed: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: Colors.PRIMARY,
      textAlign: 'left',
    },
    textBodyTableColoumn: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(12),
      color: Colors.BLACK,
      textAlign: 'left',
    },
    containerCard: {
      backgroundColor: Colors.WHITE,
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 8,
    },
    buttonSection: {
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      width: '80%',
      backgroundColor: Colors.WHITE,
      borderRadius: 8,
      padding: 16,
      marginBottom: 16,
    },
    containerTopSection: {
      width: '100%',
      justifyContent: 'space-between',
      flexDirection: 'row',
    },
    textTopSection: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.PRIMARY,
      lineHeight: 17,
    },
    textButtonSection: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
      lineHeight: 20,
    },

    // isOpenTest Section

    containerTop: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      alignItems: 'center',
    },
    textTitle: {
      fontSize: getFontSize(20),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
      alignSelf: 'flex-start',
    },
    textSubTitle: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.TEXTINFO,
      alignSelf: 'flex-start',
    },
    containerTimer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    containerExercise: {
      width: '100%',
    },
    textExercise: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansRegular400,
      color: Colors.BLACK,
    },
    layoutRadio: {
      backgroundColor: 'transparent',
    },
    containerRadio: {
      backgroundColor: 'transparent',
      borderRadius: 24,
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
    },
    textRadio: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      backgroundColor: 'transparent',
      color: Colors.BLACK,
      marginLeft: 10,
      paddingRight: 10,
    },
    containerButton: {
      flexDirection: 'row',
      alignSelf: 'center',
      justifyContent: 'space-between',
      width: '100%',
    },
    buttonExercise: {
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      borderRadius: 32,
      height: 40,
      width: 112,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      gap: 12,
    },
    textButtonExercise: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.BLACK,
    },
    buttonMidGray: {
      borderWidth: 1,
      borderColor: Colors.GRAY,
      backgroundColor: Colors.WHITE,
      borderRadius: 32,
      height: 40,
      width: 112,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      gap: 12,
    },
    textSubmitGray: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.GRAY,
    },
    textPagination: {
      fontSize: getFontSize(14),
      fontFamily: FontType.openSansRegular400,
      color: Colors.PLACEHOLDER,
      lineHeight: 20,
      alignSelf: 'center',
    },
    containerModalIcon: {
      borderRadius: 88,
      width: 88,
      height: 88,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#00C82C1A',
    },
    containerButtonSingle: {
      width: 100,
    },
    buttonStartTest: {
      flexDirection: 'row',
      gap: 10,
      height: 36,
    },
  });
