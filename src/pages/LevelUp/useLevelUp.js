import React from 'react';
import {Text, View} from 'react-native';
import {IconCup} from '../../assets/svg';
import Octicons from 'react-native-vector-icons/Octicons';
import {Colors} from '../../utility';
import ModalCustom from '../../widgets/ModalCustom';
import {magicModal} from 'react-native-magic-modal';
import usePostSubmitQuizLevelUp from '../../api/usePostSubmitQuizLevelUp';
import useGetQuizLevelUp from '../../api/useGetQuizLevelUp';
import {useTranslation} from 'react-i18next';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import usePostRetakeQuizLevelUp from '../../api/usePostRetakeQuizLevelUp';

const dataSection = [
  {
    id: 1,
    section: 'Level 1',
    isFinish: false,
    isStarted: false,
  },
  {
    id: 2,
    section: 'Level 2',
    isFinish: false,
    isStarted: false,
  },
  {
    id: 3,
    section: 'Level 3',
    isFinish: false,
    isStarted: false,
  },
];

const useLevelUp = (id, styles) => {
  const authMethod = useSelector(state => state.auth);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const {
    dataQuizLevelUp,
    isLoading: isLoadingApi,
    accessQuiz,
  } = useGetQuizLevelUp();
  const {
    dataSubmitQuiz,
    setDataSubmitQuiz,
    isLoading: isLoadingPostSubmit,
    accessSubmit,
  } = usePostSubmitQuizLevelUp();
  const [heightTableData, setHeightTableData] = React.useState([]);
  const [tableProfile, setTableProfile] = React.useState([]);
  const [buttonSections, setButtonSections] = React.useState(dataSection);
  const [dataExercise, setDataExercise] = React.useState([]);
  const [selectedIndex, setSelectedIndex] = React.useState('');
  const [indexSoal, setIndexSoal] = React.useState(0);
  const time = new Date();
  const [timer, setTimer] = React.useState(0);
  const {isSuccess, retakeDataQuiz} = usePostRetakeQuizLevelUp();

  React.useEffect(() => {
    if (!authMethod.isLogin) {
      navigation.goBack();
    }
  }, [authMethod]);

  React.useEffect(() => {
    if (id) {
      accessQuiz(id);
    }
  }, [id]);

  const hideModalSukses = () => {
    magicModal.hide();
    setDataSubmitQuiz('');
    navigation.goBack();
  };

  const handleSubmitLevelUp = () => {
    let answer = {};
    dataExercise.map(item => {
      answer[item.id] = item.answer;
    });
    handleFinishSection();
    accessSubmit(dataQuizLevelUp.id, answer);
  };

  React.useEffect(() => {
    if (dataSubmitQuiz) {
      magicModal.show(() => (
        <ModalCustom
          hideModalSukses={hideModalSukses}
          iconModal={
            <View style={styles.containerModalIcon}>
              <Octicons
                name={'check-circle-fill'}
                size={40}
                color={Colors.GREEN}
              />
            </View>
          }
          singleButton={true}
          textButton={'OK'}
          textModalJudul={
            dataSubmitQuiz.is_passed ? 'Congratulations !' : 'Sorry !'
          }
          textCustom={
            <View style={{alignItems: 'center', marginTop: 24, gap: 5}}>
              <Text style={styles.textBodyTable}>
                {dataSubmitQuiz.is_passed == 1
                  ? `${translate('DescResultTest')} ${Number(
                      dataQuizLevelUp.duration,
                    )} second`
                  : `${translate('DescFailResultTest')} ${Number(
                      dataQuizLevelUp.duration,
                    )} second`}
              </Text>
              {elementRed(`Score : ${dataSubmitQuiz.score}`)}
            </View>
          }
          containerButtonSingle={styles.containerButtonSingle}
        />
      ));
      return;
    }

    if (isSuccess) {
      navigation.goBack();
      return;
    }

  }, [dataSubmitQuiz, isSuccess]);

  const handleChooseRadioAnswer = React.useCallback(
    (idx, indexSoal, dataExercise) => {
      dataExercise[indexSoal]?.options?.map((key, ind) => {
        if (idx == ind) {
          dataExercise[indexSoal].answer = key.option;
          setSelectedIndex(idx);
        }
      });
    },
    [],
  );

  const handleTimeCountDown = React.useCallback(() => {
    // alert('Time is over');
    handleSubmitLevelUp();
  }, []);

  const handleButtonLeft = () => {
    setSelectedIndex('');
    let resulIndex = indexSoal === 0 ? indexSoal : indexSoal - 1;
    setIndexSoal(resulIndex);
    dataExercise[resulIndex]?.options?.map((key, ind) => {
      if (key.option == dataExercise[resulIndex].answer) {
        setSelectedIndex(ind);
      }
    });
  };

  const handleButtonRight = () => {
    setSelectedIndex('');
    let resulIndex =
      indexSoal === dataExercise.length - 1 ? indexSoal : indexSoal + 1;
    setIndexSoal(resulIndex);
    dataExercise[resulIndex]?.options?.map((key, ind) => {
      if (key.option == dataExercise[resulIndex].answer) {
        setSelectedIndex(ind);
      }
    });
  };

  const elementSkillLevel = text => (
    <View
      style={{
        paddingVertical: 5,
      }}>
      <Text style={styles.textBodyTableColoumn}>{text}</Text>
    </View>
  );

  const tableTitleProfile = [
    elementSkillLevel('Skill Level'),
    'Duration',
    'Last Updated',
    'Section',
  ];

  const elementBeginner = text => (
    <View
      style={{
        flexDirection: 'row',
        gap: 5,
        borderRadius: 20,
        backgroundColor: '#F5E7E7',
        paddingLeft: 10,
        paddingVertical: 5,
      }}>
      <IconCup width={20} height={20} />
      <Text style={styles.textBodyTableRed}>{text}</Text>
    </View>
  );

  const elementRed = text => (
    <View>
      <Text style={styles.textBodyTableRed}>{text}</Text>
    </View>
  );

  React.useEffect(() => {
    if (dataQuizLevelUp) {
      let dataTemp = dataQuizLevelUp;
      if (dataTemp.is_completed) {
        let dataAnswer = JSON.parse(dataTemp.answers);
        let dataReview = JSON.parse(dataTemp.review);
        dataTemp.questions.map(item => {
          let dataReviewResult = dataReview.find(
            key => key.question_id === item.id,
          );
          item.answer = dataAnswer[item.id];
          if (dataReviewResult) {
            item.review = dataReviewResult;
          }
          return item;
        });
        setDataExercise(dataTemp.questions);
        let index = dataTemp.questions[indexSoal].options.findIndex(
          item => item.option === Object.values(dataAnswer)[0],
        );
        setSelectedIndex(index);
        setTimer(0);
      } else {
        let dataAnswer = JSON.parse(dataTemp.answers);
        dataTemp.questions.map(item => {
          item.answer = dataAnswer ? dataAnswer[item.id] : '';
          return item;
        });
        setDataExercise(dataTemp.questions);
        setTimer(
          time.setSeconds(
            time.getSeconds() + dataTemp.remaining_time &&
              dataTemp.remaining_time > 0
              ? dataTemp.remaining_time
              : dataTemp.duration * 60,
          ),
        );
        if (dataAnswer) {
          let index = dataTemp.questions[indexSoal].options.findIndex(
            item => item.option === Object.values(dataAnswer)[0],
          );
          setSelectedIndex(index);
          return;
        }
        setSelectedIndex('');
      }
    }
  }, [dataQuizLevelUp]);

  React.useEffect(() => {
    let dataTemp = [
      [elementBeginner('Beginner')],
      [elementRed('5m')],
      ['Jan 26, 2024'],
      ['6 Section'],
    ];
    setTableProfile(dataTemp);
    let heightTemp = [];
    tableTitleProfile.map(item => {
      if (item === 'Skill Level') {
        heightTemp.push(55);
      }
      heightTemp.push(50);
    });
    setHeightTableData(heightTemp);
  }, []);

  const handlePressSection = value => {
    let result = buttonSections.map(section => {
      if (value.section === section.section) {
        section.isStarted = true;
      }
      return section;
    });
    setIndexSoal(0);
    setButtonSections(result);
    accessQuiz(1);
  };

  const handleFinishSection = () => {
    let result = buttonSections.map(section => {
      if (section.isStarted === true) {
        section.isFinish = true;
      }
      return section;
    });
    setButtonSections(result);
  };

  return {
    dataExercise,
    selectedIndex,
    indexSoal,
    timer,
    handleSubmitLevelUp,
    handleChooseRadioAnswer,
    handleTimeCountDown,
    handleButtonLeft,
    handleButtonRight,
    dataQuizLevelUp,
    translate,
    retakeDataQuiz,
    isLoadingApi,
  };
};

export default useLevelUp;
