const UrlLogin = '/api-iam/login';
const UrlRegister = '/api-iam/register';
const UrlLogout = '/api-iam/logout';
const UrlUpdateProfile = '/api-iam/update-profile';
const UrlGoogle = '/api-iam/auth/google';
const UrlRDIS = '/api-iam/auth/rdis';
const UrlCertificate = '/api-lms/external-certificate';
const UrlCourse = '/api-lms/course';
const UrlCourseRecomendations = '/api-lms/course/recommendation';
const UrlMyCourse = '/api-lms/mycourse';
const UrlCourseEnroll = '/api-lms/course/enroll';
const UrlCourseProgress = '/api-lms/course/progress';
const UrlCourseCategory = '/api-lms/category';
const UrlCourseLevel = '/api-lms/level';
const UrlCourseDetail = '/api-lms/course/id';
const UrlCourseContentType = '/api-lms/content-type';
const UrlCourseLike = '/api-lms/course-like';
const UrlCourseComment = '/api-lms/course-comment';
const UrlCourseReport = '/api-lms/course-report';
const UrlInvestment = '/api-portal/investment-partner';
const UrlInvestmentCategory = '/api-portal/investment-partner-category';
const UrlNews = '/api-portal/news';
const UrlNewsPortal = '/api-portal/news-all';
const UrlNewsRSS = '/api-portal/news-rss/id/';
const UrlNewsCategory = '/api-portal/news-category';
const UrlEvent = '/api-portal/event';
const UrlEventPortal = '/api-portal/event-all';
const UrlEventCategory = '/api-portal/event-category';
const UrlFaq = '/api-portal/faqs';
const UrlFaqPortal = '/api-portal/faq-all';
const UrlAbout = '/api-portal/general-content/id/1';
const UrlDisclaimer = '/api-portal/general-content/id/2';
const UrlTermsCondition = '/api-portal/general-content/id/3';
const UrlPrivacyPolicy = '/api-portal/general-content/id/4';
const UrlStockScreener = '/support/stock-screener/api/v1/stock-screener/get';
const UrlStockScreenerProfileCompany =
  '/primary/ListedCompany/GetCompanyProfilesDetail';
const UrlStockScreenerCategorySector =
  '/support/stock-screener/api/v1/stock-screener/sector';
const UrlStockScreenerCategorySubSector =
  '/support/stock-screener/api/v1/stock-screener/sub-sector';
const UrlStockScreenerMinMax =
  '/support/stock-screener/api/v1/stock-screener/min-max';
const UrlQuizLevelUp = '/api-lms/test-attempt/';
const UrlQuizSubmit = '/api-lms/test-attempt/submit';
const UrlQuizSave = '/api-lms/test-attempt/save';
const UrlQuizRetake = '/api-lms/test-attempt/retake';
const UrlUploadFileCertificate = '/api-lms/external-certificate/upload-file';
const UrlUploadFilePortal = '/api-portal/upload-file';
const UrlUploadFileLMS = '/api-lms/course/upload-file';
const UrlUploadFileIAM = '/api-iam/upload-file';
const UrlSearchAll = '/api-portal/search-all-portal';
const UrlSearchNews = '/api-portal/search-all-news';
const UrlSearchEvent = '/api-portal/search-event';
const UrlSearchCourse = '/api-lms/course/search/';
const UrlNotification = '/api-iam/notification';
const UrlCollection = '/api-lms/collection';
const UrlCollectionContent = '/api-lms/collection-content';
const UrlGetCountry = '/api-portal/countries';
const UrlChangePassword = '/api-iam/change-password';
const UrlResendEmail = '/api-iam/resend-email-verification';
const UrlChangeEmail = '/api-iam/change-email';
const UrlForgotPassword = '/api-iam/forgot-password';
const UrlResetPassword = '/api-iam/reset-password';
const UrlValidateResetToken = '/api-iam/validate-reset-token';
const UrlPreferences = '/api-lms/user-preference';
const UrlProfile = '/api-iam/profile-info';
const UrlLearningJourney = '/api-lms/user-level';
const UrlVerification = '/api-iam/email/verify';
const UrlPlacementTest = '/api-lms/placement-test';
const UrlIDXWebsite = '/api-iam/auth/webidx';
const UrlTicmi = '/api-iam/auth/ticmiedu';
const UrlWebIDXStockScreener = '/api-iam/info/webidx-base-url';
const UrlApple = '/api-iam/auth/apple/verify-token';
const UrlDeleteAccount = '/api-iam/user';

export {
  UrlAbout,
  UrlApple,
  UrlCertificate,
  UrlChangeEmail,
  UrlChangePassword,
  UrlCollection,
  UrlCollectionContent,
  UrlCourse,
  UrlCourseCategory,
  UrlCourseComment,
  UrlCourseContentType,
  UrlCourseDetail,
  UrlCourseEnroll,
  UrlCourseLevel,
  UrlCourseLike,
  UrlCourseProgress,
  UrlCourseRecomendations,
  UrlCourseReport,
  UrlDeleteAccount,
  UrlDisclaimer,
  UrlEvent,
  UrlEventCategory,
  UrlEventPortal,
  UrlFaq,
  UrlFaqPortal,
  UrlForgotPassword,
  UrlGetCountry,
  UrlGoogle,
  UrlIDXWebsite,
  UrlInvestment,
  UrlInvestmentCategory,
  UrlLearningJourney,
  UrlLogin,
  UrlLogout,
  UrlMyCourse,
  UrlNews,
  UrlNewsCategory,
  UrlNewsPortal,
  UrlNewsRSS,
  UrlNotification,
  UrlPlacementTest,
  UrlPreferences,
  UrlPrivacyPolicy,
  UrlProfile,
  UrlQuizLevelUp,
  UrlQuizRetake,
  UrlQuizSave,
  UrlQuizSubmit,
  UrlRDIS,
  UrlRegister,
  UrlResendEmail,
  UrlResetPassword,
  UrlSearchAll,
  UrlSearchCourse,
  UrlSearchEvent,
  UrlSearchNews,
  UrlStockScreener,
  UrlStockScreenerCategorySector,
  UrlStockScreenerCategorySubSector,
  UrlStockScreenerMinMax,
  UrlStockScreenerProfileCompany,
  UrlTermsCondition,
  UrlTicmi,
  UrlUpdateProfile,
  UrlUploadFileCertificate,
  UrlUploadFileIAM,
  UrlUploadFileLMS,
  UrlUploadFilePortal,
  UrlValidateResetToken,
  UrlVerification,
  UrlWebIDXStockScreener,
};
