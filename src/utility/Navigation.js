import React from 'react';

const navigationRef = React.createRef();
console.log('navigationRef', navigationRef);

// You can export navigation functions to use throughout your app, without accessing the `navigation` prop.
function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}

// This is the function you want, it will return the complete navigation state
function getCurrentRoute() {
  return navigationRef.current?.getCurrentRoute();
}

export { getCurrentRoute, navigate, navigationRef };

