const MENU = "menu-idx-mobile";
const BUTTON_COLOR = [
  {
    id: 1,
    color: '#9F0E0F',
  },
  {
    id: 2,
    color: '#23B56B',
  },
  {
    id: 3,
    color: '#0097C7',
  },
  {
    id: 4,
    color: '#D0C802',
  },
  {
    id: 5,
    color: '#FFA500',
  },
];

const CATEGORY_STATUS = [
  {
    value: "all",
    label: "All",
    color: "#9F0E0F",
  },
  {
    value: "published",
    label: "Published",
    color: "#23B56B",
  },
  {
    value: "pending",
    label: "Pending",
    color: "#D0C802",
  },
  {
    value: "unpublished",
    label: "Unpublished",
    color: "#FFA500",
  },
  {
    value: "draft",
    label: "Draft",
    color: "#0097C7",
  },
  {
    value: "rejected",
    label: "Rejected",
    color: "#CB0125",
  },
];

export { BUTTON_COLOR, CATEGORY_STATUS, MENU };

