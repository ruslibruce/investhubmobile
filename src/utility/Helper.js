import {Linking, Platform} from 'react-native';
import Config from 'react-native-config';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import Toast from 'react-native-toast-message';
import { CATEGORY_STATUS } from './StorageStatis';

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
function createAction(type, payload) {
  return {
    type,
    payload,
  };
}
function numberOnly(number) {
  if (isNaN(number)) {
    return 0;
  }
  return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}
function validateEmail(email) {
  return String(email)
    .toLowerCase()
    .match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
}
function validateUrl(url) {
  const urlPattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
    '((([a-zA-Z\\d]([a-zA-Z\\d-]*[a-zA-Z\\d])*)\\.)+[a-zA-Z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-zA-Z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-zA-Z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-zA-Z\\d_]*)?$', 'i' // fragment locator
  );
  return urlPattern.test(url);
}

function isYoutubeUrl(url) {
  const youtubePattern =
    /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/; // fragment locator
  return youtubePattern.test(url);
}
function validatePhoneNumber(noTlpn) {
  if (isNaN(noTlpn)) {
    return false;
  }
  // const pattern = /^[0-9]{9,15}$/;
  const pattern = /^08[0-9]{8,11}$/g;
  return pattern.test(noTlpn);
}
function normalisasiNomorHP(phone) {
  phone = String(phone).trim();
  if (phone.startsWith('+62')) {
    phone = '0' + phone.slice(3);
  } else if (phone.startsWith('62')) {
    phone = '0' + phone.slice(2);
  }
  return phone.replace(/[- .]/g, '');
}

function getDeepLink(path = '') {
  const scheme = Config.SCHEME_CALLBACK;
  const prefix = Platform.OS == 'android' ? `${scheme}://` : `${scheme}://`;
  return prefix + path;
}

function isHTML(str) {
  const htmlRegex =
    /<(br|basefont|hr|input|source|frame|param|area|meta|!--|col|link|option|base|img|wbr|!DOCTYPE).*?>|<(a|abbr|acronym|address|applet|article|aside|audio|b|bdi|bdo|big|blockquote|body|button|canvas|caption|center|cite|code|colgroup|command|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frameset|head|header|hgroup|h1|h2|h3|h4|h5|h6|html|i|iframe|ins|kbd|keygen|label|legend|li|map|mark|menu|meter|nav|noframes|noscript|object|ol|optgroup|output|p|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video).*?<\/\2>/i;
  return htmlRegex.test(str);
}

function checkIsEmptyObject(object) {
  return Object.values(object).some(x => x === null || x === '');
}

function checkIsBooleanObject(object, value) {
  return Object.values(object).some(x => x === value);
}

const arrayContainsEmptyString = array => {
  return array.some(object =>
    Object.values(object).some(
      value => typeof value === 'string' && value.trim() === '',
    ),
  );
};

const handleOpenLink = async url => {
  try {
    if (await InAppBrowser.isAvailable()) {
      if (await InAppBrowser.isAvailable()) {
        const response = await InAppBrowser.open(url, {
          // iOS Properties
          ephemeralWebSession: false,
          // Android Properties
          showTitle: false,
          enableUrlBarHiding: true,
          enableDefaultShare: false,
        });
      }
    } else {
      Linking.openURL(url);
    }
  } catch (error) {
    Toast.show({
      type: 'error',
      position: 'bottom',
      text1: 'Open Browser Failed',
      text2: error.message ?? 'Your Access Browser Failed',
    });
  }
};

const regexUrl = () => {
  return /^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})$/;
};

const regexEmail = () => {
  return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
};

const regexPhoneNumber = () => {
  return /^08[0-9]{8,11}$/;
};

const getStatusColor = (status) => {
  const data = CATEGORY_STATUS.find((item) => item.value == status);
  return data?.color;
};

export {
  arrayContainsEmptyString,
  checkIsBooleanObject,
  checkIsEmptyObject,
  createAction,
  getDeepLink,
  handleOpenLink,
  isHTML,
  isYoutubeUrl,
  normalisasiNomorHP,
  numberOnly,
  regexEmail,
  regexPhoneNumber,
  regexUrl,
  sleep,
  validateEmail,
  validatePhoneNumber,
  validateUrl,
  getStatusColor,
};
