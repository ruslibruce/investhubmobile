import Toast from 'react-native-toast-message';
import {t as translate} from 'i18next';
import {getCurrentRoute} from './Navigation';
import {AUTH_ROUTES} from './path';
import {store} from '../redux/store';
import {logout} from '../redux/reducers/auth.reducers';

function handlingErrors(error, text, top) {
  const Router = getCurrentRoute();
  console.log('error.config', error.config);
  console.log('error', error);
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log('error.response.data', error.response.data);
    console.log('error.response.status', error.response.status);
    console.log('error.response.headers', error.response.headers);
    if (error.response.status === 401) {
      let check = false;
      AUTH_ROUTES.forEach(route => {
        if (Router.name === route) {
          check = true;
        }
      });
      if (check) {
        Toast.show({
          type: 'error',
          position: top ? 'top' : 'bottom',
          text1: error.response?.data?.message ?? `${text} Failed`,
          text2: error.message ?? `Your ${text} Failed`,
        });
        return;
      }
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Session Habis.',
        text2: 'Silakan Login Lagi.',
      });
      return;
    }
    Toast.show({
      type: 'error',
      position: top ? 'top' : 'bottom',
      text1: error.response?.data?.message ?? `${text} Failed`,
      text2: error.message ?? `Your ${text} Failed`,
    });
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log('error.request', error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error', error.message);
    Toast.show({
      type: 'error',
      position: top ? 'top' : 'bottom',
      text1: 'Failed',
      text2: error.message ?? `Your ${text} Failed`,
    });
  }
}

export {handlingErrors};
