import Colors from "./Colors";

export default {
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    statusBar: {
        headerStyle: {
            backgroundColor: Colors.PRIMARY,

        },
        headerTintColor: Colors.WHITE,
        headerTitleStyle: {
            color: Colors.WHITE,
        },
    }
}