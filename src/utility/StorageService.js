import AsyncStorage from '@react-native-async-storage/async-storage';
import {MENU} from './StorageStatis';

class StorageService {
  async getMenu() {
    const value = await AsyncStorage.getItem(MENU);
    if (value) return JSON.parse(value);
  }
  async setMenu(array) {
    console.log(JSON.stringify(array));
    await AsyncStorage.setItem(MENU, JSON.stringify(array));
  }
  async removeMenu() {
    await AsyncStorage.removeItem(MENU);
  }
}
export default new StorageService();
