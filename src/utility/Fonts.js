import { PixelRatio, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
const fontScale = PixelRatio.getFontScale();

const matrics = {
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  width,
  height,
};

const getFontSize = size => size / fontScale;

const FontWeight = {
  light: '300',
  low: '400',
  medium: '500',
  semi: '600',
  bold: 'bold',
  full: '900',
  normal: 'normal',
};

const FontType = {
  openSansLight300: 'OpenSans-Light',
  openSansRegular400: 'OpenSans-Regular',
  openSansMedium500: 'OpenSans-Medium',
  openSansSemiBold600: 'OpenSans-SemiBold',
  openSansBold700: 'OpenSans-Bold',
  openSansExtraBold800: 'OpenSans-ExtraBold',
};

export {
  FontWeight,
  FontType,
  matrics,
  getFontSize,
};