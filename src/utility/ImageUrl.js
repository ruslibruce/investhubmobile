import Config from "react-native-config";

const imageLMS = `${Config.BASE_URL}/lms-files`;

const imageIAM = `${Config.BASE_URL}/iam-files`;

const imagePORTAL = `${Config.BASE_URL}/portal-files`;

export { imageLMS, imageIAM, imagePORTAL };
