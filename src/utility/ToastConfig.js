import {Text, View} from 'react-native';
import {Colors, FontType, getFontSize} from './';
const toastConfig = {
  success: props => {
    return (
      <View
        style={{
          width: '100%',
          alignSelf: 'center',
          backgroundColor: Colors.GREEN,
          paddingVertical: 5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontFamily: FontType.openSansSemiBold600,
            color: Colors.WHITE,
            fontSize: getFontSize(12),
          }}>
          {props.text1}
        </Text>
        <Text
          style={{
            fontFamily: FontType.openSansRegular400,
            color: Colors.WHITE,
            fontSize: getFontSize(10),
          }}>
          {props.text2}
        </Text>
      </View>
    );
  },
  info: props => {
    return (
      <View
        style={{
          width: '100%',
          alignSelf: 'center',
          backgroundColor: Colors.BLUE,
          paddingVertical: 5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontFamily: FontType.openSansSemiBold600,
            color: Colors.WHITE,
            fontSize: getFontSize(12),
          }}>
          {props.text1}
        </Text>
        <Text
          style={{
            fontFamily: FontType.openSansRegular400,
            color: Colors.WHITE,
            fontSize: getFontSize(10),
          }}>
          {props.text2}
        </Text>
      </View>
    );
  },
  error: props => {
    return (
      <View
        style={{
          width: '100%',
          alignSelf: 'center',
          backgroundColor: Colors.PRIMARY,
          paddingVertical: 5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontFamily: FontType.openSansSemiBold600,
            color: Colors.WHITE,
            fontSize: getFontSize(12),
          }}>
          {props.text1}
        </Text>
        <Text
          style={{
            fontFamily: FontType.openSansRegular400,
            color: Colors.WHITE,
            fontSize: getFontSize(10),
          }}>
          {props.text2}
        </Text>
      </View>
    );
  },
};

export {toastConfig};
