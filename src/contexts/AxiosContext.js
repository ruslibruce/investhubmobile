import axios from 'axios';
import React from 'react';
import Config from 'react-native-config';
import { useSelector } from 'react-redux';
import usePostLogout from '../api/usePostLogout';

const AxiosContext = React.createContext();

const baseURL = {
  baseURL: Config.BASE_URL,
  timeout: 15000,
  timeoutErrorMessage: 'Gagal Terhubung',
};

const baseURLIDX = {
  baseURL: Config.BASE_URL_IDX,
  timeout: 15000,
  timeoutErrorMessage: 'Gagal Terhubung',
};

const AxiosContextProvider = ({children}) => {
  const {token} = useSelector(state => state.auth);
  const tokenIDX = Config.TOKEN_IDX;
  const authAxios = axios.create(baseURL);
  const publicAxios = axios.create(baseURL);
  const authFormDataAxios = axios.create(baseURL);
  const idxAxios = axios.create(baseURLIDX);
  const idxAuthAxios = axios.create(baseURLIDX);
  const {accessLogout} = usePostLogout();

  idxAxios.interceptors.request.use(
    config => {
      if (!config.headers.Authorization) {
        config.headers.setContentType = 'application/json';
        config.headers.getAccept = 'application/json';
      }

      return config;
    },
    error => {
      return Promise.reject(error);
    },
  );
  idxAuthAxios.interceptors.request.use(
    config => {
      if (!config.headers.Authorization) {
        config.headers.Authorization = `Bearer ${tokenIDX}`;
        config.headers.setContentType = 'application/json';
        config.headers.getAccept = 'application/json';
      }

      return config;
    },
    error => {
      return Promise.reject(error);
    },
  );
  publicAxios.interceptors.request.use(
    config => {
      if (!config.headers.Authorization) {
        config.headers.setContentType = 'application/json;charset=UTF-8';
        config.headers.getAccept = 'application/json';
      }

      return config;
    },
    error => {
      return Promise.reject(error);
    },
  );
  authAxios.interceptors.request.use(
    config => {
      if (!config.headers.Authorization) {
        config.headers.Authorization = `Bearer ${token}`;
        config.headers.setContentType = 'application/json;charset=UTF-8';
        config.headers.getAccept = 'application/json';
      }

      return config;
    },
    error => {
      return Promise.reject(error);
    },
  );
  authFormDataAxios.interceptors.request.use(
    config => {
      if (!config.headers.Authorization) {
        config.headers.Authorization = `Bearer ${token}`;
        config.headers.setContentType = 'multipart/form-data';
      }

      return config;
    },
    error => {
      return Promise.reject(error);
    },
  );

  idxAxios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      return Promise.reject(error);
    },
  );
  idxAuthAxios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      return Promise.reject(error);
    },
  );
  publicAxios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      return Promise.reject(error);
    },
  );
  authAxios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      if (error.response.status === 401) {
        accessLogout(token);
      }
      return Promise.reject(error);
    },
  );
  authFormDataAxios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      if (error.response.status === 401) {
        accessLogout(token);
      }
      return Promise.reject(error);
    },
  );
  return (
    <AxiosContext.Provider
      value={{
        authAxios,
        publicAxios,
        authFormDataAxios,
        idxAxios,
        idxAuthAxios,
        baseURL: Config.BASE_URL,
        baseURLIDX: Config.BASE_URL_IDX,
      }}>
      {children}
    </AxiosContext.Provider>
  );
};

const useAxiosContext = () => {
  return React.useContext(AxiosContext);
};

export { AxiosContextProvider, useAxiosContext };

