import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCourseCategory, UrlCourseContentType, handlingErrors } from '../utility';

const useGetCourseContentType = () => {
  const {authAxios} = useAxiosContext();
  const [dataCourseContentType, setDataCourseContentType] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessCourseContentType();
  }, []);

  const accessCourseContentType = async () => {
    try {
      const response = await authAxios.get(`${UrlCourseContentType}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        let dataResult = [];
        dataResponse.map(item => {
          dataResult.push({
            id: item.id,
            label: item.name,
            value: item.name,
          });
        });
        setDataCourseContentType(dataResult);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Course Content Type Failed',
          text2: response.message ?? 'Your Access Course Content Type Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {isLoading, dataCourseContentType, setDataCourseContentType};
};

export default useGetCourseContentType;
