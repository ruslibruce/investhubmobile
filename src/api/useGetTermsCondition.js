import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlTermsCondition, handlingErrors } from '../utility';

const useGetTermsCondition = () => {
  const {authAxios} = useAxiosContext();
  const [dataTermsCondition, setDataTermsCondition] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessTermsCondition();
  }, []);

  const accessTermsCondition = async () => {
    try {
      const response = await authAxios.get(UrlTermsCondition);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataTermsCondition(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Terms Condition Failed',
          text2: response.message ?? 'Your Access Terms Condition Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Terms Condition');
    }
  };
  return {dataTermsCondition, isLoading};
};

export default useGetTermsCondition;
