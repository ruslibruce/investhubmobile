import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlFaqPortal, handlingErrors} from '../utility';

const useGetFaq = () => {
  const {authAxios} = useAxiosContext();
  const [dataFaq, setDataFaq] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessFaq();
  }, []);

  const accessFaq = async (params = '') => {
    try {
      const response = await authAxios.get(
        `${UrlFaqPortal}${params}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataFaq(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access FAQ Failed',
          text2: response.message ?? 'Your Access FAQ Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access FAQ');
    }
  };
  return {dataFaq, isLoading, accessFaq};
};

export default useGetFaq;
