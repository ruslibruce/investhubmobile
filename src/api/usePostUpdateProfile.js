import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useDispatch, useSelector } from 'react-redux';
import { useAxiosContext } from '../contexts';
import { login, updateProfile } from '../redux/reducers/auth.reducers';
import {
  DASHBOARD_HOME,
  PREFERENCES,
  PRELIMINARY,
  UrlUpdateProfile,
  handlingErrors,
  imageIAM,
} from '../utility';

const usePostUpdateProfile = () => {
  const {authAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const [isLoading, setIsLoading] = React.useState(false);

  const accessUpdateProfile = async data => {
    setIsLoading(true);
    try {
      const response = await authAxios.post(UrlUpdateProfile, data);
      if (response.status === 200) {
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate('ProfileSuccess'),
          visibilityTime: 2000,
          onHide: () => {
            let temp = {
              ...authMethod,
              user: {
                ...authMethod.user,
                is_profile_updated: true,
              },
            };
            dispatch(login(temp));
            dispatch(
              updateProfile({
                ...data,
                photo: `${imageIAM}/${data.photo}`,
              }),
            );
            if (
              authMethod.user?.is_preference_updated &&
              !authMethod.user?.is_placement_test_taken
            ) {
              navigation.navigate(PRELIMINARY);
              return;
            } else if (!authMethod.user?.is_preference_updated) {
              navigation.navigate(PREFERENCES);
              return;
            } else {
              navigation.navigate(DASHBOARD_HOME);
            }
          },
        });
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Update Profile Failed',
          text2: response.message ?? 'Your Update Profile Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Update Profile');
    }
  };
  return {isLoading, accessUpdateProfile};
};

export default usePostUpdateProfile;
