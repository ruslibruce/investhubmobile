import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlStockScreenerMinMax, handlingErrors} from '../utility';

const useGetStockScreenerMinMax = () => {
  const dataButtonMaxMin = [
    {
      id: 1,
      itemName: 'Total Rev (Rp.M)',
      max: '',
      min: '',
      name: 'tRevenue',
      maxModify: '',
      minModify: '',
    },
    {
      id: 2,
      itemName: 'Net Profit Margin (NPM) (%)',
      max: '',
      min: '',
      name: 'npm',
      maxModify: '',
      minModify: '',
    },
    {
      id: 3,
      itemName: 'PER (times)',
      max: '',
      min: '',
      name: 'per',
      maxModify: '',
      minModify: '',
    },
    {
      id: 4,
      itemName: 'PBV (times)',
      max: '',
      min: '',
      name: 'pbv',
      maxModify: '',
      minModify: '',
    },
    {
      id: 5,
      itemName: 'ROA %',
      max: '',
      min: '',
      name: 'roa',
      maxModify: '',
      minModify: '',
    },
    {
      id: 6,
      itemName: 'ROE %',
      max: '',
      min: '',
      name: 'roe',
      maxModify: '',
      minModify: '',
    },
    {
      id: 7,
      itemName: 'Debit Equity Ratio (DER)',
      max: '',
      min: '',
      name: 'der',
      maxModify: '',
      minModify: '',
    },
    {
      id: 8,
      itemName: '4-wk %Pr. Chg.',
      max: '',
      min: '',
      name: 'week4',
      maxModify: '',
      minModify: '',
    },
    {
      id: 9,
      itemName: '13-wk %Pr. Chg.',
      max: '',
      min: '',
      name: 'week13',
      maxModify: '',
      minModify: '',
    },
    {
      id: 10,
      itemName: '26-wk %Pr. Chg.',
      max: '',
      min: '',
      name: 'week26',
      maxModify: '',
      minModify: '',
    },
    {
      id: 11,
      itemName: '52-wk %Pr. Chg.',
      max: '',
      min: '',
      name: 'week52',
      maxModify: '',
      minModify: '',
    },
    {
      id: 12,
      itemName: 'Price MonthtoDate (MTD)',
      max: '',
      min: '',
      name: 'mtd',
      maxModify: '',
      minModify: '',
    },
    {
      id: 13,
      itemName: 'Price YeartoDate (YTD)',
      max: '',
      min: '',
      name: 'ytd',
      maxModify: '',
      minModify: '',
    },
    {
      id: 14,
      itemName: 'Market Cap',
      max: '',
      min: '',
      name: 'mCap',
      maxModify: '',
      minModify: '',
    },
  ];
  const {idxAuthAxios} = useAxiosContext();
  const [dataFilterMinMax, setDataFilterMinMax] =
    React.useState(dataButtonMaxMin);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessStockScreenerMinMax();
  }, []);

  const accessStockScreenerMinMax = async () => {
    try {
      const response = await idxAuthAxios.get(UrlStockScreenerMinMax);
      if (response.status === 200) {
        const dataResponse = response.data;
        let dataTemp = [];
        dataFilterMinMax.map(item => {
          item.max = dataResponse[item.name + 'Max'];
          item.min = dataResponse[item.name + 'Min'];
          dataTemp.push(item);
        });
        setDataFilterMinMax(dataTemp);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Stock Screener MinMax Failed',
          text2: response.message ?? 'Your Access Stock Screener MinMax Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Stock Screener MinMax');
    }
  };
  return {dataFilterMinMax, setDataFilterMinMax, isLoading};
};

export default useGetStockScreenerMinMax;
