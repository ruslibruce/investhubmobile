import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlEventCategory, handlingErrors } from '../utility';

const useGetEventCategory = () => {
  const {authAxios} = useAxiosContext();
  const [dataTabs, setDataTabs] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessEventCategory();
  }, []);

  const accessEventCategory = async () => {
    try {
      const response = await authAxios.get(`${UrlEventCategory}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        let dataResult = [
          {
            id: 0,
            name: 'All',
            isSelected: true,
          },
        ];
        dataResponse.map(item => {
          dataResult.push({
            id: item.id,
            name: item.name,
            isSelected: false,
          });
        });
        setDataTabs(dataResult);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Failed',
          text2: response.message ?? 'Your Access News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {isLoading, dataTabs, setDataTabs};
};

export default useGetEventCategory;
