import { useNavigation } from '@react-navigation/native';
import React from 'react';
import Toast from 'react-native-toast-message';
import { useDispatch } from 'react-redux';
import { useAxiosContext } from '../contexts';
import { register } from '../redux/reducers/auth.reducers';
import { EMAIL_CONFIRM, handlingErrors, UrlRegister } from '../utility';
import { jwtDecode } from 'jwt-decode';
import { useTranslation } from 'react-i18next';

const usePostRegister = () => {
  const {publicAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const [isError, setIsError] = React.useState(false);
  const {t: translate} = useTranslation();

  const accessRegister = async value => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await publicAxios.post(UrlRegister, value);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate('Register'),
          text2: translate('RegisterSuccess'),
          visibilityTime: 2000,
        });
        const decodedToken = jwtDecode(dataResponse.authorisation.token);
        let data = {
          user: decodedToken.user,
          token: dataResponse.authorisation.token,
        };
        dispatch(register(data));
        setTimeout(() => {
          setIsError(true);
          navigation.navigate(EMAIL_CONFIRM);
        }, 2000);
      } else {
        setIsError(true);
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Register Failed',
          text2: response?.message ?? 'Your Register Failed',
        });
      }
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
      handlingErrors(error, 'Your Register');
    }
  };
  return {isLoading, accessRegister, isError};
};

export default usePostRegister;
