import {useNavigation} from '@react-navigation/native';
import React from 'react';
import Toast from 'react-native-toast-message';
import {useDispatch} from 'react-redux';
import {useAxiosContext} from '../contexts';
import {login} from '../redux/reducers/auth.reducers';
import {
  DASHBOARD,
  DASHBOARD_HOME,
  PREFERENCES,
  PRELIMINARY,
  UPDATE_PROFILE,
  UrlIDXWebsite,
  UrlTicmi,
  handlingErrors,
} from '../utility';
import useGetProfileInfo from './useGetProfileInfo';
import { jwtDecode } from 'jwt-decode';

const useGetLoginTicmi = () => {
  const {publicAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {accessGetProfile} = useGetProfileInfo();

  const accessLoginTicmi = async data => {
    setIsLoading(false);
    try {
      const response = await publicAxios.get(
        `${UrlTicmi}/callback?jwt_token=${encodeURIComponent(data.jwt_token)}`,
      );
      console.log('response', response);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Login Success',
          visibilityTime: 2000,
          onHide: () => {
            const decodedToken = jwtDecode(dataResponse.authorisation.token);
            let data = {
              user: decodedToken.user,
              token: dataResponse.authorisation.token,
            };
            if (
              data.user.is_profile_updated &&
              data.user.is_placement_test_taken &&
              data.user.is_preference_updated
            ) {
              dispatch(login(data));
              accessGetProfile(data.token);
              setIsLoading(false);
              navigation.navigate(DASHBOARD, {
                screen: DASHBOARD_HOME,
              });
            } else {
              dispatch(login(data));
              accessGetProfile(data.token);
              setIsLoading(false);
              if (!data.user.is_profile_updated) {
                navigation.navigate(UPDATE_PROFILE);
                return;
              } else if (!data.user.is_preference_updated) {
                navigation.navigate(PREFERENCES);
                return;
              } else {
                navigation.navigate(PRELIMINARY);
                return;
              }
            }
          },
        });
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Login Failed',
          text2: response.message ?? 'Your Login Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Login');
    }
  };
  return {isLoading, accessLoginTicmi};
};

export default useGetLoginTicmi;
