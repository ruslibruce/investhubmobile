import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlNewsRSS, handlingErrors } from '../utility';

const useGetNewsDetailRSS = () => {
  const {authAxios} = useAxiosContext();
  const [dataNewsDetailRSS, setDataNewsDetailRSS] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  const accessNewsDetailRSS = async id => {
    try {
      const response = await authAxios.get(`${UrlNewsRSS}${id}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataNewsDetailRSS(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Detail Failed',
          text2: response.message ?? 'Your Access News Detail Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access News Detail');
    }
  };
  return {
    isLoading,
    dataNewsDetailRSS,
    accessNewsDetailRSS,
  };
};

export default useGetNewsDetailRSS;
