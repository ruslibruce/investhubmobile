import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCourseReport, handlingErrors } from '../utility';

const usePostReport = () => {
  const {authAxios} = useAxiosContext();
  const [dataReport, setDataReport] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isError, setIsError] = React.useState(false);

  const accessPostReport = async data => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await authAxios.post(UrlCourseReport, data);
      if (response.status === 200) {
        const dataResponse = response.data;
        setDataReport(dataResponse);
        setIsLoading(false);
        setIsError(true);
      } else {
        setIsLoading(false);
        setIsError(true);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Post Report Failed',
          text2: response.message ?? 'Your Post Report Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
      handlingErrors(error, 'Post Report');
    }
  };
  return {dataReport, isLoading, accessPostReport, isError};
};

export default usePostReport;
