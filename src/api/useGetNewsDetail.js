import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlNews, handlingErrors } from '../utility';

const useGetNewsDetail = (id) => {
  const {authAxios} = useAxiosContext();
  const [dataNewsDetail, setDataNewsDetail] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessNewsDetail();
  },[])

  const accessNewsDetail = async () => {
    try {
      const response = await authAxios.get(`${UrlNews}/id/${id}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataNewsDetail(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Detail Failed',
          text2: response.message ?? 'Your Access News Detail Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access News Detail');
    }
  };
  return {
    isLoading,
    dataNewsDetail,
  };
};

export default useGetNewsDetail;
