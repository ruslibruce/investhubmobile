import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlSearchAll, handlingErrors, paramsToString} from '../utility';

const useGetSearchAll = () => {
  const {authAxios} = useAxiosContext();
  const [dataSearchAll, setDataSearchAll] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [params, setParams] = React.useState({
    page: 1,
    limit: 10,
    search: '',
  });
  const [totalPage, setTotalPage] = React.useState(1);
  const [isLoadingPage, setIsLoadingPage] = React.useState(false);

  React.useEffect(() => {
    accessSearchAll(params);
  }, [params]);

  const accessSearchAll = async params => {
    if (!params.search) {
      setDataSearchAll([]);
      return
    }
    try {
      setIsLoading(true);
      const response = await authAxios.get(
        `${UrlSearchAll}${paramsToString(params)}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data;
        console.log('dataResponse', JSON.stringify(dataResponse, null, 2));
        let dataFilter = dataResponse.filter(item => {
          return !item.question;
        });
        // console.log('dataFilter', JSON.stringify(dataFilter, null, 2));
        setDataSearchAll(prev =>
          isLoadingPage ? [...prev, ...dataFilter] : dataFilter,
        );
        setTotalPage(response.data.totalPages);
        setIsLoading(false);
        setIsLoadingPage(false);
      } else {
        setIsLoading(false);
        setIsLoadingPage(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Failed',
          text2: response.message ?? 'Your Access News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsLoadingPage(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {
    isLoading,
    dataSearchAll,
    setParams,
    isLoadingPage,
    setIsLoadingPage,
    totalPage,
    params,
  };
};

export default useGetSearchAll;
