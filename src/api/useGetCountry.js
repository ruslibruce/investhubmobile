import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlGetCountry, handlingErrors } from '../utility';

const useGetCountry = () => {
  const {authAxios} = useAxiosContext();
  const [dataCountry, setDataCountry] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);

  React.useEffect(() => {
    accessGetCountry();
  }, []);

  const accessGetCountry = async () => {
    setIsLoading(true);
    try {
      const response = await authAxios.get(UrlGetCountry);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        let temp = dataResponse.map(item => {
          item.label = item.name;
          item.value = item.id;
          return item;
        });
        setDataCountry(temp);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Access Search News & Event Failed',
          text2: response.message ?? 'Your Access Search News & Event Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Search Course');
    }
  };
  return {dataCountry, isLoading};
};

export default useGetCountry;
