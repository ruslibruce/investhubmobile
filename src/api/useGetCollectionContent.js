import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCollectionContent, handlingErrors } from '../utility';

const useGetCollectionContent = params => {
  const {authAxios} = useAxiosContext();
  const [dataCollectionContent, setDataCollectionContent] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    if (params) {
      accessGetCollectionContent(params);
    }
  }, [params]);

  const accessGetCollectionContent = async value => {
    try {
      const response = await authAxios.get(
        `${UrlCollectionContent}/id/${value}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataCollectionContent(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Collection Content Failed',
          text2: response.message ?? 'Your Access Collection Content Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Collection Content');
    }
  };
  return {dataCollectionContent, isLoading};
};

export default useGetCollectionContent;
