import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useDispatch } from 'react-redux';
import { useAxiosContext } from '../contexts';
import { changeEmail } from '../redux/reducers/auth.reducers';
import { EMAIL_CONFIRM, UrlChangeEmail, handlingErrors } from '../utility';

const usePostChangeEmail = () => {
  const {authAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();

  const accessChangeEmail = async value => {
    setIsLoading(true);
    try {
      const response = await authAxios.post(UrlChangeEmail, value);
      if (response.status === 200) {
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Email Change',
          text2: translate('Email_Success'),
          visibilityTime: 2000,
        });
        dispatch(changeEmail(value.email));
        setTimeout(() => {
          navigation.navigate(EMAIL_CONFIRM);
        }, 2000);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Register Failed',
          text2: response?.message ?? 'Your Email Change Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Email Change');
    }
  };
  return {isLoading, accessChangeEmail};
};

export default usePostChangeEmail;
