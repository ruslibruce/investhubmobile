import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useDispatch } from 'react-redux';
import { useAxiosContext } from '../contexts';
import { deleteValidateValue } from '../redux/reducers/app.reducers';
import { LOGIN, UrlResetPassword, handlingErrors } from '../utility';

const usePostResetPassword = () => {
  const {publicAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();

  const accessResetPassword = async value => {
    setIsLoading(true);
    try {
      const response = await publicAxios.post(UrlResetPassword, value);
      if (response.status === 200) {
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Password Reset',
          text2: translate('Desc_Modal_ForgotPasswordForm'),
          visibilityTime: 2000,
        });
        dispatch(deleteValidateValue(value));
        setTimeout(() => {
          navigation.navigate(LOGIN);
        }, 2000);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Password Reset Failed',
          text2: response?.message ?? 'Your Password Reset Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Password Reset');
    }
  };
  return {isLoading, accessResetPassword};
};

export default usePostResetPassword;
