import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlQuizSave, handlingErrors} from '../utility';

const usePostSaveQuizLevelUp = () => {
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(true);
  const [isSuccess, setIsSuccess] = React.useState(false);

  const saveDataQuiz = async (test_attempt_id, answers, remaining_time) => {
    setIsSuccess(false);
    let body = {
      test_attempt_id,
      answers,
      remaining_time,
    };
    try {
      const response = await authAxios.post(UrlQuizSave, body);
      if (response.status === 200) {
        const dataResponse = response.data;
        // console.log('dataResponse save Quiz', dataResponse);
        setIsSuccess(true);
        setIsLoading(false);
      } else {
        setIsSuccess(false);
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Post Quiz Levelup Failed',
          text2: response.message ?? 'Your Access Post Quiz Levelup Failed',
        });
      }
    } catch (error) {
      setIsSuccess(false);
      setIsLoading(false);
      handlingErrors(error, 'Access Post Quiz Levelup');
    }
  };
  return {saveDataQuiz, isSuccess};
};

export default usePostSaveQuizLevelUp;
