import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlCourseComment, UrlPrivacyPolicy, handlingErrors} from '../utility';

const useGetComment = () => {
  const {authAxios} = useAxiosContext();
  const [dataComment, setDataComment] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  const accessComment = async (id, type) => {
    setIsLoading(true);
    try {
      const response = await authAxios.get(
        `${UrlCourseComment}/${id}?type=${type}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data;
        setDataComment(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Get Comment Failed',
          text2: response.message ?? 'Your Access Get Comment Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Get Comment');
    }
  };
  return {dataComment, isLoading, accessComment};
};

export default useGetComment;
