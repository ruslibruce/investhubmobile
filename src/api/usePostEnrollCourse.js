import {useTranslation} from 'react-i18next';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {
  COURSE_DETAIL_PROGRESS,
  UrlCourseEnroll,
  UrlCourseProgress,
  handlingErrors,
} from '../utility';
import {useNavigation} from '@react-navigation/native';
import React from 'react';

const usePostEnrollCourse = () => {
  const {authAxios} = useAxiosContext();
  const {t: translate} = useTranslation();
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = React.useState(false);

  const accessPostEnrollCourse = async value => {
    setIsLoading(true);
    try {
      const response = await authAxios.post(UrlCourseEnroll, value);
      if (response.status === 200) {
        setIsLoading(false);
        navigation.navigate(COURSE_DETAIL_PROGRESS, {
          id: value.id,
        });
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Enroll Course Failed',
          text2: response?.message ?? 'Your Post Enroll Course Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Post Enroll Course');
    }
  };
  return {accessPostEnrollCourse, isLoading};
};

export default usePostEnrollCourse;
