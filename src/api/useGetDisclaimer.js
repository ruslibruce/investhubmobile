import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlDisclaimer, handlingErrors } from '../utility';

const useGetDisclaimer = () => {
  const {authAxios} = useAxiosContext();
  const [dataDisclaimer, setDataDisclaimer] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessDisclaimer();
  }, []);

  const accessDisclaimer = async () => {
    try {
      const response = await authAxios.get(UrlDisclaimer);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataDisclaimer(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Disclaimer Failed',
          text2: response.message ?? 'Your Access Disclaimer Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Disclaimer');
    }
  };
  return {dataDisclaimer, isLoading};
};

export default useGetDisclaimer;
