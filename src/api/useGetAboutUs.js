import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlAbout, handlingErrors } from '../utility';

const useGetAboutUs = () => {
  const {authAxios} = useAxiosContext();
  const [dataAbout, setDataAbout] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessAbout();
  }, []);

  const accessAbout = async () => {
    try {
      const response = await authAxios.get(UrlAbout);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataAbout(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Akses About Failed',
          text2: response.message ?? 'Your Akses About Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Akses About');
    }
  };
  return {dataAbout, isLoading};
};

export default useGetAboutUs;
