import React from 'react';
import {useAxiosContext} from '../contexts';
import {UrlResendEmail} from '../utility';
import NoData from '../components/NoData';

const usePostResendEmail = () => {
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(false);
  const [isSuccess, setIsSuccess] = React.useState(false);
  const [isError, setIsError] = React.useState(false);

  const accessPostResendEmail = async data => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await authAxios.post(UrlResendEmail, data);
      if (response.status === 200) {
        setIsLoading(false);
        setIsSuccess(true);
        setIsError(true);
      } else {
        setIsLoading(false);
        setIsSuccess(false);
        setIsError(true);
      }
    } catch (error) {
      setIsLoading(false);
      setIsSuccess(false);
      setIsError(true);
    }
  };
  return {accessPostResendEmail, isLoading, isSuccess, isError};
};

export default usePostResendEmail;
