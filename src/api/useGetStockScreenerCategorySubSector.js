import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlStockScreenerCategorySubSector, handlingErrors } from '../utility';

const useGetStockScreenerCategorySubSector = () => {
  const {idxAuthAxios} = useAxiosContext();
  const [
    dataStockScreenerCategorySubSector,
    setDataStockScreenerCategorySubSector,
  ] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  const accessStockScreenerCategorySub = async params => {
    try {
      const response = await idxAuthAxios.get(
        `${UrlStockScreenerCategorySubSector}${params}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data;
        let dataTemp = [];
        dataResponse.map(item => {
          let itemData = {
            label: item,
            value: item,
          };
          dataTemp.push(itemData);
        });
        setDataStockScreenerCategorySubSector(dataTemp);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Category Sub Sector Failed',
          text2: response.message ?? 'Your Access Category Sub Sector Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Category Sub Sector');
    }
  };
  return {
    dataStockScreenerCategorySubSector,
    accessStockScreenerCategorySub,
    isLoading,
  };
};

export default useGetStockScreenerCategorySubSector;
