import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlStockScreenerCategorySector, handlingErrors } from '../utility';

const useGetStockScreenerCategorySector = () => {
  const {idxAuthAxios} = useAxiosContext();
  const [dataStockScreenerCategorySector, setDataStockScreenerCategorySector] =
    React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessStockScreenerCategory();
  }, []);

  const accessStockScreenerCategory = async () => {
    try {
      const response = await idxAuthAxios.get(UrlStockScreenerCategorySector);
      if (response.status === 200) {
        const dataResponse = response.data;
        let dataTemp = [];
        dataResponse.map(item => {
          let itemData = {
            label: item,
            value: item,
          };
          dataTemp.push(itemData);
        });
        setDataStockScreenerCategorySector(dataTemp);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Category Sector Failed',
          text2: response.message ?? 'Your Access Category Sector Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Category Sector');
    }
  };
  return {dataStockScreenerCategorySector, isLoading};
};

export default useGetStockScreenerCategorySector;
