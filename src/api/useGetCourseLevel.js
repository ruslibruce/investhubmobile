import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCourseLevel, handlingErrors } from '../utility';

const useGetCourseLevel = () => {
  const {authAxios} = useAxiosContext();
  const [dataCourseLevel, setDataCourseLevel] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessCourseLevel();
  }, []);

  const accessCourseLevel = async () => {
    try {
      const response = await authAxios.get(`${UrlCourseLevel}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        let dataResult = [];
        dataResponse.map(item => {
          dataResult.push({
            id: item.id,
            label: item.name,
            value: item.id,
          });
        });
        setDataCourseLevel(dataResult);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Failed',
          text2: response.message ?? 'Your Access News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {isLoading, dataCourseLevel, setDataCourseLevel};
};

export default useGetCourseLevel;
