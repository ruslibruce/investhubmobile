import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlSearchNews, handlingErrors} from '../utility';

const useGetSearchNews = () => {
  const {publicAxios} = useAxiosContext();
  const [dataSearchNews, setDataSearchNews] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);

  const accessSearchNews = async (params = '') => {
    setIsLoading(true);
    try {
      const response = await publicAxios.get(`${UrlSearchNews}${params}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataSearchNews(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Access Search News Failed',
          text2: response.message ?? 'Your Access Search News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Search News');
    }
  };
  return {dataSearchNews, accessSearchNews, isLoading};
};

export default useGetSearchNews;
