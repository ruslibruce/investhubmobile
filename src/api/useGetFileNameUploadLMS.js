import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { handlingErrors, UrlUploadFileLMS } from '../utility';

const useGetFileNameUploadLMS = () => {
  const {authFormDataAxios} = useAxiosContext();
  const [dataFilename, setDataFilename] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(true);
  const [percentage, setPercentage] = React.useState(0);

  const accessUploadFile = async (data) => {
    let percent = 0;
    const config = {
      onUploadProgress: progressEvent => {
        const {loaded, total} = progressEvent;
        percent = Math.floor((loaded * 100) / total);
        console.log(`${loaded}kb of ${total}kb | ${percent}%`); // just to see whats happening in the console

        if (percent <= 100) {
          setPercentage(percent); // hook to set the value of current level that needs to be passed to the progressbar
        }
      },
    };
    try {
      const response = await authFormDataAxios.postForm(
        UrlUploadFileLMS,
        data,
        config,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data.filename;
        setDataFilename(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Upload Failed',
          text2: response.message ?? 'Your Upload File Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Upload File');
    }
  };
  return {dataFilename, isLoading, accessUploadFile, setDataFilename, percentage};
};

export default useGetFileNameUploadLMS;
