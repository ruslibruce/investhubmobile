import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCertificate, handlingErrors } from '../utility';

const useGetCertificate = () => {
  const {authAxios} = useAxiosContext();
  const [dataCertificate, setDataCertificate] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessCertificate();
  }, []);

  const accessCertificate = async () => {
    try {
      const response = await authAxios.get(UrlCertificate);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataCertificate(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Certificate Failed',
          text2: response.message ?? 'Your Access Certificate Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Certificate');
    }
  };
  return {dataCertificate, isLoading};
};

export default useGetCertificate;
