import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCollection, handlingErrors } from '../utility';

const useGetCollection = () => {
  const {authAxios} = useAxiosContext();
  const [dataCollection, setDataCollection] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);

  React.useEffect(() => {
    accessCollection();
  }, []);

  const accessCollection = async () => {
    setIsLoading(true);
    try {
      const response = await authAxios.get(UrlCollection);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataCollection(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Collection Failed',
          text2: response.message ?? 'Your Access Collection Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Collection');
    }
  };
  return {dataCollection, isLoading, accessCollection};
};

export default useGetCollection;
