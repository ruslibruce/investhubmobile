import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlNotification, handlingErrors } from '../utility';

const usePutNotification = () => {
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(true);
  const [dataResult, setDataResult] = React.useState('');

  const accessReadNotif = async (id, data) => {
    try {
      const response = await authAxios.put(`${UrlNotification}/${id}`, data);
      if (response.status === 200) {
        const dataResponse = response.data;
        setDataResult(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Upload Sertifikat Failed',
          text2: response.message ?? 'Your Upload Sertifikat Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Upload Sertifikat');
    }
  };
  return {isLoading, accessReadNotif, dataResult};
};

export default usePutNotification;
