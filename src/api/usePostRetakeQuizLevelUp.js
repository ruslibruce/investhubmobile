import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlQuizRetake, handlingErrors } from '../utility';

const usePostRetakeQuizLevelUp = () => {
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(true);
  const [isSuccess, setIsSuccess] = React.useState(false);

  const retakeDataQuiz = async test_attempt_id => {
    setIsSuccess(false);
    let body = {
      test_attempt_id,
    };
    try {
      const response = await authAxios.post(UrlQuizRetake, body);
      if (response.status === 200) {
        const dataResponse = response.data;
        // console.log('dataResponse save Quiz', dataResponse);
        setIsSuccess(true);
        setIsLoading(false);
      } else {
        setIsSuccess(false);
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Post Retake Quiz Failed',
          text2: response.message ?? 'Your Access Post Retake Quiz Failed',
        });
      }
    } catch (error) {
      setIsSuccess(false);
      setIsLoading(false);
      handlingErrors(error, 'Access Post Retake Quiz');
    }
  };
  return {retakeDataQuiz, isSuccess};
};

export default usePostRetakeQuizLevelUp;
