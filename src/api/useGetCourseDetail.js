import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlCourseDetail, handlingErrors} from '../utility';

const useGetCourseDetail = () => {
  const {authAxios} = useAxiosContext();
  const [dataCourseDetail, setDataCourseDetail] = React.useState({});
  const [isLoading, setIsLoading] = React.useState(true);

  const accessCourseDetail = async (idCourse, isLoading) => {
    isLoading && setIsLoading(true);
    try {
      const response = await authAxios.get(`${UrlCourseDetail}/${idCourse}`);
      if (response.status === 200) {
        setIsLoading(false);
        const dataResponse = response.data.data;
        setDataCourseDetail(dataResponse);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Login Failed',
          text2: response.message ?? 'Your Login Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Course Detail');
    }
  };
  return {dataCourseDetail, accessCourseDetail, isLoading};
};

export default useGetCourseDetail;
