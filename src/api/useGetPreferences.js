import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlPreferences, handlingErrors } from '../utility';

const useGetPreferences = () => {
  const {authAxios} = useAxiosContext();
  const [dataPreferences, setDataPreferences] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessPreferences();
  }, []);

  const accessPreferences = async () => {
    try {
      const response = await authAxios.get(UrlPreferences);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        dataResponse.map(item => {
          item.choosen = item.check === 1 ? true : false;
        });
        setDataPreferences(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Preferences Failed',
          text2: response.message ?? 'Your Access Preferences Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Preferences');
    }
  };
  return {isLoading, dataPreferences};
};

export default useGetPreferences;
