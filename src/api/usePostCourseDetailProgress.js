import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCourseProgress, handlingErrors } from '../utility';

const usePostCourseDetailProgress = () => {
  const {authAxios} = useAxiosContext();
  const {t: translate} = useTranslation();

  const accessPostProgress = async value => {
    try {
      console.log('data progress', value);
      const response = await authAxios.post(UrlCourseProgress, value);
      console.log('data progress', response.data);
      if (response.status === 200) {
      } else {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Register Failed',
          text2: response?.message ?? 'Your Post Progress Failed',
        });
      }
    } catch (error) {
      handlingErrors(error, 'Your Post Progress');
    }
  };
  return {accessPostProgress};
};

export default usePostCourseDetailProgress;
