import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import {
  UrlWebIDXStockScreener,
  handleOpenLink,
  handlingErrors,
  paramsToString,
} from '../utility';

const useGetUrlWebIDXStockScreener = () => {
  const {publicAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(false);
  const {i18n} = useTranslation();

  const accessGetUrlWebIDXStockScreener = async value => {
    setIsLoading(true);
    try {
      const response = await publicAxios.get(UrlWebIDXStockScreener);
      console.log('response', response);
      if (response.status === 200) {
        const dataResponse = response.data;
        console.log('dataResponse', dataResponse);
        setIsLoading(false);
        handleOpenLink(
          `${dataResponse}/${
            i18n.language
          }/investhub/stock-screener${paramsToString(value)}`,
        );
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Get Url IDX Failed',
          text2: response.message ?? 'Your Get Url IDX Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Get Url IDX');
    }
  };
  return {isLoading, accessGetUrlWebIDXStockScreener};
};

export default useGetUrlWebIDXStockScreener;
