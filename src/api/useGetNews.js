import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlNewsPortal, handlingErrors, paramsToString } from '../utility';

const useGetNews = () => {
  const {authAxios} = useAxiosContext();
  const [dataNews, setDataNews] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  const [params, setParams] = React.useState({
    page: 1,
    category: '',
    limit: 10,
  });
  const [totalPage, setTotalPage] = React.useState(1);
  const [isLoadingPage, setIsLoadingPage] = React.useState(false);

  React.useEffect(() => {
    accessNews(params);
  }, [params]);

  const accessNews = async params => {
    try {
      const response = await authAxios.get(
        `${UrlNewsPortal}${paramsToString(params)}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data;
        const filterRSS = dataResponse.map(item => {
          if (item.category === null || item.category === '') {
            item.category = 'RSS NEWS';
          }
          return item;
        });
        setDataNews(prev =>
          isLoadingPage ? [...prev, ...filterRSS] : filterRSS,
        );
        setIsLoading(false);
        setTotalPage(response.data.npage);
        setIsLoadingPage(false);
      } else {
        setIsLoading(false);
        setIsLoadingPage(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Failed',
          text2: response.message ?? 'Your Access News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsLoadingPage(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {
    isLoading,
    dataNews,
    setParams,
    isLoadingPage,
    setIsLoadingPage,
    totalPage,
    params,
  };
};

export default useGetNews;
