import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlEvent, handlingErrors } from '../utility';

const useGetEventDetail = id => {
  const {authAxios} = useAxiosContext();
  const [dataEventDetail, setDataEventDetail] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessEventDetail();
  }, []);

  const accessEventDetail = async () => {
    try {
      const response = await authAxios.get(`${UrlEvent}/id/${id}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataEventDetail(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Event Detail Failed',
          text2: response.message ?? 'Your Access Event Detail Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Event Detail');
    }
  };
  return {
    isLoading,
    dataEventDetail,
  };
};

export default useGetEventDetail;
