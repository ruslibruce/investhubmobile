import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCourseComment, handlingErrors } from '../utility';

const usePostComment = () => {
  const {authAxios} = useAxiosContext();
  const [dataComment, setDataComment] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isSuccess, setIsSuccess] = React.useState(false);

  const accessPostComment = async data => {
    setIsLoading(true);
    setIsSuccess(false);
    try {
      const response = await authAxios.post(UrlCourseComment, data);
      if (response.status === 200) {
        const dataResponse = response.data;
        setIsSuccess(true);
        setIsLoading(false);
      } else {
        setIsSuccess(false);
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Post Comment Failed',
          text2: response.message ?? 'Your Post Comment Failed',
        });
      }
    } catch (error) {
      setIsSuccess(false);
      setIsLoading(false);
      handlingErrors(error, 'Post Comment');
    }
  };
  return {isLoading, accessPostComment, isSuccess};
};

export default usePostComment;
