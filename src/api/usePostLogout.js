import React from 'react';
import Toast from 'react-native-toast-message';
import {UrlLogout, handlingErrors} from '../utility';
import axios from 'axios';
import Config from 'react-native-config';
import { useDispatch } from 'react-redux';
import { logout } from '../redux/reducers/auth.reducers';

const usePostLogout = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const dispatch = useDispatch();

  const accessLogout = async token => {
    setIsLoading(true);
    try {
      const response = await axios.post(
        `${Config.BASE_URL}${UrlLogout}`,
        null,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
      );
      if (response.status === 200) {
        const dataResponse = response.data;
        console.log('data logout======>', dataResponse, '<============');
        dispatch(logout());
        setIsLoading(false);
      } else {
        dispatch(logout());
        setIsLoading(false);
      }
    } catch (error) {
      dispatch(logout());
      setIsLoading(false);
    }
  };
  return {isLoading, accessLogout};
};

export default usePostLogout;
