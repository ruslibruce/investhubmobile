import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlForgotPassword, handlingErrors } from '../utility';

const usePostForgotPassword = () => {
  const {publicAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(false);
  const [isSuccess, setIsSuccess] = React.useState(false);
  const [isError, setIsError] = React.useState(false);

  const accessPostForgotPassword = async data => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await publicAxios.post(UrlForgotPassword, data);
      if (response.status === 200) {
        const dataResponse = response.data;
        setIsLoading(false);
        setIsSuccess(true);
        setIsError(true);
      } else {
        setIsError(true);
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Forgot Password Failed',
          text2: response.message ?? 'Your Access Forgot Password Failed',
        });
        setIsSuccess(false);
      }
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
      handlingErrors(error, 'Access Forgot Password');
      setIsSuccess(false);
    }
  };
  return {accessPostForgotPassword, isLoading, isSuccess, isError};
};

export default usePostForgotPassword;
