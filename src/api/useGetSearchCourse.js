import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlSearchCourse, handlingErrors } from '../utility';

const useGetSearchCourse = () => {
  const {publicAxios} = useAxiosContext();
  const [dataSearchCourse, setDataSearchCourse] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);

  const accessSearchCourse = async params => {
    setIsLoading(true);
    try {
      const response = await publicAxios.get(`${UrlSearchCourse}${params}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataSearchCourse(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Access Search News & Event Failed',
          text2: response.message ?? 'Your Access Search News & Event Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Search Course');
    }
  };
  return {dataSearchCourse, accessSearchCourse, isLoading};
};

export default useGetSearchCourse;
