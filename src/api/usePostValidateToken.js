import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useDispatch } from 'react-redux';
import { useAxiosContext } from '../contexts';
import { addValidateValue } from '../redux/reducers/app.reducers';
import {
  RESET_PASSWORD_FORM,
  UrlValidateResetToken,
  handlingErrors,
} from '../utility';

const usePostValidateToken = () => {
  const {publicAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const [isError, setIsError] = React.useState(false);

  const accessValidateToken = async value => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await publicAxios.post(UrlValidateResetToken, value);
      if (response.status === 200) {
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate("Title_Modal_ValidateTokenForm"),
          text2: translate("Desc_Modal_ValidateTokenForm"),
          visibilityTime: 2000,
          onHide: () => {
            dispatch(addValidateValue(value));
            navigation.navigate(RESET_PASSWORD_FORM);
            setIsError(true);
          }
        });
      } else {
        setIsLoading(false);
        setIsError(true);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Validate Token Failed',
          text2: response?.message ?? 'Your Validate Token Failed',
        });
      }
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
      handlingErrors(error, 'Your Validate Token');
    }
  };
  return {isLoading, accessValidateToken, isError};
};

export default usePostValidateToken;
