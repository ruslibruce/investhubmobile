import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlQuizSubmit, handlingErrors } from '../utility';
import { useDispatch, useSelector } from 'react-redux';
import { login } from '../redux/reducers/auth.reducers';

const usePostSubmitQuizLevelUp = () => {
  const {authAxios} = useAxiosContext();
  const [dataSubmitQuiz, setDataSubmitQuiz] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const authMethod = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const accessSubmit = async (value, answers) => {
    setIsLoading(true);
    let body = {
      test_attempt_id: value,
      answers,
    };
    try {
      const response = await authAxios.post(UrlQuizSubmit, body);
      if (response.status === 200) {
        if (!authMethod.user.is_placement_test_taken) {
          let temp = {
            ...authMethod,
            user: {
              ...authMethod.user,
              is_placement_test_taken: true,
            },
          };
          dispatch(login(temp));
        }
        const dataResponse = response.data.data;
        setDataSubmitQuiz(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Post Quiz Levelup Failed',
          text2: response.message ?? 'Your Access Post Quiz Levelup Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Post Quiz Levelup');
    }
  };
  return {dataSubmitQuiz, setDataSubmitQuiz, isLoading, accessSubmit};
};

export default usePostSubmitQuizLevelUp;
