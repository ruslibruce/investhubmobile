import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {
  UrlCourseRecomendations,
  handlingErrors,
  paramsToString,
} from '../utility';
import { useSelector } from 'react-redux';

const useGetCourseRecomendation = () => {
  const {authAxios} = useAxiosContext();
  const [dataCourse, setDataCourse] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [params, setParams] = React.useState({
    page: 1,
    category: '',
    limit: 10,
  });
  const [totalPage, setTotalPage] = React.useState(1);
  const [isLoadingPage, setIsLoadingPage] = React.useState(false);
  const authMethod = useSelector(state => state.auth);

  React.useEffect(() => {
    if(authMethod.user?.is_preference_updated){
      accessCourse(params);
    }
  }, [params, authMethod]);

  const accessCourse = async params => {
    setIsLoading(true);
    try {
      const response = await authAxios.get(
        `${UrlCourseRecomendations}${paramsToString(params)}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataCourse(prev =>
          isLoadingPage ? [...prev, ...dataResponse] : dataResponse,
        );
        setTotalPage(response.data.npage);
        setIsLoading(false);
        setIsLoadingPage(false);
      } else {
        setIsLoading(false);
        setIsLoadingPage(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Event Failed',
          text2: response.message ?? 'Your Access Event Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsLoadingPage(false);
      handlingErrors(error, 'Access Event');
    }
  };
  return {
    isLoading,
    dataCourse,
    setParams,
    isLoadingPage,
    setIsLoadingPage,
    totalPage,
    params,
  };
};

export default useGetCourseRecomendation;
