import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlStockScreener, handlingErrors } from '../utility';

const useGetStockScreener = () => {
  const {idxAxios} = useAxiosContext();
  const [dataStockScreener, setDataStockScreener] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessStockScreener();
  }, []);

  const accessStockScreener = async (params = '') => {
    try {
      const response = await idxAxios.get(`${UrlStockScreener}${params}`);
      if (response.status === 200) {
        const dataResponse = response.data.results;
        setDataStockScreener(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Stock Screener Failed',
          text2: response.message ?? 'Your Access Stock Screener Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Stock Screener');
    }
  };
  return {accessStockScreener, dataStockScreener, isLoading};
};

export default useGetStockScreener;
