import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlDeleteAccount, handlingErrors } from '../utility';
import usePostLogout from './usePostLogout';

const usePostDeleteAccount = () => {
  const {authAxios} = useAxiosContext();
  const [dataDelete, setDataDelete] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isError, setIsError] = React.useState(false);
  const {accessLogout} = usePostLogout();

  const accessPostDeleteAccount = async token => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await authAxios.delete(UrlDeleteAccount);
      if (response.status === 200) {
        const dataResponse = response.data;
        console.log('data response delete', dataResponse)
        accessLogout(token)
        setIsLoading(false);
        setIsError(true);
      } else {
        setIsLoading(false);
        setIsError(true);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Post Delete Account Failed',
          text2: response.message ?? 'Your Post Delete Account Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
      handlingErrors(error, 'Post Delete Account');
    }
  };
  return {isLoading, accessPostDeleteAccount, isError};
};

export default usePostDeleteAccount;
