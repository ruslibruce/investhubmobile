import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCourseCategory, handlingErrors } from '../utility';

const useGetCourseCategory = () => {
  const {authAxios} = useAxiosContext();
  const [dataCourseCategory, setDataCourseCategory] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessCourseCategory();
  }, []);

  const accessCourseCategory = async () => {
    try {
      const response = await authAxios.get(`${UrlCourseCategory}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        let dataResult = [];
        dataResponse.map(item => {
          dataResult.push({
            id: item.id,
            label: item.name,
            value: item.id,
          });
        });
        setDataCourseCategory(dataResult);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Failed',
          text2: response.message ?? 'Your Access News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {isLoading, dataCourseCategory, setDataCourseCategory};
};

export default useGetCourseCategory;
