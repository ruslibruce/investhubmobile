import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlChangePassword, handlingErrors } from '../utility';

const usePostSetNewPassword = () => {
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();

  const accessNewPassword = async value => {
    setIsLoading(true);
    try {
      const response = await authAxios.post(UrlChangePassword, value);
      if (response.status === 200) {
        setIsLoading(false);
        const dataResponse = response.data.data;
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate('PasswordSuccess'),
          visibilityTime: 2000,
          onHide: () => {
            navigation.goBack();
          },
        });
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Set Password Failed',
          text2: response.message ?? 'Your Set Password Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Set Password');
    }
  };
  return {isLoading, accessNewPassword};
};

export default usePostSetNewPassword;
