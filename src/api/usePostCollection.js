import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlCollection, handlingErrors} from '../utility';
import {useTranslation} from 'react-i18next';

const usePostCollection = () => {
  const {t: translate} = useTranslation();
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(false);

  const accessPostCollection = async value => {
    setIsLoading(true);
    let data = {
      name: value,
    };
    try {
      const response = await authAxios.post(UrlCollection, data);
      if (response.status === 200) {
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Access Add Collection',
          text2: response.message ?? translate('CollectionTitleSuccess'),
        });
        return true;
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Add Collection Failed',
          text2: response.message ?? 'Your Access Add Collection Failed',
        });
        return false;
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Add Collection', true);
      return false;
    }
  };
  return {accessPostCollection, isLoading};
};

export default usePostCollection;
