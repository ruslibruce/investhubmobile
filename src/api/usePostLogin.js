import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import Toast from 'react-native-toast-message';
import {useDispatch} from 'react-redux';
import {useAxiosContext} from '../contexts';
import {login, register} from '../redux/reducers/auth.reducers';
import {
  DASHBOARD,
  DASHBOARD_HOME,
  EMAIL_CONFIRM,
  PREFERENCES,
  PRELIMINARY,
  UPDATE_PROFILE,
  UrlLogin,
  handlingErrors,
} from '../utility';
import useGetProfileInfo from './useGetProfileInfo';
import {jwtDecode} from 'jwt-decode';

const usePostLogin = () => {
  const {publicAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const {accessGetProfile} = useGetProfileInfo();
  const [isError, setIsError] = React.useState(false);

  const accessLogin = async value => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await publicAxios.post(UrlLogin, value);
      if (response.status === 200) {
        setIsLoading(false);
        const dataResponse = response.data.data;
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Login Success',
          visibilityTime: 2000,
          onHide: () => {
            const decodedToken = jwtDecode(dataResponse.authorisation.token);
            let data = {
              user: decodedToken.user,
              token: dataResponse.authorisation.token,
            };
            if (!data.user.is_email_verified) {
              dispatch(register(data));
              Toast.show({
                type: 'info',
                position: 'top',
                text1: translate('VerifyEmail'),
                text2: translate('VerifyEmailDesc'),
              });
              setIsError(true);
              navigation.navigate(EMAIL_CONFIRM);
              return;
            }
            if (
              data.user.is_profile_updated &&
              data.user.is_placement_test_taken &&
              data.user.is_preference_updated
            ) {
              dispatch(login(data));
              accessGetProfile(data.token);
              setIsError(true);
              navigation.navigate(DASHBOARD, {
                screen: DASHBOARD_HOME,
              });
            } else {
              dispatch(login(data));
              accessGetProfile(data.token);
              setIsError(true);
              if (!data.user.is_profile_updated) {
                navigation.navigate(UPDATE_PROFILE);
                return;
              } else if (!data.user.is_preference_updated) {
                navigation.navigate(PREFERENCES);
                return;
              } else {
                navigation.navigate(PRELIMINARY);
                return;
              }
            }
          },
        });
      } else {
        setIsError(true);
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Login Failed',
          text2: response.message ?? 'Your Login Failed',
        });
      }
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
      handlingErrors(error, 'Your Login');
    }
  };
  return {isLoading, accessLogin, isError};
};

export default usePostLogin;
