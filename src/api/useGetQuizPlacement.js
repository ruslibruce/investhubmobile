import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlPlacementTest, UrlQuizLevelUp, handlingErrors} from '../utility';

const useGetQuizPlacement = () => {
  const {authAxios} = useAxiosContext();
  const [dataQuizLevelUp, setDataQuizLevelUp] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  const accessQuiz = async () => {
    try {
      const response = await authAxios.get(`${UrlPlacementTest}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataQuizLevelUp(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Quiz Levelup Failed',
          text2: response.message ?? 'Your Access Quiz Levelup Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Quiz Levelup');
    }
  };
  return {dataQuizLevelUp, isLoading, accessQuiz};
};

export default useGetQuizPlacement;
