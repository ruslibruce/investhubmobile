import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import Toast from 'react-native-toast-message';
import {useDispatch, useSelector} from 'react-redux';
import {useAxiosContext} from '../contexts';
import {login} from '../redux/reducers/auth.reducers';
import {
  DASHBOARD_HOME,
  PRELIMINARY,
  UrlPreferences,
  handlingErrors,
} from '../utility';

const usePutPreferences = () => {
  const {authAxios} = useAxiosContext();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState(false);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);

  const accessPutPreferences = async value => {
    setIsLoading(true);
    try {
      const response = await authAxios.put(UrlPreferences, value);
      if (response.status === 200) {
        setIsLoading(false);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate('PreferenceSuccess'),
          visibilityTime: 2000,
          onHide: () => {
            let temp = {
              ...authMethod,
              user: {
                ...authMethod.user,
                is_preference_updated: true,
              },
            };
            if (!authMethod.user.is_placement_test_taken) {
              dispatch(login(temp));
              navigation.navigate(PRELIMINARY);
              return;
            } else {
              dispatch(login(temp));
              navigation.navigate(DASHBOARD_HOME);
              return;
            }
          },
        });
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Preferences Update Failed',
          text2: response.message ?? 'Your Preferences Update Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Preferences Update');
    }
  };
  return {isLoading, accessPutPreferences};
};

export default usePutPreferences;
