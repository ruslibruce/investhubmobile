import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import {
  UrlVerification,
  handlingErrors
} from '../utility';

const usePostVerification = () => {
  const {publicAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(true);
  const navigation = useNavigation();
  const {t: translate} = useTranslation();

  const accessVerification = async params => {
    try {
      const response = await publicAxios.get(`${UrlVerification}/${params}`);
      if (response.status === 200) {
        setIsLoading(false);
        const dataResponse = response.data;
        // console.log('data verification', dataResponse);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate('VerifySuccess'),
          visibilityTime: 2000,
        });
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Verification Failed',
          text2: response.message ?? 'Your Verification Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Your Verification');
    }
  };
  return {isLoading, accessVerification};
};

export default usePostVerification;
