import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { handlingErrors, UrlLearningJourney } from '../utility';

const useGetLearningJourney = () => {
  const {authAxios} = useAxiosContext();
  const [dataLearningJourney, setDataLearningJourney] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessJourney();
  }, []);

  const accessJourney = async () => {
    setIsLoading(true);
    try {
      const response = await authAxios.get(UrlLearningJourney);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataLearningJourney(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access FAQ Failed',
          text2: response.message ?? 'Your Access FAQ Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access FAQ');
    }
  };
  return {dataLearningJourney, isLoading, accessJourney};
};

export default useGetLearningJourney;
