import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlNotification, handlingErrors, paramsToString} from '../utility';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';

const useGetNotification = () => {
  const {i18n} = useTranslation();
  // console.log('i18n', i18n.language);
  const {authAxios} = useAxiosContext();
  const [dataNotification, setDataNotification] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  const [params, setParams] = React.useState({
    page: 1,
    limit: 10,
    app: 'portal',
    lan: i18n.language,
  });
  const [totalPage, setTotalPage] = React.useState(1);
  const [isLoadingPage, setIsLoadingPage] = React.useState(false);
  const [badge, setBadge] = React.useState(0);
  const authMethod = useSelector(state => state.auth);

  React.useEffect(() => {
    accessNotification(params, authMethod.isLogin);
  }, [params, authMethod]);

  const accessNotification = async (params, isLogin) => {
    try {
      if (!isLogin) {
        return;
      }
      const response = await authAxios.get(
        `${UrlNotification}${paramsToString(params)}`,
      );
      if (response.status === 200) {
        const dataResponse = response.data.data;
        // console.log('dataResponse notif', dataResponse);
        setDataNotification(prev =>
          isLoadingPage ? [...prev, ...dataResponse] : dataResponse,
        );
        setBadge(response.data.unread);
        setIsLoading(false);
        setTotalPage(response.data.npage);
        setIsLoadingPage(false);
      } else {
        setIsLoading(false);
        setIsLoadingPage(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access News Failed',
          text2: response.message ?? 'Your Access News Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsLoadingPage(false);
      handlingErrors(error, 'Access News');
    }
  };
  return {
    isLoading,
    dataNotification,
    setParams,
    isLoadingPage,
    setIsLoadingPage,
    totalPage,
    params,
    badge,
    accessNotification,
  };
};

export default useGetNotification;
