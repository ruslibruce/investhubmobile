import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlPrivacyPolicy, handlingErrors } from '../utility';

const useGetPrivacyPolicy = () => {
  const {authAxios} = useAxiosContext();
  const [dataPrivacyPolicy, setDataPrivacyPolicy] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessDisclaimer();
  }, []);

  const accessDisclaimer = async () => {
    try {
      const response = await authAxios.get(UrlPrivacyPolicy);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataPrivacyPolicy(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Privacy Policy Failed',
          text2: response.message ?? 'Your Access Privacy Policy Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Privacy Policy');
    }
  };
  return {dataPrivacyPolicy, isLoading};
};

export default useGetPrivacyPolicy;
