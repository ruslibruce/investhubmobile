import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlCourseLike, handlingErrors} from '../utility';
import {useTranslation} from 'react-i18next';

const usePostLike = () => {
  const {t: translate} = useTranslation();
  const {authAxios} = useAxiosContext();
  const [dataLike, setDataLike] = React.useState('');
  const [isSuccess, setIsSuccess] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);

  const accessPostLike = async data => {
    setIsSuccess(false);
    try {
      const response = await authAxios.post(UrlCourseLike, data);
      if (response.status === 200) {
        const dataResponse = response.data;
        setDataLike(dataResponse);
        setIsLoading(false);
        setIsSuccess(true);
        Toast.show({
          type: 'success',
          position: 'top',
          text1: translate('LikeSuccess'),
          text2: dataResponse.liked
            ? translate('DescLikeSuccess')
            : translate('DescUnLikeSuccess'),
        });
      } else {
        setIsLoading(false);
        setIsSuccess(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Post Like Failed',
          text2: response.message ?? 'Your Post Like Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      setIsSuccess(false);
      handlingErrors(error, 'Post Like');
    }
  };
  return {dataLike, isLoading, accessPostLike, isSuccess};
};

export default usePostLike;
