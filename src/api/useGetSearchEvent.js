import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlSearchEvent, handlingErrors} from '../utility';

const useGetSearchEvent = () => {
  const {publicAxios} = useAxiosContext();
  const [dataSearchEvent, setDataSearchEvent] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);

  const accessSearchNews = async (params = '') => {
    setIsLoading(true);
    try {
      const response = await publicAxios.get(`${UrlSearchEvent}${params}`);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataSearchEvent(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Access Search Event Failed',
          text2: response.message ?? 'Your Access Search Event Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Search Event');
    }
  };
  return {dataSearchEvent, accessSearchNews, isLoading};
};

export default useGetSearchEvent;
