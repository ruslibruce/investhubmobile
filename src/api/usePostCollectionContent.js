import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCollectionContent, handlingErrors } from '../utility';

const usePostCollectionContent = () => {
  const {authAxios} = useAxiosContext();
  const [isLoading, setIsLoading] = React.useState(false);

  const accessPostCollectionContent = async data => {
    setIsLoading(true);
    try {
      const response = await authAxios.post(UrlCollectionContent, data);
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setIsLoading(false);
        console.log('response accessPostCollectionContent', dataResponse);
        return true;
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Collection Failed',
          text2: response.message ?? 'Your Access Collection Failed',
        });
        return false;
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Collection');
      return false;
    }
  };
  return {accessPostCollectionContent, isLoading};
};

export default usePostCollectionContent;
