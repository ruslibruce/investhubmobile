import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlCertificate, handlingErrors } from '../utility';

const usePostSertifikat = () => {
  const {authAxios} = useAxiosContext();
  const [dataSertifikat, setDataSertifikat] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);

  const accessUploadSertifikat = async data => {
    setIsLoading(true);
    try {
      const response = await authAxios.post(UrlCertificate, data);
      if (response.status === 200) {
        const dataResponse = response.data;
        setDataSertifikat(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Upload Sertifikat Failed',
          text2: response.message ?? 'Your Upload Sertifikat Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Upload Sertifikat');
    }
  };
  return {dataSertifikat, isLoading, accessUploadSertifikat};
};

export default usePostSertifikat;
