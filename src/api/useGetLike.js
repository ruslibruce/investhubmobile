import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlCourseLike, handlingErrors} from '../utility';
import {useTranslation} from 'react-i18next';

const useGetLike = () => {
  const {t: translate} = useTranslation();
  const {authAxios} = useAxiosContext();
  const [dataGetLike, setDataGetLike] = React.useState('');
  const [dataResultLike, setDataResultLike] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(true);

  const accessGetLike = async id => {
    try {
      const response = await authAxios.get(`${UrlCourseLike}/${id}`);
      if (response.status === 200) {
        const dataResponse = response.data;
        console.log('dataResponse', dataResponse);
        setDataGetLike(dataResponse.data);
        setDataResultLike(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Post Like Failed',
          text2: response.message ?? 'Your Post Like Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Post Like');
    }
  };
  return {dataGetLike, isLoading, accessGetLike, dataResultLike};
};

export default useGetLike;
