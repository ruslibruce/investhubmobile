import React from 'react';
import Toast from 'react-native-toast-message';
import { useAxiosContext } from '../contexts';
import { UrlStockScreenerProfileCompany, handlingErrors } from '../utility';

const useGetStockScreenerProfileCompany = params => {
  const {idxAxios} = useAxiosContext();
  const [dataStockScreenerProfileCompany, setDataStockScreenerProfileCompany] =
    React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    accessStockScreenerProfileCompany();
  }, []);

  const accessStockScreenerProfileCompany = async () => {
    try {
      const response = await idxAxios.get(
        `${UrlStockScreenerProfileCompany}?KodeEmiten=${params}&language=id-id`,
      );
      if (response.status === 200) {
        const dataResponse = response.data;
        setDataStockScreenerProfileCompany(dataResponse);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Stock Screener Profile Company Failed',
          text2:
            response.message ??
            'Your Access Stock Screener Profile Company Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Stock Screener Profile Company');
    }
  };
  return {dataStockScreenerProfileCompany, isLoading};
};

export default useGetStockScreenerProfileCompany;
