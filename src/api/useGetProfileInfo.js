import React from 'react';
import Toast from 'react-native-toast-message';
import {useAxiosContext} from '../contexts';
import {UrlProfile, handlingErrors} from '../utility';
import {useDispatch} from 'react-redux';
import {updateProfile} from '../redux/reducers/auth.reducers';

const useGetProfileInfo = () => {
  const {authAxios} = useAxiosContext();
  const [dataProfile, setDataProfile] = React.useState({});
  const [isLoading, setIsLoading] = React.useState(true);
  const dispatch = useDispatch();

  const accessGetProfile = async token => {
    try {
      const response = await authAxios.get(UrlProfile, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        const dataResponse = response.data.data;
        setDataProfile(dataResponse);
        setIsLoading(false);
        dispatch(updateProfile(dataResponse));
      } else {
        setIsLoading(false);
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Access Get Profile Failed',
          text2: response.message ?? 'Your Access Get Profile Failed',
        });
      }
    } catch (error) {
      setIsLoading(false);
      handlingErrors(error, 'Access Get Profile');
    }
  };
  return {isLoading, dataProfile, accessGetProfile};
};

export default useGetProfileInfo;
