import {createSlice} from '@reduxjs/toolkit';

export const appSlice = createSlice({
  name: 'app',
  initialState: {
    isLoading: true,
    initialRoute: 'Home',
    validateValue: '',
  },
  reducers: {
    loadingSplash: state => {
      state.isLoading = !state.isLoading;
    },
    addValidateValue: (state, action) => {
      state.validateValue = action.payload;
    },
    deleteValidateValue: state => {
      state.validateValue = '';
    },
  },
});

// Action creators are generated for each case reducer function
export const {loadingSplash, addValidateValue, deleteValidateValue} =
  appSlice.actions;

export default appSlice.reducer;
