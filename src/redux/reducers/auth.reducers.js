import {createSlice} from '@reduxjs/toolkit';

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoading: true,
    isLogin: false,
    user: null,
    initialRoute: 'Home',
    token: null,
    fcmToken: null,
    language: 'en',
    profile: null,
    menuHistory: [],
  },
  reducers: {
    login: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
      state.isLogin = true;
    },
    updateProfile: (state, action) => {
      state.profile = action.payload;
    },
    logout: state => {
      state.user = null;
      state.token = null;
      state.fcmToken = null;
      state.isLogin = false;
      state.profile = null;
    },
    loading: state => {
      state.isLoading = !state.isLoading;
    },
    changeLanguage: (state, action) => {
      state.language = action.payload;
    },
    register: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    deleteRegister: state => {
      state.user = null;
      state.token = null;
    },
    changeEmail: (state, action) => {
      if (state.user) {
        state.user.email = action.payload;
      }
    },
    addMenuHistory: (state, action) => {
      const findItem = state.menuHistory.find(
        item => item.id === action.payload.id,
      );
      if (findItem) {
        state.menuHistory.splice(state.menuHistory.indexOf(findItem), 1);
        state.menuHistory.unshift(action.payload);
        return;
      }
      if (state.menuHistory.length === 4) {
        state.menuHistory.pop();
        state.menuHistory.splice(0, 0, action.payload);
        return;
      }
      state.menuHistory.unshift(action.payload);
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  login,
  updateProfile,
  logout,
  loading,
  changeLanguage,
  register,
  deleteRegister,
  changeEmail,
  addMenuHistory,
} = authSlice.actions;

export default authSlice.reducer;
