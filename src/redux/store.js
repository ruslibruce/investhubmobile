import {configureStore, combineReducers} from '@reduxjs/toolkit';
import authReducer from './reducers/auth.reducers';
import appReducers from './reducers/app.reducers';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  createTransform,
} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CryptoJS from 'crypto-js';

const encrypt = createTransform(
  (inboundState, key) => {
    if (!inboundState) return inboundState;
    const cryptedText = CryptoJS.AES.encrypt(
      JSON.stringify(inboundState),
      '1234567890abcdef0987654321',
    );

    return cryptedText.toString();
  },
  (outboundState, key) => {
    if (!outboundState) return outboundState;
    const bytes = CryptoJS.AES.decrypt(
      outboundState,
      '1234567890abcdef0987654321',
    );
    const decrypted = bytes.toString(CryptoJS.enc.Utf8);

    return JSON.parse(decrypted);
  },
);

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  transforms: [encrypt],
};

const authPersistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  transforms: [encrypt],
  // whitelist: ['isLoading']
};

const reducers = combineReducers({
  auth: persistReducer(persistConfig, authReducer),
  app: appReducers,
});

const persistedReducer = persistReducer(persistConfig, reducers);
export const store = configureStore({
  reducer: reducers,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});
export const persistor = persistStore(store);
