import React from 'react';
import {FlatList, Text, View} from 'react-native';
import {IconRightCarousel} from '../../assets/svg';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import useUpcomingEvent from './useUpcomingEvent';
import LongCard from '../LongCard';

const UpcomingEvent = ({divider}) => {
  const {dataDummy, indexFlatlist, onViewCallBack} = useUpcomingEvent();
  const styles = styling((divider, indexFlatlist, dataDummy));
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>Upcoming Events</Text>
        <View style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>See All</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </View>
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          onViewableItemsChanged={onViewCallBack}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 50,
          }}
          showsHorizontalScrollIndicator={false}
          data={dataDummy}
          horizontal
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <LongCard item={item} isBottom={true} />
          )}
        />
        <View style={styles.containerDot}>
          <View style={styles.stylingLeftSlider} />
          <View style={styles.stylingMidSlider} />
          <View style={styles.stylingRightSlider} />
        </View>
      </View>
    </View>
  );
};

export default UpcomingEvent;
