import React from 'react';
import { imageDummyCard } from '../../assets/images';

const dataDummy = [
  {
    id: 1,
    name: 'Rusli Wanasuria',
    section: '3',
    totalCourse: '28',
    seeing: 5,
    totalHours: '3 hours, 20 min',
    date: '22 Oct 2023 09:12',
    title: 'Get rich quick by investing',
    description:
      'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
    image: imageDummyCard,
    level: 'Beginner',
    owner: 'IDX Stock',
    likes: '12 K',
    comments: '823',
    shared: '1k',
  },
  {
    id: 2,
    name: 'Albert Flores',
    section: '3',
    totalCourse: '28',
    seeing: 5,
    totalHours: '3 hours, 20 min',
    date: '22 Oct 2023 09:12',
    title: 'Get rich quick by investing',
    description:
      'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
    image: imageDummyCard,
    level: 'Beginner',
    owner: 'IDX Stock',
    likes: '12 K',
    comments: '823',
    shared: '1k',
  },
  {
    id: 3,
    name: 'Albert Flores',
    section: '3',
    totalCourse: '28',
    seeing: 5,
    totalHours: '3 hours, 20 min',
    date: '22 Oct 2023 09:12',
    title: 'Get rich quick by investing',
    description:
      'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
    image: imageDummyCard,
    level: 'Beginner',
    owner: 'IDX Stock',
    likes: '12 K',
    comments: '823',
    shared: '1k',
  },
  {
    id: 4,
    name: 'Albert Flores',
    section: '3',
    totalCourse: '28',
    seeing: 5,
    totalHours: '3 hours, 20 min',
    date: '22 Oct 2023 09:12',
    title: 'Get rich quick by investing',
    description:
      'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
    image: imageDummyCard,
    level: 'Beginner',
    owner: 'IDX Stock',
    likes: '12 K',
    comments: '823',
    shared: '1k',
  },
  {
    id: 5,
    name: 'Albert Flores',
    section: '3',
    totalCourse: '28',
    seeing: 5,
    totalHours: '3 hours, 20 min',
    date: '22 Oct 2023 09:12',
    title: 'Get rich quick by investing',
    description:
      'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
    image: imageDummyCard,
    level: 'Beginner',
    owner: 'IDX Stock',
    likes: '12 K',
    comments: '823',
    shared: '1k',
  },
  {
    id: 6,
    name: 'Albert Flores',
    section: '3',
    totalCourse: '28',
    seeing: 5,
    totalHours: '3 hours, 20 min',
    date: '22 Oct 2023 09:12',
    title: 'Get rich quick by investing',
    description:
      'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
    image: imageDummyCard,
    level: 'Beginner',
    owner: 'IDX Stock',
    likes: '12 K',
    comments: '823',
    shared: '1k',
  },
];

const useUpcomingEvent = () => {
  const [indexFlatlist, setIndexFlatlist] = React.useState(0);

  const onViewCallBack = React.useCallback(event => {
    setIndexFlatlist(event.viewableItems[0].index);
    // Use viewable items in state or as intended
  }, []);
  return {dataDummy, indexFlatlist, setIndexFlatlist, onViewCallBack};
};

export default useUpcomingEvent;
