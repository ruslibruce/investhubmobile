import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {IconRightCarousel} from '../../assets/svg';
import SizedBox from '../../widgets/SizedBox';
import EventCard from '../EventCard';
import LoadingSpinner from '../LoadingSpinner';
import {styling} from './styles';
import useLatestEvent from './useLatestEvent';

const LatestEvent = () => {
  const {
    indexFlatlist,
    onViewCallBack,
    translate,
    dataEvent,
    isLoading,
    handleNavigation,
  } = useLatestEvent();

  if (isLoading) {
    return <LoadingSpinner />;
  }

  if (dataEvent.length === 0) {
    return null;
  }

  const styles = styling(indexFlatlist, dataEvent);

  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{translate('LatestEvent')}</Text>
        <TouchableOpacity
          onPress={handleNavigation}
          style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>{translate('ViewAll')}</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </TouchableOpacity>
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          onViewableItemsChanged={onViewCallBack}
          initialScrollIndex={0}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 60,
            minimumViewTime: 500,
          }}
          showsHorizontalScrollIndicator={false}
          data={dataEvent}
          horizontal
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <EventCard item={item} horizontal={true} />
          )}
        />
        <View style={styles.containerDot}>
          <View style={styles.stylingLeftSlider} />
          <View style={styles.stylingMidSlider} />
          <View style={styles.stylingRightSlider} />
        </View>
      </View>
    </View>
  );
};

export default LatestEvent;
