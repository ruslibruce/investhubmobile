import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (indexFlatlist, array) =>
  StyleSheet.create({
    container: {
      backgroundColor: 'white',
    },
    containerLabel: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    textLabel: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
    },
    containerSeeAll: {
      flexDirection: 'row',
    },
    textSeeAll: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(14),
      color: Colors.PRIMARY,
    },
    containerFlatlist: {
      flex: 1,
    },
    contentFlatlist: {
      gap: 10,
      backgroundColor: Colors.WHITE,
      paddingBottom: 20,
      paddingRight: 30,
    },
    containerDot: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      gap: 4,
    },
    stylingLeftSlider: {
      backgroundColor: indexFlatlist === 1 ? Colors.PRIMARY : Colors.GRAY,
      width: indexFlatlist === 1 ? 16 : 8,
      height: 8,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    stylingMidSlider: {
      backgroundColor:
        indexFlatlist !== 1 && indexFlatlist !== array.length - 1
          ? Colors.PRIMARY
          : Colors.GRAY,
      width: indexFlatlist !== 1 && indexFlatlist !== array.length - 1 ? 16 : 8,
      height: 8,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    stylingRightSlider: {
      backgroundColor:
        indexFlatlist === array.length - 1 ? Colors.PRIMARY : Colors.GRAY,
      width: indexFlatlist === array.length - 1 ? 16 : 8,
      height: 8,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
