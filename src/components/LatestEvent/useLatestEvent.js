import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {EVENTS} from '../../utility';
import useGetEvent from '../../api/useGetEvent';

const useLatestEvent = () => {
  const [indexFlatlist, setIndexFlatlist] = React.useState(0);
  const {t: translate} = useTranslation();
  const {dataEvent, isLoading} = useGetEvent();
  const navigation = useNavigation();

  const onViewCallBack = React.useCallback(event => {
    if (event.viewableItems[0] && event.viewableItems[0].index) {
      setIndexFlatlist(event.viewableItems[0].index);
    }
    // Use viewable items in state or as intended
  }, []);

  const handleNavigation = () => {
    navigation.navigate(EVENTS);
  };
  return {
    indexFlatlist,
    onViewCallBack,
    translate,
    dataEvent,
    isLoading,
    handleNavigation,
  };
};

export default useLatestEvent;
