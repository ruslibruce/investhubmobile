import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = () =>
  StyleSheet.create({
    containerSignUp: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    stylingLine: {
      width: '35%',
      borderWidth: 0.5,
      borderColor: Colors.PLACEHOLDER,
      height: 1,
    },
    textSignUpWith: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(14),
      color: Colors.PLACEHOLDER,
      paddingBottom: 6,
      marginHorizontal: 11,
    },
    containerButtonAuth: {
      flexDirection: 'row',
      gap: 10,
      justifyContent: 'space-between',
      width: '100%',
    },
  });
