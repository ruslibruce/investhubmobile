import React from 'react';
import { useTranslation } from 'react-i18next';
import { Text, View } from 'react-native';
import ButtonApple from '../../widgets/ButtonApple';
import ButtonGoogle from '../../widgets/ButtonGoogle';
import ButtonIDX from '../../widgets/ButtonIDX';
import ButtonIDXMobile from '../../widgets/ButtonIDXMobile';
import ButtonRDIS from '../../widgets/ButtonRDIS';
import ButtonTicmi from '../../widgets/ButtonTicmi';
import SizedBox from '../../widgets/SizedBox';
import { styling } from './styles';

const GroupButtonAuth = ({isLogin = false}) => {
  const {t: translate} = useTranslation();
  const styles = styling();
  return (
    <>
      <SizedBox height={20} />
      <View style={styles.containerSignUp}>
        <View style={styles.stylingLine} />
        <Text style={styles.textSignUpWith}>
          {isLogin ? translate('LoginWith') : translate('RegisterWith')}
        </Text>
        <View style={styles.stylingLine} />
      </View>
      <SizedBox height={18} />
      <View style={styles.containerButtonAuth}>
        <ButtonApple />
        <ButtonGoogle />
        <ButtonIDX />
        <ButtonIDXMobile />
      </View>
      <SizedBox height={18} />
      <View style={styles.containerButtonAuth}>
        <ButtonRDIS />
        <ButtonTicmi />
      </View>
    </>
  );
};

export default GroupButtonAuth;
