import {View, Text} from 'react-native';
import React from 'react';
import {matrics} from '../../utility';
import RenderHTML, {TNodeChildrenRenderer} from 'react-native-render-html';
import NoData from '../NoData';

const WebDisplay = React.memo(
  ({html, tagsStyles, isOneLine, styleText, numberOfLines}) => {
    if (!html) {
      return <NoData />;
    }
    const ParagraphRenderer = ({TDefaultRenderer, tnode, type, ...props}) => {
      return (
        <TDefaultRenderer tnode={tnode} {...props}>
          <TNodeChildrenRenderer
            tnode={tnode}
            parentMarkers={props.markers}
            renderChild={({childTnode, childElement}) =>
              isOneLine ? (
                <Text
                  style={[
                    styleText,
                    {
                      marginTop: -10,
                      marginBottom: -20,
                    },
                  ]}
                  numberOfLines={1}>
                  {childElement}
                </Text>
              ) : (
                <Text
                  style={[
                    styleText,
                    {
                      marginTop: -10,
                      marginBottom: -20,
                    },
                  ]}
                  numberOfLines={numberOfLines}>
                  {childElement}
                </Text>
              )
            }
          />
        </TDefaultRenderer>
      );
    };

    const renderHtmlProps = {
      source: {html},
      renderers: {
        p: ParagraphRenderer,
      },
    };

    return (
      <RenderHTML
        contentWidth={matrics.screenWidth}
        source={{
          html: html,
        }}
        tagsStyles={tagsStyles}
        {...renderHtmlProps}
      />
    );
  },
);

export default WebDisplay;
