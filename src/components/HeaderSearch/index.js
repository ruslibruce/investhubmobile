import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {hero} from '../../assets/images';
import {IconSearch} from '../../assets/svg';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import useHeaderSearch from './useHeaderSearch';
import TextInputCustom from '../../widgets/TextInputCostum';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {Colors, STOCK_SCREENER} from '../../utility';
import LoadingSpinner from '../LoadingSpinner';

const HeaderSearch = ({
  hiddenSearch,
  placeholder,
  showMenu,
  height,
  lokalSearch,
  search,
  setSearch,
}) => {
  const {
    filteredTopContent,
    filteredBottomContent,
    handleNavigationMenuShortcut,
    handleNavigationSearch,
    isLoadingGetUrl,
  } = useHeaderSearch();
  const styles = styling(height);
  return (
    <React.Fragment>
      <View style={styles.container}>
        <Image source={hero} style={styles.containerHero} />
        {/* <View style={styles.containerTop} />
        <View style={styles.containerBottom} /> */}
      </View>
      {!hiddenSearch && (
        <View style={styles.containerSearch}>
          <TouchableOpacity
            activeOpacity={0.9}
            style={styles.buttonSearch}
            onPress={() => handleNavigationSearch()}>
            <Text style={styles.textSearch}>{placeholder}</Text>
            <IconSearch />
          </TouchableOpacity>
        </View>
      )}
      {lokalSearch && (
        <View style={styles.containerSearch}>
          <TextInputCustom
            iconRight={
              search ? (
                <AntDesign
                  onPress={() => setSearch(null)}
                  name="close"
                  color={Colors.BLACK}
                />
              ) : (
                <Feather name="search" color={Colors.BLACK} />
              )
            }
            placeholder={placeholder}
            styleInput={{width: '100%'}}
            placeholderTextColor={Colors.GRAYTEXT}
            autoCapitalize="none"
            value={search}
            containerInputText={styles.inputSearch}
            onChangeText={text => setSearch(text)}
          />
        </View>
      )}
      {showMenu && (
        <React.Fragment>
          <View style={styles.containerMenuShortcut}>
            <View style={styles.containerViewIconShortcut}>
              {filteredTopContent.map(item => (
                <TouchableOpacity
                  key={item.id}
                  onPress={() =>
                    handleNavigationMenuShortcut(item.url, item.nesting)
                  }
                  style={styles.containerButtonShortcut}>
                  {isLoadingGetUrl && item.url === STOCK_SCREENER ? (
                    <View style={styles.containerButtonIcon}>
                      <LoadingSpinner />
                    </View>
                  ) : (
                    <View style={styles.containerButtonIcon}>{item.icon}</View>
                  )}
                  <Text style={styles.textButtonShortcut}>{item.name}</Text>
                </TouchableOpacity>
              ))}
            </View>
            <SizedBox height={16} />
            <View style={styles.containerViewIconShortcut}>
              {filteredBottomContent.map(item => (
                <TouchableOpacity
                  key={item.id}
                  onPress={() =>
                    handleNavigationMenuShortcut(item.url, item.nesting)
                  }
                  style={styles.containerButtonShortcut}>
                  {isLoadingGetUrl && item.url === STOCK_SCREENER ? (
                    <View style={styles.containerButtonIcon}>
                      <LoadingSpinner />
                    </View>
                  ) : (
                    <View style={styles.containerButtonIcon}>{item.icon}</View>
                  )}
                  <Text style={styles.textButtonShortcut}>{item.name}</Text>
                </TouchableOpacity>
              ))}
            </View>
          </View>
          <SizedBox height={10} />
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default HeaderSearch;
