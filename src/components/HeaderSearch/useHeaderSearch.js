import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {
  IconCalendarRed,
  IconEbookRed,
  IconGlossariumRed,
  IconPreferenced,
  IconSeeAllRed,
  IconSpeakerRed,
  IconStockScreenRed,
  IconStudentRed,
} from '../../assets/svg';
import {
  DASHBOARD_TOOLS,
  EVENTS,
  handleOpenLink,
  MY_MENU,
  SEARCH_ALL,
  STOCK_SCREENER,
  TAB_TOOLS_FORUM,
  PREFERENCES,
} from '../../utility';
import {useSelector} from 'react-redux';
import Config from 'react-native-config';
import useGetUrlWebIDXStockScreener from '../../api/useGetUrlWebIDXStockScreener';
import Toast from 'react-native-toast-message';

const useHeaderSearch = () => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const topContent = [
    {
      id: 1,
      name: translate('Forum_Tab_Capital'),
      icon: <IconStudentRed width={20} height={20} />,
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 2,
      name: translate('Ebook_Capital'),
      icon: <IconEbookRed width={20} height={20} />,
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 3,
      name: translate('Glossarium_Capital'),
      icon: <IconGlossariumRed width={20} height={20} />,
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 4,
      name: translate('Event_Tab_Capital'),
      icon: <IconSpeakerRed width={20} height={20} />,
      url: EVENTS,
      nesting: '',
      isLogin: false,
    },
  ];
  const bottomContent = [
    {
      id: 1,
      name: translate('Class_Capital'),
      icon: <IconCalendarRed width={20} height={20} />,
      url: '',
      nesting: '',
      isLogin: false,
    },
    {
      id: 2,
      name: translate('Stock_Capital'),
      icon: <IconStockScreenRed width={20} height={20} />,
      url: STOCK_SCREENER,
      nesting: '',
      isLogin: false,
    },
    {
      id: 3,
      name: translate('Preferences'),
      icon: <IconPreferenced width={20} height={20} />,
      url: PREFERENCES,
      nesting: '',
      isLogin: true,
    },
    {
      id: 4,
      name: translate('ViewAll'),
      icon: <IconSeeAllRed width={20} height={20} />,
      url: MY_MENU,
      nesting: '',
      isLogin: false,
    },
  ];
  const navigation = useNavigation();
  const {accessGetUrlWebIDXStockScreener, isLoading: isLoadingGetUrl} = useGetUrlWebIDXStockScreener();

  const filteredTopContent = topContent.filter(item => {
    // Add your condition to hide items when not logged in
    if (!authMethod.isLogin && item.isLogin === true) {
      return false; // Hide 'Profile' when not logged in
    }
    return true; // Show other items
  });

  const filteredBottomContent = bottomContent.filter(item => {
    // Add your condition to hide items when not logged in
    if (!authMethod.isLogin && item.isLogin === true) {
      return false; // Hide 'Profile' when not logged in
    }
    return true; // Show other items
  });

  const handleNavigationMenuShortcut = (url, nesting) => {
    if (url === '') {
      Toast.show({
        type: 'info',
        position: 'bottom',
        text1: translate('UpdateTitle'),
        text2: translate('UpdateDesc'),
      });
      return;
    }
    if (url === STOCK_SCREENER) {
      accessGetUrlWebIDXStockScreener({
        tokenInvesthub: authMethod?.token,
      });
      return;
    }
    if (nesting) {
      navigation.navigate(url, {screen: nesting});
    } else {
      navigation.navigate(url);
    }
  };

  const handleNavigationSearch = () => {
    navigation.navigate(SEARCH_ALL);
  };
  return {
    filteredTopContent,
    filteredBottomContent,
    handleNavigationMenuShortcut,
    handleNavigationSearch,
    translate,
    isLoadingGetUrl,
  };
};

export default useHeaderSearch;
