import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SizedBox from '../../widgets/SizedBox';
import { styles } from './styles';
import useProfileButtonMore from './useProfileButtonMore';

const ProfileButtonMore = ({profile}) => {
  const {groupArray, handleNavigation} = useProfileButtonMore();
  return (
    <React.Fragment>
      {groupArray.map(item => {
        if (!profile && item.name == 'Logout') {
          return false;
        }
        if (profile && item.name == 'Login') {
          return false;
        }
        return (
          <React.Fragment key={item.id}>
            <SizedBox height={10} />
            <TouchableOpacity
              onPress={() => handleNavigation(item.url)}
              style={styles.containerButton}>
              <View style={styles.containerIcon}>{item.icon}</View>
              <SizedBox width={12} />
              <Text style={styles.textButton}>{item.name}</Text>
              <MaterialIcons name='arrow-forward-ios' color={'#000'}  size={15} />
              <SizedBox width={12} />
            </TouchableOpacity>
          </React.Fragment>
        );
      })}
    </React.Fragment>
  );
};

export default ProfileButtonMore;
