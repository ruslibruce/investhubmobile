import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  IconAbout,
  IconDisclaimerRed,
  IconFAQ,
  IconLanguage,
  IconPrivacyRed,
} from '../../assets/svg';
import { LANGUAGE, TAB_TOOLS_ABOUT_US, TAB_TOOLS_DISCLAIMER, TAB_TOOLS_FAQ, TAB_TOOLS_PRIVACY_POLICY } from '../../utility';

const useProfileButtonMore = () => {
  const {t: translate} = useTranslation();
  const groupArray = [
    {
      id: 1,
      icon: <IconFAQ width={25} height={25} />,
      name: translate('Faq_Capital'),
      url: TAB_TOOLS_FAQ,
    },
    {
      id: 2,
      icon: <IconPrivacyRed width={25} height={25} />,
      name: translate('PrivacyPolicy'),
      url: TAB_TOOLS_PRIVACY_POLICY,
    },
    {
      id: 3,
      icon: <IconDisclaimerRed width={25} height={25} />,
      name: 'Disclaimer',
      url: TAB_TOOLS_DISCLAIMER,
    },
    {
      id: 4,
      icon: <IconAbout width={25} height={25} />,
      name: translate('About_Capital'),
      url: TAB_TOOLS_ABOUT_US,
    },
    {
      id: 5,
      icon: <IconLanguage width={25} height={25} />,
      name: translate('Language'),
      url: LANGUAGE,
    },
  ];
  const navigation = useNavigation();

  const handleNavigation = React.useCallback(async value => {
    navigation.navigate(value);
  }, []);
  return {groupArray, handleNavigation};
};

export default useProfileButtonMore;
