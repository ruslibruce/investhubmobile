import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {Text, TouchableOpacity, View} from 'react-native';
import {
  IconBrokerTab,
  IconCourseTab,
  IconEventTab,
  IconHomeTab,
  IconProfileTab,
} from '../../assets/svg';
import {
  DASHBOARD,
  DASHBOARD_COURSE,
  DASHBOARD_HOME,
  DASHBOARD_INVESTMENT,
  DASHBOARD_NEWS,
  DASHBOARD_TOOLS,
  TAB_COURSE_MAIN,
  TAB_NEWS_MAIN,
  TAB_TOOLS_MAIN,
} from '../../utility';
import {styles} from './styles';

const BottomTabOnlyView = ({hiddenNavigate}) => {
  const navigation = useNavigation();
  const {t: translate} = useTranslation();

  const handleNavigator = (screen, screen2) => {
    if (hiddenNavigate) {
      return;
    }
    if (screen2) {
      navigation.navigate(screen, {screen: screen2});
      return;
    }
    navigation.navigate(screen);
  };
  return (
    <View style={styles.containerBottomTabOnlyView}>
      <TouchableOpacity
        onPress={() => handleNavigator(DASHBOARD_HOME, '')}
        style={styles.tabStyleFirstOnlyView}>
        <IconHomeTab />
        <Text style={styles.textTabOnlyView}>{translate('Home_Capital')}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleNavigator(DASHBOARD_COURSE, TAB_COURSE_MAIN)}
        style={styles.tabStyleMidOnlyView}>
        <IconCourseTab />
        <Text style={styles.textTabOnlyView}>
          {translate('Course_Tab_Capital')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleNavigator(DASHBOARD_NEWS, TAB_NEWS_MAIN)}
        style={styles.tabStyleMidOnlyView}>
        <IconEventTab />
        <Text style={styles.textTabOnlyView}>
          {translate('News_Tab_Capital')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleNavigator(DASHBOARD_INVESTMENT, '')}
        style={styles.tabStyleMidOnlyView}>
        <IconBrokerTab />
        <Text style={styles.textTabOnlyView}>
          {translate('Investment_Capital')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleNavigator(DASHBOARD_TOOLS, TAB_TOOLS_MAIN)}
        style={styles.tabStyleEndOnlyView}>
        <IconProfileTab />
        <Text style={styles.textTabOnlyView}>{translate('Tools_Capital')}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default BottomTabOnlyView;
