import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  containerBottomTabOnlyView: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'space-between',
    gap: 12,
    borderWidth: 1,
    borderBlockStartColor: '#DDDDDD',
    borderBlockEndColor: Colors.WHITE,
    borderEndColor: Colors.WHITE,
    borderStartColor: Colors.WHITE,
    paddingVertical: 12,
    flex: 1,
    width: matrics.screenWidth,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    backgroundColor: Colors.WHITE,
  },
  tabStyleFirstOnlyView: {
    maxWidth: 64,
    marginLeft: 23.5,
  },
  tabStyleMidOnlyView: {
    maxWidth: 64,
    alignItems: 'center',
  },
  tabStyleEndOnlyView: {
    maxWidth: 64,
    marginRight: 23.5,
  },
  textTabOnlyView: {
    fontSize: getFontSize(10),
    fontFamily: FontType.openSansRegular400,
    color: Colors.GRAY,
  },
});
