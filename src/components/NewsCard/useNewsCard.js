import { useNavigation } from '@react-navigation/native';
import { DASHBOARD_NEWS, TAB_NEWS_DETAIL, handleOpenLink } from '../../utility';

const useNewsCard = () => {
  const navigation = useNavigation();
  const handleNavigation = (home, id, value) => {
    if (value.source_id) {
      handleOpenLink(value.link);
      return;
    }
    if (home) {
      navigation.navigate(DASHBOARD_NEWS, {
        screen: TAB_NEWS_DETAIL,
        params: {id: id},
      });
    } else {
      navigation.navigate(TAB_NEWS_DETAIL, {
        id: id,
      });
    }
  };
  return {handleNavigation};
};

export default useNewsCard;
