import {StyleSheet} from 'react-native';
import { Colors, FontType, getFontSize } from '../../utility';

export const styles = StyleSheet.create({
  containerListItems: {
    width: '100%',
    // height: 88,
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    gap: 12,
    marginBottom: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  containerImageList: {
    width: 100,
    height: 100,
    backgroundColor: Colors.WHITE,
    overflow: 'hidden',
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },
  widthImage: {
    width: '100%',
    height: '100%',
  },
  containerRightItem: {
    maxWidth: '60%',
  },
  textTopTitleItem: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
  containerBottomItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    gap: 8,
  },
  containerBottomLeftItem: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderWidth: 1,
    borderColor: Colors.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
  },
  textBottomLeftItem: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.PRIMARY,
  },
  textBottomRightItem: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.GRAY,
  },
});
