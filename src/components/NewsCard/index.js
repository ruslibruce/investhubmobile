import moment from 'moment';
import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { isHTML } from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import WebDisplay from '../WebDisplay';
import { styles } from './styles';
import useNewsCard from './useNewsCard';

const NewsCard = ({item, home}) => {
  const {handleNavigation} = useNewsCard();

  const tagsStyles = {
    p: {
      fontSize: 14,
      fontWeight: 700,
      fontFamily: 'OpenSans-Regular',
      color: '#A62829',
    },
  };

  return (
    <TouchableOpacity
      onPress={() => handleNavigation(home, item?.id, item)}
      style={styles.containerListItems}>
      <View style={styles.containerImageList}>
        <Image source={{uri: item?.cover_image}} style={styles.widthImage} />
      </View>
      <View style={styles.containerRightItem}>
        {isHTML(item?.subject) ? (
          <View
            style={{
              height: 40,
              overflow: 'hidden',
            }}>
            <WebDisplay html={item?.subject} tagsStyles={tagsStyles}  numberOfLines={2}/>
          </View>
        ) : (
          <>
            <Text numberOfLines={2} style={styles.textTopTitleItem}>
              {item?.subject}
            </Text>
          </>
        )}
        <SizedBox height={11} />
        <View style={styles.containerBottomItem}>
          <View style={styles.containerBottomLeftItem}>
            <Text style={styles.textBottomLeftItem}>
              {item?.category ? item?.category : 'RSS NEWS'}
            </Text>
          </View>
          <Text style={styles.textBottomRightItem}>
            {moment(item?.publish_time).format('MMM DD, YYYY')}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default NewsCard;
