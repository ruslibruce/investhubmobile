import React from 'react';
import { useTranslation } from 'react-i18next';
import { Text, View } from 'react-native';
import { IconEmptyTable } from '../../assets/svg/emptyData';
import SizedBox from '../../widgets/SizedBox';
import { styles } from './styles';

const NoData = () => {
  const {t: translate} = useTranslation();
  return (
    <View style={styles.containerNoData}>
      <IconEmptyTable marginRight={30} width={150} height={150} />
      <SizedBox height={20} />
      <Text style={styles.textNoData}>{translate('SorryNoData')}</Text>
    </View>
  );
};

export default NoData;
