import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  containerNoData: {
    width: matrics.screenWidth,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNoData: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(16),
    color: Colors.PRIMARY,
    marginRight: 50,
  },
});
