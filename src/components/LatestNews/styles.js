import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  containerLabel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textLabel: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
  },
  containerSeeAll: {
    flexDirection: 'row',
  },
  textSeeAll: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
  containerFlatlist: {
    flex: 1,
  },
  contentFlatlist: {
    gap: 10,
  },
});
