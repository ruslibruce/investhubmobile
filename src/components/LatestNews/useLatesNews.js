import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import useGetNews from '../../api/useGetNews';
import { DASHBOARD_NEWS } from '../../utility';

const useLatesNews = () => {
  const {t: translate} = useTranslation();
  const {dataNews, isLoading} = useGetNews();
  const navigation = useNavigation();

  const handleNavigation = () => {
    navigation.navigate(DASHBOARD_NEWS);
  };
  return {translate, dataNews, isLoading, handleNavigation};
};

export default useLatesNews;
