import React from 'react';
import { FlatList, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IconRightCarousel } from '../../assets/svg';
import SizedBox from '../../widgets/SizedBox';
import LoadingSpinner from '../LoadingSpinner';
import NewsCard from '../NewsCard';
import { styles } from './styles';
import useLatesNews from './useLatesNews';

const LatestNews = ({title}) => {
  const {translate, handleNavigation, dataNews, isLoading} = useLatesNews();

  if (isLoading) {
    return <LoadingSpinner />;
  }

  if (dataNews.length === 0) {
    return null;
  }
  
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{title}</Text>
        <TouchableOpacity
          onPress={handleNavigation}
          style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>{translate('ViewAll')}</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </TouchableOpacity>
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          data={dataNews}
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => <NewsCard item={item} home={true} />}
        />
      </View>
    </View>
  );
};

export default LatestNews;
