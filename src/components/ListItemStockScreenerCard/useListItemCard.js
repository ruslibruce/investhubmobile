import React from 'react';

const useListItemCard = () => {
  const [expanded, setExpanded] = React.useState(false);
  const [inputValueMin, setInputValueMin] = React.useState(0);
  const [inputValueMax, setInputValueMax] = React.useState(0);

  const onItemPress = () => {
    setExpanded(!expanded);
  };

  const onChangeInputTextMax = React.useCallback(value => {
    setInputValueMax(`${value}`);
  }, []);

  const onChangeInputTextMin = React.useCallback(value => {
    setInputValueMin(`${value}`);
  }, []);

  return {
    expanded,
    setExpanded,
    onItemPress,
    inputValueMax,
    inputValueMin,
    setInputValueMin,
    setInputValueMax,
    onChangeInputTextMax,
    onChangeInputTextMin,
  };
};

export default useListItemCard;
