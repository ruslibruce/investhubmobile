import Slider from '@react-native-community/slider';
import {Button, Card, Input} from '@ui-kitten/components';
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {Colors} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {CollapsableContainer} from '../CollapsableContainer';
import {styling} from './styles';
import useListItemCard from './useListItemCard';

const ListItemStockScreenerCard = ({
  item,
  handleSubmitChange,
  handleClearFilterMaxMin,
  isClickSubmitChange,
  setIsClickSubmitChange,
}) => {
  const {
    expanded,
    setExpanded,
    onItemPress,
    inputValueMax,
    inputValueMin,
    setInputValueMin,
    setInputValueMax,
    onChangeInputTextMax,
    onChangeInputTextMin,
  } = useListItemCard();
  const styles = styling(item.maxModify);

  React.useEffect(() => {
    if (isClickSubmitChange) {
      setExpanded(false);
      setIsClickSubmitChange(false);
    }
  }, [isClickSubmitChange]);

  React.useEffect(() => {
    if (expanded) {
      setInputValueMax(item.max);
      setInputValueMin(item.min);
    }
  }, [expanded]);
  return (
    <Card style={styles.containerCard}>
      <TouchableOpacity style={styles.containerButton} onPress={onItemPress}>
        <Text style={styles.textTitle}>{item.itemName}</Text>
        {item.maxModify ? (
          <Feather name="x-circle" color={Colors.PRIMARY} size={20} />
        ) : (
          <Feather name="plus-circle" color={Colors.GRAY2} size={20} />
        )}
      </TouchableOpacity>
      <CollapsableContainer expanded={expanded}>
        <SizedBox height={20} />
        <View>
          <Text style={styles.textMaxMin}>{'Max'}</Text>
          <SizedBox height={10} />
          <Input
            style={{backgroundColor: Colors.WHITE, borderColor: Colors.GRAY}}
            textStyle={{color: Colors.BLACK}}
            value={`${inputValueMax}`}
            placeholder={`${item.max}`}
            onChangeText={onChangeInputTextMax}
            keyboardType="numeric"
          />
        </View>
        <View>
          <Slider
            style={{width: '100%', height: 40}}
            minimumValue={0}
            maximumValue={Number(item.max)}
            value={Number(inputValueMax)}
            step={1}
            minimumTrackTintColor={Colors.GRAY}
            maximumTrackTintColor={Colors.PRIMARY}
            thumbTintColor={Colors.PRIMARY}
            onValueChange={onChangeInputTextMax}
          />
        </View>
        <View>
          <Text style={styles.textMaxMin}>{'Min'}</Text>
          <SizedBox height={10} />
          <Input
            style={{backgroundColor: Colors.WHITE, borderColor: Colors.GRAY}}
            textStyle={{color: Colors.BLACK}}
            value={`${inputValueMin}`}
            placeholder={`${item.min}`}
            onChangeText={onChangeInputTextMin}
            keyboardType="numeric"
          />
        </View>
        <View>
          <Slider
            style={{width: '100%', height: 40}}
            minimumValue={Number(item.min)}
            maximumValue={0}
            value={Number(inputValueMin)}
            step={1}
            minimumTrackTintColor={Colors.GRAY}
            maximumTrackTintColor={Colors.PRIMARY}
            thumbTintColor={Colors.PRIMARY}
            onValueChange={onChangeInputTextMin}
          />
        </View>
        <View>
          <Button
            style={{backgroundColor: Colors.WHITE}}
            appearance="outline"
            onPress={() =>
              handleSubmitChange(item, inputValueMax, inputValueMin)
            }>
            {evaProps => <Text style={styles.textMaxMin}>Atur</Text>}
          </Button>
          <Button
            style={{marginTop: 10, backgroundColor: Colors.WHITE}}
            appearance="outline"
            onPress={() =>
              handleClearFilterMaxMin(item, inputValueMax, inputValueMin)
            }>
            {evaProps => <Text style={styles.textMaxMin}>Hapus</Text>}
          </Button>
        </View>
      </CollapsableContainer>
    </Card>
  );
};

export default ListItemStockScreenerCard;
