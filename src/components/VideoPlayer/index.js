import Slider from '@react-native-community/slider';
import {
  useFocusEffect,
  useNavigation,
  useNavigationState,
} from '@react-navigation/native';
import React, {useCallback} from 'react';
import {useTranslation} from 'react-i18next';
import {
  BackHandler,
  Pressable,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import Entypo from 'react-native-vector-icons/Entypo';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Video from 'react-native-video';
import {
  SelectedTrackType,
  TextTrackType,
} from 'react-native-video/lib/types/video';
import {useSelector} from 'react-redux';
import usePostCourseDetailProgress from '../../api/usePostCourseDetailProgress';
import {Colors, COURSE_CONTENT, imageIAM} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import LoadingSpinner from '../LoadingSpinner';
import {styles} from './styles';
import Config from 'react-native-config';

const VideoPlayer = ({
  styleVideo,
  content,
  idProgress,
  idCourse,
  accessCourseDetail,
  isNavigateBack,
}) => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const videoRef = React.useRef(null);
  const [pause, setPaused] = React.useState(false);
  const [duration, setDuration] = React.useState(0);
  const [currentTime, setCurrentTime] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(false);
  const [showControl, setShowControl] = React.useState(true);
  const [showError, setShowError] = React.useState(false);
  const [isVideoEnd, setIsVideoEnd] = React.useState(false);
  const {accessPostProgress} = usePostCourseDetailProgress();
  const state = useNavigationState(state => state);
  const navigation = useNavigation();
  const [isMuted, setIsMuted] = React.useState(false);
  const [playbackRate, setPlaybackRate] = React.useState(1);
  const [playbackText, setPlaybackText] = React.useState('1x');
  const [quality, setQuality] = React.useState([]);
  const [subtitles, setSubtitles] = React.useState([
    {
      key: 'off',
      label: 'OFF',
      language: 'off',
      title: 'offtext/vtt',
      type: 'text/vtt',
      uri: null,
    },
  ]);
  const [subtitlesList, setSubtitlesList] = React.useState([]);
  const [urlVideo, setUrlVideo] = React.useState('');
  const [selectedKeysQuality, setSelectedKeysQuality] = React.useState('');
  const [selectedKeysSubtitle, setSelectedKeysSubtitle] =
    React.useState('offtext/vtt');
  const [typeTrack, setTypeTrack] = React.useState(SelectedTrackType.DISABLED);
  // console.log('content', content);
  // console.log('idProgress', idProgress);

  React.useEffect(() => {
    if (content) {
      setUrlVideo(content.content_file);
      if (content.content_files) {
        let resultParse =
          content.content_files && JSON.parse(content.content_files);
        let resultMap = resultParse.map(item => ({
          ...item,
          label: item.resolution,
          key: item.filename,
          theme: 'dark',
        }));
        setSelectedKeysQuality(resultMap[0].key);
        setQuality(resultMap);
      }
      if (content.subtitles && content.subtitles.length > 0) {
        let subtitleList = content.subtitles.map(item => ({
          type: TextTrackType.VTT,
          uri: item.filename,
          language: item.language,
          title: item.language + TextTrackType.VTT,
          key: item.language,
          label: item.language,
        }));
        setSubtitlesList(subtitleList);
        setSubtitles(prev => [...prev, ...subtitleList]);
      }
    }
  }, [content]);

  const itemPlayback = [
    {key: 1, text: '1x'},
    {key: 1.5, text: '1.5x'},
    {key: 2, text: '2x'},
  ];

  useFocusEffect(
    React.useCallback(() => {
      if (state.routes[state.index].name === COURSE_CONTENT) {
        setPaused(false);
      }
    }, [state]),
  );

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      // do something
      setPaused(true);
    });

    return unsubscribe;
  }, [navigation]);

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      handleAccessProgress(currentTime);
    }, 60000); // 60000 milliseconds = 1 minute

    // Cleanup interval on component unmount
    return () => clearInterval(intervalId);
  }, [currentTime]); // Empty dependency array ensures this runs once when the component mounts

  React.useEffect(() => {
    const backAction = () => {
      if (content && content?.progress) {
        handleAccessProgress(currentTime);
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return () => backHandler.remove();
  }, [currentTime]);

  React.useEffect(() => {
    if (isNavigateBack) {
      handleAccessProgress(currentTime);
    }
  }, [isNavigateBack, currentTime]);

  const handleAccessProgress = useCallback(
    currentTime => {
      if (!authMethod.isLogin) {
        return;
      }
      if (!idProgress && !content) {
        return;
      }
      accessPostProgress({
        participant_id: idProgress,
        content_id: content?.id,
        position: Math.ceil(currentTime),
      });
      accessCourseDetail(idCourse, false);
    },
    [
      idCourse,
      idProgress,
      content,
      accessCourseDetail,
      accessPostProgress,
      authMethod,
    ],
  );

  const onProgress = data => {
    if (!isLoading) {
      setCurrentTime(data.currentTime);
    }
  };

  const onLoad = data => {
    setShowError(false);
    setDuration(data.duration);
    setIsLoading(false);
    if (content && content?.progress?.position > 0) {
      videoRef.current?.seek(content?.progress?.position);
    }
  };

  const skipBackward = () => {
    videoRef.current.seek(currentTime - 10);
    setCurrentTime(currentTime - 10);
  };

  const skipForward = () => {
    if (content && content?.progress?.is_completed) {
      videoRef.current.seek(currentTime + 10);
      setCurrentTime(currentTime + 10);
    }
  };

  const getMinutesFromSeconds = time => {
    const minutes = time >= 60 ? Math.ceil(time / 60) : 0;
    const seconds = Math.ceil(time - minutes * 60);

    return `${minutes >= 10 ? minutes : '0' + minutes} : ${
      seconds >= 10 ? seconds : '0' + seconds
    }`;
  };

  const position = getMinutesFromSeconds(currentTime);
  const fullDuration = getMinutesFromSeconds(duration);

  const replayVideo = () => {
    videoRef.current && videoRef.current.seek(0);
    setCurrentTime(0);
    setPaused(true);
    setIsVideoEnd(false);
  };

  const getPercentage = (currentTime, duration) => {
    if (duration > 0) {
      return (currentTime / duration) * 100;
    }
    return 0; // Return 0% if duration is not positive
  };

  const onSeek = value => {
    if (duration) {
      const targetTime = (value / 100) * duration;
      if (content && content?.progress?.is_completed) {
        videoRef.current?.seek(targetTime);
        setCurrentTime(targetTime);
        return;
      }
      if (currentTime > targetTime) {
        videoRef.current?.seek(targetTime);
        setCurrentTime(targetTime);
      }
    }
  };

  const onEnd = currentTime => {
    setIsVideoEnd(true);
    handleAccessProgress(currentTime);
  };

  const handleShowModalQuality = () => {
    magicModal.show(() => (
      <View style={styles.containerModal}>
        <Text style={styles.textTitlePlayback}>Select Quality</Text>
        <View style={{flexDirection: 'row', gap: 5, marginTop: 10}}>
          {quality.map(item => (
            <Pressable
              style={[
                styles.buttonPlayback,
                {
                  backgroundColor:
                    item.key == selectedKeysQuality
                      ? Colors.PRIMARY
                      : Colors.GRAY,
                },
              ]}
              onPress={() => {
                setSelectedKeysQuality(item.key);
                setUrlVideo(`${content.base_url}${item.key}`);
                magicModal.hide();
              }}>
              <Text style={styles.textPlayback}>{item.label}</Text>
            </Pressable>
          ))}
        </View>
      </View>
    ));
  };

  const handleShowModalSubtitle = () => {
    magicModal.show(() => (
      <View style={styles.containerModal}>
        <Text style={styles.textTitlePlayback}>Select Subtitles</Text>
        <View style={{flexDirection: 'row', gap: 5, marginTop: 10}}>
          {subtitles?.map(item => (
            <Pressable
              style={styles.buttonPlayback(item.title, selectedKeysSubtitle)}
              onPress={() => {
                setSelectedKeysSubtitle(item.title);
                if (item.key === 'off') {
                  setTypeTrack(SelectedTrackType.DISABLED);
                } else {
                  setTypeTrack(SelectedTrackType.TITLE);
                }
                magicModal.hide();
              }}>
              <Text style={styles.textPlayback}>{item.key}</Text>
            </Pressable>
          ))}
        </View>
      </View>
    ));
  };

  const handleShowModalPlayback = () => {
    magicModal.show(() => (
      <View style={styles.containerModal}>
        <Text style={styles.textTitlePlayback}>Playback Rate</Text>
        <View style={{flexDirection: 'row', gap: 5, marginTop: 10}}>
          {itemPlayback.map(item => (
            <Pressable
              style={styles.buttonPlayback(item.key, playbackRate)}
              onPress={() => {
                setPlaybackRate(item.key);
                setPlaybackText(item.text);
                magicModal.hide();
              }}>
              <Text style={styles.textPlayback}>{item.text}</Text>
            </Pressable>
          ))}
        </View>
      </View>
    ));
  };

  return (
    <TouchableWithoutFeedback onPress={() => setShowControl(!showControl)}>
      <View>
        <Video
          // Can be a URL or a local file.
          source={{uri: urlVideo, textTracks: subtitlesList}}
          // Store reference
          ref={videoRef}
          // Callback when remote video is buffering
          onBuffer={buffer => setIsLoading(buffer.isBuffering)}
          // Callback when video cannot be loaded
          onError={err => {
            setShowError(true);
          }}
          style={[styles.backgroundVideo, styleVideo]}
          paused={pause}
          onProgress={onProgress}
          onLoad={onLoad}
          onEnd={() => onEnd(currentTime)}
          pictureInPicture
          poster={`${Config.BASE_URL}/images/logo/logo_investhub.webp`}
          resizeMode="cover"
          muted={isMuted}
          rate={playbackRate}
          selectedTextTrack={{
            type: typeTrack,
            value: selectedKeysSubtitle,
          }}
          subtitleStyle={styles.subtitleStyle}
        />
        {showError && (
          <View style={styles.contentError}>
            <Text style={styles.textError}>
              {content?.content_type.toLowerCase() === 'video'
                ? translate('VideoLoadError')
                : translate('AudioLoadError')}
            </Text>
          </View>
        )}
        {showControl && !showError && (
          <View style={styles.contentOverlay}>
            <SizedBox height={'auto'} />
            <View style={styles.containerPlay}>
              {/* <TouchableOpacity onPress={skipBackward}>
                <MaterialIcons
                  name="replay-10"
                  size={24}
                  color={Colors.WHITE}
                />
              </TouchableOpacity> */}
              {isLoading ? (
                <LoadingSpinner />
              ) : (
                <>
                  {isVideoEnd ? (
                    <TouchableOpacity onPress={replayVideo}>
                      <MaterialIcons
                        name="replay"
                        size={30}
                        color={Colors.WHITE}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity onPress={() => setPaused(!pause)}>
                      {!pause ? (
                        <>
                          {isVideoEnd ? (
                            <MaterialIcons
                              name="replay"
                              size={30}
                              color={Colors.WHITE}
                            />
                          ) : (
                            <MaterialIcons
                              name="pause"
                              size={30}
                              color={Colors.WHITE}
                            />
                          )}
                        </>
                      ) : (
                        <Entypo
                          name="controller-play"
                          size={30}
                          color={Colors.WHITE}
                        />
                      )}
                    </TouchableOpacity>
                  )}
                </>
              )}
              {/* <TouchableOpacity onPress={skipForward}>
                <MaterialIcons
                  name="forward-10"
                  size={24}
                  color={Colors.WHITE}
                />
              </TouchableOpacity> */}
            </View>
            <View style={styles.alignBottomControl}>
              <View style={styles.containerProgressSlider}>
                <Slider
                  style={{width: '100%', height: 10}}
                  minimumValue={0}
                  maximumValue={100}
                  minimumTrackTintColor={Colors.GREEN}
                  maximumTrackTintColor={Colors.GRAY}
                  value={getPercentage(currentTime, duration)}
                  onValueChange={onSeek}
                  thumbTintColor={'transparent'}
                  step={1}
                />
              </View>
              <View style={styles.containerProgressTime}>
                <Text
                  style={
                    styles.textTime
                  }>{`${position} / ${fullDuration}`}</Text>
                {isMuted ? (
                  <Ionicons
                    name="volume-mute"
                    size={24}
                    color={Colors.WHITE}
                    onPress={() => setIsMuted(false)}
                  />
                ) : (
                  <Ionicons
                    name="volume-high"
                    size={24}
                    color={Colors.WHITE}
                    onPress={() => setIsMuted(true)}
                  />
                )}
                <TouchableOpacity
                  onPress={handleShowModalPlayback}
                  style={{
                    alignSelf: 'flex-start',
                    backgroundColor: Colors.GRAY2,
                    padding: 5,
                    borderRadius: 5,
                  }}>
                  <Text style={styles.textNext}>{playbackText}</Text>
                </TouchableOpacity>
                {quality.length > 0 && (
                  <TouchableOpacity
                    onPress={handleShowModalQuality}
                    style={{
                      alignSelf: 'flex-start',
                      padding: 5,
                      borderRadius: 5,
                    }}>
                    <Fontisto
                      size={15}
                      name="player-settings"
                      color={Colors.WHITE}
                    />
                  </TouchableOpacity>
                )}
                {subtitles.length > 0 && (
                  <TouchableOpacity
                    onPress={handleShowModalSubtitle}
                    style={{
                      alignSelf: 'flex-start',
                      padding: 5,
                      borderRadius: 5,
                    }}>
                    <MaterialIcons
                      size={15}
                      name="textsms"
                      color={Colors.WHITE}
                    />
                  </TouchableOpacity>
                )}
              </View>
              <SizedBox height={10} />
            </View>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VideoPlayer;
