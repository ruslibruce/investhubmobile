import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styles = StyleSheet.create({
  containerText: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textYourInfo: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.TEXTINFO,
    marginLeft: 6,
  },
});
