import React from 'react';
import {Text, View} from 'react-native';
import {IconLockLogin} from '../../assets/svg';
import {styles} from './styles';
import {useTranslation} from 'react-i18next';

const TextYourInfo = () => {
  const {t: translate} = useTranslation();
  return (
    <View style={styles.containerText}>
      <IconLockLogin />
      <Text style={styles.textYourInfo}>{translate("Your_Info_Footer_Auth")}</Text>
    </View>
  );
};

export default TextYourInfo;
