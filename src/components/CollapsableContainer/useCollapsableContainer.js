import React from 'react';
import {useAnimatedStyle, useSharedValue, withTiming} from 'react-native-reanimated';

const useCollapsableContainer = expanded => {
  const [height, setHeight] = React.useState(0);
  const animatedHeight = useSharedValue(0);

  const onLayout = React.useCallback(event => {
    const onLayoutHeight = event.nativeEvent.layout.height;

    if (onLayoutHeight > 0 && height !== onLayoutHeight) {
      setHeight(onLayoutHeight);
    }
  }, []);

  const collapsableStyle = useAnimatedStyle(() => {
    animatedHeight.value = expanded ? withTiming(height) : withTiming(0);

    return {
      height: animatedHeight.value,
    };
  }, [expanded, height]);

  return {
    collapsableStyle,
    onLayout,
  };
};

export default useCollapsableContainer;
