import React from 'react';
import { View } from 'react-native';
import Animated from 'react-native-reanimated';
import useCollapsableContainer from './useCollapsableContainer';
import { Colors } from '../../utility';

export const CollapsableContainer = ({children, expanded}) => {
  const {collapsableStyle, onLayout} = useCollapsableContainer(expanded);

  return (
    <Animated.View style={[collapsableStyle, {overflow: 'hidden'}]}>
      <View style={{position: 'absolute', width: '100%', backgroundColor: Colors.WHITE}} onLayout={onLayout}>
        {children}
      </View>
    </Animated.View>
  );
};
