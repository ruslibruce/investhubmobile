import moment from 'moment';
import React from 'react';
import {
  ImageBackground,
  Pressable,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  IconCommentRed,
  IconCup,
  IconShareRed,
  IconThumbRed,
} from '../../assets/svg';
import {Colors, handleOpenLink} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import useCourseCard from './useCourseCard';
import {ProgressBar} from '@ui-kitten/components';
import Config from 'react-native-config';
import Recaptcha from 'react-native-recaptcha-that-works';

const CourseCard = ({item, isCourse, isCollection, isMyCourse}) => {
  const {
    handleNavigateCourse,
    handleNavigationCollection,
    handleOpenModalReport,
    recaptchaRef,
    onVerify,
    onExpire,
    authMethod,
  } = useCourseCard(item.id);
  const styles = styling();
  return (
    <TouchableOpacity
      onPress={() => handleNavigateCourse(item, isCourse)}
      style={styles.containerFlatlistItem}>
      <ImageBackground
        source={
          item.cover_image_file ? {uri: item.cover_image_file} : item.image
        }
        style={styles.imageBackgroundStyle}>
        {/* <View style={styles.containerPositionCup}>
          <Text style={styles.textLevelCup}>{item.category}</Text>
        </View> */}
      </ImageBackground>
      <View style={styles.containerTextCard}>
        <View style={{height: 35}}>
          <Text numberOfLines={2} style={styles.textTitle}>
            {item.title}
          </Text>
        </View>
        <SizedBox height={6} />
        <Text style={styles.textSubtitle}>{item.conten_provider ?? '--'}</Text>
        <SizedBox height={6} />
        <View style={styles.containerTag}>
          {/* <View style={styles.containerTagChild}> */}
          <Text style={styles.textTag}>{item.category}</Text>
          {/* </View> */}
        </View>
        {/* <View style={styles.containerTotalCourse}>
          <Text style={styles.textTotalCourse}>
            {'3'}
            {' Course'}
          </Text>
          <Text style={styles.textTotalHours}>{'3 hours, 20 min'}</Text>
        </View> */}
        <SizedBox height={10} />
        {isMyCourse && (
          <View>
            <ProgressBar
              status={'success'}
              style={styles.progressColor}
              progress={Number(item?.progress) / 100}
            />
            <View
              style={{
                position: 'absolute',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.textProgress}>{item?.progress} %</Text>
            </View>
          </View>
        )}
        <SizedBox height={10} />
        <View style={styles.containerBottomCard}>
          {authMethod.isLogin && (
            <>
              <Pressable onPress={() => handleNavigationCollection(item)}>
                <AntDesign
                  color={Colors.PRIMARY}
                  size={15}
                  name="pluscircleo"
                />
              </Pressable>
              <Pressable style={{marginLeft: 10}} onPress={() => handleOpenModalReport(item)}>
                <MaterialIcons color={Colors.PRIMARY} size={20} name="report" />
              </Pressable>
            </>
          )}
          <View style={styles.containerPositionIcon}>
            <View style={styles.alignIcon}>
              <IconThumbRed />
              <SizedBox width={3} />
              <Text style={styles.textIcon}>{item.number_of_likes}</Text>
            </View>
            <View style={styles.alignIcon}>
              <IconCommentRed />
              <SizedBox width={3} />
              <Text style={styles.textIcon}>{item.number_of_comments}</Text>
            </View>
            <View style={styles.alignIcon}>
              <IconShareRed />
              <SizedBox width={3} />
              <Text style={styles.textIcon}>{item.number_of_shares}</Text>
            </View>
          </View>
        </View>
      </View>
      <Recaptcha
        ref={recaptchaRef}
        siteKey={Config.RECAPTCHA_SITE_KEY}
        baseUrl={Config.BASE_URL}
        onVerify={onVerify}
        onExpire={onExpire}
        size="normal"
      />
    </TouchableOpacity>
  );
};

export default CourseCard;
