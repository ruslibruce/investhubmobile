import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (isLogin) => StyleSheet.create({
  containerFlatlistItem: {
    borderRadius: 12,
    backgroundColor: Colors.WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    // minWidth: 160,
    flexBasis: '48%',
    maxWidth: matrics.screenWidth / 2.3,
    width: matrics.screenWidth / 2.3,
  },
  imageBackgroundStyle: {
    height: matrics.screenHeight / 6,
    maxWidth: '100%',
    overflow: 'hidden',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: Colors.WHITE,
  },
  containerPositionCup: {
    backgroundColor: Colors.PINK,
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    maxWidth: 85,
    width: 85,
    paddingHorizontal: 10,
    paddingVertical: 5,
    position: 'absolute',
    right: 0,
    bottom: 4,
  },
  textLevelCup: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.PRIMARY,
  },
  containerTextCard: {
    padding: 12,
  },
  textTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
  },
  textSubtitle: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.GRAY5,
  },
  containerTotalCourse: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textTotalCourse: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(8),
    color: Colors.PRIMARY,
  },
  textTotalHours: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.PRIMARY,
  },
  containerBottomCard: {
    flexDirection: 'row',
    justifyContent: isLogin ? 'space-between' : 'flex-end',
    alignItems: 'center',
  },
  containerPositionIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  alignIcon: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  textIcon: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.BLACK,
    lineHeight: 15,
  },
  progressColor: {
    backgroundColor: '#D9D9D9',
  },
  textProgress: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(8),
    color: Colors.WHITE,
  },
  containerTag: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    gap: 10,
  },
  containerTagChild: {
    backgroundColor: Colors.PINK,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 15,
  },
  textTag: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.PRIMARY,
  },
  textTitleModal: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(18),
    color: Colors.BLACK,
  },
});
