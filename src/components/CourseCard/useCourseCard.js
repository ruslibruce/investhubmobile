import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {Text, View} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import Toast from 'react-native-toast-message';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import usePostReport from '../../api/usePostReport';
import {
  COLLECTION_CHOOSE,
  Colors,
  DASHBOARD_COURSE,
  TAB_COURSE_DETAIL,
} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import TextInputWithLabel from '../../widgets/TextInputWithLabel';
import {styles} from './styles';
import { useSelector } from 'react-redux';

const useCourseCard = idCourse => {
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const navigation = useNavigation();
  const [formReport, setFormReport] = React.useState({
    report: '',
    course_id: idCourse,
  });
  const recaptchaRef = React.useRef(null);
  const [tokenRecaptcha, setTokenRecaptcha] = React.useState('');
  const {
    accessPostReport,
    isLoading: isLoadingPostReport,
    isError,
  } = usePostReport();

  const onVerify = token => {
    setTokenRecaptcha(token);
  };

  const onExpire = () => {
    setTokenRecaptcha('');
    Toast.show({
      type: 'info',
      position: 'top',
      text1: translate('Recaptcha_Expired'),
      onHide: () => setTokenRecaptcha(''),
    });
  };

  const onOpenRecaptcha = React.useCallback(() => {
    if (!tokenRecaptcha) {
      recaptchaRef.current.open();
    }
  }, [tokenRecaptcha]);

  const handleNavigateCourse = (value, isCourse) => {
    if (isCourse) {
      navigation.navigate(TAB_COURSE_DETAIL, {id: value.id});
      return;
    }

    navigation.navigate(DASHBOARD_COURSE, {
      screen: TAB_COURSE_DETAIL,
      params: {
        id: value.id,
      },
    });
  };

  const handleNavigationCollection = item => {
    navigation.navigate(COLLECTION_CHOOSE, {id: item.id});
  };

  const onChangeText = value => {
    setFormReport(prev => ({
      ...prev,
      report: value,
    }));
  };

  React.useEffect(() => {
    if (isError) {
      magicModal.hide();
      setTokenRecaptcha('');
    }
  }, [isError]);

  React.useEffect(() => {
    if (tokenRecaptcha) {
      handlePostReport();
    }
  }, [tokenRecaptcha]);

  const handlePostReport = React.useCallback(() => {
    if (!formReport.report) {
      Toast.show({
        type: 'info',
        position: 'top',
        text1: translate('ReportEmpty'),
      });
      return;
    }
    accessPostReport({...formReport, 'g-recaptcha-response': tokenRecaptcha});
  }, [formReport, tokenRecaptcha]);

  const handleOpenModalReport = () => {
    setFormReport({
      report: '',
      course_id: idCourse,
    });
    magicModal.show(() => (
      <View
        style={{
          backgroundColor: Colors.WHITE,
          padding: 20,
          marginHorizontal: 20,
        }}>
        <Text style={[styles.textTitleModal, {alignSelf: 'center'}]}>
          {translate('ReportCourse')}
        </Text>
        <SizedBox height={10} />
        <View>
          <TextInputWithLabel
            label={translate('DescReport')}
            placeholder={translate('EnterReport')}
            multiline
            name={'report'}
            onChangeText={onChangeText}
            containerInputText={{height: 100}}
            styleInput={{padding: 30}}
            required={false}
          />
          <SizedBox height={10} />
          <ButtonPrimary
            loading={isLoadingPostReport}
            disabled={isLoadingPostReport}
            icon={
              <MaterialIcons color={Colors.WHITE} size={20} name="report" />
            }
            label={translate('Report')}
            onPress={() => {
              onOpenRecaptcha();
            }}
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              gap: 10,
              backgroundColor: Colors.PRIMARY,
            }}
          />
        </View>
      </View>
    ));
  };

  return {
    handleNavigateCourse,
    handleNavigationCollection,
    handleOpenModalReport,
    onExpire,
    onVerify,
    recaptchaRef,
    authMethod,
  };
};

export default useCourseCard;
