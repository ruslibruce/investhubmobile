import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SizedBox from '../../widgets/SizedBox';
import {styles} from './styles';
import useProfileButtonGeneral from './useProfileButtonGeneral';

const ProfileButtonGeneral = () => {
  const {groupArray, navigation, setIsModalSukses} = useProfileButtonGeneral();
  return (
    <>
      {groupArray.map(item => (
        <React.Fragment key={item.id}>
          <SizedBox height={10} />
          <TouchableOpacity
            onPress={() =>
              item.url === 'delete_acc'
                ? setIsModalSukses(true)
                : navigation.navigate(item.url)
            }
            style={styles.containerButton}>
            <View style={styles.containerIcon}>{item.icon}</View>
            <SizedBox width={12} />
            <Text style={styles.textButton}>{item.name}</Text>
            <MaterialIcons name="arrow-forward-ios" color={'#000'} size={15} />
            <SizedBox width={12} />
          </TouchableOpacity>
        </React.Fragment>
      ))}
    </>
  );
};

export default ProfileButtonGeneral;
