import {StyleSheet} from 'react-native';
import { Colors, FontType, getFontSize } from '../../utility';

export const styles = StyleSheet.create({
  containerButton: {
    flexDirection: 'row',
    borderColor: '#E8E8E8',
    borderWidth: 1,
    borderRadius: 32,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerIcon: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PINK,
    borderRadius: 43,
  },
  textButton: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
    flex: 1,
  },
  containerModalIcon: {
    borderRadius: 88,
    width: 88,
    height: 88,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.PINK,
  },
  containerButtonSingle: {
    width: 100,
  },
});
