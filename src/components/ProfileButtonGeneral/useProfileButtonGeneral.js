import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { magicModal } from 'react-native-magic-modal';
import { useSelector } from 'react-redux';
import usePostLogout from '../../api/usePostLogout';
import {
  IconChangePasswordProfile,
  IconDeleteAccount,
  IconPreferenced,
  IconUpdateProfile,
} from '../../assets/svg';
import {
  PREFERENCES,
  TAB_TOOLS_CHANGE_PASSWORD,
  UPDATE_PROFILE,
} from '../../utility';
import ModalCustom from '../../widgets/ModalCustom';
import { styles } from './styles';
import usePostDeleteAccount from '../../api/usePostDeleteAccount';

const useProfileButtonGeneral = () => {
  const {t: translate} = useTranslation();
  const groupArray = [
    {
      id: 1,
      icon: <IconUpdateProfile width={25} height={25} />,
      name: translate('UpdateProfile'),
      url: UPDATE_PROFILE,
    },
    {
      id: 2,
      icon: <IconPreferenced width={25} height={25} />,
      name: translate('Preferences'),
      url: PREFERENCES,
    },
    {
      id: 3,
      icon: <IconChangePasswordProfile width={25} height={25} />,
      name: translate('ChangePassword'),
      url: TAB_TOOLS_CHANGE_PASSWORD,
    },
    {
      id: 4,
      icon: <IconDeleteAccount width={25} height={25} />,
      name: translate('DeleteAccount'),
      url: 'delete_acc',
    },
  ];
  const navigation = useNavigation();
  const [isModalSukses, setIsModalSukses] = React.useState(false);
  const {accessPostDeleteAccount} = usePostDeleteAccount();
  const {token} = useSelector(state => state.auth);

  const hideModalSukses = e => {
    magicModal.hide();
    setIsModalSukses(false);
    if (e === 'OK') {
      accessPostDeleteAccount(token);
      magicModal.show(() => (
        <ModalCustom
          hideModalSukses={() => magicModal.hide()}
          iconModal={
            <View style={styles.containerModalIcon}>
              <IconDeleteAccount width={35} height={35} />
            </View>
          }
          singleButton={true}
          textButton={'OK'}
          textModalJudul={translate('DeleteAccountSuccess')}
          textModalSub={translate('DeleteAccountSuccessDesc')}
          containerButtonSingle={styles.containerButtonSingle}
        />
      ));
    }
  };

  React.useEffect(() => {
    if (isModalSukses) {
      magicModal.show(() => (
        <ModalCustom
          hideModalSukses={e => hideModalSukses(e)}
          iconModal={
            <View style={styles.containerModalIcon}>
              <IconDeleteAccount width={35} height={35} />
            </View>
          }
          doubleButton={true}
          textButton={'OK'}
          textButton2={'Cancel'}
          textModalJudul={translate('deleteAccount')}
          textModalSub={translate('DeleteAccountDesc')}
          containerButtonSingle={styles.containerButtonSingle}
        />
      ));
    }
  }, [isModalSukses]);

  return {groupArray, navigation, setIsModalSukses};
};

export default useProfileButtonGeneral;
