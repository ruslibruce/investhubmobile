import React from 'react';
import {useTranslation} from 'react-i18next';
import {
  KeyboardAvoidingView,
  Platform,
  Pressable,
  Text,
  View,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useMagicModal} from 'react-native-magic-modal';
import Toast from 'react-native-toast-message';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import useGetComment from '../../api/useGetComment';
import usePostComment from '../../api/usePostComment';
import {Colors} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import TextInputWithLabel from '../../widgets/TextInputWithLabel';
import ReplyCommentComponent from '../ReplyCommentComponent';
import {styles} from './styles';

const CommentComponent = ({
  isModalVisibleComment,
  type,
  dataCourseDetail,
  setIsModalVisibleComment,
}) => {
  const {t: translate} = useTranslation();
  const [formComment, setFormComment] = React.useState({
    comment: '',
  });
  const {accessPostComment, isSuccess, isLoading} = usePostComment();
  const {
    accessComment: accessGetComment,
    dataComment,
    isLoading: isLoadingGetComment,
  } = useGetComment();
  console.log('dataComment', dataComment);

  const onChangeText = value => {
    setFormComment({
      ...formComment,
      comment: value,
    });
  };

  React.useEffect(() => {
    if (isModalVisibleComment) {
      console.log('isModalVisibleComment', isModalVisibleComment);

      handleGetComment();
    }
  }, [isModalVisibleComment]);

  React.useEffect(() => {
    if (isSuccess) {
      handleGetComment();
    }
  }, [isSuccess]);

  const handleGetComment = () => {
    setFormComment({
      parent_id: dataCourseDetail?.id,
      parent_type: type,
      comment: '',
    });
    accessGetComment(dataCourseDetail?.id, type);
  };

  const handlePostComment = formComment => {
    console.log(formComment);
    if (!formComment.comment) {
      Toast.show({
        type: 'info',
        position: 'top',
        text1: translate('CommentEmpty'),
      });
      return;
    }
    accessPostComment(formComment);
  };

  return (
    <View
      style={{
        backgroundColor: Colors.WHITE,
        padding: 20,
        marginHorizontal: 20,
      }}>
      <Pressable
        style={{
          alignSelf: 'flex-end',
          alignItems: 'center',
          position: 'absolute',
          top: 10,
          right: 10,
        }}>
        <Entypo
          onPress={() => setIsModalVisibleComment(false)}
          name="cross"
          color={Colors.BLACK}
          size={25}
        />
      </Pressable>
      <Text style={[styles.textTitleModal, {alignSelf: 'center'}]}>
        {translate('CommentCourse')}
      </Text>
      <SizedBox height={10} />
      <View>
        <TextInputWithLabel
          placeholder={translate('EnterComment')}
          multiline
          name={'comment'}
          onChangeText={onChangeText}
          containerInputText={{height: 100}}
          styleInput={{padding: 30}}
          value={formComment.comment}
          required={false}
        />
        <SizedBox height={10} />
        <ButtonPrimary
          loading={isLoading}
          icon={
            <MaterialIcons
              color={Colors.WHITE}
              size={20}
              name="insert-comment"
            />
          }
          label={translate('Comment')}
          onPress={() => {
            handlePostComment(formComment);
          }}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            gap: 10,
            backgroundColor:
              formComment.comment === '' ? Colors.GRAY : Colors.PRIMARY,
          }}
          disabled={isLoading || formComment.comment === ''}
        />
      </View>
      <SizedBox height={10} />
      <Text style={styles.textTitleModal}>{translate('ListComment')}</Text>
      <SizedBox height={10} />
      <View style={{height: 350}}>
        <ScrollView
          contentContainerStyle={{paddingHorizontal: 5}}
          showsVerticalScrollIndicator={false}>
          {dataComment &&
            dataComment?.data?.map((item, index) => (
              <ReplyCommentComponent
                key={index}
                dataCourseDetail={dataCourseDetail}
                handleGetComment={handleGetComment}
                isModalVisibleComment={isModalVisibleComment}
                item={item}
                type={type}
                isLoadingGetComment={isLoadingGetComment}
              />
            ))}
        </ScrollView>
      </View>
    </View>
  );
};

export default CommentComponent;
