import React from 'react';
import {Image, View} from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
import {Colors, matrics} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {styles} from './styles';
import useCarouselTop from './useCarouselTop';

function CarouselTop() {
  const {dataCarousel, indexCarousel, setIndexCarousel} = useCarouselTop();
  return (
    <View style={styles.container}>
      <Carousel
        // loop
        width={matrics.screenWidth - 40}
        height={150}
        autoPlay={true}
        data={dataCarousel}
        panGestureHandlerProps={{
          activeOffsetX: [-10, 10],
        }}
        onProgressChange={(_, absoluteProgress) => {
          setIndexCarousel(Math.round(absoluteProgress));
        }}
        scrollAnimationDuration={1000}
        renderItem={({item, index}) => (
          <View key={index} style={styles.containerBackground}>
            <Image source={item.image} />
          </View>
        )}
      />
      <SizedBox height={20} />
      <View style={styles.containerItem}>
        {dataCarousel.map((item, index) => {
          return (
            <View
              key={item.id}
              style={[
                styles.carouselItem,
                {
                  backgroundColor:
                    indexCarousel === index ? Colors.PRIMARY : Colors.GRAY,
                  width: indexCarousel === index ? 16 : 8,
                },
              ]}
            />
          );
        })}
      </View>
    </View>
  );
}

export default CarouselTop;
