import {View, Text} from 'react-native';
import React from 'react';
import {dummyCarousel} from '../../assets/images';

const useCarouselTop = () => {
  const dataCarousel = [
    {
      id: 1,
      image: dummyCarousel,
    },
    {
      id: 2,
      image: dummyCarousel,
    },
    {
      id: 3,
      image: dummyCarousel,
    },
    {
      id: 4,
      image: dummyCarousel,
    },
  ];

  const [indexCarousel, setIndexCarousel] = React.useState(0);

  return {dataCarousel, indexCarousel, setIndexCarousel};
};

export default useCarouselTop;
