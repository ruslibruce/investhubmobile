import {StyleSheet} from 'react-native';
import {Colors} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerBackground: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 10,
    borderRadius: 16,
    overflow: 'hidden',
    maxWidth: 335,
  },
  containerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    gap: 4,
  },
  carouselItem: {
    height: 8,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
