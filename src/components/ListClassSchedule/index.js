import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import {IconRightCarousel} from '../../assets/svg';
import useListClassSchedule from './useListClassSchedule';

const ListClassSchedule = () => {
  const {dataDummy, indexFlatlist, onViewCallBack, translate, authMethod} =
    useListClassSchedule();
  const styles = styling(indexFlatlist, dataDummy);
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{translate('Class_For_You')}</Text>
        {/* <TouchableOpacity style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>{translate('ViewAll')}</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </TouchableOpacity> */}
      </View>
      <View style={styles.containerFlatlist}>
        <FlatList
          onViewableItemsChanged={onViewCallBack}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 50,
          }}
          showsHorizontalScrollIndicator={false}
          data={dataDummy}
          horizontal
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <View style={styles.ContainerItemFlatlist}>
              <View style={styles.containerTopCard}>
                <View style={styles.containerImage}>
                  <Image style={styles.imageStyle} source={item.image} />
                </View>
                <View style={styles.widthText}>
                  {/* <Text numberOfLines={2} style={styles.textForum}>
                      {item.forum}
                    </Text> */}
                  <SizedBox height={4} />
                  <Text numberOfLines={2} style={styles.textTitle}>
                    {item.title}
                  </Text>
                  <SizedBox height={4} />
                  <Text numberOfLines={2} style={styles.textDesc}>
                    {item.description}
                  </Text>
                </View>
              </View>
            </View>
          )}
        />
        <View style={styles.containerDotCarousel}>
          <View style={styles.stylingLeftSlider} />
          <View style={styles.stylingMidSlider} />
          <View style={styles.stylingRightSlider} />
        </View>
      </View>
      <SizedBox height={20} />
    </View>
  );
};

export default ListClassSchedule;
