import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = horizontal =>
  StyleSheet.create({
    containerListItems: {
      width: horizontal ? 375 : '100%',
      maxWidth: horizontal ? 375 : '100%',
      // height: 88,
      backgroundColor: Colors.WHITE,
      borderTopLeftRadius: 16,
      borderBottomLeftRadius: 16,
      borderTopRightRadius: 8,
      borderBottomRightRadius: 8,
      flexDirection: 'row',
      alignItems: 'center',
      gap: 12,
      marginBottom: 15,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 3,
      },
      shadowOpacity: 0.29,
      shadowRadius: 4.65,
      elevation: 7,
    },
    containerImageList: {
      width: 100,
      height: 100,
      backgroundColor: Colors.WHITE,
      borderTopLeftRadius: 16,
      borderBottomLeftRadius: 16,
      overflow: 'hidden',
    },
    widthImage: {
      width: '100%',
      height: '100%',
    },
    containerRightItem: {
      maxWidth: '60%',
    },
    textTopTitleItem: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(14),
      color: Colors.PRIMARY,
    },
    containerBottomItem: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      gap: 8,
      marginBottom: 10,
    },
    containerBottomLeftItem: {
      paddingHorizontal: 10,
      paddingVertical: 5,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 15,
    },
    textBottomLeftItem: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: Colors.PRIMARY,
    },
    containerBottomRightItem: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 5,
    },
    textBottomRightItemRed: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(8),
      color: Colors.BLACK,
      lineHeight: 18,
    },
    dotStyling: {
      backgroundColor: Colors.GRAY,
      width: 4,
      height: 4,
      borderRadius: 4,
    },
    textBottomRightItem: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(8),
      color: Colors.GRAY,
    },
  });
