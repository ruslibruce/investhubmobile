import {useNavigation} from '@react-navigation/native';
import { EVENT_DETAIL_PAGE } from '../../utility';

const useEventCard = () => {
  const navigation = useNavigation();
  handleNavigation = (id) => {
    if (!id) return;
    navigation.navigate(EVENT_DETAIL_PAGE, {
      id: id,
    });
  };
  return {handleNavigation};
};

export default useEventCard;
