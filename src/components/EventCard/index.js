import moment from 'moment';
import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {isHTML} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import WebDisplay from '../WebDisplay';
import {styling} from './styles';
import useEventCard from './useEventCard';

const EventCard = ({item, horizontal}) => {
  const {handleNavigation} = useEventCard();
  const styles = styling(horizontal);

  const tagsStyles = {
    p: {
      fontSize: 14,
      fontWeight: 700,
      fontFamily: 'OpenSans-Regular',
      color: '#A62829',
    },
  };

  return (
    <TouchableOpacity
      onPress={() => handleNavigation(item.id)}
      style={styles.containerListItems}>
      <View style={styles.containerImageList}>
        <Image source={{uri: item.cover_image}} style={styles.widthImage} />
      </View>
      <View style={styles.containerRightItem}>
        {isHTML(item.title) ? (
          <View
            style={{
              height: 65,
              overflow: 'hidden',
            }}>
            <WebDisplay html={item.title} tagsStyles={tagsStyles} />
          </View>
        ) : (
          <>
            <Text numberOfLines={2} style={styles.textTopTitleItem}>
              {item.title}
            </Text>
            <SizedBox height={11} />
          </>
        )}
        <View style={styles.containerBottomItem}>
          <View style={styles.containerBottomLeftItem}>
            <Text style={styles.textBottomLeftItem}>{item.category}</Text>
          </View>
          <View style={styles.containerBottomRightItem}>
            <Text style={styles.textBottomRightItemRed}>{item.location}</Text>
            <View style={styles.dotStyling} />
            <Text style={styles.textBottomRightItem}>
              {moment(item.start_time).format('MMM DD, YYYY')}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default EventCard;
