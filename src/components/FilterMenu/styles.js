import {StyleSheet} from 'react-native';
import { Colors, FontType, getFontSize, matrics } from '../../utility';

export const styles = StyleSheet.create({
  containerFilter: {
    position: 'absolute',
    zIndex: 4,
    backgroundColor: Colors.WHITE,
  },
  containerTitleFilter: {
    flexDirection: 'row',
    gap: 20,
    height: 100,
    backgroundColor: Colors.PRIMARY,
    alignItems: 'flex-end',
    paddingBottom: 10,
    paddingHorizontal: 10,
  },
  textFilterTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(20),
    color: Colors.WHITE,
    lineHeight: 20,
  },
  containerCheckbox: {
    backgroundColor: Colors.WHITE,
    width: matrics.screenWidth,
    paddingHorizontal: 10,
  },
  checkbox: {
    marginBottom: 10,
  },
  textFilter: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: '#696F79',
    marginLeft: 10,
  },
  styleButton: {
    width: '90%',
    alignSelf: 'center',
  },
  textButton: {
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.WHITE,
    fontSize: getFontSize(12),
  },
});
