import {CheckBox} from '@ui-kitten/components';
import React from 'react';
import {Text, View} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {Colors} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import { styles } from './styles';

const FilterMenu = ({dataFilter, handleChecked, setFiltered}) => {
  return (
    <View style={styles.containerFilter}>
      <View style={styles.containerTitleFilter}>
        <Entypo
          onPress={() => setFiltered(false)}
          name="cross"
          color={Colors.WHITE}
          size={25}
        />
        <Text style={styles.textFilterTitle}>Filter FAQ</Text>
      </View>
      <SizedBox height={10} />
      {dataFilter.map((item, index) => (
        <View key={index} style={styles.containerCheckbox}>
          <CheckBox
            style={styles.checkbox}
            checked={item.isChecked}
            onChange={() => handleChecked(item)}>
            {evaProps => <Text style={styles.textFilter}>{item.name}</Text>}
          </CheckBox>
        </View>
      ))}
      <ButtonPrimary
        style={styles.styleButton}
        styleLabel={styles.textButton}
        label={'Apply'}
      />
      <SizedBox height={10} />
    </View>
  );
};

export default FilterMenu;
