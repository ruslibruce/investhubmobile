import React from 'react';
import { Text, View } from 'react-native';
import useGetStockScreenerCategorySector from '../../api/useGetStockScreenerCategorySector';
import useGetStockScreenerCategorySubSector from '../../api/useGetStockScreenerCategorySubSector';
import useGetStockScreenerMinMax from '../../api/useGetStockScreenerMinMax';
import { Colors, FontType, getFontSize, paramsToString } from '../../utility';

const useFilterStockScreen = () => {
  const itemsCategory = [
    {
      id: 1,
      name: 'Sektor',
    },
    {
      id: 2,
      name: 'SubSektor',
    },
  ];
  const [dataFilter, setDataFilter] = React.useState(itemsCategory);
  const [category, setCategory] = React.useState({
    Sector: '',
    SubSector: '',
  });
  const {dataStockScreenerCategorySector} = useGetStockScreenerCategorySector();
  const {accessStockScreenerCategorySub, dataStockScreenerCategorySubSector} =
    useGetStockScreenerCategorySubSector();
  const {dataFilterMinMax, setDataFilterMinMax, isLoading} =
    useGetStockScreenerMinMax();
  const [isClickSubmitChange, setIsClickSubmitChange] = React.useState(false);
  React.useEffect(() => {
    accessStockScreenerCategorySub();
  }, []);

  const handleChangeDropdown = React.useCallback((item, params, category) => {
    if (params == 'Sector') {
      let data = paramsToString({
        sector: item.value,
      });
      accessStockScreenerCategorySub(data);
      setCategory(() => ({
        ...category,
        [params]: item.value,
        SubSector: '',
      }));
      return;
    }
    setCategory(() => ({
      ...category,
      [params]: item.value,
    }));
  }, []);

  const renderItemDropdown = item => {
    return (
      <View
        style={{
          padding: 12,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text
          style={{
            flex: 1,
            fontSize: getFontSize(14),
            fontFamily: FontType.openSansSemiBold600,
            color: Colors.BLACK,
          }}>
          {item.value}
        </Text>
      </View>
    );
  };

  const handleSubmitChange = (item, inputValueMax, inputValueMin) => {
    item.maxModify = inputValueMax;
    item.minModify = inputValueMin;
    let categoryTemp = {
      [item.name + 'Min']: item.minModify,
      [item.name + 'Max']: item.maxModify,
    };
    setCategory({
      ...category,
      ...categoryTemp,
    });
    setDataFilterMinMax([...dataFilterMinMax, item]);
    setIsClickSubmitChange(true);
  };

  const handleClearFilter = () => {
    let dataFilter = dataFilterMinMax.map(item => {
      item.maxModify = '';
      item.minModify = '';
      return item;
    });
    setDataFilterMinMax(dataFilter);
    let temp = {
      Sector: '',
      SubSector: '',
    };
    setCategory({
      ...temp,
    });
  };

  const handleClearFilterMaxMin = item => {
    item.maxModify = '';
    item.minModify = '';
    delete category[item.name + 'Min'];
    delete category[item.name + 'Max'];
    setDataFilterMinMax([...dataFilterMinMax, item]);
  };

  return {
    renderItemDropdown,
    handleChangeDropdown,
    dataStockScreenerCategorySubSector,
    dataStockScreenerCategorySector,
    category,
    dataFilter,
    dataFilterMinMax,
    handleSubmitChange,
    handleClearFilter,
    handleClearFilterMaxMin,
    isClickSubmitChange,
    setIsClickSubmitChange,
  };
};

export default useFilterStockScreen;
