import {StyleSheet} from 'react-native';
import { Colors, FontType, getFontSize, matrics } from '../../utility';

export const styles = StyleSheet.create({
  containerFilter: {
    position: 'absolute',
    zIndex: 4,
    backgroundColor: Colors.WHITE,
    flex: 1,
  },
  containerTitleFilter: {
    flexDirection: 'row',
    gap: 20,
    height: 100,
    backgroundColor: Colors.PRIMARY,
    alignItems: 'flex-end',
    paddingBottom: 10,
    paddingHorizontal: 10,
  },
  textFilterTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(20),
    color: Colors.WHITE,
    lineHeight: 20,
  },
  checkbox: {
    marginBottom: 10,
  },
  textFilter: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: '#696F79',
    marginLeft: 10,
  },
  containerButton:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  styleButtonClear: {
    width: '45%',
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    borderColor: Colors.PRIMARY,
    borderWidth: 1,
  },
  textButtonClear: {
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.PRIMARY,
    fontSize: getFontSize(12),
  },
  styleButton: {
    width: '45%',
    alignSelf: 'center',
  },
  textButton: {
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.WHITE,
    fontSize: getFontSize(12),
  },
});
