import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {Colors} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import ListItemStockScreenerCard from '../ListItemStockScreenerCard';
import ListItemStockScreenerDropdown from '../ListItemStockScreenerDropdown';
import {styles} from './styles';
import useFilterStockScreen from './useFilterStockScreen';

const FilterStockScreen = ({setIsFiltered, handleGetParamsFilter}) => {
  const {
    renderItemDropdown,
    handleChangeDropdown,
    dataStockScreenerCategorySubSector,
    dataStockScreenerCategorySector,
    category,
    dataFilter,
    dataFilterMinMax,
    handleSubmitChange,
    handleClearFilter,
    handleClearFilterMaxMin,
    isClickSubmitChange,
    setIsClickSubmitChange,
  } = useFilterStockScreen();
  return (
    <View style={styles.containerFilter}>
      <View style={styles.containerTitleFilter}>
        <Entypo
          onPress={() => setIsFiltered(false)}
          name="cross"
          color={Colors.WHITE}
          size={25}
        />
        <Text style={styles.textFilterTitle}>Filter Stock Screener</Text>
      </View>
      <SizedBox height={10} />
      <View style={{height: 400}}>
        <ScrollView>
          {dataFilter.map((item, index) => (
            <ListItemStockScreenerDropdown
              key={index}
              category={category}
              item={item}
              dataStockScreenerCategorySubSector={
                dataStockScreenerCategorySubSector
              }
              dataStockScreenerCategorySector={dataStockScreenerCategorySector}
              handleChangeDropdown={handleChangeDropdown}
              renderItemDropdown={renderItemDropdown}
            />
          ))}
          {dataFilterMinMax.map((item, index) => (
            <ListItemStockScreenerCard
              handleClearFilterMaxMin={handleClearFilterMaxMin}
              handleSubmitChange={handleSubmitChange}
              key={index}
              item={item}
              isClickSubmitChange={isClickSubmitChange}
              setIsClickSubmitChange={setIsClickSubmitChange}
            />
          ))}
          <View style={styles.containerButton}>
            <ButtonPrimary
              onPress={handleClearFilter}
              style={styles.styleButtonClear}
              styleLabel={styles.textButtonClear}
              label={'Clear Filter'}
            />
            <ButtonPrimary
              onPress={() => handleGetParamsFilter(category)}
              style={styles.styleButton}
              styleLabel={styles.textButton}
              label={'Apply'}
            />
          </View>
          <SizedBox height={10} />
        </ScrollView>
      </View>
    </View>
  );
};

export default FilterStockScreen;
