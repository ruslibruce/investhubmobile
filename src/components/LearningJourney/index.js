import {ProgressBar} from '@ui-kitten/components';
import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import PieChart from 'react-native-pie-chart';
import {
  IconJourney100,
  IconJourneyLock,
  IconJourneyRun,
} from '../../assets/svg';
import {Colors, imageLMS} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import LoadingSpinner from '../LoadingSpinner';
import {styles} from './styles';
import useLearningJourney from './useLearningJourney';

const LearningJourney = () => {
  const {translate, dataLearningJourney, handleNavigate, isLoading} =
    useLearningJourney();
  if (isLoading) return <LoadingSpinner isFullScreen={true} />;
  if (dataLearningJourney.length === 0) return null;
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{translate('MyLearningJourney')}</Text>
        {/* <View style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>{translate('ViewAll')}</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </View> */}
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={dataLearningJourney}
          horizontal
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => {
            const testCompleted = item?.is_test_passed ? 1 : 0;
            const completedCount =
              item.requirements.length > 0
                ? item.requirements.filter(
                    val => val.progress && val.progress.is_completed,
                  ).length + testCompleted
                : 0;
            const coursePlusTest = item.requirements?.length + 1;
            const valueCircular =
              completedCount === 0
                ? 0
                : (completedCount / coursePlusTest) * 100;
            return (
              <TouchableOpacity
                onPress={() => handleNavigate(item)}
                style={styles.containerItem}>
                <PieChart
                  widthAndHeight={50}
                  series={[valueCircular, 100 - valueCircular]}
                  sliceColor={[Colors.GREEN, Colors.GRAY]}
                  coverRadius={0.75}
                  coverFill={'#FFF'}
                />
                {item.badge_icon ? (
                  <Image
                    source={{
                      uri: `${imageLMS}/${item.badge_icon}`,
                    }}
                    style={{
                      width: 25,
                      height: 25,
                      position: 'absolute',
                      top: 21,
                    }}
                  />
                ) : (
                  <View style={{position: 'absolute', top: 21}}>
                    {item.achieved_at ? (
                      <IconJourney100 />
                    ) : coursePlusTest === 0 ? (
                      <IconJourneyLock />
                    ) : (
                      <IconJourneyRun />
                    )}
                  </View>
                )}
                <SizedBox height={6} />
                <View style={{height: 40}}>
                  <Text
                    numberOfLines={2}
                    style={[
                      styles.textTitle,
                      {
                        color:
                          item.requirements.length === 0
                            ? '#8A8A8A'
                            : Colors.BLACK,
                      },
                    ]}>
                    {item.name}
                  </Text>
                </View>
                <View style={styles.containerProgress}>
                  <ProgressBar
                    status={'success'}
                    style={styles.progressColor}
                    progress={
                      item.requirements.length > 0
                        ? completedCount / coursePlusTest
                        : 0
                    }
                  />
                </View>
                <SizedBox height={10} />
                <Text
                  style={[
                    styles.textStatus,
                    {
                      color: item.archieved_at
                        ? '#4EB252'
                        : item.requirements.length === 0
                        ? '#8A8A8A'
                        : Colors.BLACK,
                    },
                  ]}>
                  {item.achieved_at
                    ? translate('Completed')
                    : `${completedCount} of ${coursePlusTest} ${translate(
                        'Done',
                      )}`}
                </Text>
                <SizedBox height={6} />
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
};

export default LearningJourney;
