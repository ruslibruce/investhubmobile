import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  containerLabel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textLabel: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
  },
  containerSeeAll: {
    flexDirection: 'row',
  },
  textSeeAll: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
  containerFlatlist: {
    width: matrics.screenWidth,
    flex: 1,
  },
  contentFlatlist: {
    gap: 10,
    backgroundColor: Colors.WHITE,
    paddingBottom: 20,
    paddingRight: 30,
  },
  containerItem: {
    borderRadius: 12,
    backgroundColor: Colors.WHITE2,
    overflow: 'hidden',
    maxWidth: 100,
    width: 100,
    padding: 8,
    alignItems: 'center',
  },
  textTitle: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(10),
    textAlign: 'center',
  },
  textStatus: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(10),
  },
  containerProgress: {
    width: '100%',
  },
});
