import {useTranslation} from 'react-i18next';
import useGetLearningJourney from '../../api/useGetLearningJourney';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {MY_LEARNING_JOURNEY_DETAIL} from '../../utility';
import {useCallback} from 'react';

const useLearningJourney = () => {
  const {t: translate} = useTranslation();
  const navigation = useNavigation();
  const {dataLearningJourney, isLoading, accessJourney} =
    useGetLearningJourney();

  useFocusEffect(
    useCallback(() => {
      accessJourney();
    }, []),
  );

  const handleNavigate = value => {
    navigation.navigate(MY_LEARNING_JOURNEY_DETAIL, {
      id: value.id,
      name: value.name,
    });
  };

  return {
    dataLearningJourney,
    translate,
    handleNavigate,
    isLoading,
  };
};

export default useLearningJourney;
