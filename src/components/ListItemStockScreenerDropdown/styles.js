import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  containerCheckbox: {
    backgroundColor: Colors.WHITE,
    width: matrics.screenWidth,
    paddingHorizontal: 20,
  },
  containerButtonMidDropdown:{
    borderWidth: 0.5,
    borderColor: Colors.GRAY,
    borderRadius: 10,
    marginBottom: 10,
    height: 50,
    paddingHorizontal: 20,
  },
  placeholderDropdown:{
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
  },
  selectedDropdown:{
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
  },
  textInputSearchDropdown:{
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
  },
});
