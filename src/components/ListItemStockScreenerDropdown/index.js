import React from 'react';
import {View} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {IconChevUp} from '../../assets/svg';
import {Colors} from '../../utility';
import {styles} from './styles';

const ListItemStockScreenerDropdown = ({
  item,
  dataStockScreenerCategorySubSector,
  dataStockScreenerCategorySector,
  renderItemDropdown,
  handleChangeDropdown,
  category,
}) => {
  const [isFocus, setIsFocus] = React.useState(false);
  return (
    <View style={styles.containerCheckbox}>
      <Dropdown
        style={styles.containerButtonMidDropdown}
        placeholderStyle={styles.placeholderDropdown}
        selectedTextStyle={styles.selectedDropdown}
        inputSearchStyle={styles.textInputSearchDropdown}
        iconStyle={styles.iconStyleDropdown}
        data={
          item.name === 'Sektor'
            ? dataStockScreenerCategorySector
            : dataStockScreenerCategorySubSector
        }
        search
        maxHeight={300}
        labelField="value"
        valueField="value"
        placeholder={item.name}
        searchPlaceholder="Search..."
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        value={
          item.name == 'Sektor'
            ? category.Sector
              ? category.Sector
              : 'Sektor'
            : category.SubSector
            ? category.SubSector
            : 'Sub Sektor'
        }
        renderItem={renderItemDropdown}
        onChange={value => {
          setIsFocus(false);
          handleChangeDropdown(
            value,
            item.name === 'Sektor' ? 'Sector' : 'SubSector',
            category,
          );
        }}
        renderRightIcon={() => {
          if (isFocus) {
            return <IconChevUp />;
          } else {
            return (
              <MaterialIcons name="keyboard-arrow-down" color={Colors.GRAY} />
            );
          }
        }}
      />
    </View>
  );
};

export default ListItemStockScreenerDropdown;
