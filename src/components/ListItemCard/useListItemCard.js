import React from 'react';

const useListItemCard = () => {
  const [expanded, setExpanded] = React.useState(false);

  const onItemPress = () => {
    setExpanded(!expanded);
  };
  return {
    expanded,
    onItemPress,
  };
};

export default useListItemCard;
