import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = expanded =>
  StyleSheet.create({
    containerCard: {
      backgroundColor: Colors.WHITE,
      // paddingLeft: 16,
      // paddingRight: 8,
      // marginHorizontal: -20,
      borderColor: '#E8E8E8',
      borderRadius: 12,
      marginBottom: 10,
    },
    containerButton: {
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'space-between',
    },
    textTitle: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansSemiBold600,
      color: expanded ? Colors.PRIMARY : Colors.BLACK,
      maxWidth: '90%',
    },
    divider: {
      width: matrics.screenWidth,
    },
    textDescription: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansRegular400,
      color: Colors.BLACK,
    },
  });
