import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import useListItemCard from './useListItemCard';
import {Card} from '@ui-kitten/components';
import {styling} from './styles';
import Feather from 'react-native-vector-icons/Feather';
import {CollapsableContainer} from '../CollapsableContainer';
import SizedBox from '../../widgets/SizedBox';
import Divider from '../../widgets/Divider';
import {Colors} from '../../utility';

const ListItemCard = ({item}) => {
  const {expanded, onItemPress} = useListItemCard();
  const styles = styling(expanded);
  return (
    <Card style={styles.containerCard}>
      <TouchableOpacity style={styles.containerButton} onPress={onItemPress}>
        <Text style={styles.textTitle}>{item.question}</Text>
        {expanded ? (
          <Feather name="minus" color={Colors.PRIMARY} size={20} />
        ) : (
          <Feather name="plus" size={20} />
        )}
      </TouchableOpacity>
      <CollapsableContainer expanded={expanded}>
        <SizedBox height={10} />
        <Divider styleProps={styles.divider} />
        <SizedBox height={10} />
        <Text style={styles.textDescription}>{item.answer}</Text>
      </CollapsableContainer>
    </Card>
  );
};

export default ListItemCard;
