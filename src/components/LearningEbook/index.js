import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {IconRightCarousel} from '../../assets/svg';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import useLearningBook from './useLearningBook';
import LongCard from '../LongCard';

const LearningEbook = () => {
  const {
    dataDummy,
    indexFlatlist,
    setIndexFlatlist,
    onViewCallBack,
    translate,
  } = useLearningBook();
  const styles = styling(indexFlatlist, dataDummy);
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{translate('Ebook_Capital')}</Text>
        {/* <TouchableOpacity style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>{translate('ViewAll')}</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </TouchableOpacity> */}
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          onViewableItemsChanged={onViewCallBack}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 50,
          }}
          showsHorizontalScrollIndicator={false}
          data={dataDummy}
          horizontal
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => <LongCard item={item} />}
        />
        <View style={styles.containerDot}>
          <View style={styles.stylingLeftSlider} />
          <View style={styles.stylingMidSlider} />
          <View style={styles.stylingRightSlider} />
        </View>
      </View>
    </View>
  );
};

export default LearningEbook;
