import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    gap: 8,
  },
  containerItem: {
    backgroundColor: 'white',
    padding: 8,
    flexBasis: '48%',
    height: 60,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderWidth: 1,
    borderColor: '#E8E8E8',
  },
  text: {
    fontSize: getFontSize(12),
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.BLACK,
  },
  containerIcon: {
    height: 24,
    width: 24,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PINK,
    borderRadius: 43,
  },
});
