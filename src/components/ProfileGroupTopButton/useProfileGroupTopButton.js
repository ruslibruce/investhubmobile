import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Feather from 'react-native-vector-icons/Feather';
import { IconCollection, IconMyCourse, IconNotificationProfile } from '../../assets/svg';
import { COLLECTION, Colors, NOTIFICATION } from '../../utility';
import { DASHBOARD_COURSE, TAB_MY_COURSE } from '../../utility/path';

const useProfileGroupTopButton = () => {
  const {t: translate} = useTranslation();
  const navigation = useNavigation();
  const groupArray = [
    // {
    //   id: 1,
    //   icon: <Feather name={'bookmark'} size={13} color={Colors.PRIMARY} />,
    //   name: translate('Bookmarks'),
    //   url: '',
    // },
    {
      id: 2,
      icon: <IconNotificationProfile width={16} height={16} />,
      name: translate('Notification'),
      url: NOTIFICATION,
    },
    {
      id: 3,
      icon: <IconCollection width={12} height={12} />,
      name: translate('MyCollection'),
      url: COLLECTION,
    },
    {
      id: 4,
      icon: <IconMyCourse width={12} height={12} />,
      name: translate('My_Course'),
      url: TAB_MY_COURSE,
    },
  ];

  const handleNavigation = value => {
    if (value === TAB_MY_COURSE) {
      navigation.navigate(DASHBOARD_COURSE, {screen: value});
      return
    }
    if (value) {
      navigation.navigate(value);
    }
  };

  return {groupArray, handleNavigation};
};

export default useProfileGroupTopButton;
