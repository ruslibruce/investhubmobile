import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {Colors} from '../../utility';
import {styles} from './styles';
import useProfileGroupTopButton from './useProfileGroupTopButton';

const ProfileGroupTopButton = () => {
  const {groupArray, handleNavigation} = useProfileGroupTopButton();
  return (
    <View style={styles.container}>
      {groupArray.map(item => (
        <TouchableOpacity
          key={item.id}
          onPress={() => handleNavigation(item.url)}
          style={[styles.containerItem, item.id === 4 && {flexBasis: '100%'}]}>
          {/* style={styles.containerItem}> */}
          <View style={styles.containerIcon}>{item.icon}</View>
          <Text style={styles.text}>{item.name}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default ProfileGroupTopButton;
