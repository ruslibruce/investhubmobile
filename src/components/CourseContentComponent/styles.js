import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = isSub =>
  StyleSheet.create({
    containerContent: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      alignItems: 'center',
      marginLeft: isSub ? 10 : 0,
    },
    textContentList: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
    },
    textContentTotal: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#A0A5BD',
    },
    containerListCourse: {
      flexDirection: 'row',
      width: '100%',
      alignItems: 'center',
      gap: 12,
    },
    containerIconPlay: {
      padding: 10,
      borderRadius: 11,
      backgroundColor: Colors.PINK,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerListTextTitle: {
      flex: 1,
      gap: 6,
    },
    textListTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
    },
    textSubListTitle: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(12),
      color: '#A0A5BD',
    },
    containerTimeListTitle: {
      alignItems: 'center',
      justifyContent: 'center',
      gap: 12,
    },
    colorDivider: {
      borderColor: '#F3F5FF',
    },
  });
