import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import {COURSE_CONTENT, COURSE_QUIZ, Colors} from '../../utility';
import Divider from '../../widgets/Divider';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import Toast from 'react-native-toast-message';
import {useTranslation} from 'react-i18next';

const CourseContentComponent = ({
  id,
  navigation,
  content,
  isSub,
  authMethod,
}) => {
  const {t: translate} = useTranslation();
  const styles = styling(isSub);
  return (
    <React.Fragment>
      {content?.sections?.map((section, index) => (
        <React.Fragment key={index}>
          <View style={styles.containerContent}>
            <View>
              <Text style={styles.textContentList}>{section.title}</Text>
              {/* <Text style={styles.textContentTotal}>{'2/7 Done'}</Text> */}
            </View>
            {/* <Text style={styles.textContentTotal}>{'35 Mins'}</Text> */}
          </View>
          <SizedBox height={10} />
          <Divider styleProps={styles.colorDivider} />
          {section?.pre_test?.map((post, indexPost) => {
            return (
              <React.Fragment key={indexPost}>
                <SizedBox height={10} />
                <TouchableOpacity
                  activeOpacity={navigation ? 0.5 : 1}
                  disabled={navigation ? false : true}
                  onPress={() => {
                    if (authMethod.isLogin) {
                      if (!post.attempt?.id) {
                        Toast.show({
                          type: 'info',
                          position: 'bottom',
                          text1: 'Quiz not found, please contact administrator',
                        });
                        return;
                      }
                      navigation.navigate(COURSE_QUIZ, {
                        id: post.attempt?.id,
                        name: post.name,
                      });
                    }
                  }}
                  style={styles.containerListCourse}>
                  <View style={styles.containerIconPlay}>
                    <MaterialCommunityIcons
                      name={'notebook-edit'}
                      size={20}
                      color={Colors.PRIMARY}
                    />
                  </View>
                  <View style={styles.containerListTextTitle}>
                    <Text style={styles.textListTitle}>{post.name}</Text>
                  </View>
                  <View style={styles.containerTimeListTitle}>
                    <Text style={styles.textSubListTitle}>{`${
                      post.duration
                    } ${translate('Minutes')}`}</Text>
                    {authMethod.isLogin && (
                      <Text
                        style={[
                          styles.textSubListTitle,
                          {color: Colors.GREEN},
                        ]}>{`${post.attempt?.is_completed ? 100 : 0}%`}</Text>
                    )}
                  </View>
                </TouchableOpacity>
              </React.Fragment>
            );
          })}
          {section?.contents?.map((content, indexContent) => {
            const minutes = Math.floor(content.content_length / 60);
            const seconds = content.content_length % 60;
            return (
              <React.Fragment key={indexContent}>
                <SizedBox height={10} />
                <TouchableOpacity
                  activeOpacity={navigation ? 0.5 : 1}
                  disabled={navigation ? false : true}
                  onPress={() =>
                    navigation.navigate(COURSE_CONTENT, {
                      id: id,
                      content: content,
                    })
                  }
                  style={styles.containerListCourse}>
                  <View style={styles.containerIconPlay}>
                    <MaterialCommunityIcons
                      name={
                        content.content_type.toLowerCase() === 'article'
                          ? 'file-document'
                          : content.content_type.toLowerCase() === 'audio'
                          ? 'volume-medium'
                          : 'play'
                      }
                      size={20}
                      color={Colors.PRIMARY}
                    />
                  </View>
                  <View style={styles.containerListTextTitle}>
                    <Text style={styles.textListTitle}>{content.title}</Text>
                    <Text numberOfLines={2} style={styles.textSubListTitle}>
                      {content.content_file}
                    </Text>
                  </View>
                  <View style={styles.containerTimeListTitle}>
                    <Text style={styles.textSubListTitle}>{`${minutes
                      .toString()
                      .padStart(2, '0')}:${seconds
                      .toString()
                      .padStart(2, '0')}`}</Text>
                    {authMethod.isLogin && (
                      <Text
                        style={[
                          styles.textSubListTitle,
                          {color: Colors.GREEN},
                        ]}>{`${
                        content.progress?.is_completed
                          ? 100
                          : content.progress?.progress
                          ? content.progress?.progress
                          : 0
                      }%`}</Text>
                    )}
                  </View>
                </TouchableOpacity>
              </React.Fragment>
            );
          })}
          <SizedBox height={10} />
          <Divider styleProps={styles.colorDivider} />
          {section?.post_test?.map((post, indexPost) => {
            return (
              <React.Fragment key={indexPost}>
                <SizedBox height={10} />
                <TouchableOpacity
                  activeOpacity={navigation ? 0.5 : 1}
                  disabled={navigation ? false : true}
                  onPress={() => {
                    if (authMethod.isLogin) {
                      if (!post.attempt?.id) {
                        Toast.show({
                          type: 'info',
                          position: 'bottom',
                          text1: 'Quiz not found, please contact administrator',
                        });
                        return;
                      }
                      navigation.navigate(COURSE_QUIZ, {
                        id: post.attempt?.id,
                        name: post.name,
                      });
                    }
                  }}
                  style={styles.containerListCourse}>
                  <View style={styles.containerIconPlay}>
                    <MaterialCommunityIcons
                      name={'notebook-edit'}
                      size={20}
                      color={Colors.PRIMARY}
                    />
                  </View>
                  <View style={styles.containerListTextTitle}>
                    <Text style={styles.textListTitle}>{post.name}</Text>
                  </View>
                  <View style={styles.containerTimeListTitle}>
                    <Text style={styles.textSubListTitle}>{`${
                      post.duration
                    } ${translate('Minutes')}`}</Text>
                    {authMethod.isLogin && (
                      <Text
                        style={[
                          styles.textSubListTitle,
                          {color: Colors.GREEN},
                        ]}>{`${post.attempt?.is_completed ? 100 : 0}%`}</Text>
                    )}
                  </View>
                </TouchableOpacity>
              </React.Fragment>
            );
          })}
          <SizedBox height={10} />
          <CourseContentComponent
            id={id}
            navigation={navigation}
            content={section}
            isSub={true}
            authMethod={authMethod}
          />
        </React.Fragment>
      ))}
      <SizedBox height={10} />
    </React.Fragment>
  );
};

export default CourseContentComponent;
