import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styles = StyleSheet.create({
  containerSignIn: {
    flexDirection: 'row',
    gap: 5,
  },
  textSignInGray: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.LABELINPUT,
  },
  textSignInRed: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
});
