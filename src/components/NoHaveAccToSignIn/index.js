import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {Text, TouchableOpacity, View} from 'react-native';
import {LOGIN} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {styles} from './styles';

const NoHaveAccToSignIn = ({backToLogin}) => {
  const navigation = useNavigation();
  const {t: translate} = useTranslation();
  return (
    <View>
      <SizedBox height={30} />
      <View style={styles.containerSignIn}>
        <Text style={styles.textSignInGray}>
          {backToLogin ? translate('Back_To') : translate('Already_have_acc')}
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate(LOGIN)}>
          <Text style={styles.textSignInRed}>{translate('Login')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default NoHaveAccToSignIn;
