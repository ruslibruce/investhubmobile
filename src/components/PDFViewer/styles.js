import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
  backgroundPDF: {
    width: '100%',
    height: 300,
    flex: 1,
  },
  contentError: {
    position: 'absolute',
    top: 50,
    left: 0,
    height: 300,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textError: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(18),
    color: Colors.WHITE,
    textAlign: 'center',
  },
  containerPlay: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerProgressTIme: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: 5,
    alignItems: 'center',
  },
  groupButtonNext: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  containerButton: {
    flexDirection: 'row',
    gap: 7,
    alignItems: 'center',
  },
  textTime: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.WHITE,
  },
  progressColor: {
    backgroundColor: '#D9D9D9',
    width: '80%',
  },
  alignBottomControl: {
    // position: 'relative',
    // top: 40,
  },
});
