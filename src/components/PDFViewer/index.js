import {useNavigation} from '@react-navigation/native';
import React, {useCallback} from 'react';
import {BackHandler, Text, TouchableWithoutFeedback, View} from 'react-native';
import Pdf from 'react-native-pdf';
import usePostCourseDetailProgress from '../../api/usePostCourseDetailProgress';
import {styles} from './styles';
import {useTranslation} from 'react-i18next';
import { useSelector } from 'react-redux';

const PDFViewer = ({
  url,
  styleVideo,
  content,
  idProgress,
  idCourse,
  accessCourseDetail,
  isNavigateBack,
}) => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const PDFRef = React.useRef(null);
  const [totalPage, setTotalPage] = React.useState(0);
  const [currentPage, setCurrentPage] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(false);
  const [showError, setShowError] = React.useState(false);
  const {accessPostProgress} = usePostCourseDetailProgress();
  const navigation = useNavigation();

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      // do something
      handleAccessProgress(currentPage);
    });

    return unsubscribe;
  }, [navigation, currentPage]);

  React.useEffect(() => {
    const backAction = () => {
      handleAccessProgress(currentPage);
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return () => backHandler.remove();
  }, [currentPage]);

  React.useEffect(() => {
    if (isNavigateBack) {
      handleAccessProgress(currentPage);
    }
  }, [isNavigateBack, currentPage]);

  const handleAccessProgress = useCallback(
    currentPage => {
      if (!authMethod.isLogin) {
        return;
      }
      if (!idProgress && !content) {
        return;
      }
      accessPostProgress({
        participant_id: idProgress,
        content_id: content?.id,
        position: currentPage,
      });
      accessCourseDetail(idCourse, false);
    },
    [
      content,
      idCourse,
      idProgress,
      accessCourseDetail,
      accessPostProgress,
      authMethod,
    ],
  );

  return (
    <React.Fragment>
      <View style={styles.container}>
        <Pdf
          // Can be a URL or a local file.
          source={{uri: url}}
          // Store reference
          ref={PDFRef}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`Number of pages: ${numberOfPages}`);
            setShowError(false);
            setTotalPage(numberOfPages);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`Current page: ${page}`);
            setCurrentPage(page);
          }}
          onError={error => {
            console.log('PDF error', JSON.stringify(error));
            setShowError(true);
          }}
          onPressLink={uri => {
            console.log(`Link pressed: ${uri}`);
          }}
          style={[styles.backgroundPDF, styleVideo]}
        />
      </View>
      {showError && (
        <View style={styles.contentError}>
          <Text style={styles.textError}>{translate('PDFLoadError')}</Text>
        </View>
      )}
    </React.Fragment>
  );
};

export default PDFViewer;
