import React from 'react';
import { FlatList, Image, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { IconAvatarHome, IconChevRight, IconRightCarousel } from '../../assets/svg';
import { Colors } from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import { styles } from './styles';
import useTopEvent from './useTopEvent';

const TopEvent = ({title}) => {
  const {dataDummy} = useTopEvent();
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{title}</Text>
        <View style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>See All</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </View>
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal
          data={dataDummy}
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <TouchableOpacity style={styles.containerItem}>
              <View style={styles.containerTopCard}>
                {title !== 'Top Event' && (
                  <View style={styles.containerAvatar}>
                    {/* <Image style={styles.widthImage} source={{uri: }} /> */}
                    <IconAvatarHome />
                  </View>
                )}
                <Text style={styles.textName}>
                  {title !== 'Top Event' ? item.name : 'Jakarta'}
                </Text>
                <View style={styles.dotStyling} />
                <Text style={styles.textDate}>{item.date}</Text>
                {title !== 'Top Event' && (
                  <>
                    <View style={styles.dotStyling} />
                    <View style={styles.containerShared}>
                      <Ionicons
                        size={15}
                        color={Colors.GRAYTEXT}
                        name="share-social-outline"
                      />
                      <Text style={styles.textDate}>{item.shared}</Text>
                    </View>
                  </>
                )}
              </View>
              <SizedBox height={16} />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={styles.containerImage}>
                  <Image style={styles.widthImage} source={item.image} />
                </View>
                <SizedBox width={8} />
                <View style={styles.containerLengthText}>
                  <Text numberOfLines={2} style={styles.textTitle}>
                    {item.title}
                  </Text>
                </View>
              </View>
              <SizedBox height={16} />
              <Text numberOfLines={3} style={styles.textDesc}>
                {item.description}
              </Text>
              <SizedBox height={10} />
              <View style={styles.containerBottomCard}>
                {/* <View style={styles.positionIconText}>
                    <Icon
                      name="heart"
                      style={styles.iconHeart}
                      fill={'#FF3D00'}
                    />
                    <SizedBox width={5} />
                    <Text style={styles.textIcon}>
                      {item.likes}
                      {' likes'}
                    </Text>
                    <SizedBox width={10} />
                    <View style={styles.dotStyling} />
                    <SizedBox width={10} />
                    <IconComment />
                    <SizedBox width={4} />
                    <Text
                      style={
                        styles.textIcon
                      }>{`${item.comments} comments`}</Text>
                  </View> */}
                <View />
                <View style={styles.positionIconText}>
                  <Text style={styles.textSeeDetail}>{'See Detail'}</Text>
                  <SizedBox width={5} />
                  <IconChevRight />
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
};

export default TopEvent;
