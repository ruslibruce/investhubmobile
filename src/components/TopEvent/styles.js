import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  containerLabel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textLabel: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
  },
  containerSeeAll: {
    flexDirection: 'row',
  },
  textSeeAll: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
  containerFlatlist: {
    width: matrics.screenWidth,
    flex: 1,
  },
  contentFlatlist: {
    gap: 10,
  },
  containerItem: {
    padding: 16,
    borderRadius: 16,
    backgroundColor: Colors.WHITE2,
    maxWidth: 330,
  },
  containerTopCard: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    gap: 8,
  },
  containerAvatar: {
    height: 24,
    width: 24,
    borderRadius: 50,
    overflow: 'hidden',
  },
  widthImage: {
    height: '100%',
    width: '100%',
  },
  textName: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
  },
  dotStyling: {
    backgroundColor: Colors.GRAY,
    width: 4,
    height: 4,
    borderRadius: 4,
  },
  textDate: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.GRAY4,
  },
  containerShared: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerImage: {
    height: 48,
    width: 48,
    borderRadius: 16,
    overflow: 'hidden',
  },
  containerLengthText: {
    maxWidth: '80%',
  },
  textTitle: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
  },
  textDesc: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.GRAY4,
  },
  containerBottomCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  positionIconText: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconHeart: {
    width: 25,
    height: 25,
  },
  textIcon: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(10),
    color: Colors.GRAY4,
  },
  textSeeDetail: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.PRIMARY,
  },
});
