import { View, Text } from 'react-native'
import React from 'react'
import { imageDummyCard } from '../../assets/images';

const dataDummy = [
    {
      id: 1,
      name: 'Rusli Wanasuria',
      date: '22 Oct 2023 09:12',
      title: 'Class adds $30 million to its balance sheet for a Zoom ...',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      likes: '12 K',
      comments: '823',
      shared: '1k',
    },
    {
      id: 2,
      name: 'Albert Flores',
      date: '22 Oct 2023 09:12',
      title: 'Class adds $30 million to its balance sheet for a Zoom ...',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      likes: '12 K',
      comments: '823',
      shared: '1k',
    },
    {
      id: 3,
      name: 'Albert Flores',
      date: '22 Oct 2023 09:12',
      title: 'Class adds $30 million to its balance sheet for a Zoom ...',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      likes: '12 K',
      comments: '823',
      shared: '1k',
    },
    {
      id: 4,
      name: 'Albert Flores',
      date: '22 Oct 2023 09:12',
      title: 'Class adds $30 million to its balance sheet for a Zoom ...',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      likes: '12 K',
      comments: '823',
      shared: '1k',
    },
    {
      id: 5,
      name: 'Albert Flores',
      date: '22 Oct 2023 09:12',
      title: 'Class adds $30 million to its balance sheet for a Zoom ...',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      likes: '12 K',
      comments: '823',
      shared: '1k',
    },
    {
      id: 6,
      name: 'Albert Flores',
      date: '22 Oct 2023 09:12',
      title: 'Class adds $30 million to its balance sheet for a Zoom ...',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      likes: '12 K',
      comments: '823',
      shared: '1k',
    },
  ];

const useTopEvent = () => {
  return {dataDummy}
}

export default useTopEvent