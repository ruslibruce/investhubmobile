import {View, Text} from 'react-native';
import React from 'react';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import {useCameraPermission} from 'react-native-vision-camera';
import {magicModal} from 'react-native-magic-modal';
import {useTranslation} from 'react-i18next';
import {openSettings} from 'react-native-permissions';
import {styles} from './styles';
import {Colors} from '../../utility';
import Ionicons from 'react-native-vector-icons/Ionicons';
const PermissionsPage = ({title, isCameraPermission, icon, handleAction}) => {
  const {requestPermission} = useCameraPermission();
  const {t: translate} = useTranslation();

  const hideModalSetting = () => {
    magicModal.hide();
  };

  const showModalSetting = () => {
    magicModal.show(() => (
      <View
        style={{
          backgroundColor: 'white',
          padding: 20,
          borderRadius: 10,
          width: '80%',
          alignSelf: 'center',
          alignItems: 'center',
        }}>
        <Ionicons size={80} name="settings" color={Colors.PRIMARY} />
        <Text style={[styles.textPermission, {marginTop: 20}]}>
          {translate('PermissionSetting')}
        </Text>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginTop: 20,
            width: '100%',
          }}>
          <ButtonPrimary
            style={{
              width: '45%',
            }}
            label={translate('Cancel')}
            onPress={hideModalSetting}
          />
          <View style={{flex: 1}} />
          <ButtonPrimary
            style={{
              width: '45%',
            }}
            label={translate('Yes')}
            onPress={() => {
              openSettings();
              hideModalSetting();
            }}
          />
        </View>
      </View>
    ));
  };

  const requestCameraPermission = async () => {
    try {
      const permission = await requestPermission();
      if (permission) {
        hideModalSetting();
        handleAction();
      } else {
        console.log('Camera permission denied');
        magicModal.hide();
        showModalSetting();
      }
    } catch (err) {
      console.warn(err);
    }
  };

  return (
    <View style={styles.container}>
      {icon}
      <Text style={styles.textPermission}>{title}</Text>
      <ButtonPrimary
        onPress={isCameraPermission ? requestCameraPermission : null}
        label={translate('Next_Capital')}
        style={styles.buttonContainer}
      />
    </View>
  );
};

export default PermissionsPage;
