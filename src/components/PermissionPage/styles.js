import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    padding: 10,
    width: matrics.screenWidth - 100,
    alignItems: 'center',
  },
  textPermission: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(16),
    color: Colors.BLACK,
    textAlign: 'justify',
  },
  buttonContainer: {
    width: "100%",
    marginTop: 20,
  },
});
