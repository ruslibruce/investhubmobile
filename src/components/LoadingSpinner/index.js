import React from 'react';
import {Image, View} from 'react-native';
import iconLoader from '../../assets/images/gif/graph.gif';
import {matrics} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import {styles} from './styles';

const LoadingSpinner = ({isFullScreen = false}) => {
  return (
    <View
      style={
        isFullScreen ? styles.containerNoDataFullScreen : styles.containerNoData
      }>
      {isFullScreen && <SizedBox height={matrics.height / 4} />}
      <Image
        source={iconLoader}
        style={isFullScreen ? styles.iconLoaderFullScreen : styles.iconLoader}
      />
      {isFullScreen && <SizedBox height={matrics.height / 2} />}
    </View>
  );
};

export default LoadingSpinner;
