import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  containerNoData: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
  },
  containerNoDataFullScreen: {
    width: matrics.screenWidth,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
  },
  textNoData: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(16),
    color: Colors.PRIMARY,
    marginRight: 50,
  },
  iconLoader: {
    width: 30,
    height: 30,
  },
  iconLoaderFullScreen: {
    width: 100,
    height: 100,
  },
});
