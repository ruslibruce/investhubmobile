import {StyleSheet} from 'react-native';
import { Colors, FontType, getFontSize } from '../../utility';

export const styles = StyleSheet.create({
  progressBar: {
    backgroundColor: Colors.WHITE,
    width: '100%',
  },
  progressColor: {
    backgroundColor: '#D9D9D9',
  },
  textProgress: {
    fontFamily: FontType.openSansBold700,
    fontSize: getFontSize(8),
    color: Colors.WHITE,
  },
});
