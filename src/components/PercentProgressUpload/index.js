import {View, Text} from 'react-native';
import React from 'react';
import {styles} from './styles';
import {ProgressBar} from '@ui-kitten/components';

const PercentProgressUpload = ({percentage}) => {
  return (
    <View style={styles.progressBar}>
      <ProgressBar
        status={'success'}
        style={styles.progressColor}
        progress={percentage / 100}
        size="giant"
      />
      <View
        style={{
          position: 'absolute',
          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={styles.textProgress}>{`${percentage} %`}</Text>
      </View>
    </View>
  );
};

export default PercentProgressUpload;
