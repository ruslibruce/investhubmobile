import moment from 'moment';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Pressable, Text, View } from 'react-native';
import Toast from 'react-native-toast-message';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import usePostComment from '../../api/usePostComment';
import { Colors } from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import TextInputWithLabel from '../../widgets/TextInputWithLabel';
import LoadingSpinner from '../LoadingSpinner';
import { styles } from './styles';

const ReplyCommentComponent = ({
  item,
  isModalVisibleComment,
  handleGetComment,
  dataCourseDetail,
  type,
  isLoadingGetComment,
  isReplyComponent,
}) => {
  const {t: translate} = useTranslation();
  const [formCommentReply, setFormCommentReply] = React.useState({
    comment: '',
  });
  const {accessPostComment, isSuccess} = usePostComment();
  const [isReplyComment, setIsReplyComment] = React.useState(false);

  React.useEffect(() => {
    if (isSuccess) {
      setFormCommentReply({
        ...formCommentReply,
        comment: '',
      });
      setIsReplyComment(false);
      handleGetComment(dataCourseDetail?.id, type);
    }

    if (isModalVisibleComment) {
      setIsReplyComment(false);
    }
  }, [isSuccess, isModalVisibleComment]);

  const onChangeText = value => {
    setFormCommentReply({
      ...formCommentReply,
      comment: value,
    });
  };

  React.useEffect(() => {
    if (isReplyComment) {
      setFormCommentReply({
        parent_id: item?.id,
        parent_type: 'comment',
        comment: '',
      });
    }
  }, [isReplyComment]);

  const handlePostComment = formCommentReply => {
    console.log(formCommentReply);
    if (!formCommentReply.comment) {
      Toast.show({
        type: 'info',
        position: 'top',
        text1: translate('CommentEmpty'),
      });
      return;
    }
    accessPostComment(formCommentReply);
  };

  return (
    <>
      <View
        style={{
          backgroundColor: Colors.WHITE,
          borderRadius: 8,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.29,
          shadowRadius: 4.65,
          elevation: 7,
          padding: 10,
          marginLeft: isReplyComponent ? 15 : 0,
        }}>
        {isLoadingGetComment && (
          <LoadingSpinner />
        )}
        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <Text style={styles.textTitleComment}>{item.name}</Text>
          <Text style={styles.textTime}>
            {moment(item.created_at).fromNow()}
          </Text>
        </View>
        <SizedBox height={5} />
        <Text style={styles.textComment}>{item.comment}</Text>
      </View>
      <SizedBox height={10} />
      <Pressable
        style={{
          marginLeft: isReplyComponent ? 15 : 0,
        }}
        onPress={() => setIsReplyComment(true)}>
        <Text style={styles.textReply}>{translate('Reply')}</Text>
      </Pressable>
      <SizedBox height={10} />
      {item.replies?.length > 0 &&
        item.replies?.map(val => (
          <ReplyCommentComponent
            key={val.id}
            dataCourseDetail={dataCourseDetail}
            handleGetComment={handleGetComment}
            isModalVisibleComment={isModalVisibleComment}
            item={val}
            type={type}
            isLoadingGetComment={isLoadingGetComment}
            isReplyComponent={true}
          />
        ))}
      {isReplyComment && (
        <View>
          <TextInputWithLabel
            placeholder={translate('EnterComment')}
            multiline
            name={'comment'}
            onChangeText={onChangeText}
            containerInputText={{height: 100}}
            styleInput={{padding: 30}}
            value={formCommentReply.comment}
          />
          <SizedBox height={10} />
          <ButtonPrimary
            icon={
              <MaterialIcons
                color={Colors.WHITE}
                size={20}
                name="insert-comment"
              />
            }
            label={translate('Comment')}
            onPress={() => {
              handlePostComment(formCommentReply);
            }}
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              gap: 10,
              backgroundColor:
                formCommentReply.comment === '' ? Colors.GRAY : Colors.PRIMARY,
            }}
            disabled={formCommentReply.comment === ''}
          />
          <SizedBox height={20} />
        </View>
      )}
    </>
  );
};

export default ReplyCommentComponent;
