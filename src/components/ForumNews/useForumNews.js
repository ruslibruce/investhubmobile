import React from 'react';
import {imageDummyCard} from '../../assets/images';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';

const useForumNews = () => {
  const dataDummy = [
    {
      id: 1,
      name: 'Rusli Wanasuria',
      section: '3',
      totalCourse: '28',
      seeing: 5,
      totalHours: '3 hours, 20 min',
      date: '22 Oct 2023 09:12',
      title: 'Get rich quick by investing in shares',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      level: 'Beginner',
      owner: 'IDX Stock',
      likes: '12 K',
      comments: '823',
      shared: '1k',
      forum: 'Investing Forum',
    },
    {
      id: 2,
      name: 'Albert Flores',
      section: '3',
      totalCourse: '28',
      seeing: 5,
      totalHours: '3 hours, 20 min',
      date: '22 Oct 2023 09:12',
      title: 'Get rich quick by investing in shares',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      level: 'Beginner',
      owner: 'IDX Stock',
      likes: '12 K',
      comments: '823',
      shared: '1k',
      forum: 'Investing Forum',
    },
    {
      id: 3,
      name: 'Albert Flores',
      section: '3',
      totalCourse: '28',
      seeing: 5,
      totalHours: '3 hours, 20 min',
      date: '22 Oct 2023 09:12',
      title: 'Get rich quick by investing in shares',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      level: 'Beginner',
      owner: 'IDX Stock',
      likes: '12 K',
      comments: '823',
      shared: '1k',
      forum: 'Investing Forum',
    },
    {
      id: 4,
      name: 'Albert Flores',
      section: '3',
      totalCourse: '28',
      seeing: 5,
      totalHours: '3 hours, 20 min',
      date: '22 Oct 2023 09:12',
      title: 'Get rich quick by investing in shares',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      level: 'Beginner',
      owner: 'IDX Stock',
      likes: '12 K',
      comments: '823',
      shared: '1k',
      forum: 'Investing Forum',
    },
    {
      id: 5,
      name: 'Albert Flores',
      section: '3',
      totalCourse: '28',
      seeing: 5,
      totalHours: '3 hours, 20 min',
      date: '22 Oct 2023 09:12',
      title: 'Get rich quick by investing in shares',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      level: 'Beginner',
      owner: 'IDX Stock',
      likes: '12 K',
      comments: '823',
      shared: '1k',
      forum: 'Investing Forum',
    },
    {
      id: 6,
      name: 'Albert Flores',
      section: '3',
      totalCourse: '28',
      seeing: 5,
      totalHours: '3 hours, 20 min',
      date: '22 Oct 2023 09:12',
      title: 'Get rich quick by investing in shares',
      description:
        'Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...',
      image: imageDummyCard,
      level: 'Beginner',
      owner: 'IDX Stock',
      likes: '12 K',
      comments: '823',
      shared: '1k',
      forum: 'Investing Forum',
    },
  ];
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const [indexFlatlist, setIndexFlatlist] = React.useState(0);

  const onViewCallBack = React.useCallback(event => {
    setIndexFlatlist(event.viewableItems[0].index);
    // Use viewable items in state or as intended
  }, []);

  return {
    dataDummy,
    indexFlatlist,
    onViewCallBack,
    translate,
    authMethod,
  };
};

export default useForumNews;
