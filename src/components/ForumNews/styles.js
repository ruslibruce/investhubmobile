import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = (indexFlatlist, array) =>
  StyleSheet.create({
    container: {
      backgroundColor: Colors.WHITE,
      width: matrics.screenWidth,
      marginLeft: -20,
    },
    containerLabel: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 20,
    },
    textLabel: {
      fontFamily: FontType.openSansSemiBold600,
      fontSize: getFontSize(14),
      color: Colors.BLACK,
    },
    containerSeeAll: {
      flexDirection: 'row',
    },
    textSeeAll: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(14),
      color: Colors.PRIMARY,
    },
    containerFlatlist: {
      width: matrics.screenWidth,
      flex: 1,
    },
    contentFlatlist: {
      gap: 10,
      backgroundColor: Colors.WHITE,
      paddingVertical: 20,
      paddingRight: 30,
      paddingLeft: 20,
    },
    ContainerItemFlatlist: {
      borderRadius: 16,
      overflow: 'hidden',
      paddingVertical: 16,
      paddingHorizontal: 18,
      maxWidth: 335,
      height: 153,
      backgroundColor: Colors.WHITE,
    },
    containerTopCard: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 12,
    },
    containerImage: {
      height: 100,
      width: 100,
      borderTopLeftRadius: 16,
      borderBottomLeftRadius: 16,
      overflow: 'hidden',
    },
    imageStyle: {
      height: '100%',
      width: '100%',
    },
    widthText: {
      maxWidth: 175,
    },
    textForum: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: '#777795',
    },
    textTitle: {
      fontFamily: FontType.openSansExtraBold800,
      fontSize: getFontSize(14),
      color: Colors.PRIMARY,
    },
    textDesc: {
      fontFamily: FontType.openSansRegular400,
      fontSize: getFontSize(10),
      color: '#4D4D4D',
    },
    containerDotCarousel: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      gap: 4,
    },
    stylingLeftSlider: {
      backgroundColor: indexFlatlist === 0 ? Colors.PRIMARY : Colors.GRAY,
      width: indexFlatlist === 0 ? 16 : 8,
      height: 8,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    stylingMidSlider: {
      backgroundColor:
        indexFlatlist !== 0 && indexFlatlist !== array.length - 2
          ? Colors.PRIMARY
          : Colors.GRAY,
      width: indexFlatlist !== 0 && indexFlatlist !== array.length - 2 ? 16 : 8,
      height: 8,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    stylingRightSlider: {
      backgroundColor:
        indexFlatlist === array.length - 2 ? Colors.PRIMARY : Colors.GRAY,
      width: indexFlatlist === array.length - 2 ? 16 : 8,
      height: 8,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
