import Slider from '@react-native-community/slider';
import {
  useFocusEffect,
  useNavigation,
  useNavigationState,
} from '@react-navigation/native';
import React, {useCallback} from 'react';
import {useTranslation} from 'react-i18next';
import {
  BackHandler,
  Pressable,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import YoutubePlayer from 'react-native-youtube-iframe';
import {useSelector} from 'react-redux';
import usePostCourseDetailProgress from '../../api/usePostCourseDetailProgress';
import {Colors, COURSE_CONTENT} from '../../utility';
import SizedBox from '../../widgets/SizedBox';
import LoadingSpinner from '../LoadingSpinner';
import {styles} from './styles';

const VideoPlayerYoutube = ({
  styleVideo,
  content,
  idProgress,
  idCourse,
  accessCourseDetail,
  isNavigateBack,
}) => {
  const authMethod = useSelector(state => state.auth);
  const {t: translate} = useTranslation();
  const videoRef = React.useRef(null);
  const [pause, setPaused] = React.useState(false);
  const [duration, setDuration] = React.useState(0);
  const [currentTime, setCurrentTime] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(false);
  const [showControl, setShowControl] = React.useState(true);
  const [showError, setShowError] = React.useState(false);
  const [isVideoEnd, setIsVideoEnd] = React.useState(false);
  const {accessPostProgress} = usePostCourseDetailProgress();
  const state = useNavigationState(state => state);
  const navigation = useNavigation();
  const urlObj = new URL(content?.content_file);
  const videoId = urlObj.searchParams.get('v');
  const [isMuted, setIsMuted] = React.useState(false);
  const [playbackRate, setPlaybackRate] = React.useState(1);
  const [playbackText, setPlaybackText] = React.useState('1x');

  const actions = [
    {key: 1, text: '1x'},
    {key: 1.5, text: '1.5x'},
    {key: 2, text: '2x'},
  ];

  React.useEffect(() => {
    const interval = setInterval(async () => {
      const elapsed_sec = await videoRef.current.getCurrentTime(); // this is a promise. dont forget to await

      // calculations
      setCurrentTime(elapsed_sec);
    }, 1000); // 1000 ms refresh. increase it if you don't require millisecond precision

    return () => {
      clearInterval(interval);
    };
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      if (state.routes[state.index].name === COURSE_CONTENT) {
        setPaused(false);
      }
    }, [state]),
  );

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      // do something
      setPaused(true);
    });

    return unsubscribe;
  }, [navigation]);

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      handleAccessProgress(currentTime);
    }, 60000); // 60000 milliseconds = 1 minute

    // Cleanup interval on component unmount
    return () => clearInterval(intervalId);
  }, [currentTime]); // Empty dependency array ensures this runs once when the component mounts

  React.useEffect(() => {
    const backAction = () => {
      if (content && content?.progress) {
        handleAccessProgress(currentTime);
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return () => backHandler.remove();
  }, [currentTime]);

  React.useEffect(() => {
    if (isNavigateBack) {
      handleAccessProgress(currentTime);
    }
  }, [isNavigateBack, currentTime]);

  const handleAccessProgress = useCallback(
    currentTime => {
      if (!authMethod.isLogin) {
        return;
      }
      if (!idProgress && !content) {
        return;
      }
      accessPostProgress({
        participant_id: idProgress,
        content_id: content?.id,
        position: Math.ceil(currentTime),
      });
      accessCourseDetail(idCourse, false);
    },
    [
      idCourse,
      idProgress,
      content,
      accessCourseDetail,
      accessPostProgress,
      authMethod,
    ],
  );

  const onReady = () => {
    videoRef.current?.getCurrentTime().then(currentTime => {
      console.log('currentTime', currentTime);
      setCurrentTime(currentTime);
    });

    videoRef.current?.getDuration().then(getDuration => {
      console.log('duration', duration);
      setDuration(getDuration);
    });

    setShowError(false);
    setIsLoading(false);
    // setDuration(data.duration);
    if (content && content?.progress?.position > 0) {
      videoRef.current.seekTo(content?.progress?.position, true);
    }
  };

  const skipBackward = () => {
    videoRef.current?.getCurrentTime().then(currentTime => {
      videoRef.current.seekTo(currentTime - 10, true);
      setCurrentTime(currentTime - 10);
    });
  };

  const skipForward = () => {
    if (content && content?.progress?.is_completed) {
      videoRef.current?.getCurrentTime().then(currentTime => {
        videoRef.current.seekTo(currentTime + 10, true);
        setCurrentTime(currentTime + 10);
      });
    }
  };

  const getMinutesFromSeconds = time => {
    const minutes = time >= 60 ? Math.ceil(time / 60) : 0;
    const seconds = Math.ceil(time - minutes * 60);

    return `${minutes >= 10 ? minutes : '0' + minutes} : ${
      seconds >= 10 ? seconds : '0' + seconds
    }`;
  };

  const position = getMinutesFromSeconds(currentTime);
  const fullDuration = getMinutesFromSeconds(duration);

  const replayVideo = () => {
    videoRef.current?.getCurrentTime().then(currentTime => {
      videoRef.current.seekTo((currentTime = 0), true);
    });
    videoRef.current?.getDuration().then(getDuration => {
      console.log('duration', duration);
      setDuration(getDuration);
    });
    setCurrentTime(0);
    setPaused(true);
    setIsVideoEnd(false);
  };

  const getPercentage = (currentTime, duration) => {
    if (duration > 0) {
      return (currentTime / duration) * 100;
    }
    return 0; // Return 0% if duration is not positive
  };

  const onSeek = value => {
    if (duration) {
      const targetTime = (value / 100) * duration;
      if (content && content?.progress?.is_completed) {
        videoRef.current.seekTo(targetTime, true);
        setCurrentTime(targetTime);
        return;
      }
      if (currentTime > targetTime) {
        videoRef.current.seekTo(targetTime, true);
        setCurrentTime(targetTime);
      }
    }
  };

  const onStateChange = useCallback(state => {
    console.log('state', state);
    if (state === 'buffering') {
      setIsLoading(true);
    }

    if (state === 'paused') {
      setIsLoading(false);
    }

    if (state === 'playing') {
      setIsLoading(false);
    }

    if (state === 'ended') {
      videoRef.current?.getCurrentTime().then(currentTime => {
        console.log('currentTime', currentTime);
        setCurrentTime(currentTime);
      });
      setIsVideoEnd(true);
      setPaused(true);
      handleAccessProgress();
    }
  }, []);

  const handleShowModalPlayback = () => {
    magicModal.show(() => (
      <View style={styles.containerModal}>
        <Text style={styles.textTitlePlayback}>Playback Rate</Text>
        <View style={{flexDirection: 'row', gap: 5, marginTop: 10}}>
          {actions.map(item => (
            <Pressable
              style={styles.buttonPlayback}
              onPress={() => {
                setPlaybackRate(item.key);
                setPlaybackText(item.text);
                magicModal.hide();
              }}>
              <Text style={styles.textPlayback}>{item.text}</Text>
            </Pressable>
          ))}
        </View>
      </View>
    ));
  };

  return (
    <TouchableWithoutFeedback onPress={() => setShowControl(!showControl)}>
      <View>
        <View pointerEvents="none">
          <YoutubePlayer
            // Can be a URL or a local file.
            // videoId={'LYabT4zZnJQ'}
            videoId={videoId}
            // Store reference
            ref={videoRef}
            // Callback when video cannot be loaded
            onError={err => {
              setShowError(true);
            }}
            // style={[styles.backgroundVideo, styleVideo]}
            play={!pause}
            onChangeState={onStateChange}
            onReady={onReady}
            width={'100%'}
            height={180}
            mute={isMuted}
            playbackRate={playbackRate}
            // textTracks={[
            //   {
            //     title: "English CC",
            //     language: "en",
            //     type: TextTrackType.VTT, // "text/vtt"
            //     uri: "https://bitdash-a.akamaihd.net/content/sintel/subtitles/subtitles_en.vtt"
            //   },
            //   {
            //     title: "Spanish Subtitles",
            //     language: "es",
            //     type: TextTrackType.SRT, // "application/x-subrip"
            //     uri: "https://durian.blender.org/wp-content/content/subtitles/sintel_es.srt"
            //   }
            // ]}
          />
        </View>
        {showError && (
          <View style={styles.contentError}>
            <Text style={styles.textError}>
              {content.content_type.toLowerCase() === 'video'
                ? translate('VideoLoadError')
                : translate('AudioLoadError')}
            </Text>
          </View>
        )}
        {showControl && !showError && (
          <View style={styles.contentOverlay}>
            <SizedBox height={'auto'} />
            <View style={styles.containerPlay}>
              {/* <TouchableOpacity onPress={skipBackward}>
                <MaterialIcons
                  name="replay-10"
                  size={24}
                  color={Colors.WHITE}
                />
              </TouchableOpacity> */}
              {isLoading ? (
                <LoadingSpinner />
              ) : (
                <>
                  {isVideoEnd ? (
                    <TouchableOpacity onPress={replayVideo}>
                      <MaterialIcons
                        name="replay"
                        size={30}
                        color={Colors.WHITE}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity onPress={() => setPaused(!pause)}>
                      {!pause ? (
                        <>
                          {isVideoEnd ? (
                            <MaterialIcons
                              name="replay"
                              size={30}
                              color={Colors.WHITE}
                            />
                          ) : (
                            <MaterialIcons
                              name="pause"
                              size={30}
                              color={Colors.WHITE}
                            />
                          )}
                        </>
                      ) : (
                        <Entypo
                          name="controller-play"
                          size={30}
                          color={Colors.WHITE}
                        />
                      )}
                    </TouchableOpacity>
                  )}
                </>
              )}
              {/* <TouchableOpacity onPress={skipForward}>
                <MaterialIcons
                  name="forward-10"
                  size={24}
                  color={Colors.WHITE}
                />
              </TouchableOpacity> */}
            </View>
            <View style={styles.alignBottomControl}>
              <View style={styles.containerProgressSlider}>
                <Slider
                  style={{width: '100%', height: 10}}
                  minimumValue={0}
                  maximumValue={100}
                  minimumTrackTintColor={Colors.GREEN}
                  maximumTrackTintColor={Colors.GRAY}
                  value={getPercentage(currentTime, duration)}
                  onValueChange={onSeek}
                  thumbTintColor={'transparent'}
                  step={1}
                />
              </View>
              <View style={styles.containerProgressTime}>
                <Text
                  style={
                    styles.textTime
                  }>{`${position} / ${fullDuration}`}</Text>
                {isMuted ? (
                  <Ionicons
                    name="volume-mute"
                    size={24}
                    color={Colors.WHITE}
                    onPress={() => setIsMuted(false)}
                  />
                ) : (
                  <Ionicons
                    name="volume-high"
                    size={24}
                    color={Colors.WHITE}
                    onPress={() => setIsMuted(true)}
                  />
                )}
                <TouchableOpacity
                  onPress={handleShowModalPlayback}
                  style={{
                    alignSelf: 'flex-start',
                    backgroundColor: Colors.GRAY2,
                    padding: 5,
                    borderRadius: 5,
                  }}>
                  <Text style={styles.textNext}>{playbackText}</Text>
                </TouchableOpacity>
              </View>
              <SizedBox height={10} />
            </View>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VideoPlayerYoutube;
