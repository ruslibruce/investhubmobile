import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  backgroundVideo: {
    width: 'auto',
    height: 180,
  },
  contentOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
  },
  containerPlay: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerProgressSlider: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerProgressTime: {
    flexDirection: 'row',
    gap: 10,
    alignItems: 'center',
  },
  groupButtonNext: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  containerButton: {
    flexDirection: 'row',
    gap: 7,
    alignItems: 'center',
  },
  textNext: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(10),
    color: Colors.WHITE,
  },
  textTime: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.WHITE,
  },
  progressColor: {
    backgroundColor: '#D9D9D9',
    width: '80%',
  },
  alignBottomControl: {
    // position: 'relative',
    // top: 40,
  },
  contentError: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 180,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textError: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(18),
    color: Colors.WHITE,
    textAlign: 'center',
  },
  containerModal:{
    backgroundColor: Colors.WHITE,
    borderRadius: 20,
    padding: 20,
    width: '40%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  textTitlePlayback:{
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(16),
    color: Colors.BLACK,
  },
  textPlayback:{
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(16),
    color: Colors.WHITE,
  },
  buttonPlayback:{
    backgroundColor: Colors.PRIMARY,
    padding: 10,
    borderRadius: 10,
  },
});
