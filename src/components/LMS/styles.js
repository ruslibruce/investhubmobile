import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  containerLabel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textLabel: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(14),
    color: Colors.BLACK,
  },
  containerSeeAll: {
    flexDirection: 'row',
  },
  textSeeAll: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(14),
    color: Colors.PRIMARY,
  },
  containerFlatlist: {
    width: matrics.screenWidth,
    flex: 1,
    marginLeft: -20,
  },
  contentFlatlist: {
    gap: 10,
    backgroundColor: Colors.WHITE,
    paddingBottom: 20,
    paddingRight: 30,
    paddingLeft: 20,
  },
});
