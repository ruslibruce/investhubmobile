import {useNavigation} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import useGetCourseRecomendation from '../../api/useGetCourseRecomendation';
import {DASHBOARD_COURSE} from '../../utility';

const useLMS = () => {
  const {t: translate} = useTranslation();
  const authMethod = useSelector(state => state.auth);
  const {
    dataCourse,
    isLoading,
    isLoadingPage,
    params,
    setParams,
    setIsLoadingPage,
    totalPage,
  } = useGetCourseRecomendation();
  const navigation = useNavigation();

  const handleNavigateCourse = () => {
    navigation.navigate(DASHBOARD_COURSE);
  };

  return {
    translate,
    authMethod,
    dataCourse,
    isLoading,
    handleNavigateCourse,
    isLoadingPage,
    params,
    setParams,
    setIsLoadingPage,
    totalPage,
  };
};

export default useLMS;
