import React from 'react';
import {
  FlatList,
  Text,
  View
} from 'react-native';
import SizedBox from '../../widgets/SizedBox';
import CourseCard from '../CourseCard';
import LoadingSpinner from '../LoadingSpinner';
import { styles } from './styles';
import useLMS from './useLMS';

const LMS = () => {
  const {
    dataCourse,
    isLoading,
    authMethod,
    translate,
    handleNavigateCourse,
    isLoadingPage,
    params,
    setParams,
    setIsLoadingPage,
    totalPage,
  } = useLMS();

  if (isLoading) {
    return <LoadingSpinner isFullScreen={true} />;
  }

  if (dataCourse.length === 0 && !authMethod.user?.is_preference_updated) {
    return null;
  }

  const renderFooter = () => (
    <>{isLoadingPage ? <LoadingSpinner /> : null}</>
  );
  return (
    <View style={styles.container}>
      <View style={styles.containerLabel}>
        <Text style={styles.textLabel}>{translate('Course_For_You')}</Text>
        {/* <TouchableOpacity
          onPress={handleNavigateCourse}
          style={styles.containerSeeAll}>
          <Text style={styles.textSeeAll}>{translate('ViewAll')}</Text>
          <SizedBox width={8} />
          <IconRightCarousel />
        </TouchableOpacity> */}
      </View>
      <SizedBox height={20} />
      <View style={styles.containerFlatlist}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={dataCourse}
          horizontal
          contentContainerStyle={styles.contentFlatlist}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => <CourseCard item={item} />}
          onEndReached={() => {
            if (totalPage > params.page) {
              setIsLoadingPage(true);
              setParams(prev => ({
                ...prev,
                page: prev.page + 1,
              }));
            }
          }}
          ListFooterComponent={renderFooter()}
        />
      </View>
    </View>
  );
};

export default LMS;
