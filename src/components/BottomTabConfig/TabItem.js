import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {
  IconBrokerTab,
  IconBrokerTabActive,
  IconCourseTab,
  IconCourseTabActive,
  IconEventTab,
  IconEventTabActive,
  IconHomeTab,
  IconHomeTabActive,
  IconProfileTab,
  IconProfileTabActive,
} from '../../assets/svg';
import SizedBox from '../../widgets/SizedBox';
import {styling} from './styles';
import {t as translate} from 'i18next';

const TabItem = ({isFocused, onPress, onLongPress, label, options}) => {
  let resultTranslate = translate('Home_Capital');
  const styles = styling(isFocused);

  switch (label) {
    case 'Home':
      resultTranslate = translate('Home_Capital');
      break;
    case 'Course':
      resultTranslate = translate('Course_Tab_Capital');
      break;
    case 'News':
      resultTranslate = translate('News_Tab_Capital');
      break;
    case 'Investment':
      resultTranslate = translate('Investment_Capital');
      break;
    case 'Tools':
      resultTranslate = translate('Tools_Capital');
      break;

    default:
      break;
  }

  const Icon = () => {
    if (label === 'Home')
      return isFocused ? <IconHomeTabActive /> : <IconHomeTab />;

    if (label === 'Course')
      return isFocused ? <IconCourseTabActive /> : <IconCourseTab />;

    if (label === 'News')
      return isFocused ? <IconEventTabActive /> : <IconEventTab />;

    if (label === 'Investment')
      return isFocused ? <IconBrokerTabActive /> : <IconBrokerTab />;

    if (label === 'Tools')
      return isFocused ? <IconProfileTabActive /> : <IconProfileTab />;

    return <IconHomeTab />;
  };

  return (
    <TouchableOpacity
      accessibilityRole="button"
      accessibilityState={isFocused ? {selected: true} : {}}
      accessibilityLabel={options.tabBarAccessibilityLabel}
      testID={options.tabBarTestID}
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.containerItem}>
      <View style={styles.icon}>
        <Icon />
        <Text style={styles.text}>{resultTranslate}</Text>
        <SizedBox height={8} />
        <View style={styles.view} />
      </View>
    </TouchableOpacity>
  );
};

export default TabItem;
