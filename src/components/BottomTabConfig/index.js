import {View} from 'react-native';
import TabItem from './TabItem';
import {styling} from './styles';
import { useSelector } from 'react-redux';
import React from 'react';
import useGetNotification from '../../api/useGetNotification';

export function BottomTabConfig({state, descriptors, navigation}) {
  const authMethod = useSelector(state => state.auth);
  const {accessNotification, params} = useGetNotification();

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      console.log('accessNotification', params);
      console.log('isLogin?', authMethod.isLogin);
      accessNotification(params, authMethod.isLogin);
    }, 60_000); // 60000 milliseconds = 1 minute

    // Cleanup interval on component unmount
    return () => clearInterval(intervalId);
  }, [params, authMethod]); // Empty dependency array ensures this runs once when the component mounts
  
  const styles = styling();
  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate({
              name: route.name,
              params: route.params,
              // merge: true,
            });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TabItem
            key={route.name}
            onPress={onPress}
            onLongPress={onLongPress}
            options={options}
            isFocused={isFocused}
            label={label}
          />
        );
      })}
    </View>
  );
}
