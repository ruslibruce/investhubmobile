import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = isFocused =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      backgroundColor: Colors.WHITE,
      justifyContent: 'space-between',
      borderTopWidth: 0.5,
      borderColor: Colors.GRAY,
      paddingTop: 12,
      paddingBottom: 10,
    },
    containerItem: {
      alignItems: 'center',
    },
    icon: {
      alignItems: 'center',
      height: 46,
      width: 64,
      justifyContent: 'center',
    },
    text: {
      fontSize: getFontSize(10),
      fontFamily: FontType.openSansRegular400,
      color: isFocused ? Colors.PRIMARY : Colors.GRAY,
    },
    view: {
      width: '50%',
      borderWidth: 1,
      borderColor: isFocused ? Colors.PRIMARY : Colors.WHITE,
      height: 1,
    },
  });
