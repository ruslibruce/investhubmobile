import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {Card, Divider} from '@ui-kitten/components';
import {
  IconComment,
  IconDotHorizontal,
  IconShareBlack,
  IconThumbBlack,
} from '../../assets/svg';
import {styles} from './styles';
import SizedBox from '../../widgets/SizedBox';
import FontAwesome6 from 'react-native-vector-icons/FontAwesome6';
import TextInputCustom from '../../widgets/TextInputCostum';
import {imageDummyCard} from '../../assets/images';
import AntDesign from 'react-native-vector-icons/AntDesign';

const ForumCard = () => {
  return (
    <Card style={styles.containerCard}>
      <View style={styles.containerHeader}>
        <View style={styles.containerHeaderTop}>
          <View style={styles.containerLeftHeaderTop}>
            <View style={styles.containerImageHeader}>
              <Image source={imageDummyCard} style={styles.widthImage} />
            </View>
            <View>
              <Text style={styles.textHeaderTitle}>Forum Name 1</Text>
              <Text style={styles.textHeaderSubTitle}>
                User Group . 2 hour{' '}
              </Text>
            </View>
          </View>
          <View style={styles.containerButtonRight}>
            <TouchableOpacity>
              <IconDotHorizontal />
            </TouchableOpacity>
            <SizedBox width={12} />
            <TouchableOpacity>
              <AntDesign name="close" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <SizedBox height={16} />
        <Text style={styles.textContent}>
          {
            'Lorem ipsum dolor sit amet consectetur. Lacus et gravida sem lectus ipsum vitae porttitor. Dui quis leo nulla massa felis habitant interdum vitae. Gravida consectetur vulputate sagittis rhoncus. Nisi amet tellus semper elit arcu.'
          }
        </Text>
        <SizedBox height={16} />
        <View style={styles.containerImageContent}>
          <Image source={imageDummyCard} style={styles.widthImage} />
        </View>
        <SizedBox height={16} />
        <Divider style={styles.divider} />
      </View>
      <SizedBox height={10} />
      <View style={styles.containerViewButton}>
        <TouchableOpacity style={styles.containerButton}>
          <IconThumbBlack />
          <Text style={styles.textButton}>Like</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.containerButton}>
          <IconComment />
          <Text style={styles.textButton}>Comment</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.containerButton}>
          <IconShareBlack />
          <Text style={styles.textButton}>Share</Text>
        </TouchableOpacity>
      </View>
      <SizedBox height={12} />
      <View style={styles.containerViewButton}>
        <View style={styles.containerAvatar}>
          <FontAwesome6 name="user-large" size={13} />
        </View>
        <TextInputCustom
          placeholder={'Create Comment...'}
          autoCapitalize={'none'}
          onChangeText={text => console.log(text)}
          styleInput={styles.containerInputStyle}
          containerInputText={styles.containerViewStyle}
          placeholderTextColor={'#8692A6'}
        />
      </View>
    </Card>
  );
};

export default ForumCard;
