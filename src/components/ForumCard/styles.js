import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styles = StyleSheet.create({
  containerHeader: {
    marginHorizontal: -25,
  },
  divider: {
    borderColor: '#E8E8E8',
    borderWidth: 0.5,
  },
  containerHeaderTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  containerLeftHeaderTop: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 8,
  },
  containerImageHeader: {
    width: 28,
    height: 28,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  widthImage: {
    width: '100%',
    height: '100%',
  },
  textHeaderTitle: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
    lineHeight: 17,
  },
  textHeaderTitle: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
    lineHeight: 17,
  },
  textHeaderSubTitle: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.BLACK,
    lineHeight: 17,
  },
  containerButtonRight: {
    flexDirection: 'row',
  },
  textContent: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
    lineHeight: 17,
    marginHorizontal: 8,
  },
  containerImageContent: {
    width: 335,
    maxWidth: 335,
    height: 196,
  },
  containerCard: {
    marginBottom: 10,
    borderRadius: 15,
    backgroundColor: Colors.WHITE,
    borderColor: Colors.WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  containerViewButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: -25,
    paddingHorizontal: 16,
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 5,
  },
  textButton: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(8),
    color: Colors.BLACK,
    lineHeight: 18,
  },
  containerAvatar: {
    padding: 8,
    width: 32,
    height: 32,
    borderRadius: 32,
    backgroundColor: Colors.GRAY3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerViewStyle: {
    borderRadius: 25,
    paddingHorizontal: 0,
    width: '85%',
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerInputStyle: {
    backgroundColor: Colors.GRAY3,
    height: 30,
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(9),
    color: Colors.BLACK,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
  },
});
