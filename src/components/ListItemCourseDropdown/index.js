import React from 'react';
import {View} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {IconChevUp} from '../../assets/svg';
import {Colors} from '../../utility';
import {styles} from './styles';

const ListItemCourseDropdown = ({
  data,
  placeholder,
  value,
  renderItemDropdown,
  name,
  handleChangeDropdown,
}) => {
  const [isFocus, setIsFocus] = React.useState(false);
  return (
    <View style={styles.containerCheckbox}>
      <Dropdown
        style={styles.containerButtonMidDropdown}
        placeholderStyle={styles.placeholderDropdown}
        selectedTextStyle={styles.selectedDropdown}
        inputSearchStyle={styles.textInputSearchDropdown}
        iconStyle={styles.iconStyleDropdown}
        data={data}
        search
        maxHeight={300}
        labelField="label"
        valueField="value"
        placeholder={placeholder}
        searchPlaceholder="Search..."
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        value={value}
        renderItem={renderItemDropdown}
        onChange={result => {
          setIsFocus(false);
          handleChangeDropdown(result, name);
        }}
        renderRightIcon={() => {
          if (isFocus) {
            return <IconChevUp />;
          } else {
            return (
              <MaterialIcons name="keyboard-arrow-down" color={Colors.GRAY} />
            );
          }
        }}
      />
    </View>
  );
};

export default ListItemCourseDropdown;
