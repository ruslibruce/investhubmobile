import React from 'react';
import {ImageBackground, Text, View} from 'react-native';
import {styling} from './styles';
import SizedBox from '../../widgets/SizedBox';

const LongCard = ({item, isBottom}) => {
  const styles = styling(isBottom);
  return (
    <ImageBackground source={item.image} style={styles.containerItemImage}>
      {isBottom ? (
        <>
          <Text numberOfLines={2} style={styles.textTitleBottom}>
            {item.title}
          </Text>
          <SizedBox height={4} />
          <View style={styles.containerTextSubTitle}>
            <Text style={styles.textSubTitle}>Health Care</Text>
            <View style={styles.dotStyling} />
            <Text style={styles.textSubTitle}>Jan 24, 2024</Text>
          </View>
        </>
      ) : (
        <Text numberOfLines={2} style={styles.textTitle}>
          {item.title}
        </Text>
      )}
    </ImageBackground>
  );
};

export default LongCard;
