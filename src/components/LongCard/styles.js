import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = isBottom =>
  StyleSheet.create({
    containerItemImage: {
      borderTopLeftRadius: 2,
      borderTopRightRadius: 12,
      borderBottomLeftRadius: 12,
      borderBottomRightRadius: 2,
      paddingVertical: 17,
      paddingHorizontal: 20,
      minWidth: 150,
      width: 150,
      height: 185,
      justifyContent: isBottom ? 'flex-end' : 'flex-start',
      overflow: 'hidden',
      backgroundColor: Colors.WHITE,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 3,
      },
      shadowOpacity: 0.29,
      shadowRadius: 4.65,
      elevation: 7,
    },
    textTitle: {
      fontFamily: FontType.openSansBold700,
      fontSize: getFontSize(20),
      color: Colors.WHITE,
    },
    textTitleBottom: {
      fontFamily: FontType.openSansExtraBold800,
      fontSize: getFontSize(14),
      color: Colors.WHITE,
      lineHeight: 21,
    },
    containerTextSubTitle: {
      flexDirection: 'row',
      alignItems: 'center',
      gap: 7,
      justifyContent: 'center',
    },
    textSubTitle: {
      fontSize: getFontSize(8),
      fontFamily: FontType.openSansSemiBold600,
      color: Colors.WHITE,
      lineHeight: 12,
    },
    dotStyling: {
      backgroundColor: Colors.WHITE,
      width: 4,
      height: 4,
      borderRadius: 4,
    },
  });
