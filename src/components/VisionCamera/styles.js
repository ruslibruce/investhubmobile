import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize, matrics} from '../../utility';

export const styling = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'black',
      position: 'relative',
    },
    captureButton: {
      position: 'absolute',
      alignSelf: 'center',
      bottom: insets.bottom + 20,
    },
    text: {
      color: Colors.WHITE,
      fontSize: getFontSize(11),
      fontFamily: FontType.openSansRegular400,
      textAlign: 'center',
    },
    emptyContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    containerImageResult: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.BLACK,
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      top: 0,
    },
    containerImageButton: {
      zIndex: 2,
      position: 'absolute',
      flexDirection: 'row',
      paddingHorizontal: 30,
      bottom: insets.bottom + 25,
      width: matrics.screenWidth,
      justifyContent: 'space-between',
    },
    widthImage: {
      height: '100%',
      width: matrics.screenWidth,
    },
    buttonResultCamera: {
      height: 70,
      width: 70,
      backgroundColor: Colors.PRIMARY,
      borderRadius: 50,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
