import mime from 'mime';
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {magicModal} from 'react-native-magic-modal';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Camera,
  useCameraDevice,
  useCameraFormat,
} from 'react-native-vision-camera';
import {Colors} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import LoadingSpinner from '../LoadingSpinner';
import {styling} from './styles';

const VisionCamera = ({isFront, isBack, handleUpload}) => {
  const insets = useSafeAreaInsets();
  const devices = Camera.getAvailableCameraDevices();
  const hasFlash = devices.filter(d => d.hasFlash);
  const frontCameras = devices.filter(d => d.position === 'front');
  const backCameras = devices.filter(d => d.position === 'back');
  const device = useCameraDevice('front');
  const format = useCameraFormat(device, [
    {photoResolution: {width: 100, height: 100}},
  ]);
  const camera = React.useRef(null);
  const [dataImage, setDataImage] = React.useState();
  const [dataUpload, setDataUpload] = React.useState();
  const [isLoading, setIsLoading] = React.useState(false);
  const styles = styling(insets);

  const takePhoto = React.useCallback(async () => {
    setIsLoading(true);
    try {
      if (camera.current == null) throw new Error('Camera ref is null!');
      const photo = await camera.current.takePhoto({
        enableShutterSound: false,
      });
      const imageURI = 'file://' + photo.path;
      const blob = {
        uri: imageURI,
        type: mime.getType(imageURI),
        name: imageURI.split('/').pop(),
      };
      const formData = new FormData();
      formData.append('file', blob);
      setDataImage(imageURI);
      setDataUpload(formData);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.error('Failed to take photo!', e);
    }
  }, [camera]);

  return (
    <View style={styles.container}>
      {device != null ? (
        <>
          {frontCameras && isFront && !dataImage && (
            <>
              <Camera
                photo={true}
                style={StyleSheet.absoluteFill}
                device={device}
                isActive={true}
                ref={camera}
                format={format}
              />
              <TouchableOpacity
                onPress={takePhoto}
                style={styles.captureButton}>
                <MaterialCommunityIcons
                  name="circle-slice-8"
                  color={Colors.WHITE}
                  size={70}
                />
              </TouchableOpacity>
            </>
          )}
          {backCameras && isBack && !dataImage && (
            <>
              <Camera
                photo={true}
                style={StyleSheet.absoluteFill}
                device={device}
                isActive={true}
                ref={camera}
                format={format}
              />
              <TouchableOpacity
                onPress={takePhoto}
                style={styles.captureButton}>
                <MaterialCommunityIcons
                  name="circle-slice-8"
                  color={Colors.WHITE}
                />
              </TouchableOpacity>
            </>
          )}
          {!frontCameras && !backCameras && (
            <>
              {isFront && (
                <View style={styles.emptyContainer}>
                  <Text style={styles.text}>
                    Your phone does not have a Front Camera.
                  </Text>
                  <ButtonPrimary
                    label={'Back'}
                    onPress={() => magicModal.hide()}
                    style={{marginTop: 10, width: '70%'}}
                  />
                </View>
              )}
              {isBack && (
                <View style={styles.emptyContainer}>
                  <Text style={styles.text}>
                    Your phone does not have a Back Camera.
                  </Text>
                  <ButtonPrimary
                    label={'Back'}
                    onPress={() => magicModal.hide()}
                    style={{marginTop: 10, width: '70%'}}
                  />
                </View>
              )}
            </>
          )}
          {dataImage && (
            <View style={styles.containerImageResult}>
              <Image source={{uri: dataImage}} style={styles.widthImage} />
              <View style={styles.containerImageButton}>
                <TouchableOpacity onPress={() => setDataImage(null)}>
                  <View style={styles.buttonResultCamera}>
                    <Ionicons
                      name="reload-circle-sharp"
                      color={Colors.WHITE}
                      size={40}
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => handleUpload(dataUpload)}>
                  <View style={styles.buttonResultCamera}>
                    <Ionicons name="save" color={Colors.WHITE} size={40} />
                  </View>
                </TouchableOpacity>
              </View>
              {isLoading && <LoadingSpinner />}
            </View>
          )}
        </>
      ) : (
        <View style={styles.emptyContainer}>
          <Text style={styles.text}>Your phone does not have a Camera.</Text>
          <ButtonPrimary
            label={'Back'}
            onPress={() => magicModal.hide()}
            style={{marginTop: 10, width: '70%'}}
          />
        </View>
      )}
    </View>
  );
};

export default VisionCamera;
