import {Card, Modal} from '@ui-kitten/components';
import React from 'react';
import {matrics} from '../../utility';
import {Keyboard} from 'react-native';

const MyModalBottom = ({visible, onBackdropPress, children}) => {
  const [keyboardSize, setKeyboardSize] = React.useState(0);
  React.useEffect(() => {
    const showListener = Keyboard.addListener('keyboardDidShow', e => {
      setKeyboardSize(e.endCoordinates.height);
    });
    const hideListener = Keyboard.addListener('keyboardDidHide', e => {
      setKeyboardSize(0);
    });
    return () => {
      showListener.remove();
      hideListener.remove();
    };
  });

  return (
    <Modal
      visible={visible}
      style={{marginTop: 250 - keyboardSize}}
      animationType={'fade'}
      backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
      onBackdropPress={onBackdropPress}>
      <Card
        style={{
          flex: 1,
          width: matrics.screenWidth,
          backgroundColor: 'white',
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
        }}
        disabled={true}>
        {children}
      </Card>
    </Modal>
  );
};

export default MyModalBottom;
