import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {Colors} from '../../utility';
import ButtonPrimary from '../../widgets/ButtonPrimary';
import SizedBox from '../../widgets/SizedBox';
import ListItemCourseDropdown from '../ListItemCourseDropdown';
import {styles} from './styles';
import useFilterCourse from './useFilterCourse';

const FilterCourse = ({setIsFiltered, handleGetParamsFilter}) => {
  const {
    resultFilter,
    handleChangeDropdown,
    handleClearFilter,
    dataCourseContentType,
    renderItemDropdown,
    dataCourseCategory,
    dataCourseLevel,
    authMethod,
  } = useFilterCourse();
  return (
    <View style={styles.containerFilter}>
      <View style={styles.containerTitleFilter}>
        <Entypo
          onPress={() => setIsFiltered(false)}
          name="cross"
          color={Colors.WHITE}
          size={25}
        />
        <Text style={styles.textFilterTitle}>Filter Course</Text>
      </View>
      <SizedBox height={10} />
      <View style={{height: authMethod.isLogin ? 250 : 200}}>
        <ScrollView>
          {authMethod.isLogin && (
            <ListItemCourseDropdown
              data={dataCourseLevel}
              handleChangeDropdown={handleChangeDropdown}
              placeholder={'Filter Level'}
              renderItemDropdown={renderItemDropdown}
              value={resultFilter.level}
              name={'level'}
            />
          )}
          <ListItemCourseDropdown
            data={dataCourseContentType}
            handleChangeDropdown={handleChangeDropdown}
            placeholder={'Filter Content Type'}
            renderItemDropdown={renderItemDropdown}
            value={resultFilter.content_type}
            name={'content_type'}
          />
          <ListItemCourseDropdown
            data={dataCourseCategory}
            handleChangeDropdown={handleChangeDropdown}
            placeholder={'Filter Category'}
            renderItemDropdown={renderItemDropdown}
            value={resultFilter.category}
            name={'category'}
          />
          <View style={styles.containerButton}>
            <ButtonPrimary
              onPress={handleClearFilter}
              style={styles.styleButtonClear}
              styleLabel={styles.textButtonClear}
              label={'Clear Filter'}
            />
            <ButtonPrimary
              onPress={() => handleGetParamsFilter(resultFilter)}
              style={styles.styleButton}
              styleLabel={styles.textButton}
              label={'Apply'}
            />
          </View>
          <SizedBox height={10} />
        </ScrollView>
      </View>
    </View>
  );
};

export default FilterCourse;
