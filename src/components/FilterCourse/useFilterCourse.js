import React from 'react';
import {Text, View} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';
import useGetCourseCategory from '../../api/useGetCourseCategory';
import useGetCourseLevel from '../../api/useGetCourseLevel';
import useGetCourseContentType from '../../api/useGetCourseContentType';
import { useSelector } from 'react-redux';

const initialFilter = {
  level: '',
  category: '',
  content_type: '',
};

const useFilterCourse = () => {
  const authMethod = useSelector(state => state.auth);
  const {dataCourseCategory} = useGetCourseCategory();
  const {dataCourseLevel} = useGetCourseLevel();
  const {dataCourseContentType} = useGetCourseContentType();
  const [resultFilter, setResultFilter] = React.useState(initialFilter);

  const handleChangeDropdown = (item, params) => {
    setResultFilter(prev => ({
      ...prev,
      [params]: item.value,
    }));
  };

  const renderItemDropdown = item => {
    return (
      <View
        style={{
          padding: 12,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text
          style={{
            flex: 1,
            fontSize: getFontSize(14),
            fontFamily: FontType.openSansSemiBold600,
            color: Colors.BLACK,
          }}>
          {item.label}
        </Text>
      </View>
    );
  };

  const handleClearFilter = () => {
    setResultFilter(initialFilter);
  };

  return {
    renderItemDropdown,
    handleChangeDropdown,
    resultFilter,
    handleClearFilter,
    dataCourseContentType,
    dataCourseCategory,
    dataCourseLevel,
    authMethod,
  };
};

export default useFilterCourse;
