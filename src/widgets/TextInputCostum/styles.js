import {StyleSheet} from 'react-native';
import {Colors, FontType, getFontSize} from '../../utility';

export const styling = (multiline, isFocused, value, placeholderTextColor) =>
  StyleSheet.create({
    label: {
      fontFamily: FontType.openSansBold700,
      textAlign: 'left',
      fontSize: getFontSize(14),
      color: Colors.LABELINPUT,
    },
    inputText: {
      backgroundColor: Colors.WHITE,
      borderRadius: 24,
      borderWidth: 1,
      paddingHorizontal: 20,
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      height: multiline ? 70 : 48,
      borderColor: isFocused ? Colors.REDBAR : Colors.BORDER_RADIUS_AUTH,
    },
    buttonStyle: {
      flex: 1,
      backgroundColor: Colors.WHITE,
      borderWidth: 0,
      borderRadius: 24,
    },
    textButton: {
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansBold700,
      color: value ? Colors.BLACK : placeholderTextColor,
    },
    textInputStyle: {
      flex: 1,
      fontSize: getFontSize(12),
      fontFamily: FontType.openSansBold700,
      backgroundColor: Colors.WHITE,
      borderWidth: 0,
      borderRadius: 24,
      color: Colors.BLACK,
    },
  });
