import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Colors} from '../../utility';
import SizedBox from '../SizedBox';
import useTextInput from '../TextInputWithLabel/useTextInput';
import {styling} from './styles';
const TextInputCustom = ({
  placeholder,
  placeholderTextColor,
  containerInputText,
  secureTextEntry = false,
  numberOfLines = 1,
  styleInput,
  editable = true,
  keyboardType = 'default',
  multiline = false,
  value,
  onChangeText,
  iconLeft,
  date,
  autoCapitalize,
  iconRight,
}) => {
  const {eyesOpen, setEyesOpen, isFocused, setFocused} =
    useTextInput(secureTextEntry);
  const styles = styling(multiline, isFocused, value, placeholderTextColor);
  return (
    <View style={[styles.inputText, containerInputText]}>
      {iconLeft ? (
        <>
          {iconLeft}
          <SizedBox width={13} />
        </>
      ) : (
        <></>
      )}
      {date ? (
        <TouchableOpacity style={styles.buttonStyle} onPress={onChangeText}>
          <Text style={[styles.textButton, styleInput]}>
            {value ? value : placeholder}
          </Text>
        </TouchableOpacity>
      ) : (
        <TextInput
          onFocus={() => setFocused(true)}
          onBlur={() => setFocused(false)}
          placeholderTextColor={placeholderTextColor}
          placeholder={placeholder}
          autoCapitalize={autoCapitalize ?? 'sentences'}
          multiline={multiline}
          numberOfLines={numberOfLines}
          secureTextEntry={eyesOpen}
          style={[styles.textInputStyle, styleInput]}
          keyboardType={keyboardType}
          value={value}
          onChangeText={onChangeText}
          editable={editable}
          autoComplete={secureTextEntry ? 'off' : 'on'}
        />
      )}
      {secureTextEntry ? (
        <Ionicons
          name={eyesOpen ? 'eye-off' : 'eye'}
          size={24}
          color={Colors.GRAYTEXT}
          onPress={() => setEyesOpen(!eyesOpen)}
        />
      ) : (
        <View />
      )}
      {iconRight}
    </View>
  );
};

export default TextInputCustom;
