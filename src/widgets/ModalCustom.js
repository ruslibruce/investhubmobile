import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Platform} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTranslation} from 'react-i18next';
import {Colors, FontType, getFontSize} from '../utility';
const ModalCustom = ({
  textModalJudul,
  textModalSub,
  hideModalSukses,
  iconModal,
  doubleButton,
  singleButton,
  textButton,
  textButton2,
  textCustom,
  containerButtonDouble,
  containerButtonSingle,
}) => {
  const {t} = useTranslation();

  return (
    <View style={styles.containerModal}>
      {iconModal ?? <View />}
      {textModalJudul && <Text style={styles.textJudul}>{textModalJudul}</Text>}
      {textModalSub && <Text style={styles.textIsi}>{textModalSub}</Text>}
      {textCustom}
      <TouchableOpacity
        onPress={e => hideModalSukses((e = doubleButton ? 'X' : textButton))}
        style={{position: 'absolute', right: 20, top: 20}}>
        <MaterialCommunityIcons name={'close'} color={Colors.BLACK} size={20} />
      </TouchableOpacity>
      {doubleButton && (
        <View style={[styles.containerButton, containerButtonDouble]}>
          <TouchableOpacity
            style={styles.containerButtonNo}
            onPress={e =>
              hideModalSukses((e = textButton2 ? textButton2 : 'No'))
            }>
            <Text style={styles.textNo}>{textButton2 ?? 'No'}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.containerButtonYes}
            onPress={e =>
              hideModalSukses((e = textButton ? textButton : 'Yes'))
            }>
            <Text style={styles.textYes}>{textButton ?? 'Yes'}</Text>
          </TouchableOpacity>
        </View>
      )}
      {singleButton && (
        <TouchableOpacity
          style={[styles.containerButtonLabel, containerButtonSingle]}
          onPress={e =>
            hideModalSukses((e = textButton ? textButton : 'empty'))
          }>
          <Text style={styles.textLabel}>{textButton}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  containerModal: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
  textJudul: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(16),
    marginTop: 5,
    color: Colors.BLACK,
    textAlign: 'center',
  },
  textIsi: {
    fontFamily: FontType.openSansRegular400,
    fontSize: getFontSize(12),
    color: Colors.BLACK,
    textAlign: 'center',
    marginTop: 5,
  },
  containerButton: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  textYes: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.WHITE,
  },
  textNo: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.PRIMARY,
  },
  containerButtonLabel: {
    width: '100%',
    backgroundColor: Colors.PRIMARY,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  containerButtonNo: {
    width: '45%',
    backgroundColor: Colors.WHITE,
    borderColor: Colors.PRIMARY,
    borderWidth: 1,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  containerButtonYes: {
    width: '45%',
    backgroundColor: Colors.PRIMARY,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  textLabel: {
    fontFamily: FontType.openSansSemiBold600,
    fontSize: getFontSize(12),
    color: Colors.WHITE,
  },
});

export default ModalCustom;
