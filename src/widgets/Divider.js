import React from 'react';
import {View} from 'react-native';
import {Colors} from '../utility';

const Divider = ({styleProps}) => {
  return (
    <View
      style={[
        {
          width: '100%',
          borderWidth: 0.5,
          borderColor: Colors.PLACEHOLDER,
          height: 1,
        },
        styleProps,
      ]}
    />
  );
};

export default Divider;
