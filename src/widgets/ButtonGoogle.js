import {
  GoogleSignin,
  isErrorWithCode,
  isSuccessResponse,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import React from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import Config from 'react-native-config';
import useGetLoginGoogle from '../api/useGetLoginGoogle';
import {iconGoogle} from '../assets/images';
import {Colors} from '../utility';
import LoadingSpinner from '../components/LoadingSpinner';

GoogleSignin.configure({
  webClientId: Config.WEB_CLIENT_ID, // client ID of type WEB for your server. Required to get the `idToken` on the user object, and for offline access.
  scopes: [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile',
  ], // what API you want to access on behalf of the user, default is email and profile
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  accountName: '', // [Android] specifies an account name on the device that should be used
  iosClientId: Config.IOS_CLIENT_ID, // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
  googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
  openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
  profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
});

const ButtonGoogle = () => {
  const [fetching, setFetching] = React.useState(false);
  const {accessLoginGoogle, isLoading} = useGetLoginGoogle();

  const doLoginGmail = async () => {
    try {
      await GoogleSignin.hasPlayServices();

      const isSignedIn = await GoogleSignin.hasPreviousSignIn();
      console.log('isSignedIn', isSignedIn);

      if (isSignedIn) {
        await GoogleSignin.signOut();
      }

      const response = await GoogleSignin.signIn();
      console.log('response', JSON.stringify(response));
      const dataResponse = response.data;
      if (isSuccessResponse(response)) {
        let scopeData = dataResponse.scopes?.join('+');
        await accessLoginGoogle(dataResponse.serverAuthCode, scopeData);
      }
    } catch (error) {
      if (isErrorWithCode(error)) {
        switch (error.code) {
          case statusCodes.SIGN_IN_CANCELLED:
            // user cancelled the login flow
            break;
          case statusCodes.IN_PROGRESS:
            // operation (eg. sign in) already in progress
            break;
          case statusCodes.PLAY_SERVICES_NOT_AVAILABLE:
            // play services not available or outdated
            break;
          default:
          // some other error happened
        }
      } else {
        // an error that's not related to google sign in occurred
      }
    }
  };

  return (
    <TouchableOpacity
      disabled={isLoading}
      onPress={doLoginGmail}
      style={{
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.BORDER_RADIUS_AUTH,
        borderWidth: 1,
        borderRadius: 18,
        padding: 14,
      }}>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <Image source={iconGoogle} style={{width: 30, height: 30}} />
      )}
    </TouchableOpacity>
  );
};

export default ButtonGoogle;
