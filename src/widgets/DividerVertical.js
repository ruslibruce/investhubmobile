import React from 'react';
import {View} from 'react-native';
import {Colors} from '../utility';

const DividerVertical = ({styleProps}) => {
  return (
    <View
      style={[
        {
          width: 1,
          borderWidth: 0.5,
          borderColor: Colors.BLACK,
          height: 15,
        },
        styleProps,
      ]}
    />
  );
};

export default DividerVertical;
