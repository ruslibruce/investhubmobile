// FILE: UsbManager.js
import {NativeModules} from 'react-native';

const {UsbModule} = NativeModules;

export const isUsbDebuggingEnabled = callback => {
  if (UsbModule && UsbModule.isUsbDebuggingEnabled) {
    UsbModule.isUsbDebuggingEnabled(callback);
  } else {
    console.error('UsbModule or isUsbDebuggingEnabled method not found');
  }
};

export const openDeveloperOptions = () => {
  if (UsbModule && UsbModule.openDeveloperOptions) {
    UsbModule.openDeveloperOptions();
  } else {
    console.error('UsbModule or openDeveloperOptions method not found');
  }
};

export const isUsbDeviceConnected = (callback) => {
  if (UsbModule && UsbModule.isUsbDeviceConnected) {
    UsbModule.isUsbDeviceConnected(callback);
  } else {
    console.error('UsbModule or isUsbDeviceConnected method not found');
  }
};
