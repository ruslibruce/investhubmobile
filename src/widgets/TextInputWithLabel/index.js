import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Colors} from '../../utility';
import SizedBox from '../SizedBox';
import {styling} from './styles';
import useTextInput from './useTextInput';
const TextInputWithLabel = ({
  label,
  required = true,
  styleProps,
  placeholder,
  placeholderTextColor,
  styleLabel,
  containerInputText,
  secureTextEntry,
  numberOfLines = 1,
  styleInput,
  editable = true,
  keyboardType = 'default',
  multiline = false,
  value,
  onChangeText,
  icon,
  date,
  autoCapitalize,
  name,
  onBlur,
}) => {
  const {eyesOpen, setEyesOpen, isFocused, setFocused} = useTextInput(
    secureTextEntry ? secureTextEntry : false,
  );
  const styles = styling(multiline, isFocused, value, placeholderTextColor);
  return (
    <View style={[styles.containerInputLabel, styleProps]}>
      {label && (
        <>
          <Text style={[styles.label, styleLabel]}>
            {label}
            {required ? (
              <Text style={{color: Colors.REDBAR}}>{' *'}</Text>
            ) : null}
          </Text>
          <View style={{height: 8}} />
        </>
      )}
      <View style={[styles.inputText, containerInputText]}>
        {icon}
        <SizedBox width={13} />
        {date ? (
          <TouchableOpacity style={styles.buttonStyle} onPress={onChangeText}>
            <Text style={[styles.textButton, styleInput]}>
              {value ? value : placeholder}
            </Text>
          </TouchableOpacity>
        ) : (
          <TextInput
            onFocus={() => setFocused(true)}
            onBlur={() => {
              if (onBlur) {
                onBlur(name);
              }
              setFocused(false);
            }}
            placeholderTextColor={
              placeholderTextColor ? placeholderTextColor : Colors.GRAYTEXT
            }
            placeholder={placeholder}
            autoCapitalize={autoCapitalize ?? 'sentences'}
            multiline={multiline}
            numberOfLines={numberOfLines}
            secureTextEntry={eyesOpen}
            style={[styles.textInputStyle, styleInput]}
            keyboardType={keyboardType}
            value={value}
            onChangeText={onChangeText}
            editable={editable}
            name={name}
            autoComplete={secureTextEntry ? 'off' : 'on'}
          />
        )}
        {secureTextEntry != null ? (
          <Ionicons
            name={eyesOpen ? 'eye-off' : 'eye'}
            size={24}
            color={Colors.GRAYTEXT}
            onPress={() => setEyesOpen(!eyesOpen)}
          />
        ) : (
          <View />
        )}
      </View>
    </View>
  );
};

export default TextInputWithLabel;
