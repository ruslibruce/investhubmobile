import queryString from 'query-string'; // Import the query-string library
import React from 'react';
import {Image, Platform, TouchableOpacity} from 'react-native';
import Config from 'react-native-config';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import useGetLoginRDIS from '../api/useGetLoginRDIS';
import {iconLoginRDIS} from '../assets/images';
import LoadingSpinner from '../components/LoadingSpinner';
import {Colors, getDeepLink, UrlRDIS} from '../utility';

const ButtonRDIS = () => {
  const {accessLoginRDIS, isLoading} = useGetLoginRDIS();

  const doLoginRDIS = async () => {
    try {
      // const deepLink = `${Config.URL_CALLBACK}/rdis/callback`;
      const url = `${Config.BASE_URL}${UrlRDIS}`;
      const deepLink = getDeepLink('rdis/callback');
      console.log('deepLink', deepLink);
      console.log('url', url);
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.openAuth(url, deepLink, {
          // iOS Properties
          ephemeralWebSession: false,
          // Android Properties
          showTitle: false,
          enableUrlBarHiding: true,
          enableDefaultShare: false,
        });
        console.log('response result', result);
        const parsedUrl = queryString.parseUrl(result.url);
        const code = parsedUrl.query.code;
        console.log('code', code);
        await accessLoginRDIS(code);
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  return (
    <TouchableOpacity
      disabled={isLoading}
      onPress={doLoginRDIS}
      style={{
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.BORDER_RADIUS_AUTH,
        borderWidth: 1,
        borderRadius: 18,
        padding: 14,
        width: '44%',
      }}>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <Image source={iconLoginRDIS} style={{height: 30}} />
      )}
    </TouchableOpacity>
  );
};

export default ButtonRDIS;
