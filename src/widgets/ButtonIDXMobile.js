import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {iconLoginIDXMobile} from '../assets/images';
import {Colors} from '../utility';
import Toast from 'react-native-toast-message';
import {useTranslation} from 'react-i18next';

const ButtonIDXMobile = () => {
  const {t: translate} = useTranslation();
  const doLoginIDXMobile = async () => {
    Toast.show({
      type: 'info',
      position: 'bottom',
      text1: translate('UpdateTitle'),
      text2: translate('UpdateDesc'),
    });
  };
  return (
    <TouchableOpacity
      disabled={true}
      onPress={doLoginIDXMobile}
      style={{
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.BORDER_RADIUS_AUTH,
        borderWidth: 1,
        borderRadius: 18,
        padding: 14,
      }}>
      <Image source={iconLoginIDXMobile} style={{width: 30, height: 30}} />
    </TouchableOpacity>
  );
};

export default ButtonIDXMobile;
