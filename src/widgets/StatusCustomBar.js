import {StatusBar} from 'react-native';
import React from 'react';
import {Colors} from '../utility';

const StatusCustomBarDark = () => {
  return <StatusBar barStyle={'dark-content'} backgroundColor={Colors.WHITE} />;
};

const StatusCustomBarLight = () => {
  return (
    <StatusBar barStyle={'light-content'} backgroundColor={Colors.BLACK} />
  );
};

const StatusCustomBarRed = () => {
  return (
    <StatusBar barStyle={'light-content'} translucent backgroundColor="transparent" />
  );
};

const StatusCustomBarTranslucent = () => {
  return <StatusBar barStyle={'light-content'} translucent backgroundColor="transparent" />;
};

export {
  StatusCustomBarDark,
  StatusCustomBarLight,
  StatusCustomBarRed,
  StatusCustomBarTranslucent,
};
