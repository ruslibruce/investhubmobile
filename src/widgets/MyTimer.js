import React, { useCallback, useEffect } from 'react';
import { Text, View } from 'react-native';
import { useTimer } from 'react-timer-hook';
import usePostSaveQuizLevelUp from '../api/usePostSaveQuizLevelUp';
import { Colors, FontType, getFontSize } from '../utility';

export default function MyTimer({
  expiryTimestamp,
  handleTimeCountDown,
  transparent,
  id,
  dataExercise,
}) {
  const {
    seconds,
    minutes,
    hours,
    days,
    isRunning,
    start,
    pause,
    resume,
    restart,
    totalSeconds,
  } = useTimer({
    expiryTimestamp,
    onExpire: () => {
      // console.warn('onExpire called');
      handleTimeCountDown();
    },
  });
  const {saveDataQuiz} = usePostSaveQuizLevelUp();

  useEffect(() => {
    console.log('expiryTimestamp', expiryTimestamp);

    const intervalId = setInterval(() => {
      // Call your function here
      console.log('Function called every 10 seconds');
      handleSaveRemaining();
    }, 10000); // 10000 milliseconds = 10 seconds

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  const handleSaveRemaining = useCallback(() => {
    let answer = {};
    dataExercise.map(item => {
      answer[item.id] = item.answer;
    });
    if (Object.keys(answer).length > 0) {
      saveDataQuiz(id, answer, totalSeconds);
    }
  }, [totalSeconds]);

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: transparent ? 'transparent' : Colors.WHITE,
        alignItems: 'flex-start',
        justifyContent: 'center',
      }}>
      <Text
        style={{
          color: Colors.PRIMARY,
          fontSize: getFontSize(20),
          fontFamily: FontType.openSansRegular400,
        }}>
        {minutes}
      </Text>
      <View style={{width: 5}} />
      <Text
        style={{
          color: Colors.PRIMARY,
          fontSize: getFontSize(20),
          fontFamily: FontType.openSansRegular400,
        }}>
        {' : '}
      </Text>
      <View style={{width: 5}} />
      <Text
        style={{
          color: Colors.PRIMARY,
          fontSize: getFontSize(20),
          fontFamily: FontType.openSansRegular400,
        }}>
        {seconds}
      </Text>
    </View>
  );
}
