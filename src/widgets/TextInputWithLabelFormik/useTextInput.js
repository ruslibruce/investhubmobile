import React from 'react';

const useTextInput = (secureTextEntry) => {
  const [eyesOpen, setEyesOpen] = React.useState(secureTextEntry);
  const [isFocused, setFocused] = React.useState(false);
  
  return {eyesOpen, setEyesOpen, isFocused, setFocused};
};

export default useTextInput;
