import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Colors, FontType, getFontSize} from '../utility';
import LoadingSpinner from '../components/LoadingSpinner';
import SizedBox from './SizedBox';
const ButtonPrimary = ({
  onPress,
  style,
  styleLabel,
  label,
  icon,
  disabled,
  props,
  loading,
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      {...props}
      style={[styles.containerButton, style]}
      onPress={onPress}>
      {loading && (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <LoadingSpinner />
          <SizedBox width={5} />
        </View>
      )}
      <Text style={[styles.textButton, styleLabel]}>{label}</Text>
      {icon}
    </TouchableOpacity>
  );
};

export default ButtonPrimary;

const styles = StyleSheet.create({
  containerButton: {
    width: '100%',
    height: 48,
    backgroundColor: Colors.PRIMARY,
    borderRadius: 24,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  textButton: {
    fontFamily: FontType.openSansSemiBold600,
    color: Colors.WHITE,
    fontSize: getFontSize(16),
  },
});
