import queryString from 'query-string';
import React from 'react';
import {Image, Platform, TouchableOpacity} from 'react-native';
import Config from 'react-native-config';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import useGetLoginTicmi from '../api/useGetLoginTicmi';
import {iconLoginTICMI} from '../assets/images';
import LoadingSpinner from '../components/LoadingSpinner';
import {Colors, getDeepLink, UrlTicmi} from '../utility';

const ButtonTicmi = () => {
  const {accessLoginTicmi, isLoading} = useGetLoginTicmi();

  const doLoginTicmi = async () => {
    try {
      const url = `${Config.BASE_URL}${UrlTicmi}`;
      const deepLink = getDeepLink('ticmiedu/callback');
      console.log('deepLink', deepLink);
      console.log('url', url);
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.openAuth(url, deepLink, {
          // iOS Properties
          ephemeralWebSession: false,
          // Android Properties
          showTitle: false,
          enableUrlBarHiding: true,
          enableDefaultShare: false,
        });
        console.log('response result', result);
        const parsedUrl = queryString.parseUrl(result.url);
        const data = parsedUrl.query;
        console.log('data', data);
        await accessLoginTicmi(data);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  return (
    <TouchableOpacity
      disabled={isLoading}
      onPress={doLoginTicmi}
      style={{
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.BORDER_RADIUS_AUTH,
        borderWidth: 1,
        borderRadius: 18,
        padding: 14,
        width: '44%',
      }}>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <Image source={iconLoginTICMI} style={{height: 30}} />
      )}
    </TouchableOpacity>
  );
};

export default ButtonTicmi;
