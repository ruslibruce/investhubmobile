import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useEffect, useState} from 'react';
import {AppState, BackHandler, Image, Platform, Text, View} from 'react-native';
import Config from 'react-native-config';
import {magicModal} from 'react-native-magic-modal';
import {useDispatch} from 'react-redux';
import {iconLogo} from '../assets/images';
import {loadingSplash} from '../redux/reducers/app.reducers';
import {loading} from '../redux/reducers/auth.reducers';
import {Colors, FontType, getFontSize, matrics, sleep} from '../utility';
import ModalCustom from './ModalCustom';
import {StatusCustomBarDark} from './StatusCustomBar';
import {
  isUsbDebuggingEnabled,
  isUsbDeviceConnected,
  openDeveloperOptions,
} from './UsbManager';
import {useTranslation} from 'react-i18next';

const Splash = () => {
  const {t: translate} = useTranslation();
  const dispatch = useDispatch();
  const [appState, setAppState] = useState(AppState.currentState);
  const [usbConnected, setUsbConnected] = useState();

  const handleAppStateChange = async nextAppState => {
    if (appState.match(/inactive|background/) && nextAppState === 'active') {
      // App has come to the foreground
      console.log('App has come to the foreground!');
      checkUsbDeviceConnected();
    }
    setAppState(nextAppState);
  };

  React.useEffect(() => {
    const interval = setInterval(async () => {
      Platform.OS === 'android' && checkUsbDeviceConnected();
    }, 1000); // 1000 ms refresh. increase it if you don't require millisecond precision

    if (Platform.OS === 'ios') {
      sleep(2000).then(() => dispatch(loadingSplash()));
      return;
    }
    
    return () => {
      clearInterval(interval);
    };
  }, []);

  const checkUsbDeviceConnected = () => {
    isUsbDeviceConnected(connected => {
      console.log('isUsbDeviceConnected====>', connected);
      if (connected) {
        setUsbConnected(true);
      } else {
        setUsbConnected(false);
      }
    });
  };

  const checkUsbDebugging = isUsbConnected => {
    if (isUsbConnected) {
      isUsbDebuggingEnabled(enabled => {
        if (enabled && Config.DEVELOPER_MODE === 'false') {
          magicModal.show(() => (
            <ModalCustom
              hideModalSukses={e => {
                console.log('event nhhhhhh', e);
                if (e === translate('OpenUsbSetting')) {
                  magicModal.hide();
                  setUsbConnected();
                  openDeveloperOptions();
                  return;
                }
                BackHandler.exitApp();
              }}
              doubleButton={true}
              textButton={translate('ExitApp')}
              textButton2={translate('OpenUsbSetting')}
              textModalJudul={translate('USBDebugging')}
              textModalSub={translate('DetectUSB')}
              containerButtonDouble={{
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            />
          ));
          return;
        }
        magicModal.hide();
        sleep(2000).then(() => dispatch(loadingSplash()));
      });
    } else {
      magicModal.hide();
      sleep(2000).then(() => dispatch(loadingSplash()));
    }
  };

  useEffect(() => {
    if (usbConnected !== undefined) {
      checkUsbDebugging(usbConnected);
    }
  }, [usbConnected]);

  useEffect(() => {
    const subscription = AppState.addEventListener(
      'change',
      handleAppStateChange,
    );

    return () => {
      subscription.remove();
    };
  }, [appState]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.WHITE,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <StatusCustomBarDark />
      <Image source={iconLogo} style={{width: 150, height: 50}} />
      <Text
        style={{
          fontSize: getFontSize(14),
          fontFamily: FontType.openSansSemiBold600,
          color: '#8A8A8A',
        }}>
        {'by IDX BEI'}
      </Text>
      <View style={{position: 'absolute', bottom: matrics.screenHeight / 30}}>
        <Text
          style={{
            fontSize: getFontSize(14),
            fontFamily: FontType.openSansSemiBold600,
            color: '#8A8A8A',
          }}>{`Version ${Config.VERSION_CODE}`}</Text>
      </View>
    </View>
  );
};

export default Splash;
