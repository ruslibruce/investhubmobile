import appleAuth, {
  appleAuthAndroid,
} from '@invertase/react-native-apple-authentication';
import {jwtDecode} from 'jwt-decode';
import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {iconApple} from '../assets/images';
import LoadingSpinner from '../components/LoadingSpinner';
import {Colors, handlingErrors} from '../utility';
import {encode, decode} from 'base-64';
import useGetLoginApple from '../api/useGetLoginApple';
import Config from 'react-native-config';
import 'react-native-get-random-values';
import {v4 as uuid} from 'uuid';
import {useTranslation} from 'react-i18next';
global.atob = decode;
global.btoa = encode;

const ButtonApple = () => {
  const {t: translate} = useTranslation();
  const {accessLoginApple, isLoading} = useGetLoginApple();

  const doLoginAppleAndroid = async () => {
    const rawNonce = uuid();
    const state = uuid();

    try {
      // Initialize the module
      appleAuthAndroid.configure({
        // The Service ID you registered with Apple
        clientId: 'id.co.idx.investhub2',

        // Return URL added to your Apple dev console. We intercept this redirect, but it must still match
        // the URL you provided to Apple. It can be an empty route on your backend as it's never called.
        redirectUri: `${Config.BASE_URL}/apple/callback`,

        // [OPTIONAL]
        // Scope.ALL (DEFAULT) = 'email name'
        // Scope.Email = 'email';
        // Scope.Name = 'name';
        scope: appleAuthAndroid.Scope.ALL,

        // [OPTIONAL]
        // ResponseType.ALL (DEFAULT) = 'code id_token';
        // ResponseType.CODE = 'code';
        // ResponseType.ID_TOKEN = 'id_token';
        responseType: appleAuthAndroid.ResponseType.ALL,

        // [OPTIONAL]
        // A String value used to associate a client session with an ID token and mitigate replay attacks.
        // This value will be SHA256 hashed by the library before being sent to Apple.
        // This is required if you intend to use Firebase to sign in with this credential.
        // Supply the response.id_token and rawNonce to Firebase OAuthProvider
        nonce: rawNonce,

        // [OPTIONAL]
        // Unique state value used to prevent CSRF attacks. A UUID will be generated if nothing is provided.
        state,
      });

      let userName = 'unknown';

      const response = await appleAuthAndroid.signIn();
      if (response) {
        const code = response.code; // Present if selected ResponseType.ALL / ResponseType.CODE
        const id_token = response.id_token; // Present if selected ResponseType.ALL / ResponseType.ID_TOKEN
        const user = response.user; // Present when user first logs in using appleId
        const state = response.state; // A copy of the state value that was passed to the initial request.
        console.log('Got auth code', code);
        console.log('Got id_token', id_token);
        console.log('Got user', user);
        console.log('Got state', state);
        const resultJWT = jwtDecode(id_token);

        console.log('resultJWT=========>', resultJWT, '<================');

        // let isiUser = {
        //   email: 'ruslibruce@gmail.com',
        //   name: {firstName: 'rusli', lastName: 'wanasuria'},
        // };

        user && user.name.firstName && user.name.lastName
          ? (userName = `${user.name.firstName} ${user.name.lastName}`)
          : (userName = 'unknown');
        await accessLoginApple(id_token, userName);
      }
    } catch (error) {
      if (error && error.message) {
        switch (error.message) {
          case appleAuthAndroid.Error.NOT_CONFIGURED:
            console.log('appleAuthAndroid not configured yet.');
            break;
          case appleAuthAndroid.Error.SIGNIN_FAILED:
            console.log('Apple signin failed.');
            break;
          case appleAuthAndroid.Error.SIGNIN_CANCELLED:
            console.log('User cancelled Apple signin.');
            break;
          default:
            handlingErrors(error, 'Apple Login');
            break;
        }
      }
    }
  };

  const doLoginApple = async () => {
    appleAuth.Operation.LOGOUT;
    // performs login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.FULL_NAME, appleAuth.Scope.EMAIL],
      });

      let userName = 'unknown';

      console.log('appleAuthRequestResponse', appleAuthRequestResponse);
      const {
        user: newUser,
        email,
        fullName,
        identityToken,
        nonce,
        realUserStatus /* etc */,
      } = appleAuthRequestResponse;

      fullName && fullName.givenName && fullName.familyName
        ? (userName = `${fullName.givenName} ${fullName.familyName}`)
        : (userName = 'unknown');

      const resultJWT = jwtDecode(appleAuthRequestResponse.identityToken);

      console.log('resultJWT=========>', resultJWT, '<================');

      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );

      if (credentialState === appleAuth.State.AUTHORIZED) {
        console.log('credentialState===>', credentialState);
        await accessLoginApple(identityToken, userName);
      }
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
        handlingErrors(error, 'Apple Login');
      }
    }
  };

  return (
    <TouchableOpacity
      disabled={isLoading}
      onPress={()=>{}}
      style={{
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.BORDER_RADIUS_AUTH,
        borderWidth: 1,
        borderRadius: 18,
        padding: 14,
      }}>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <Image source={iconApple} style={{width: 30, height: 30}} />
      )}
    </TouchableOpacity>
  );
};

export default ButtonApple;
