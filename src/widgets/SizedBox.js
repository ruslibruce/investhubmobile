import {View, Text} from 'react-native';
import React from 'react';

const SizedBox = ({height, width}) => {
  return (
    <View style={width ? {width: width} : height ? {height: height} : {}} />
  );
};

export default SizedBox;
