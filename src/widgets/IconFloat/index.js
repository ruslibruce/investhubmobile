import {View, Text} from 'react-native';
import React from 'react';
import {Icon} from '@ui-kitten/components';
import {Colors} from '../../utility';
import useIconFloat from './useIconFloat';

const IconFloat = ({bottom, contentVerticalOffset, listRef, isScrollView}) => {
  const {animationIconRef, CONTENT_OFFSET_THRESHOLD} = useIconFloat();

  if (contentVerticalOffset > CONTENT_OFFSET_THRESHOLD) {
    return (
      <Icon
        animation="shake"
        animationConfig={{cycles: Infinity}}
        ref={animationIconRef}
        style={{
          width: 40,
          height: 40,
          position: 'absolute',
          bottom: bottom ? bottom : 100,
          right: 50,
          zIndex: 2,
        }}
        fill={Colors.PRIMARY}
        name="arrow-circle-up-outline"
        onPress={() => {
          if (isScrollView) {
            listRef.current.scrollTo({y: 0, animated: true});
          } else {
            listRef.current.scrollToOffset({offset: 0, animated: true});
          }
        }}
      />
    );
  }
};

export default IconFloat;
