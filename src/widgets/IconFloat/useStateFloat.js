import {View, Text} from 'react-native';
import React from 'react';

const useStateFloat = () => {
  const listRef = React.useRef(null);
  const [contentVerticalOffset, setContentVerticalOffset] = React.useState(0);

  const onScroll = event => {
    setContentVerticalOffset(event.nativeEvent.contentOffset.y);
  };
  return {
    listRef,
    contentVerticalOffset,
    onScroll,
  };
};

export default useStateFloat;
