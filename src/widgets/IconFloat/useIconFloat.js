import {View, Text} from 'react-native';
import React from 'react';

const useIconFloat = () => {
  const animationIconRef = React.useRef(null);
  const CONTENT_OFFSET_THRESHOLD = 250;

  React.useEffect(() => {
    if (animationIconRef.current) {
      animationIconRef.current.startAnimation();
    }
  }, [animationIconRef.current]);

  return {
    animationIconRef,
    CONTENT_OFFSET_THRESHOLD,
  };
};

export default useIconFloat;
