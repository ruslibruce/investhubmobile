import React from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import {iconLoginIDX} from '../assets/images';
import {Colors, getDeepLink, UrlIDXWebsite} from '../utility';
import useGetLoginIDXWebsite from '../api/useGetLoginIDXWebsite';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import Config from 'react-native-config';
import queryString from 'query-string';
import LoadingSpinner from '../components/LoadingSpinner';
import {useTranslation} from 'react-i18next';

const ButtonIDX = () => {
  const {accessLoginIDXWebsite, isLoading} = useGetLoginIDXWebsite();
  const {i18n} = useTranslation();

  const doLoginIDXWebsite = async () => {
    try {
      const url = `${Config.BASE_URL}${UrlIDXWebsite}?lan=${i18n.language}`;
      const deepLink = getDeepLink('webidx/callback');
      console.log('deepLink', deepLink);
      console.log('url', url);
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.openAuth(url, deepLink, {
          // iOS Properties
          ephemeralWebSession: false,
          // Android Properties
          showTitle: false,
          enableUrlBarHiding: true,
          enableDefaultShare: false,
        });
        console.log('response result', result);
        const parsedUrl = queryString.parseUrl(result.url);
        const {token} = parsedUrl.query;
        console.log('token', token);
        await accessLoginIDXWebsite(token);
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  return (
    <TouchableOpacity
      disabled={isLoading}
      onPress={doLoginIDXWebsite}
      style={{
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.BORDER_RADIUS_AUTH,
        borderWidth: 1,
        borderRadius: 18,
        paddingHorizontal: 15,
        paddingVertical: 10,
      }}>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <Image
          style={{width: 30, height: 30}}
          source={iconLoginIDX}
          resizeMode="contain"
        />
      )}
    </TouchableOpacity>
  );
};

export default ButtonIDX;
