import React from 'react';
import { TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Colors, handleOpenLink } from '../../utility';

const IconWhatsapp = ({bottom}) => {
  return (
    <TouchableOpacity
      onPress={() => handleOpenLink('https://wa.me/+6281181150515')}
      style={{
        width: 40,
        height: 40,
        position: 'absolute',
        bottom: bottom ? bottom : 100,
        right: 10,
        zIndex: 2,
      }}>
      <Ionicons color={Colors.PRIMARY} size={38} name="logo-whatsapp" />
    </TouchableOpacity>
  );
};

export default IconWhatsapp;
